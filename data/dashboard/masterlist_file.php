<?php

ini_set ( 'max_execution_time', 30000);
session_start();

include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/phpexcelpath.php');
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/Logsfile.php');

$logsfile = new Logsfile();

        

    
            $dbname = $_SESSION['totalcountlist_db'];
            $db ="mmdb_".$dbname;
            include('db_connPDO.php'); 

    function cellColor($cells,$color){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => $color),
                                'font'  => array(
                                                    'bold'  => true,
                                                    'color' => array('rgb' => 'FF0000'),
                                                    'size'  => 9,
                                                    'name'  => 'Verdana'
                                                )
        
                                )
                            );
    }

 



$s =  "SELECT * from mastermembertable where regularization_status!='deleted'";


$objPHPExcel = new PHPExcel(); 
$objPHPExcel->setActiveSheetIndex(0); 
$rowCount = 6; 

date_default_timezone_set('Asia/Manila');
$today = date("F j, Y, g:i a"); 

$objPHPExcel->getActiveSheet()->SetCellValue('A1', "ActiveLink - Masterlist Report");
$objPHPExcel->getActiveSheet()->SetCellValue('A2', "Account Name");
$objPHPExcel->getActiveSheet()->SetCellValue('B2',$_SESSION['totalcountlist_db']);
$objPHPExcel->getActiveSheet()->SetCellValue('B3',$today);
$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Date Created");
cellColor('A1', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('A5', "Rule Name");
cellColor('A5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('B5', "Employee Number");
cellColor('B5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('C5', "SSS_no"); 
cellColor('C5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('D5', "Philhealth_no"); 
cellColor('D5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('E5', "HMO_no"); 
cellColor('E5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('F5', "Sub Office"); 
cellColor('F5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('G5', "Sub Office Code"); 
cellColor('G5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('H5', "Site"); 
cellColor('H5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('I5', "Lastname"); 
cellColor('I5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('J5', "Firstname"); 
cellColor('J5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('K5', "Middlename"); 
cellColor('K5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('L5', "Extension_name"); 
cellColor('L5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('M5', "Date of Birth"); 
cellColor('M5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('N5', "Gender"); 
cellColor('N5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('O5', "Marital Status"); 
cellColor('O5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('P5', "Job Level"); 
cellColor('P5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('Q5', "Job Description"); 
cellColor('Q5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('R5', "HMO Level"); 
cellColor('R5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('S5', "Room n Board"); 
cellColor('S5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('T5', "MBL"); 
cellColor('T5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('U5', "Eligibility"); 
cellColor('U5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('V5', "Date to Endorse"); 
cellColor('V5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('W5', "Date Hire"); 
cellColor('W5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('X5', "Remarks"); 
cellColor('X5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('Y5', "Effective date"); 
cellColor('Y5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('Z5', "Batch Id"); 
cellColor('Z5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('AA5', "Date Upload"); 
cellColor('AA5', 'CCEEFF');



        $qselect = $conn->query($s);
        
        while($row = $qselect->fetch(PDO::FETCH_ASSOC)){    
    
  
                   $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$row['rule_name']);     
                   $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$row['emp_no']);     
                   $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,$row['sss_no']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,$row['phil_no']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,$row['hmo_no']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,$row['suboffice']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,$row['subofficecode']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount,$row['site']);         
                   $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount,$row['lastname']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount,$row['firstname']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount,$row['middlename']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount,$row['ext']);         
                   $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount,$row['dob']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount,$row['gender']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount,$row['maritalstatus']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount,$row['joblevel']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount,$row['job_desc']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount,$row['hmolevel']); 
                   $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount,$row['emp_rom']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount,$row['emp_amount']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount,$row['emp_eligibility']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount,$row['dateemp_eligibility']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount,$row['datehire']);      
                   $objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount,$row['remark']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount,$row['effectivedate']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('Z'.$rowCount,$row['batch_id']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('AA'.$rowCount,$row['date_created']);
   
   
    
            $rowCount++; 
    } 

    $fname = $dbname."_masterlist.xlsx";

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
header('Content-Disposition: attachment;filename="'.$fname.'"'); 
header('Cache-Control: max-age=0'); 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
$objWriter->save('php://output');

// $logsfile->DLfile($db."_logs",$_SESSION['username'],$fname,"masterlist excel download");

?>