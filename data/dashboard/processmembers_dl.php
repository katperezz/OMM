<?php

ini_set ( 'max_execution_time', 30000);
session_start();

include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/phpexcelpath.php');
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/Logsfile.php');

$logsfile = new Logsfile();


            $dbname = $_SESSION['processmembers_session'];
            $db ="mmdb_".$dbname;
            include('db_connPDO.php'); 


    function cellColor($cells,$color){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => $color),
                                'font'  => array(
                                                    'bold'  => true,
                                                    'color' => array('rgb' => 'FF0000'),
                                                    'size'  => 9,
                                                    'name'  => 'Verdana'
                                                )
        
                                )
                            );
    }

 


    $s = "SELECT * FROM mastermembertable WHERE regularization_status!='deleted' AND remark='ENDORSED' AND hmo_no IS NOT NULL AND member_status='ACTIVE' AND hmo_no NOT LIKE '0'"; 
  


$objPHPExcel = new PHPExcel(); 
$objPHPExcel->setActiveSheetIndex(0); 
$rowCount = 6; 

date_default_timezone_set('Asia/Manila');
$today = date("F j, Y, g:i a"); 

$objPHPExcel->getActiveSheet()->SetCellValue('A1', "ActiveLink - Process Members Report");
$objPHPExcel->getActiveSheet()->SetCellValue('A2', "Account Name");
$objPHPExcel->getActiveSheet()->SetCellValue('B2',$_SESSION['processmembers_session']);
$objPHPExcel->getActiveSheet()->SetCellValue('B3',$today);
$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Date Created");
cellColor('A1', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('A5', "Employee Number");
cellColor('A5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('B5', "SSS_no"); 
cellColor('B5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('C5', "Philhealth_no"); 
cellColor('C5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('D5', "HMO_no"); 
cellColor('D5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('E5', "Sub Office"); 
cellColor('E5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('F5', "Sub Office Code"); 
cellColor('F5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('G5', "Site"); 
cellColor('G5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('H5', "Lastname"); 
cellColor('H5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('I5', "Firstname"); 
cellColor('I5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('J5', "Middlename"); 
cellColor('J5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('K5', "Extension_name"); 
cellColor('K5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('L5', "Date of Birth"); 
cellColor('L5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('M5', "Gender"); 
cellColor('M5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('N5', "Marital Status"); 
cellColor('N5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('O5', "Job Level"); 
cellColor('O5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('P5', "Job Description"); 
cellColor('P5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('Q5', "HMO Level"); 
cellColor('Q5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('R5', "Room n Board"); 
cellColor('R5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('S5', "MBL"); 
cellColor('S5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('T5', "Eligibility"); 
cellColor('T5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('U5', "Date to Endorse"); 
cellColor('U5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('V5', "Date Endorse"); 
cellColor('V5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('W5', "Date HMO ID Upload"); 
cellColor('W5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('X5', "Date Hire"); 
cellColor('X5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('Y5', "Remarks"); 
cellColor('Y5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('Z5', "Rule name"); 
cellColor('Z5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('AA5', "Batch Id"); 
cellColor('AA5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('AB5', "Effective date"); 
cellColor('AB5', 'CCEEFF');


        $qselect = $conn->query($s);
        
        while($row = $qselect->fetch(PDO::FETCH_ASSOC)){    
    
  
  
                   $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$row['emp_no']);     
                   $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$row['sss_no']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,$row['phil_no']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,$row['hmo_no']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,$row['suboffice']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,$row['subofficecode']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,$row['site']);         
                   $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount,$row['lastname']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount,$row['firstname']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount,$row['middlename']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount,$row['ext']);         
                   $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount,$row['dob']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount,$row['gender']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount,$row['maritalstatus']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount,$row['joblevel']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount,$row['job_desc']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount,$row['hmolevel']); 
                   $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount,$row['emp_rom']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount,$row['emp_amount']);    
                   $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount,$row['emp_eligibility']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount,$row['dateemp_eligibility']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount,$row['dateendorsed']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount,$row['date_hmoid_upload']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount,$row['datehire']);      
                   $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount,$row['remark']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('Z'.$rowCount,$row['rule_name']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('AA'.$rowCount,$row['batch_id']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('AB'.$rowCount,$row['effectivedate']);
   
   
   
    
            $rowCount++; 
    } 

    $fname = $dbname."_processmembers.xlsx";

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
header('Content-Disposition: attachment;filename="'.$fname.'"'); 
header('Cache-Control: max-age=0'); 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
$objWriter->save('php://output');

// $logsfile->DLfile($db."_logs",$_SESSION['username'],$fname,"masterlist excel download");

?>