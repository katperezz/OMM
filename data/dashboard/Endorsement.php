<?php

session_start();

include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/AllClassReq.php');



function ageCalculator($dob){
    if(!empty($dob)){
        $birthdate = new DateTime($dob);
        $today   = new DateTime('today');
        $age = $birthdate->diff($today)->y;
        return $age;
    }else{
        return 0;
    }
}

      

if(isset($_SESSION['endorsement_session']) == NULL || isset($_SESSION['username']) == NULL){
        
        $jsonresponse->success = false;
        $jsonresponse->message = "excel grid";
        $jsonresponse->total = "0";
        $jsonresponse->data = "";

        echo $jsonresponse->to_json();


}else{

               $q = "SELECT hmo_provider from account_rule where account_id='".$_SESSION['endorsement_session']."'";

         // $total_provider = $dbquery->NumRow($_SESSION['endorsement_session'],$qselect);


        foreach($dbquery->DbSelect("mmdb",$q) as $r){ 

                $hmoprovider=$r['hmo_provider'];
        } 
      $qselect = "SELECT * from mastermembertable where remark!='endorsed' and  regularization_status!='deleted'"; 
      $total = $dbquery->NumRow($_SESSION['endorsement_session'],$qselect);
      $q_array = array();
       
                  if($total == 0){

                $jsonresponse->success = false;
                $jsonresponse->message = "0";
                $jsonresponse->total = "0";
                $jsonresponse->data = $q_array;

                echo $jsonresponse->to_json();

            }else{

            
            $i=0;

            foreach ($dbquery->DbSelect($_SESSION['endorsement_session'],$qselect) as $row) {


           $master = array('batch_id' => $row['batch_id'],
                           'emp_no' => $row['emp_no'],
                           'dep_no' => $row['dep_no'],
                           'hmo_no' => $row['hmo_no'],
                           'hmoprovider' => $hmoprovider,
                           'lastname' => $row['lastname'],
                           'firstname' => $row['firstname'],
                           'middlename' => $row['middlename'],
                           'ext' => $row['ext'],
                           'gender' => $row['gender'],
                           'dob' => $row['dob'],
                           'age' => ageCalculator($row['age']),
                           'maritalstatus' => $row['maritalstatus'],
                           'category' => $row['category'],
                           'job_desc' => $row['job_desc'],
                           'hmolevel' => $row['hmolevel'],
                           'site' => $row['site'],
                           'date_regularization' => $row['date_regularization'],
                           'date_est_regularization' => $row['date_est_regularization'],
                           'regularization_status' => $row['regularization_status'],
                           'datehire' => $row['datehire'],
                           'dateendorsed' => $row['dateendorsed'],
                           'idreleaseddate' => $row['idreleaseddate'],
                           'joblevel' => $row['joblevel'],
                           'subofficecode' => $row['subofficecode'],
                           'suboffice' => $row['suboffice'],
                           'extname' => $row['ext'],
                           'date_of_deactivation' => $row['date_of_deactivation'],    
                           'emp_eligibility' => $row['emp_eligibility'],
                           'dep_eligibility' => $row['dep_eligibility'],
                           'dateemp_eligibility' => $row['dateemp_eligibility'],
                           'datedep_eligibility' => $row['datedep_eligibility'],
                           'emp_rom' => $row['emp_rom'],
                           'dep_rom' => $row['dep_rom'],
                           'emp_amount' => $row['emp_amount'],
                           'dep_amount' => $row['dep_amount'],
                           'remark' => $row['remark'],
                           'date_created' => $row['date_created']);


                        
                   


   
                        $q_array[$i]=$master;
                        $i++;

            }

           
        $jsonresponse->success = true;
        $jsonresponse->message = $total;
        $jsonresponse->total = $total;
        $jsonresponse->data = $q_array;

        $_SESSION['endorsement_arr'] = $q_array;

        echo $jsonresponse->to_json();
    }


}


?>