<?php
session_start();
//Import Response Class
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/AllClassReq.php');

date_default_timezone_set('Asia/Manila');

class stat_accountname {
    public $success = "";
    public $message = "";
    public $total = "";
    public $total_count  = "";
    public $total_endorse = "";
    public $total_notendorse ="";
    public $sla = "";
    public $oversla = "";
    public $data  = "";
}


class account
{       
        var $account_name;
        var $mtotal;
        var $atotatl;
        var $notendorse;
        var $totalendorsed;
        var $sla;
        var $oversla;
        var $p1;
        var $dateemp_eli;        
}

class sla
{       
        var $account_name;
        var $sla;
        var $oversla;
        var $percentsla;
        
}


    $efdate ="";
    $endate ="";

    $sql = "SELECT rule_name,effective_date,end_date FROM account_rule WHERE account_id = '".$_SESSION['accountselect']."' AND STATUS = 'active'";


    foreach($dbquery->DbSelect("mmdb",$sql) as $row){         
        $efdate = $row['effective_date'];
        $endate = $row['end_date']; 
    }    


    $q = "SELECT * from account_setup where account_name='".$_SESSION['accountselect']."'";
    $q_array=array();
    $i=0;

    $total = $dbquery->NumRow("mmdb",$q);

    if($total == 0){

        $stat_accountname = new stat_accountname();
        $stat_accountname->success = true;
        $stat_accountname->message = "0";
        $stat_accountname->total = "0";
        $stat_accountname->data = $q_array;


        echo json_encode($stat_accountname);

        // echo $Stat_accountName->to_json();

    }else{

    foreach($dbquery->DbSelect("mmdb",$q) as $row){ 

        $q2 = "SELECT * from mastermembertable where  regularization_status!='deleted'";
        $q3 = "SELECT * from mastermembertable where remark='NOT ENDORSED' and  regularization_status!='deleted' AND date_created<='".$endate."'";
        $q4= "SELECT * from mastermembertable where remark='endorsed' and  regularization_status!='deleted' AND date_created<='".$endate."'";
        $q5 = "SELECT dateemp_eligibility from mastermembertable where remark='NOT ENDORSED' and  regularization_status!='deleted'";
        $q6="SELECT * from mastermembertable where hmo_no!='' and hmo_no!='0' and  regularization_status!='deleted' and date_created >= '".$efdate."' AND date_created<='".$endate."'";

        $bsla = 0;
        $asla = 0;

        if($dbquery->NumRow($_SESSION['accountselect'],$q6) == 0){
            
            $asla = "0";
            $bsla = "0";
        }else{
            foreach($dbquery->DbSelect($_SESSION['accountselect'],$q6) as $row2){

                    $upload_hmo_no = $row2['date_hmoid_upload'];
                    $dateendorsed = $row2['dateendorsed'];

                    $d1 = new DateTime($upload_hmo_no);
                    $d2 = new DateTime($dateendorsed);
                    $ddiff = $d2->diff($d1)->days;
                 
                    if($ddiff > 5){

                        $asla ++;

                    }else{

                        $bsla++;
                    }    
        }
        }

        $totalsla=$asla+$bsla;

        if($asla == 0){
        $percentasla = 0;
        }else{
            $percentasla = $asla/$totalsla;
        }

        if($bsla == 0){
        $percentbsla = 0;
        }else{
            $percentbsla = $bsla/$totalsla;
        }

        $percentsla = $percentasla*100;
        $percentoversla = $percentbsla*100;


        $h = array();
        $dnow = date('Y-m-d');
        $count_eli = 0;
        $t = $dbquery->NumRow($row['account_name'],$q5);
        if($t == 0)
            {
                $count_eli =0;
            }else{
                foreach ($dbquery->DbSelect($row['account_name'],$q5) as $d_emp) {
            
                $dateemp_eli = $d_emp['dateemp_eligibility'];

                $date_now = new DateTime($dnow);
                $date_eli = new DateTime($dateemp_eli);

                if ($date_eli >= $date_now) {
                    $c = $eligibilityval->GetWorkingDays($dnow,$dateemp_eli,$h);
                    if($c <= 7){ // 5 days before 
                         $count_eli ++;        
                    }   
                }
        }
        // error_log("count".$count_eli);
        }

        $notendorse = $dbquery->NumRow($row['account_name'],$q3);
        $endorsed = $dbquery->NumRow($row['account_name'],$q4);
        $totall=$dbquery->NumRow($row['account_name'],$q2);

        if($notendorse == 0){
        $percentne = 0;
        }else{
            $percentne = $notendorse/$totall;
        }

        if($endorsed == 0){
        $percente = 0;
        }else{
            $percente = $endorsed/$totall;
        }

        $percent = $percentne*100;
        $percentee = $percente*100;

        $account = new account();
        $x = $dbquery->NumRow($row['account_name'],$q2);
        $y = $dbquery->NumRow($row['account_name'],$q3);

        $account->account_name =$row['account_name'];
        $account->dateemp_eli =$count_eli;
        $account->mtotal =$totall;
        $account->sla =$bsla;
        $account->oversla =$asla;
        $account->atotatl =$dbquery->NumRow($row['account_name'],$q2);
        $account->notendorse =$dbquery->NumRow($row['account_name'],$q3);
        $account->totalendorsed =$dbquery->NumRow($row['account_name'],$q4);
        $account->p1 =round($percent,2);

        $count_master[$i] = $x; 
        $count_notendorse[$i]=$y;

        $q_array[$i] = $account;
        $i++;

    }

    foreach($dbquery->DbSelect("mmdb",$q) as $row){ 
    $sla = new sla();

        $sla->account_name =$row['account_name'];
        $sla->sla =$bsla;
        $sla->oversla =$asla;
        $sla->percentsla =round($percentsla,2);
        $q_array[$i] = $sla;
        $i++;
    }


    foreach($dbquery->DbSelect("mmdb",$q) as $row){ 
    $sla = new sla();

        $sla->account_name =$row['account_name'];
        $sla->sla =$bsla;
        $sla->oversla =$asla;
        $sla->percentsla =round($percentoversla,2);
        $q_array[$i] = $sla;
        $i++;
    }





    $totalcount_master = array_sum($count_master);
    $totalcount_notendorse=array_sum($count_notendorse);



    $stat_accountname = new stat_accountname();
    $stat_accountname->success = true;
    $stat_accountname->data = $q_array;
    $stat_accountname->message = "account stat";

    $stat_accountname->sla = $bsla."SLA";
    $stat_accountname->oversla = $asla." Over SLA";
    $stat_accountname->total = $totalcount_master;
    $stat_accountname->total_count = $totalcount_master;
    $stat_accountname->total_endorse = $endorsed." Endorsed";
    $stat_accountname->total_notendorse = $totalcount_notendorse." Not Endorsed";
    $stat_accountname->endothisweek = $count_eli;


    echo json_encode($stat_accountname);




}








?>