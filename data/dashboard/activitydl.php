<?php

ini_set ( 'max_execution_time', 30000);
session_start();

include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/phpexcelpath.php');
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/Logsfile.php');
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/Dbquery.php');
// include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/JsonResponse.php');
// $jsonresponse = new JsonResponse();
$dbquery = new Dbquery();

$logsfile = new Logsfile();

            
            $dbname = $_SESSION['processmembers_session'];
            $db ="mmdb_".$dbname;
            include('db_connPDO.php'); 
           

    function cellColor($cells,$color){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => $color),
                                'font'  => array(
                                                    'bold'  => true,
                                                    'color' => array('rgb' => 'FF0000'),
                                                    'size'  => 9,
                                                    'name'  => 'Verdana'
                                                )
        
                                )
                            );
    }

 



// $s =  "SELECT * from mastermembertable where emp_no = '".$_SESSION['getemp_no']."'";


$objPHPExcel = new PHPExcel(); 
$objPHPExcel->setActiveSheetIndex(0); 

date_default_timezone_set('Asia/Manila');
$today = date("F j, Y, g:i a"); 

$objPHPExcel->getActiveSheet()->SetCellValue('A1', "ActiveLink");
$objPHPExcel->getActiveSheet()->SetCellValue('A2', "Account Name");
$objPHPExcel->getActiveSheet()->SetCellValue('B2',$_SESSION['processmembers_session']);
$objPHPExcel->getActiveSheet()->SetCellValue('B3',$today);
$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Date Created");
cellColor('A1', 'CCEEFF');




$objPHPExcel->getActiveSheet()->SetCellValue('A5', "Action");
cellColor('A5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('B5', "User"); 
cellColor('B5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('C5', "Date Time Logs"); 
cellColor('C5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('D5', "Employee Number");
cellColor('D5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('E5', "SSS_no"); 
cellColor('E5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('F5', "Philhealth_no"); 
cellColor('F5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('G5', "HMO Number"); 
cellColor('G5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('H5', "Lastname"); 
cellColor('H5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('I5', "Firstname"); 
cellColor('I5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('J5',"Middlename" ); 
cellColor('J5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('K5', "Extension_name"); 
cellColor('K5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('L5', "Gender"); 
cellColor('L5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('M5', "Date of Birth"); 
cellColor('M5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('N5', "Marital Status");  
cellColor('N5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('O5', "Sub Office Code"); 
cellColor('O5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('P5', "Sub Office");
cellColor('P5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('Q5', "Site");
cellColor('Q5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('R5', "Job Level"); 
cellColor('R5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('S5', "Job Description"); 
cellColor('S5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('T5', "HMO Level"); 
cellColor('T5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('U5', "Room on Board"); 
cellColor('U5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('V5', "Date Hire"); 
cellColor('V5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('W5', "Rule name"); 
cellColor('W5', 'CCEEFF');



 

      $q_array = array();  
            
            $i=0;
                     $qselect="SELECT * FROM mastermembertable WHERE emp_no='".$_SESSION['getemp_no']."'";
                     $qselect1="SELECT * FROM saveupload_logs WHERE emp_no='".$_SESSION['getemp_no']."' AND process = 'IMPORT'";
                     $qselect2="SELECT * FROM endorsement_logs WHERE emp_no='".$_SESSION['getemp_no']."' and remark='ENDORSED'";
                     $qselect3="SELECT * FROM saveupload_logs WHERE emp_no='".$_SESSION['getemp_no']."' AND process = 'UPDATE'";
                     $qselect4="SELECT * FROM regularization_logs WHERE emp_no='".$_SESSION['getemp_no']."'";
                     $qselect5="SELECT * FROM deletion_logs WHERE emp_no='".$_SESSION['getemp_no']."'";
                     $qselect6="SELECT * FROM saveupload_logs WHERE emp_no='".$_SESSION['getemp_no']."' AND process = 'RENEWAL'";
            
                     $total = $dbquery->NumRow($_SESSION['processmembers_session'],$qselect);
                     $total1 = $dbquery->NumRow($_SESSION['processmembers_session'].'_logs',$qselect1);
                     $total2 = $dbquery->NumRow($_SESSION['processmembers_session'].'_logs',$qselect2);
                     $total3 = $dbquery->NumRow($_SESSION['processmembers_session'].'_logs',$qselect3);
                     $total4 = $dbquery->NumRow($_SESSION['processmembers_session'].'_logs',$qselect4);
                     $total5 = $dbquery->NumRow($_SESSION['processmembers_session'].'_logs',$qselect5);
                     $total6 = $dbquery->NumRow($_SESSION['processmembers_session'].'_logs',$qselect6);



            if($total == 0){
              $q_array[$i] = array('action' => "",
                          'user' => "",
                          'datetimelogs' => "", 
                          'emp_no' => "",
                          'sss_no' => "",
                          'phil_no' => "",
                          'hmo_no' => "",
                          'lastname' => "",
                          'firstname' => "",
                          'middlename' => "",
                          'extname' => "",
                          'gender' => "",
                          'dob' => "",
                          'maritalstatus' => "",
                          'suboffice' => "",
                          'subofficecode' => "",
                          'site' => "",
                          'joblevel' => "",
                          'jobdescpt' => "",
                          'hmolevel' => "",
                          'roomonboard' => "",
                          'datehire' => "",
                          'rule_name'=>"");

            }else{
                    foreach ($dbquery->DbSelect($_SESSION['processmembers_session'],$qselect) as $row) {
                                 // $arr2 = array('user'=>$row['user'],'emp_no'=>$row['emp_no'],'endorsement_datetime'=>$row['date_created'],'remark'=>"save to masterlist");

                     $arr = array('action' => "Current record",
                          'user' => $_SESSION['username'],
                          'datetimelogs' => $row['date_created'], 
                          'emp_no' => $row['emp_no'],
                          'sss_no' => $row['sss_no'],
                          'phil_no' => $row['phil_no'],
                          'hmo_no' => $row['hmo_no'],
                          'lastname' => $row['lastname'],
                          'firstname' => $row['firstname'],
                          'middlename' => $row['middlename'],
                          'extname' => $row['ext'],
                          'gender' => $row['gender'],
                          'dob' => $row['dob'],
                          'maritalstatus' => $row['maritalstatus'],
                          'suboffice' => $row['suboffice'],
                          'subofficecode' => $row['subofficecode'],
                          'site' => $row['site'],
                          'joblevel' => $row['joblevel'],
                          'jobdescpt' => $row['job_desc'],
                          'hmolevel' => $row['hmolevel'],
                          'roomonboard' => $row['emp_rom'],
                          'datehire' => $row['datehire'],
                          'rule_name'=>$row['rule_name']);



                                 $q_array[$i] = $arr;
                                 $i++;                        
                    }

            }



            if($total1 == 0){
             $q_array[$i] = array('action' => "",
                          'user' => "",
                          'datetimelogs' => "", 
                          'emp_no' => "",
                          'sss_no' => "",
                          'phil_no' => "",
                          'hmo_no' => "",
                          'lastname' => "",
                          'firstname' => "",
                          'middlename' => "",
                          'extname' => "",
                          'gender' => "",
                          'dob' => "",
                          'maritalstatus' => "",
                          'suboffice' => "",
                          'subofficecode' => "",
                          'site' => "",
                          'joblevel' => "",
                          'jobdescpt' => "",
                          'hmolevel' => "",
                          'roomonboard' => "",
                          'datehire' => "",
                          'rule_name'=>"");

            }else{
                    foreach ($dbquery->DbSelect($_SESSION['processmembers_session'].'_logs',$qselect1) as $row) {
                                 // $arr2 = array('user'=>$row['user'],'emp_no'=>$row['emp_no'],'endorsement_datetime'=>$row['date_created'],'remark'=>"save to masterlist");
                        

                     $arr1 = array('action' => "Save to Masterlist",
                          'user' => $row['user'],
                          'datetimelogs' => $row['date_created'], 
                          'emp_no' => $row['emp_no'],
                          'sss_no' => $row['sss_no'],
                          'phil_no' => $row['phil_no'],
                          'hmo_no' => "not include",
                          'lastname' => $row['lastname'],
                          'firstname' => $row['firstname'],
                          'middlename' => $row['middlename'],
                          'extname' => $row['ext'],
                          'gender' => $row['gender'],
                          'dob' => $row['dob'],
                          'maritalstatus' => $row['maritalstatus'],
                          'suboffice' => $row['suboffice'],
                          'subofficecode' => $row['subofficecode'],
                          'site' => $row['site'],
                          'joblevel' => $row['joblevel'],
                          'jobdescpt' => $row['job_desc'],
                          'hmolevel' => "not include",
                          'roomonboard' => "not include",
                          'datehire' => $row['datehire'],
                          'rule_name'=>$row['rule_name']);



                                 $q_array[$i] = $arr1;
                                 $i++;                        
                    }

            }


                 if($total2 == 0){
                            
                          $q_array[$i] = array('action' => "",
                          'user' => "",
                          'datetimelogs' => "", 
                          'emp_no' => "",
                          'sss_no' => "",
                          'phil_no' => "",
                          'hmo_no' => "",
                          'lastname' => "",
                          'firstname' => "",
                          'middlename' => "",
                          'extname' => "",
                          'gender' => "",
                          'dob' => "",
                          'maritalstatus' => "",
                          'suboffice' => "",
                          'subofficecode' => "",
                          'site' => "",
                          'joblevel' => "",
                          'jobdescpt' => "",
                          'hmolevel' => "",
                          'roomonboard' => "",
                          'datehire' => "",
                          'rule_name'=>"");

           }else{

                    foreach ($dbquery->DbSelect($_SESSION['processmembers_session'].'_logs',$qselect2) as $row) {
                         
                               // $arr1 = array('user'=>$row['user'],'emp_no'=>$row['emp_no'],'remark'=>$row['remark'],'date_created'=>$row['date_created'],'endorsement_datetime'=>$row['endorsement_datetime']);
                         


                     $arr2 = array('action' => "Endorsed",
                          'user' => $row['user'],
                          'datetimelogs' => $row['endorsement_datetime'], 
                          'emp_no' => $row['emp_no'],
                          'sss_no' => $row['sss_no'],
                          'phil_no' => $row['phil_no'],
                          'hmo_no' => "not include",
                          'lastname' => $row['lastname'],
                          'firstname' => $row['firstname'],
                          'middlename' => $row['middlename'],
                          'extname' => $row['ext'],
                          'gender' => $row['gender'],
                          'dob' => $row['dob'],
                          'maritalstatus' => $row['maritalstatus'],
                          'suboffice' => $row['suboffice'],
                          'subofficecode' => $row['subofficecode'],
                          'site' => $row['site'],
                          'joblevel' => $row['joblevel'],
                          'jobdescpt' => $row['job_desc'],
                          'hmolevel' => $row['hmolevel'],
                          'roomonboard' => "",
                          'datehire' => $row['datehire'],
                          'rule_name'=>"not include");


                        $q_array[$i] = $arr2; 

                        $i++;
                    }
            }

                if($total3 == 0){
                 $q_array[$i] = array('action' => "",
                          'user' => "",
                          'datetimelogs' => "", 
                          'emp_no' => "",
                          'sss_no' => "",
                          'phil_no' => "",
                          'hmo_no' => "",
                          'lastname' => "",
                          'firstname' => "",
                          'middlename' => "",
                          'extname' => "",
                          'gender' => "",
                          'dob' => "",
                          'maritalstatus' => "",
                          'suboffice' => "",
                          'subofficecode' => "",
                          'site' => "",
                          'joblevel' => "",
                          'jobdescpt' => "",
                          'hmolevel' => "",
                          'roomonboard' => "",
                          'datehire' => "",
                          'rule_name'=>"");


            }else{
                    foreach ($dbquery->DbSelect($_SESSION['processmembers_session'].'_logs',$qselect3) as $row) {
                            // $arr5 = array('user'=>$row['user'],'emp_no'=>$row['emp_no'],'endorsement_datetime'=>$row['date_created'],'remark'=>"update");

                     

            $arr3 = array('action' => "Update",
                          'user' => $row['user'],
                          'datetimelogs' => $row['date_created'], 
                          'emp_no' => $row['emp_no'],
                          'sss_no' => $row['sss_no'],
                          'phil_no' => $row['phil_no'],
                          'hmo_no' => $row['hmo_no'],
                          'lastname' => $row['lastname'],
                          'firstname' => $row['firstname'],
                          'middlename' => $row['middlename'],
                          'extname' => $row['ext'],
                          'gender' => $row['gender'],
                          'dob' => $row['dob'],
                          'maritalstatus' => $row['maritalstatus'],
                          'suboffice' => $row['suboffice'],
                          'subofficecode' => $row['subofficecode'],
                          'site' => $row['site'],
                          'joblevel' => $row['joblevel'],
                          'jobdescpt' => $row['job_desc'],
                          'hmolevel' => "",
                          'roomonboard' => "",
                          'datehire' => $row['datehire'],
                          'rule_name'=>$row['rule_name']);


                            $q_array[$i] = $arr3;
                            $i++;
                        
                    }


            }




             if($total4 == 0){
                             $q_array[$i] = array('action' => "",
                          'user' => "",
                          'datetimelogs' => "", 
                          'emp_no' => "",
                          'sss_no' => "",
                          'phil_no' => "",
                          'hmo_no' => "",
                          'lastname' => "",
                          'firstname' => "",
                          'middlename' => "",
                          'extname' => "",
                          'gender' => "",
                          'dob' => "",
                          'maritalstatus' => "",
                          'suboffice' => "",
                          'subofficecode' => "",
                          'site' => "",
                          'joblevel' => "",
                          'jobdescpt' => "",
                          'hmolevel' => "",
                          'roomonboard' => "",
                          'datehire' => "",
                          'rule_name'=>"");



             }else{
                    foreach ($dbquery->DbSelect($_SESSION['processmembers_session'].'_logs',$qselect4) as $row) {

                       

            $arr4 = array('action' => "Regularization",
                          'user' => $row['user'],
                          'datetimelogs' => $row['datetime_log'], 
                          'emp_no' => $row['emp_no'],
                          'sss_no' => $row['sss_no'],
                          'phil_no' => $row['phil_no'],
                          'hmo_no' => "not include",
                          'lastname' => $row['lastname'],
                          'firstname' => $row['firstname'],
                          'middlename' => $row['middlename'],
                          'extname' => $row['ext'],
                          'gender' => $row['gender'],
                          'dob' => $row['dob'],
                          'maritalstatus' => $row['maritalstatus'],
                          'suboffice' => "not include",
                          'subofficecode' => "not include",
                          'site' => "not include",
                          'joblevel' => "not include",
                          'jobdescpt' => "not include",
                          'hmolevel' => "not include",
                          'roomonboard' => "not include",
                          'datehire' => "not include",
                          'rule_name'=>"not include");


                        $q_array[$i] = $arr4;
                        $i++;
                    }

            } 


            if($total5 == 0){ 
                   $q_array[$i] = array('action' => "",
                          'user' => "",
                          'datetimelogs' => "", 
                          'emp_no' => "",
                          'sss_no' => "",
                          'phil_no' => "",
                          'hmo_no' => "",
                          'lastname' => "",
                          'firstname' => "",
                          'middlename' => "",
                          'extname' => "",
                          'gender' => "",
                          'dob' => "",
                          'maritalstatus' => "",
                          'suboffice' => "",
                          'subofficecode' => "",
                          'site' => "",
                          'joblevel' => "",
                          'jobdescpt' => "",
                          'hmolevel' => "",
                          'roomonboard' => "",
                          'datehire' => "",
                          'rule_name'=>"");


            }else{
                    foreach ($dbquery->DbSelect($_SESSION['processmembers_session'].'_logs',$qselect5) as $row) {
            
                     $arr5 = array('action' => "Deleted",
                          'user' => $row['user'],
                          'datetimelogs' => $row['delete_datetime'], 
                          'emp_no' => $row['emp_no'],
                          'sss_no' => $row['sss_no'],
                          'phil_no' => $row['phil_no'],
                          'hmo_no' => $row['hmo_no'],
                          'lastname' => $row['lastname'],
                          'firstname' => $row['firstname'],
                          'middlename' => $row['middlename'],
                          'extname' => $row['ext'],
                          'gender' => $row['gender'],
                          'dob' => $row['dob'],
                          'maritalstatus' => $row['maritalstatus'],
                          'suboffice' => $row['suboffice'],
                          'subofficecode' => $row['subofficecode'],
                          'site' => $row['site'],
                          'joblevel' => $row['joblevel'],
                          'jobdescpt' => $row['job_desc'],
                          'hmolevel' => $row['hmolevel'],
                          'roomonboard' => $row['emp_rom'],
                          'datehire' => $row['datehire'],
                          'rule_name'=>"not include");
                          

                        $q_array[$i] = $arr5;
                        $i++;
                    }

             }



             if($total6 == 0){
                 $q_array[$i] = array('action' => "",
                          'user' => "",
                          'datetimelogs' => "", 
                          'emp_no' => "",
                          'sss_no' => "",
                          'phil_no' => "",
                          'hmo_no' => "",
                          'lastname' => "",
                          'firstname' => "",
                          'middlename' => "",
                          'extname' => "",
                          'gender' => "",
                          'dob' => "",
                          'maritalstatus' => "",
                          'suboffice' => "",
                          'subofficecode' => "",
                          'site' => "",
                          'joblevel' => "",
                          'jobdescpt' => "",
                          'hmolevel' => "",
                          'roomonboard' => "",
                          'datehire' => "",
                          'rule_name'=>"");

            }else{
                    foreach ($dbquery->DbSelect($_SESSION['processmembers_session'].'_logs',$qselect6) as $row) {
               
            $arr6 = array('action' => "RENEWAL",
                          'user' => $row['user'],
                          'datetimelogs' => $row['date_created'], 
                          'emp_no' => $row['emp_no'],
                          'sss_no' => "not include",
                          'phil_no' => "not include",
                          'hmo_no' => "not include",
                          'lastname' => $row['lastname'],
                          'firstname' => $row['firstname'],
                          'middlename' => $row['middlename'],
                          'extname' => $row['ext'],
                          'gender' => "not include",
                          'dob' => $row['dob'],
                          'maritalstatus' => "not include",
                          'suboffice' => "not include",
                          'subofficecode' => "not include",
                          'site' => "not include",
                          'joblevel' => "not include",
                          'jobdescpt' => "not include",
                          'hmolevel' => "not include",
                          'roomonboard' => "not include",
                          'datehire' => "not include",
                          'rule_name'=>$row['rule_name']);


                            $q_array[$i] = $arr6;
                            $i++;
                        
                    }


            }



         $y = 6;   

         foreach($q_array as $rval){

                

                          $objPHPExcel->getActiveSheet()->SetCellValue('A'.$y,$rval['action']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('B'.$y,$rval['user']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('C'.$y,$rval['datetimelogs']);  

                          $objPHPExcel->getActiveSheet()->SetCellValue('D'.$y,$rval['emp_no']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('E'.$y,$rval['sss_no']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('F'.$y,$rval['phil_no']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('G'.$y,$rval['hmo_no']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('H'.$y,$rval['lastname']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('I'.$y,$rval['firstname']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('J'.$y,$rval['middlename']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('K'.$y,$rval['extname']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('L'.$y,$rval['gender']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('M'.$y,$rval['dob']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('N'.$y,$rval['maritalstatus']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('O'.$y,$rval['suboffice']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('P'.$y,$rval['subofficecode']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$y,$rval['site']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('R'.$y,$rval['joblevel']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('S'.$y,$rval['jobdescpt']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('T'.$y,$rval['hmolevel']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('U'.$y,$rval['roomonboard']);
                          $objPHPExcel->getActiveSheet()->SetCellValue('V'.$y,$rval['datehire']);
                         $objPHPExcel->getActiveSheet()->SetCellValue('W'.$y,$rval['rule_name']);

                $y++;
         }   


           
        // $jsonresponse->success = true;
        // $jsonresponse->message = $total;
        // $jsonresponse->total = $total;
        // $jsonresponse->data = $q_array;

        // echo $jsonresponse->to_json();


    $fname = $dbname."_activitylog.xlsx";

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
header('Content-Disposition: attachment;filename="'.$fname.'"'); 
header('Cache-Control: max-age=0'); 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
$objWriter->save('php://output');

// $logsfile->DLfile($db."_logs",$_SESSION['username'],$fname,"activitylog excel download");

?>