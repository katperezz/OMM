<?php

ini_set ( 'max_execution_time', 30000);
session_start();

include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/phpexcelpath.php');
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/Logsfile.php');

$logsfile = new Logsfile();

    
           if(isset($_SESSION['accountselect'])==NULL){

             echo "NO ACCOUNT SELECTED";


           }else{


             $dbname = $_SESSION['accountselect'];
              
             $db ="mmdb_".$dbname;
             include('db_connPDO.php');
           
            


    function cellColor($cells,$color){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => $color),
                                'font'  => array(
                                                    'bold'  => true,
                                                    'color' => array('rgb' => 'FF0000'),
                                                    'size'  => 9,
                                                    'name'  => 'Verdana'
                                                )
        
                                )
                            );
    }

 



$s =  "SELECT * from mastermembertable where regularization_status!='deleted'";


$objPHPExcel = new PHPExcel(); 
$objPHPExcel->setActiveSheetIndex(0); 
$rowCount = 6; 

date_default_timezone_set('Asia/Manila');
$today = date("F j, Y, g:i a"); 

$objPHPExcel->getActiveSheet()->SetCellValue('A1', "ActiveLink -  Report");
$objPHPExcel->getActiveSheet()->SetCellValue('A2', "Account Name");
$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Date Created");
$objPHPExcel->getActiveSheet()->SetCellValue('B2',$dbname);
$objPHPExcel->getActiveSheet()->SetCellValue('B3',$today);


$objPHPExcel->getActiveSheet()->SetCellValue('A5', "Employee Number");
cellColor('A5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('B5', "Date Eligibility"); 
cellColor('B5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('C5', "Date Endorsed"); 
cellColor('C5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('D5', "Date Hmo Id upload"); 
cellColor('D5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('E5', "Date Id Card release"); 
cellColor('E5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('F5', "Status"); 
cellColor('F5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('G5', "Remarks"); 
cellColor('G5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('H5', "Days"); 
cellColor('H5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('I5',"Days card release");
cellColor('I5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('J5', "Lastname"); 
cellColor('J5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('K5', "Firstname"); 
cellColor('K5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('L5', "Middlename"); 
cellColor('L5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('M5', "Extension_name"); 
cellColor('M5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('N5', "Date of Birth"); 
cellColor('N5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('O5', "Gender"); 
cellColor('O5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('P5', "Marital Status"); 
cellColor('P5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('Q5', "Job Level"); 
cellColor('Q5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('R5', "Job Description"); 
cellColor('R5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('S5', "Date Hire"); 
cellColor('S5', 'CCEEFF');




        $qselect = $conn->query($s);
        
        while($row = $qselect->fetch(PDO::FETCH_ASSOC)){ 

                  if(empty($row['dateendorsed']) || empty($row['date_hmoid_upload']) || empty($row['idreleaseddate'])){

                        $daysla = "";
                        $dayidrelease="";

                  }else{
                        $d1 = new DateTime($row['dateendorsed']);
                        $d2 = new DateTime($row['date_hmoid_upload']);
                        $d3 = new DateTime($row['idreleaseddate']);
                        $interval = $d1->diff($d2);
                        $interval2= $d1->diff($d3);
                        $daysla = $interval->format('%a');
                        $dayidrelease =$interval2->format('%a');
                   }

                  if($daysla == ""){
                        $remarks = "";
                  }else{
                      if($daysla > 5){
                          $remarks = "over sla";
                      }else{
                          $remarks = "due sla";
                      }
                  }

                
                   $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$row['emp_no']);     
                   $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$row['dateemp_eligibility']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,$row['dateendorsed']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,$row['date_hmoid_upload']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,$row['idreleaseddate']);                
                   $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,$row['remark']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,$remarks);
                   $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount,$daysla);
                   $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount,$dayidrelease);                            
                   $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount,$row['lastname']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount,$row['firstname']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount,$row['middlename']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount,$row['ext']);         
                   $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount,$row['dob']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount,$row['gender']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount,$row['maritalstatus']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount,$row['joblevel']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount,$row['job_desc']);                                         
                   $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount,$row['datehire']);      
        
   
    
            $rowCount++; 
    } 

    $fname = $dbname."_report.xlsx";

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
header('Content-Disposition: attachment;filename="'.$fname.'"'); 
header('Cache-Control: max-age=0'); 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
$objWriter->save('php://output');

}

?>