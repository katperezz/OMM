<?php

session_start();

ini_set ( 'max_execution_time', 30000);
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/AllClassReq.php');

date_default_timezone_set('Asia/Manila');

function ageCalculator($dob){
    if(!empty($dob)){
        $birthdate = new DateTime($dob);
        $today   = new DateTime('today');
        $age = $birthdate->diff($today)->y;
        return $age;
    }else{
        return 0;
    }
}

 $q_array = array();


    $q = "SELECT hmo_provider from account_rule where account_id='".$_SESSION['regularization_session']."'";
       
 
if(!isset($_SESSION['regularization_session']) || empty($_SESSION['regularization_session'])){
               
        $jsonresponse->success = false;
        $jsonresponse->message = "no data";
        $jsonresponse->total = "0";
        $jsonresponse->data = "";

        echo $jsonresponse->to_json();


}else if($dbquery->NumRow("mmdb",$q) == 0){

        $jsonresponse->success = false;
        $jsonresponse->message = "no hmo data";
        $jsonresponse->total = "0";
        $jsonresponse->data = "";

        echo $jsonresponse->to_json();




}else{
    


$q = "SELECT * from mastermembertable  WHERE regularization_status = 'probationary' and  regularization_status!='deleted'"; 
$q2 = "SELECT hmo_provider from account_rule where account_id='".$_SESSION['regularization_session']."'";
        foreach($dbquery->DbSelect("mmdb",$q2) as $r){ 

                $hmoprovider=$r['hmo_provider'];
        }

$h = array();
$dnow = date('Y-m-d');
$count = 0;

      $total = $dbquery->NumRow($_SESSION['regularization_session'],$q);
     


if($total == 0){

				$jsonresponse->success = false;
        $jsonresponse->message = "0";
        $jsonresponse->total = "0";
        $jsonresponse->data = $q_array;

        echo $jsonresponse->to_json();


}else{


		$i=0;

		foreach ($dbquery->DbSelect($_SESSION['regularization_session'],$q) as $row) {
	


            $master = array('emp_no' => $row['emp_no'],
                            'dep_no' => $row['dep_no'],
                            'hmo_no' => $row['hmo_no'],
                            'hmoprovider' => $hmoprovider,
                            'lastname' => $row['lastname'],
                            'firstname' => $row['firstname'],
                            'middlename' => $row['middlename'],
                            'gender' => $row['gender'],
                            'dob' => $row['dob'],
                            'age' => ageCalculator($row['dob']) ,
                            'maritalstatus' => $row['maritalstatus'],
                            'category' => $row['category'],
                            'job_desc' => $row['job_desc'],
                            'hmolevel' => $row['hmolevel'],
                            'site' => $row['site'],
                            'date_regularization' => $row['date_regularization'],
                            'date_est_regularization' => $row['date_est_regularization'],
                            'regularization_status' => $row['regularization_status'],
                            'datehire' => $row['datehire'],
                            'dateendorsed' => $row['dateendorsed'],
                            'idreleaseddate' => $row['idreleaseddate'],
                            'joblevel' => $row['joblevel'],
                            'subofficecode' => $row['subofficecode'],
                            'suboffice' => $row['suboffice'],
                            'extname' => $row['ext'],
                            'date_of_deactivation' => $row['date_of_deactivation'],

    
                            'emp_eligibility' => $row['emp_eligibility'],
                            'dep_eligibility' => $row['dep_eligibility'],
                            'dateemp_eligibility' => $row['dateemp_eligibility'],
                            'datedep_eligibility' => $row['datedep_eligibility'],
                            'emp_rom' => $row['emp_rom'],
                            'dep_rom' => $row['dep_rom'],
                            'emp_amount' => $row['emp_amount'],
                            'dep_amount' => $row['dep_amount'],

                            'remark' => $row['remark'],
                            'date_created' => $row['date_created']);

                            $q_array[]=$master;



     					   	$count++;
                 
     					   }   

        $jsonresponse->success = true;
        $jsonresponse->message = "employee to be regular";
        $jsonresponse->total = $count;
        $jsonresponse->data = $q_array;

        $_SESSION['regularization_arr'] = $q_array;

        echo $jsonresponse->to_json();

		}

            


}




                            // $master = new master();  
       
       
              //               $master->emp_no =$row['emp_no'];
              //               $master->dep_no =$row['dep_no'];
              //               $master->hmo_no =$row['hmo_no'];
              //               $master->hmoprovider =$hmoprovider;
              //               $master->lastname =$row['lastname'];
              //               $master->firstname =$row['firstname'];
              //               $master->middlename =$row['middlename'];
              //               $master->gender =$row['gender'];
              //               $master->dob =$row['dob'];
              //               $master->age =ageCalculator($row['dob']) ;
              //               $master->maritalstatus =$row['maritalstatus'];
              //               $master->category =$row['category'];
              //               $master->job_desc =$row['job_desc'];
              //               $master->hmolevel =$row['hmolevel'];
              //               $master->site =$row['site'];
              //               $master->date_regularization =$row['date_regularization'];
              //               $master->date_est_regularization =$row['date_est_regularization'];
              //               $master->regularization_status =$row['regularization_status'];
              //               $master->datehire =$row['datehire'];
              //               $master->dateendorsed =$row['dateendorsed'];
              //               $master->idreleaseddate =$row['idreleaseddate'];
              //               $master->joblevel =$row['joblevel'];
              //               $master->subofficecode =$row['subofficecode'];
              //               $master->suboffice =$row['suboffice'];
              //               $master->extname =$row['ext'];
              //               $master->date_of_deactivation =$row['date_of_deactivation'];

    
              //               $master->emp_eligibility =$row['emp_eligibility'];
              //               $master->dep_eligibility =$row['dep_eligibility'];
              //               $master->dateemp_eligibility =$row['dateemp_eligibility'];
              //               $master->datedep_eligibility =$row['datedep_eligibility'];
              //               $master->emp_rom =$row['emp_rom'];
              //               $master->dep_rom =$row['dep_rom'];
              //               $master->emp_amount =$row['emp_amount'];
              //               $master->dep_amount =$row['dep_amount'];

              //               $master->remark =$row['remark'];
              //               $master->date_created =$row['date_created'];
              //            $q_array[$i]=$master;
              //            $i++;



?>