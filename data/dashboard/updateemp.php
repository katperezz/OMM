<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/AllClassReq.php');


$emp_no = isset($_POST['emp_no']) ? $_POST['emp_no'] : "";
$firstname = isset($_POST['firstname']) ? strtolower($_POST['firstname']) : "";
$lastname= isset($_POST['lastname']) ? strtolower($_POST['lastname']) : "";
$middlename = isset($_POST['middlename']) ? strtolower($_POST['middlename']) : "";
$extname= isset($_POST['extname']) ? strtolower($_POST['extname']) : "";
$dateval = isset($_POST['dob']) ? $_POST['dob'] : "";
$gender = isset($_POST['gender']) ? $_POST['gender'] : "";
$maritalstatus= isset($_POST['maritalstatus']) ? $_POST['maritalstatus'] : "";
$job_desc= isset($_POST['job_desc']) ? $_POST['job_desc'] : "";
$site= isset($_POST['site']) ? $_POST['site'] : "";
$hmo_no= isset($_POST['hmo_no']) ? $_POST['hmo_no'] : "";


// $mx = 100;
// $mn = 1;



	$dob =  date('Y-m-d',strtotime($dateval));	    			 		
	$tz  = new DateTimeZone('Asia/Manila');
	$age = DateTime::createFromFormat('Y-m-d', $dob, $tz)->diff(new DateTime('now', $tz))->y;


$date_created = date('Y-m-d H:i:s');


if(isset($_SESSION['totalcountlist_db']) == NULL || isset($_SESSION['username']) == NULL){
        
			echo $jsonmsg->JmsgFail("Error Session");



}else if(empty($emp_no)){

			echo $jsonmsg->JmsgFail("Error employee no.");

}else{

				if(empty($firstname) ||empty($lastname) || empty($dob) || empty($gender) || empty($maritalstatus)  || empty($emp_no)){
 
								echo $jsonmsg->JmsgFail("Required fields");

				}else{

						$sql1 = "SELECT id FROM mastermembertable WHERE lastname = '".$lastname."' AND firstname = '".$firstname."' AND dob = '".$dob."' AND emp_no = '".$emp_no."'"; //Check if there is a changes // if greater to 0  there is change
						$sql2 = "SELECT id FROM mastermembertable WHERE lastname = '".$lastname."' AND firstname = '".$firstname."' AND dob = '".$dob."'";

						$sql3 = "SELECT remark FROM mastermembertable WHERE emp_no = '".$emp_no."' AND remark = 'endorsed'";

						$sql4 = "SELECT id FROM mastermembertable WHERE emp_no = '".$emp_no."' AND hmo_no = '".$hmo_no."'";//Check if there is a changes /// if greater to 0  there is change
						$cchange  = $dbquery->NumRow($_SESSION['totalcountlist_db'],$sql4);

						$sqla4 = "SELECT id FROM mastermembertable WHERE emp_no = '".$emp_no."' AND hmo_no IS NULL";
						$sqlb4 = "SELECT id FROM mastermembertable WHERE emp_no = '".$emp_no."' AND hmo_no = ''";
						$sqlc4 = "SELECT id FROM mastermembertable WHERE emp_no = '".$emp_no."' AND hmo_no IS NOT NULL";


						$sql5 = "SELECT id FROM mastermembertable WHERE hmo_no = '".$hmo_no."'";

						$sql6 = "SELECT hmo_num FROM account_rule WHERE STATUS = 'active' AND rule_name IN (SELECT rule_name FROM ".$_SESSION['totalcountlist_db'].".mastermembertable WHERE emp_no = '".$emp_no."' )";
						$sql7 = "SELECT minage,maxage FROM plancover WHERE account_rule IN (SELECT rule_name FROM ".$_SESSION['totalcountlist_db'].".mastermembertable WHERE emp_no = '".$emp_no."' ) AND LEVEL IN (SELECT joblevel FROM ".$_SESSION['totalcountlist_db'].".mastermembertable WHERE emp_no = '".$emp_no."' )";

						$sql8 = "SELECT id FROM mastermembertable WHERE dob = '".$dob."' AND emp_no = '".$emp_no."'";

						$m = $validation->MaritalStatusValidation($maritalstatus);
						$g = $validation->GenderValidation($gender);

						$fn = $validation->NameValidation($firstname);
						$ln = $validation->NameValidation($lastname);
						$mn = $validation->MidNameValidation($middlename);
						$en = $validation->ExtensionName($extname);

						$jdesc = $validation->CleanStr($job_desc);
						$s = $validation->CleanStr($site);


						if($validation->maritalstatusflag == 1){
								echo $jsonmsg->JmsgFail("Error marital status");

						}else if($validation->genderflag == 1){
								echo $jsonmsg->JmsgFail("Error gender");

						}else if($firstname == $lastname || $firstname == $middlename || $lastname == $middlename){

								echo $jsonmsg->JmsgFail("Error Name");

						}else if($fn !==1){

								echo $jsonmsg->JmsgFail($fn);

						}else if($ln !==1){

								echo $jsonmsg->JmsgFail($ln);

						}else if($mn !==1){

								echo $jsonmsg->JmsgFail($mn);

						}else if($en !==1){

								echo $jsonmsg->JmsgFail($en);

						}else{
								// name
									if($dbquery->NumRow($_SESSION['totalcountlist_db'],$sql1) == 0){
											if($dbquery->NumRow($_SESSION['totalcountlist_db'],$sql2) > 0){
												$msgnameflag = 1;
											}else{
												$msgnameflag = 0;
											}	

									}else{
											$msgnameflag = 0;
									}

							//HMO		
									if(empty($hmo_no)){

										if($dbquery->NumRow($_SESSION['totalcountlist_db'],$sqla4) > 0 || $dbquery->NumRow($_SESSION['totalcountlist_db'],$sqlb4) > 0){
												$msghmoflag = 0;
										}else{

												$msghmoflag = 2;
										}


									}else{

											if($cchange == 0){
													if($dbquery->NumRow($_SESSION['totalcountlist_db'],$sql5) > 0){
														$msghmoflag = 1;
													}else{

															if($dbquery->NumRow($_SESSION['totalcountlist_db'],$sql3) == 0){
																	$msghmoflag = 4;
															}else{

																		foreach ($dbquery->DbSelect("mmdb",$sql6) as $row){
																					$hmolen = $row['hmo_num'];
																		}

																				if($hmolen == strlen($hmo_no)){
																					$msghmoflag = 0;
																				}else{
																					$msghmoflag = 3;
																				}
															}
														
													}	

											}else{

												$msghmoflag = 0;
											}
									}			

							////dob		
									if($dbquery->NumRow($_SESSION['totalcountlist_db'],$sql8) == 0){
												
										foreach ($dbquery->DbSelect("mmdb",$sql7) as $row){
												$mx = $row['maxage'];
												$mn = $row['minage'];
										}

            								if($age > $mx || $age < $mn){
            									
            									$msgdobflag = 1;

            								}else{
            									$msgdobflag = 0;
            								}

									}else{

										$msgdobflag = 0;
									}	


								if($msgnameflag > 0){

											echo $jsonmsg->JmsgFail("Error : Employee Name And Date of Birth Already Exist into Another Employee Number");
								}else if($msghmoflag == 2){

											echo $jsonmsg->JmsgFail("Error : UPDATING HMO No. To Empty Value");

								}else if($msghmoflag == 4){

											echo $jsonmsg->JmsgFail("Error : Employee Not Yet Endorsed");

								}else if($msghmoflag == 3){

											echo $jsonmsg->JmsgFail("Error : HMO Number Too Short/Long");

								}else if($msghmoflag == 1){

											echo $jsonmsg->JmsgFail("Error : HMO Number Already Exist");

								}else if($msgdobflag > 0){

											echo $jsonmsg->JmsgFail("Error : Age is under or exceed the limit");


								}else{	


								$memranid = $randomid->RanId();

									
										$sql = "UPDATE mastermembertable SET hmo_no = '".$hmo_no."', date_hmoid_upload='".$date_created."', firstname = '".$firstname."',lastname = '".$lastname."',middlename = '".$middlename."',ext = '".$extname."',dob = '".$dob."',gender = '".$g."',maritalstatus = '".$m."',job_desc = '".$jdesc."',site = '".$s."' where emp_no = '".$emp_no."'";

										$l = "INSERT INTO updateupload_logs(file_id,USER, date_created,emp_no,hmo_no,hmo_no_update,site,site_update, lastname, lastname_update, firstname, firstname_update, middlename, middlename_update, ext, ext_update,gender, gender_update, maritalstatus, maritalstatus_update,job_desc, job_desc_update,PROCESS) 
					                           SELECT file_id,'".$_SESSION['username']."','".$date_created."',emp_no,hmo_no,'".$hmo_no."',site,'".$s."',lastname,'".$lastname."',firstname,'".$firstname."',middlename,'".$middlename."',ext,'".$extname."',gender,'".$g."', maritalstatus,'".$m."',job_desc,'".$jdesc."','UPDATE' FROM mmdb_".$_SESSION['totalcountlist_db'].".mastermembertable WHERE emp_no = '".$emp_no."'";

										$m = "INSERT INTO saveupload_logs(file_id,upid,USER, date_created,emp_no,hmo_no,site, lastname, firstname, middlename, ext,dob,gender, maritalstatus,job_desc,joblevel,effectivedate,end_date,datehire,rule_name, PROCESS) 
					                           SELECT file_id,'".$memranid."','".$_SESSION['username']."','".$date_created."',emp_no,'".$hmo_no."','".$s."','".$lastname."','".$firstname."','".$middlename."','".$extname."','".$dob."','".$g."','".$m."','".$jdesc."',joblevel,effectivedate,end_date,datehire,rule_name,'UPDATE' FROM mmdb_".$_SESSION['totalcountlist_db'].".mastermembertable WHERE emp_no = '".$emp_no."'";


					          			$dbquery->Dbsqlquery($_SESSION['totalcountlist_db']."_logs",$l);

					          			$dbquery->Dbsqlquery($_SESSION['totalcountlist_db']."_logs",$m);

											if(!$dbquery->Dbsqlquery($_SESSION['totalcountlist_db'],$sql)){
													echo $jsonmsg->JmsgFail("Error update");
											}else{

												if($cchange == 0){
														if($dbquery->NumRow($_SESSION['totalcountlist_db'],$sqlb4) == 0 && $dbquery->NumRow($_SESSION['totalcountlist_db'],$sqlc4) > 0){
																$updateactive = "UPDATE mastermembertable SET member_status = 'ACTIVE',date_hmoid_upload = CURDATE() where emp_no = '".$emp_no."'";
																$dbquery->Dbsqlquery($_SESSION['totalcountlist_db'],$updateactive);

														}
												}		

												echo json_encode(array('success' => true));
											}
								}	


						}
						



			}




}




?>