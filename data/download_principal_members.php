<?php
session_start();
header("Content-type: application/json");


include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/AllClassReq.php');

if(isset($_POST['acc_id']) == NULL){
    $jsonresponse->success = false;
    $jsonresponse->message = "no post";
    $jsonresponse->total = "0";
    $jsonresponse->data = "";

    echo $jsonresponse->to_json();

}else{
    // $jsonresponse->success = true;
    // $jsonresponse->message ="YEY";
    // $jsonresponse->total = "0";
    // $jsonresponse->data = "";

    // echo $jsonresponse->to_json();    


    
$dbname = $_POST['acc_id'];
$db ="mmdb_".$dbname;
include('db_connPDO.php'); 


function cellColor($cells,$color){
    global $objPHPExcel;
    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('rgb' => $color),
                            'font'  => array(
                                                'bold'  => true,
                                                'color' => array('rgb' => 'FF0000'),
                                                'size'  => 9,
                                                'name'  => 'Verdana'
                                            )
    
                            )
                        );
}


$s =  "select*from tempmembertable where status != 'valid' and user= '".$_SESSION['username']."' and file_id='".$_SESSION['delete_fileid']."'";


$objPHPExcel = new PHPExcel(); 
$objPHPExcel->setActiveSheetIndex(0); 
$rowCount = 6; 

date_default_timezone_set("Asia/Manila");
$today = date("F j, Y, g:i a"); 

$objPHPExcel->getActiveSheet()->SetCellValue('A1', "ActiveLink - Error Report");
$objPHPExcel->getActiveSheet()->SetCellValue('A2', "Account Name");
$objPHPExcel->getActiveSheet()->SetCellValue('B2',$dbname);
$objPHPExcel->getActiveSheet()->SetCellValue('B3',$today);
$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Date Created");

$objPHPExcel->getActiveSheet()->SetCellValue('A5', "ERROR STATUS");
cellColor('A5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('B5', "Employee_no"); 
cellColor('B5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('C5', "Lastname"); 
cellColor('C5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('D5', "Firstname"); 
cellColor('D5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('E5', "Middlename"); 
cellColor('E5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('F5', "Extension_name"); 
cellColor('F5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('G5', "Date of Birth"); 
cellColor('G5', 'CCEEFF');

        $qselect = $conn->query($s);
        
        while($row = $qselect->fetch(PDO::FETCH_ASSOC)){    
    
  
         $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$row['status']);
         cellColor('A'.$rowCount, 'F28A8C');
         $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$row['emp_no']);    
         $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,$row['lastname']);
         $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,$row['firstname']);
         $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,$row['middlename']);
         $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,$row['ext']);         
         $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,$row['dob']);
   
    
    $rowCount++; 
} 

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
header('Content-Disposition: attachment;filename="'.$dbname.'_error.xlsx"'); 
header('Cache-Control: max-age=0'); 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
$objWriter->save('php://output'); 

}

?>