<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once('DbConnection.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');

class Activity_Logs extends Dbconnection{
	

/*
*
*   function InsertLogs 
*       
*   it insert into activity_logs
*
*   @param  string  $user,
*   @param  string  $action 
*
*   
*   return bolean    
*
*
*
*
*/


public function InsertLogs($user,$action){

	try{

        if(!$this->OpenDB2()){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

		$sql = "INSERT INTO activity_logs (USER,ACTION) VALUES(:user,:action)";
		$q = $this->conndb2->prepare($sql);
		$values = array(':user'=>$user,':action'=>$action);


		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conndb2->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
       			return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: ". $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }			

}	



/*
*
*   function SendFileLogs 
*       
*   it insert into sendfile_logs
*
*   @param string   $dbname,
*   @param string   $user,
*   @param string   $detail,
*   @param string   $description,
*   @param string   $category,
*   @param string   $send_to,
*   @param string   $remark
*   
*
*   
*   return bolean    
*
*
*
*
*/


public function SendFileLogs($dbname,$user,$detail,$description,$category,$send_to,$remark){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

				$sql = "INSERT INTO sendfile_logs (USER,detail,description,category,send_to,remark) VALUES (:user,:detail,:description,:category,:send_to,:remark)";
				$q = $this->conn->prepare($sql);
				$values = array(':user'=> $user,':detail'=> $detail,':description'=> $description,':category'=> $category,':send_to'=> $send_to,':remark'=> $remark);


		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	


}




}






?>