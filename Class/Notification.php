<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
// include_once('DbConnection.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');

class Notification extends DbConnection{
	
/*
*
*   function AllUserArray 
*       
*   it select all user of that account
*
*   @param  string  $accountname,

*
*   
*   return array    
*
*
*
*
*/

public function AllUserArray($accountname){

  try{

        if(!$this->OpenDB2()){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

        			$sql = "SELECT username FROM USER WHERE STATUS = 'active' and accountname = '".$accountname."'";
              $q = $this->conndb2->prepare($sql);


            if(!$q->execute()){

                    $errmsg = implode(" ", $q->errorInfo());
                    $er = implode(" ", $this->conndb2->errorInfo());
                    $emsg = "error code  :".$errmsg." || error code  : ".$er; 

                
                    throw new Exception($emsg);

                      return $arr = array();

            }


                    $arr = array("admin");

                    foreach($q->fetchAll() as $r){

                      $arr[] = $r['username'];

                    }

                    return $arr;

      }catch(Exception $e){

                   $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
                   catcherror_log($err);



      }                




}

/*
*
*   function InsertNotification 
*       
*   it insert a notification table
*
* @param  string  $dbname,
* @param  string  $user,
* @param  string  $detail,
* @param  string  $description,
* @param  string  $category,
* @param  string  $date
*   
*   return boolean    
*
*
*
*
*/

public function InsertNotification($dbname,$user,$detail,$description,$category,$date){

	try{			

           $arruser = $this->AllUserArray($dbname);

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

        			

        			$user_all = json_encode($arruser);
        			$user_seen = json_encode(array());
        			$seen_status = "inc";


				$sql = "INSERT INTO notification (USER,detail,description,category,user_seen,user_all,seen_status,DATETIME) VALUES (:user,:detail,:description,:category,:user_seen,:user_all,:seen_status,:datetime)";
				$q = $this->conn->prepare($sql);
				$values = array(':user'=> $user,':detail'=> $detail,':description'=> $description,':category'=> $category,':user_seen' => $user_seen,':user_all' => $user_all,':seen_status' => $seen_status,':datetime'=> $date);


            if(!$q->execute($values)){

                    $errmsg = implode(" ", $q->errorInfo());
                    $er = implode(" ", $this->conn->errorInfo());
                    $emsg = "error code  :".$errmsg." || error code  : ".$er; 

                
                    throw new Exception($emsg);

                    return false;

            }
    
    return true;

    }catch(Exception $e){

      $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
      catcherror_log($err);



    }


	}


/*
*
*   function UpdateUserSeen 
*       
*   it update the notification if the user seen the notification
*
* @param  string  $dbname,
* @param  string  $user,
* @param  string  $id,

*   
*   return boolean    
*
*
*
*
*/




	public function UpdateUserSeen($dbname,$id,$user){


  try{

                      $arr_seen = array();
                      $arr_userall = array();


        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }


        			$sql = "SELECT user_seen,user_all from notification where id = '".$id."'";
        			$q = $this->conn->prepare($sql);


            if(!$q->execute()){

                    $errmsg = implode(" ", $q->errorInfo());
                    $er = implode(" ", $this->conn->errorInfo());
                    $emsg = "error code  :".$errmsg." || error code  : ".$er; 

                
                    throw new Exception($emsg);

                    return false;

            }

       				 		

       				 				foreach($q->fetchAll() as $r){

       				 					   $arrseen = $r['user_seen'];
       				 					   $arruserall = $r['user_all'];

       				 				}
                      
       				 				$arr_seen = json_decode($arrseen);
       				 				$arr_userall = json_decode($arruserall);
       				 				

       				 

               array_push($arr_seen, $user);

       		     $arrayuser = explode(" ", $user);  

               $arrdiff = array_diff($arr_userall,$arrayuser);


               if(empty($arrdiff)){ 
                       $seen_status = "c";
               }else{
                      $seen_status = "inc";
               }

               $arrdiff = implode(" ", $arrdiff);
               $arrdiff = explode(" ", $arrdiff);



                 $update_userall =  json_encode($arrdiff);
                 $update_userseen =   json_encode($arr_seen);               





       				 $sql2 = "UPDATE notification SET user_seen = '".$update_userseen."',user_all = '".$update_userall."',seen_status = '".$seen_status."' where id = '".$id."'";
       				 $q2 = $this->conn->prepare($sql2);
      				


            if(!$q2->execute()){

                    $errmsg = implode(" ", $q2->errorInfo());
                    $er = implode(" ", $this->conn->errorInfo());
                    $emsg = "error code  :".$errmsg." || error code  : ".$er; 

                
                    throw new Exception($emsg);

                    return false;

            }


                return true;

      }catch(Exception $e){

        $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
        catcherror_log($err);



      } 

		}


}




?>