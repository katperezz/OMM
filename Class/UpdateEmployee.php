<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once('DbConnection.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');

class UpdateEmployee extends Dbconnection{


/*
*
*   function UpdateFLname 
*       
*   update firstname and lastname and extensionname to the mastermembertable table
*
*   @param  string $dbname,
*   @param  string $emp_no,
*   @param  string $firstname,
*   @param  string $lastname,
*   @param  string $ext
*   
*
*   
*   return bolean    
*
*
*
*
*/  

public function UpdateFLname($dbname,$emp_no,$firstname,$lastname,$ext){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
	

				$sql = "UPDATE mastermembertable SET firstname = :firstname,lastname = :lastname,ext = :ext where emp_no = :emp_no";
				$q = $this->conn->prepare($sql);				
				$values =  array(':firstname'=>$firstname,':lastname'=>$lastname,':emp_no'=>$emp_no,':ext'=>$ext);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			return true;


    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }		
}


/*
*
*   function UpdateDOB 
*       
*   update date of birth to the mastermembertable table
*
*   @param  string $dbname,
*   @param  string $emp_no,
*   @param  string $dob,
*
*
*   
*   return bolean    
*
*
*
*
*/ 

public function UpdateDOB($dbname,$emp_no,$dob){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

				$sql = "UPDATE mastermembertable SET dob = :dob where emp_no = :emp_no";
				$q = $this->conn->prepare($sql);				
				$values =  array(':dob'=>$dob,':emp_no'=>$emp_no);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		

			return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }				

}


/*
*
*   function UpdateMaritalstatus 
*       
*   update marital status to the mastermembertable table
*
*   @param  string $dbname,
*   @param  string $emp_no,
*   @param  string $mstatus,
*
*
*   
*   return bolean    
*
*
*
*
*/ 

public function UpdateMaritalstatus($dbname,$emp_no,$mstatus){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
	

				$sql = "UPDATE mastermembertable SET maritalstatus = :maritalstatus where emp_no = :emp_no";
				$q = $this->conn->prepare($sql);				
				$values =  array(':maritalstatus'=>$mstatus,':emp_no'=>$emp_no);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		

			return true;	


    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }

}


/*
*
*   function UpdateSite 
*       
*   update site to the mastermembertable table
*
*   @param  string $dbname,
*   @param  string $emp_no,
*   @param  string $site,
*
*
*   
*   return bolean    
*
*
*
*
*/



public function UpdateSite($dbname,$emp_no,$site){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
	

				$sql = "UPDATE mastermembertable SET site = :site where emp_no = :emp_no";
				$q = $this->conn->prepare($sql);				
				$values =  array(':site'=>$site,':emp_no'=>$emp_no);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		

			return true;	


    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }

}


/*
*
*   function UpdateSuboffice 
*       
*   update suboffice to the mastermembertable table
*
*   @param  string $dbname,
*   @param  string $emp_no,
*   @param  string $suboffice,
*
*
*   
*   return bolean    
*
*
*
*
*/

public function UpdateSuboffice($dbname,$emp_no,$suboffice){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
	
	

				$sql = "UPDATE mastermembertable SET suboffice = :suboffice where emp_no = :emp_no";
				$q = $this->conn->prepare($sql);				
				$values =  array(':suboffice'=>$suboffice,':emp_no'=>$emp_no);
		        
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		

			return true;	


    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }
}


/*
*
*   function UpdateHmo_id 
*       
*   update hmo_no to the mastermembertable table
*
*   @param  string $dbname,
*   @param  string $emp_no,
*   @param  string $hmo_no,
*
*
*   
*   return bolean    
*
*
*
*
*/


public function UpdateHmo_id($dbname,$emp_no,$hmo_no){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
	

				$sql = "UPDATE mastermembertable SET hmo_no = :hmo_no,date_hmoid_upload = NOW(),member_status = 'ACTIVE' where emp_no = :emp_no and remark='ENDORSED'";
				$q = $this->conn->prepare($sql);				
				$values =  array(':hmo_no'=>$hmo_no,':emp_no'=>$emp_no);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		

			return true;	


    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }

}



/*
*
*   function PlancoverQuery 
*       
*   select the plan cover information to validate plan cover
*
*   @param  string $account_id,
*   @param  string $level,
*
*
*
*   
*   return array    
*
*
*
*
*/


public function PlancoverQuery($account_id,$level){



	try{

/**************		DBCONNECTION 2					*******************************************************************/

        if(!$this->OpenDB2()){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

			$sql = "SELECT account_rule.effective_date,empeligibility,depeligibility,empcoverage,depcoverage,emprom,deprom FROM plancover,account_rule WHERE account_rule.account_id = '".$account_id."' AND LEVEL = '".$level."' AND account_rule IN (SELECT rule_name FROM account_rule WHERE account_id = '".$account_id."' AND STATUS = 'active')";
				
			$qselect = $this->conndb2->query($sql);


		        if(!$qselect){

                		$errmsg = implode(" ", $qselect->errorInfo());
                		$er = implode(" ", $this->conndb2->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

		
					while($r = $qselect->fetch(PDO::FETCH_ASSOC)){
							$data[]=$r;
					}
						return $data;


    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }		



}


/*
*
*   function UpdateJoblevel 
*       
*   update joblevel to the mastermembertable table
*
*   @param  string $dbname,
*   @param  string $emp_no,
*   @param  string $joblevel,
*
*
*   
*   return bolean    
*
*
*
*
*/


public function UpdateJoblevel($dbname,$emp_no,$joblevel){

try{

				foreach($this->PlancoverQuery($dbname,$joblevel) as $row){

						
						$efdate = $row['effective_date'];
						$empeligibility = $row['empeligibility'];
						$depeligibility = $row['depeligibility'];
						$empcoverage = $row['empcoverage'];
						$depcoverage = $row['depcoverage'];
						$emprom = $row['emprom'];
						$deprom = $row['deprom'];
				}

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }


				$sql = "UPDATE mmdb_".$dbname.".mastermembertable SET hmolevel = :joblevel,joblevel = :joblevel,effectivedate = :effective_date,emp_eligibility = :empeligibility,dep_eligibility = :depeligibility,emp_amount = :empcoverage,dep_amount = :depcoverage,emp_rom = :emprom,dep_rom = :deprom WHERE emp_no = :emp_no";
				$q = $this->conn->prepare($sql);				
				$values =  array(':joblevel'=>$joblevel,':emp_no'=>$emp_no,':effective_date' =>$efdate,':empeligibility' =>$empeligibility,':depeligibility' =>$depeligibility,':empcoverage' =>$empcoverage,':depcoverage' =>$depcoverage,':emprom' =>$emprom,':deprom' =>$deprom);
		
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

       			return true;

 }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



 }	

}



/*
*
*   function UpdateEffectivedate
*       
*   update effectivedate  to the mastermembertable table
*
*   @param  string $dbname,
*   @param  string $emp_no,
*   @param  string $effectivedate,
*
*
*   
*   return bolean    
*
*
*
*
*/

public function UpdateEffectivedate($dbname,$emp_no,$effectivedate){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

				$sql = "UPDATE mastermembertable SET effectivedate = :effectivedate where emp_no = :emp_no";
				$q = $this->conn->prepare($sql);				
				$values =  array(':effectivedate'=>$effectivedate,':emp_no'=>$emp_no);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	

}


/*
*
*   function UpdateIdreleaseddate 
*       
*   update idreleaseddate to the mastermembertable table
*
*   @param  string $dbname,
*   @param  string $emp_no,
*   @param  string $idreleaseddate,
*
*
*   
*   return bolean    
*
*
*
*
*/


public function UpdateIdreleaseddate($dbname,$emp_no,$idreleaseddate){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
	

				$sql = "UPDATE mastermembertable SET idreleaseddate = :idreleaseddate where emp_no = :emp_no";
				$q = $this->conn->prepare($sql);				
				$values =  array(':idreleaseddate'=>$idreleaseddate,':emp_no'=>$emp_no);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }		


}



		






}




?>