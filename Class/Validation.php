<?php

class Validation {


  /*
*
*   function MaritalStatusValidation 
*       
*   it validate the marital status and return the maritalstatusflag value
*
*   @param  string  $mstatus,
*
*   
*   return numeric    
*
*
*
*
*/

  public $maritalstatusflag = 0;
  public $genderflag = 0;


  public function MaritalStatusValidation($mstatus){


    if($mstatus == "single" || $mstatus == "married" || $mstatus == "divorced" || $mstatus == "separated" || $mstatus == "widowed" || $mstatus == "widow" || $mstatus == "separate" || $mstatus == "divorce"){

        $maritalstatus = $mstatus;
        $this->maritalstatusflag = 0;

    }else{

      if($mstatus == "S" || $mstatus == "s"){

          $maritalstatus = "single";
          $this->maritalstatusflag = 0;
      
      }else if($mstatus == "M" || $mstatus == "m"){
        $maritalstatus = "married";
        $this->maritalstatusflag = 0;
      }else if($mstatus == "D" || $mstatus == "d"){

        $maritalstatus = "divorced";
        $this->maritalstatusflag = 0;
      }else if($mstatus == "SP" || $mstatus == "sp"){

        $maritalstatus = "separated";
        $this->maritalstatusflag = 0;
      }else if($mstatus == "SE" || $mstatus == "se"){

        $maritalstatus = "separated";
        $this->maritalstatusflag = 0;
      }else if($mstatus == "W" || $mstatus == "w"){

        $maritalstatus = "widowed";
        $this->maritalstatusflag = 0;
      }else{

        $maritalstatus = $mstatus;
        $this->maritalstatusflag = 1;
      }
      

    }

    return $maritalstatus;

}


  /*
*
*   function GenderValidation 
*       
*   it validate the gender and return the genderflag value
*
*   @param  string  $mstatus,
*
*   
*   return numeric    
*
*
*
*
*/


 public function GenderValidation($gender){

    if($gender == "male" || $gender == "female"){

      $genderval = $gender;
      $this->genderflag = 0;

    }else{

      if($gender == "m"){

        $genderval = "male";
         $this->genderflag = 0;

      }else if($gender == "f"){

        $genderval = "female";
         $this->genderflag = 0;

      }else{

          $genderval = $gender;
           $this->genderflag = 1;
      }


    }
    return $genderval;

}

  public function CleanStr($string) {  

    return preg_replace('/[^a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð .\-]/', '', $string); // Removes special chars.
  }

  public function CleanStrNoNum($string) {  

    $s = str_replace(str_split("\\/':*?<>|%-=#$^&!@;"), '', $string);
    $r = str_replace('"','',$s);
   return $r;
  }

  public function CleanDate($string){
     
     $s = preg_replace('/\/[^a-zA-Z0-9\-]/', '', $string); 
     $a = str_replace('"', "", $s);
     $b = str_replace("'", "", $a);

   return $b;

  }



  public function NameValidation($str){

        $str = mb_strtolower($str);
        $str = str_replace(str_split('\\/:*?"<>|,%#$^!():[]=_+{}.&@`1234567890'),'', $str);
        $str = str_replace("'", "", $str);

        $strarr = explode(" ", $str);



        $strspace = str_replace(" ", "", $str);

        if(strlen($strspace) == 1){

              if(!preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð]+$/u", $strspace)){
                    $statusarr[] = "char";
              }else{
                    $statusarr[] = "middle";
              }     

        }else{

            foreach($strarr as $r){

                if(strlen($r) == 1){

                    $status = "middle";

                }else if($r == "junior" || $r == "senior" || $r == "jr" || $r == "sr" || $r == "ii" || $r == "iii" || $r == "iv" || $r == "vi" || $r == "vii" || $r == "viii" || $r == "ix"){

                  $status = "ext";
                }else{

                  $status = "valid";

                }

                $statusarr[] = $status;

             }




        }


        if(in_array("ext", $statusarr)){

              $status = "ERROR column for extension name";
        
        }else if(in_array("middle", $statusarr)) {

              $status = "ERROR column for extension name/middlename";
        
        }else if(in_array("char", $statusarr)) {

              $status = "ERROR character";
        
        }else{
              $status = 1;
        }

        return $status;

}
  public function MidNameValidation($str){

        $str = mb_strtolower($str);
        $str = str_replace(str_split('\\/:*?"<>|,%#$^!():[]=_+{}`.&@1234567890'),'', $str);
        $str = str_replace("'", "", $str);

        $strarr = explode(" ", $str);



        $strspace = str_replace(" ", "", $str);

        if(strlen($strspace) == 1){

              if(preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð]+$/u", $strspace)){
                    $statusarr[] = "valid";
              }else{
                    $statusarr[] = "middle";
              }     

        }else{

            foreach($strarr as $r){

                if(strlen($r) == 1){

                    $status = "middle";

                }else if($r == "junior" || $r == "senior" || $r == "jr" || $r == "sr" || $r == "ii" || $r == "iii" || $r == "iv" || $r == "vi" || $r == "vii" || $r == "viii" || $r == "ix"){

                  $status = "ext";
                }else{

                  $status = "valid";

                }

                $statusarr[] = $status;


             }




        }


        if(in_array("ext", $statusarr)){

              $status = "ERROR column for extension name";
        
        }else if(in_array("middle", $statusarr)) {

              $status = "ERROR middlename or column for extension name";
        
        }else{
              $status = 1;
        }

        return $status;

}



public function ExtensionName($str){

        $string = $this->CleanStrNoNum($str);
        $s = mb_strtolower($string);
        $name = preg_replace('/[0-9]+/', '', $s);
        $name = str_replace(" ", "", $name);
        $name = str_replace(".", "", $name);



    
    if(empty($name)){

        $status = 1;

    }else{      
                    if(isset($name[3]) > 3){

                            if (preg_match('/\b(jr|sr|ii|iii|iv|v|vi|vii|viii|ix|x)\b/', $name)){
            
                                     $status = 1;
                            }else if($name == "senior" || $name == "junior"){

                                     $status = 1;
                            }else{
                                     $status = "ERROR extension name";

                            }

                    }else{

                            if (preg_match('/\b(jr.|jr|sr|sr.|ii|iii|iv|v|vi|vii|viii|ix|x)\b/', $name)){
            
                                    $status = 1;
                            }else{

                                    $status = "ERROR extension name";
                            }


                    }          



    }  


    return $status;

}


	public function EmptyVal($emp,$sss,$phil,$lname,$fname,$gender,$dob,$marital,$joblevel,$datehire){

				if(empty($emp) &&  empty($sss) && empty($phil) && empty($lname) && empty($fname) && empty($gender) && empty($dob) && empty($marital) && empty($joblevel) && empty($datehire)){

					 $emptyvalflag = 0;

				}else{
						if(empty($emp) && empty($sss) && empty($phil)){
								$emptyvalflag = 0;
						}elseif(empty($lname) || empty($fname) || empty($gender) || empty($dob) || empty($marital) || empty($joblevel) || empty($datehire)){
								$emptyvalflag = 0;
						}else{
								$emptyvalflag = 1;
						}
					 	
				}

				return $emptyvalflag;

	}

	public function AgeValue($minage,$maxage,$age){


			if($age >= $minage && $age <= $maxage){
				// $agestatus = "valid";
				$ageflag = 1;

			}else{
				// $agestatus = "error age";
				$ageflag = 0;
			}

			return $ageflag;

	}

	public function NameValue($lname,$fname,$mname,$extname){

			if($fname !== $lname && $fname !== $mname && $lname !== $mname){

              $f = $this->NameValidation($fname);
              $l = $this->NameValidation($lname);
              $m = $this->MidNameValidation($mname);
              $e = $this->ExtensionName($extname);

              if($f == 1 && $l == 1 && $m ==1 && $e == 1){
                       $nameflag = 1;

              }else{

                  if($f != 1){
                    $nameflag = $f;

                  }else if($l != 1){
                    $nameflag = $l;

                  }else if($m != 1){
                    $nameflag = $m;

                  }else if($e !=1){

                    $nameflag = $e;
                  }

              }


      }else{

          $nameflag = "ERROR Name ";
      }
			// 		$nameflag = 1;
			// }else{
			// 		$nameflag = 0;
			// }

			return $nameflag;
	}


	public function CodeSeq($preg,$num){

			if(preg_match($preg,$num)){
            		 			$codeseqflag = 1;
            }else{
            		 				$codeseqflag = 0;
            }

            return $codeseqflag;
	}

	public function Codelen($reqlen,$num){

			if(strlen($num) == $reqlen){
				$codelenflag = 1;

			}else{
				$codelenflag = 0;
			}
			
			return $codelenflag;
	}

	public function Validating($min_age,$max_age,$age,$lname,$fname,$mname,$extname,$len_empnum,$emp_no,$sss_no,$phil_no){





				          $ageflag = $this->AgeValue($min_age,$max_age,$age);
                  $nameflag = $this->NameValue($lname,$fname,$mname,$extname);
                  $emp_len = $this->Codelen($len_empnum,$emp_no);
                  $emp_codeseqflag = $this->CodeSeq("/^[a-zA-Z0-9]+$/",$emp_no);
                  $sss_codeseqflag = $this->CodeSeq("/^[0-9]{2}\-[0-9]{8}\-[0-9]{1,4}$/",$sss_no);
                  $phil_codeseqflag = $this->CodeSeq("/^[0-9]{2}\-[0-9]{8}\-[0-9]{1,4}$/",$phil_no);



                //   dev_log("==>".$ageflag."=>".$nameflag."=>".$emp_len."!");

                  if($nameflag != 1){


                    $status = $nameflag;

                  }else{

                  if($ageflag == 1){

                      if(!empty($emp_no)){
                        if($emp_codeseqflag == 1 && $emp_len == 1){
                          $status = "valid";
                        }else{
                          $status = "error employee number";
                        }
                      }
                    
                  }else{

                      if(!empty($emp_no)){


                          if($emp_codeseqflag == 0 || $emp_len == 0){
                                $status = "error employee number";
                          }else{
                                  if($ageflag == 0){
                                        $status = "error employee age";
                                  }
                                                    
                          }

       

                      }
                      if(!empty($sss_no)){
                        
                        if($sss_codeseqflag == 0){
                          $status = "error employee name and SSS number";
                        }else{
                          if($sss_codeseqflag == 0 || $sss_len == 0){
                            $status = "error SSS number";
                          }
                          // else if($nameflag == 0){
                          //   $status = "error employee name";
                          // }
                        }

                      }

                       if(!empty($phil_no)){

                        if($phil_codeseqflag == 0 && $nameflag == 0){
                          $status = "error employee name and PhilHealth number";
                        }else{
                          if($phil_codeseqflag == 0 || $sss_len == 0){
                            $status = "error PhilHealth number";
                          }
                          // else if($nameflag == 0){
                          //   $status = "error employee name";
                          // }
                        }
                        
                      } 

                }
              }


                return $status;

	}  





}



?>