<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');
// include_once('DbConnection.php');
include_once('DbConnection.php');

class ForceTablesLogs extends DbConnection{
		
/*
*
*   function ForceRegularLogs 
*       
*   it insert into force_regular_logs 
*
*   @param  string  $dbname,
*   @param  string  $user
*   @param  string  $emp_no
*   
*
*   
*   return bolean    
*
*
*
*
*/

public function ForceRegularLogs($dbname,$user,$emp_no){

try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

		$sql = "INSERT INTO force_regular_logs(user, activelink_id, emp_no, dep_no, hmo_no, lastname, firstname, middlename, ext, gender, dob, age, maritalstatus, category, hmolevel, site, effectivedate, datehire, dateendorsed, idreleaseddate, date_of_deactivation, joblevel, suboffice, subofficecode, job_desc, emp_eligibility, dep_eligibility, dateemp_eligibility, datedep_eligibility, emp_rom, dep_rom, emp_amount, dep_amount,sss_no, phil_no, remark, date_est_regularization, rule_name) 
				SELECT  :user,activelink_id, emp_no, dep_no, hmo_no, lastname, firstname, middlename, ext, gender, dob, age, maritalstatus, category, hmolevel,site, effectivedate, datehire, dateendorsed, idreleaseddate, date_of_deactivation, joblevel, suboffice, subofficecode, job_desc, emp_eligibility, dep_eligibility, dateemp_eligibility,datedep_eligibility, emp_rom, dep_rom, emp_amount, dep_amount, sss_no, phil_no, remark, date_est_regularization, rule_name FROM mmdb_".$dbname.".mastermembertable WHERE emp_no = :emp_no";

		$values = array(':user'=>$user,':emp_no'=>$emp_no);

		$q = $this->conn->prepare($sql);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	


	}




/*
*
*   function ForceEndorsedLogs 
*       
*   it insert into force_endorsed_logs 
*
*   @param  string  $dbname,
*   @param  string  $user
*   @param  string  $emp_no
*   
*
*   
*   return bolean    
*
*
*
*
*/
    


public function ForceEndorsedLogs($dbname,$user,$emp_no){

try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }


		$sql = "INSERT INTO force_endorsed_logs(USER, activelink_id, emp_no, dep_no, hmo_no, lastname, firstname, middlename, ext, gender, dob, age, maritalstatus, category, hmolevel, site, effectivedate, datehire, idreleaseddate, date_of_deactivation, joblevel, suboffice, subofficecode, job_desc, emp_eligibility, dep_eligibility, dateemp_eligibility, datedep_eligibility, emp_rom, dep_rom, emp_amount, dep_amount,sss_no, phil_no, remark,  date_est_regularization,date_created,date_hmoid_upload,date_regularization,regularization_status, rule_name) 
				SELECT  :user,activelink_id, emp_no, dep_no, hmo_no, lastname, firstname, middlename, ext, gender, dob, age, maritalstatus, category, hmolevel,site, effectivedate, datehire,  idreleaseddate, date_of_deactivation, joblevel, suboffice, subofficecode, job_desc, emp_eligibility, dep_eligibility, dateemp_eligibility, datedep_eligibility, emp_rom, dep_rom, emp_amount, dep_amount, sss_no, phil_no, remark, date_est_regularization,date_created,date_hmoid_upload,date_regularization,regularization_status, rule_name FROM mmdb_".$dbname.".mastermembertable WHERE emp_no = :emp_no";


		$values = array(':user'=>$user,':emp_no'=>$emp_no);

		$q = $this->conn->prepare($sql);


				        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
	
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	


}
	





}







?>