<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once('DbConnection.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');


class CheckMemExist extends DbConnection {

	// public $dbnum;
	// public $host="localhost";
	// public $user="root";
	// public $pass="";
	// public $conn;

	public function __construct(){

			$this->dbnum = new DbQuery();	

	}


	public function Chckmem($dbname,$emp_no,$sss_no,$phil_no,$firstname,$lastname,$dob){	

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
	


			if(empty($emp_no)){

				            throw new Exception("empty value emp_no ");
            				return false;

			}

				$qa1 = "SELECT emp_no from mastermembertable where emp_no = '".$emp_no."' and member_status != 'deleted'";
				$qa2 = "SELECT emp_no,lastname,firstname,dob from mastermembertable where lastname = '".$lastname."' and firstname = '".$firstname."' and dob = '".$dob."' and regularization_status != 'deleted'";

				if($this->dbnum->NumRow($dbname,$qa1) === false){
					            throw new Exception("Error check 1 count");
            					return false;
				}
				if($this->dbnum->NumRow($dbname,$qa2) === false){
								 throw new Exception("Error check 2 count");
            					return false;

				}

				$chkemp = $this->dbnum->NumRow($dbname,$qa1);
				$chkempname = $this->dbnum->NumRow($dbname,$qa2);
				

						if($chkemp > 0){

							$status = "member already exist";
						}else{

							if($chkempname > 0){

								$schkempname = $this->dbnum->DbSelect($dbname,$qa2);

								$i = 0;
								$memval_array = array();
								foreach($schkempname as $row){

									$memval = "| employee no.: ".$row['emp_no']."| Lastname: ".$row['lastname']."| Firstname: ".$row['firstname']."| Date of Birth: ".$row['dob']." |";				
									$memval_array[$i] = $memval;

									$i++;
								}
								$memvalstatus = join(",",$memval_array);
								$status = "member same name and date of birth to this existing member:".$memvalstatus." Contact the Admin if this is not an Error";						
					
							}else{

									$status = "valid";
							}

						}
	


		return $status;	


 }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	
}





public function ChckDupEn($dbname,$file_id,$user,$emp_no,$sss_no,$phil_no,$lastname,$firstname,$dob){


	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
		// $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$dbname,$this->user,$this->pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));				
			

				$q = "SELECT id from tempmembertable where lastname = '".$lastname."' and firstname = '".$firstname."' and dob = '".$dob."' and user = '".$user."' and file_id = '".$file_id."'";			
				$chkname = $this->dbnum->NumRow($dbname,$q);

				$qa1 = "SELECT id from tempmembertable where emp_no = '".$emp_no."' and user = '".$user."' and file_id = '".$file_id."'";
				$chkemp = $this->dbnum->NumRow($dbname,$qa1);

				$qa4 = "SELECT id from tempmembertable where emp_no = '".$emp_no."' and lastname = '".$lastname."' and firstname = '".$firstname."' and dob = '".$dob."' and user = '".$user."' and file_id = '".$file_id."'";			
				$chknameemp = $this->dbnum->NumRow($dbname,$qa4);


				if($this->dbnum->NumRow($dbname,$q) === false){

					            throw new Exception("Error chkname");
            					return false;

				}

				if($this->dbnum->NumRow($dbname,$qa1) === false){

					            throw new Exception("Error chkemp");
            					return false;

				}

				if($this->dbnum->NumRow($dbname,$qa4) === false){
					            throw new Exception("Error chknameemp");
            					return false;

				}





			if($chknameemp > 1){
				

										$sql = "UPDATE tempmembertable SET status = :status where emp_no = :emp_no and lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
										$q1 = $this->conn->prepare($sql);
										$s = "duplicate entry emp_no and employee name";
										$values =  array(':status'=> $s,':emp_no'=>$emp_no,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $file_id, ':user' => $user);
		
		        if(!$q1->execute($values)){

                		$errmsg = implode(" ", $q1->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}




			}else{


						if($chkname > 1){
		

										$sql = "UPDATE tempmembertable SET status = :status where lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
										$q2 = $this->conn->prepare($sql);
										$s = "duplicate entry employee name";
										$values =  array(':status'=> $s,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $file_id, ':user' => $user);
		
		       					if(!$q2->execute($values)){

                					$errmsg = implode(" ", $q2->errorInfo());
                					$er = implode(" ", $this->conn->errorInfo());
                					$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                					throw new Exception($emsg);

                					return false;

       							}



					  }else if($chkemp > 1){


										$sql = "UPDATE tempmembertable SET status = :status where emp_no = :emp_no and file_id = :file_id and user = :user";
										$q3 = $this->conn->prepare($sql);
										$s = "duplicate entry emp_no";
										$values =  array(':status'=> $s,':emp_no'=>$emp_no,':file_id' => $file_id, ':user' => $user);
									
		        				if(!$q3->execute($values)){

                					$errmsg = implode(" ", $q3->errorInfo());
                					$er = implode(" ", $this->conn->errorInfo());
                					$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                					throw new Exception($emsg);

                					return false;

       							}


							
					}

			}


	return true;


    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }		


}
	

public function InsertDupEntry($dbname,$file_id,$upid,$user,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$age,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$datehire){


	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
		

			$q = "SELECT lastname,firstname,dob from tempmembertable where lastname = '".$lastname."' and firstname = '".$firstname."' and dob = '".$dob."' and user = '".$user."' and file_id = '".$file_id."'";			
			
			if($this->dbnum->NumRow($dbname,$q) === false){
					throw new Exception("error return false  chkname");
            		return false;

			}

			$chkname = $this->dbnum->NumRow($dbname,$q);

			if(empty($emp_no)){
				    throw new Exception("empty emp_no ");
            		return false;
			}

				$qa1 = "SELECT emp_no from tempmembertable where emp_no = '".$emp_no."' and user = '".$user."' and file_id = '".$file_id."'";
				
			if($this->dbnum->NumRow($dbname,$qa1) === false){
					throw new Exception("error return false  chkname");
            		return false;

			}


				$chkemp = $this->dbnum->NumRow($dbname,$qa1);

				
						if($chkemp > 0 || $chkname > 0){

							if($chkname > 0){								
								$status = "duplicate entry name"; //return
								
							}elseif ($chkemp > 0) {
								$status = "duplicate entry employee number"; //return								
							}


							$qupdate1 = "UPDATE tempmembertable SET status = '".$status."' where status NOT LIKE '%duplicate%' and user = '".$user."' and emp_no = '".$emp_no."' and file_id = '".$file_id."'";
			
							if($this->dbnum->DbinsertUp($dbname,$file_id,$upid,$user,$status,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$age,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$datehire)){
							
								$this->dbnum->Dbsqlquery($dbname,$qupdate1);

							}
								
						}

			
		

		return true;


	}catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	

}



public function ChckDupEndraft($dbname,$file_id,$user,$emp_no,$sss_no,$phil_no,$lastname,$firstname,$dob){

				if(!$this->OpenDB($dbname)){
					dev_log("OPEN DB ERROR!");
					return false;
				}
		// $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$dbname,$this->user,$this->pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));				
			

			$q = "SELECT lastname,firstname,dob from draftmembertable where lastname = '".$lastname."' and firstname = '".$firstname."' and dob = '".$dob."' and user = '".$user."' and file_id = '".$file_id."'";			
			
			$chkname = $this->dbnum->NumRow($dbname,$q);

			if(!empty($emp_no)){

				$qa1 = "SELECT emp_no from draftmembertable where emp_no = '".$emp_no."' and user = '".$user."' and file_id = '".$file_id."'";
				$chkemp = $this->dbnum->NumRow($dbname,$qa1);

						if($chkemp > 1 || $chkname > 1){
							if($chkemp > 1){
								// dev_log("checkmem=err".$chkemp);
								$status = "e1";//"duplicate entry emp_no"
							}elseif($chkname > 1){
								// dev_log("checkmem=errr".$chkemp);
								$status = "e2";//"duplicate entry employee name"
							}else{
								$status = "e3";//"duplicate entry emp_no and employee name"
							}
						}else{
							// dev_log("checkmem=valid");			
							$status = "valid";						
						}

			}
	

		return $status;	


	}
	







}



?>