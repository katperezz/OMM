<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once('DbConnection.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');


class DeletedActivation extends Dbconnection{


/*
*
*   function InsertSelectTocollectprevdata_logs
*       
*   it insert the a logs in deletedtoactive_logs table   
*    
*	@param $dbname,
*	@param $user,
*	@param $file_id,
*	@param $emp_no
*
*   
*   return boolean    
*
*
*
*
*/	
	

			public function InsertSelectTocollectprevdata_logs($dbname,$user,$file_id,$emp_no){

	try{

        if(!$this->OpenDB($dbname."_logs")){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

					$sql = "INSERT INTO deletedtoactive_logs (user,file_id,activelink_id,emp_no,dep_no,hmo_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus,category,hmolevel,site,effectivedate,datehire,dateendorsed,joblevel,suboffice,subofficecode,job_desc,emp_eligibility,dep_eligibility,dateemp_eligibility,datedep_eligibility,emp_rom,dep_rom,emp_amount,dep_amount,sss_no,phil_no,rule_name)
							Select :user,'".$file_id."',activelink_id,emp_no,dep_no,hmo_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus,category,hmolevel,site,effectivedate,datehire,dateendorsed,joblevel,suboffice,subofficecode,job_desc,emp_eligibility,dep_eligibility,dateemp_eligibility,datedep_eligibility,emp_rom,dep_rom,emp_amount,dep_amount,sss_no,phil_no,rule_name from mmdb_".$dbname.".mastermembertable where emp_no = :emp_no";

					$q = $this->conn->prepare($sql);

					$values = array(':user'=>$user,':emp_no'=>$emp_no);


		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

       			return true;

     }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }
						
			}


/*
*
*   function ReActiveMem
*       
*   update new data of a member to re-active   
*    
*	@param string	$dbname,
*	@param string	$file_id,
*	@param string	$batch_id,
*	@param string	$emp_no,
*	@param string	$hmo_no,
*	@param string	$lastname,
*	@param string	$firstname,
*	@param string	$middlename,
*	@param string	$ext,
*	@param string	$gender,
*	@param string	$dob,
*	@param string	$maritalstatus,
*	@param string	$category,
*	@param string	$hmolevel,
*	@param string	$site,
*	@param string	$effectivedate,
*	@param string	$end_date,
*	@param string	$datehire,
*	@param string	$joblevel,
*	@param string	$suboffice,
*	@param string	$subofficecode,
*	@param string	$job_desc,
*	@param string	$emp_eligibility,
*	@param string	$dep_eligibility,
*	@param string	$dateemp_eligibility,
*	@param string	$datedep_eligibility,
*	@param string	$emp_rom,
*	@param string	$dep_rom,
*	@param string	$emp_amount,
*	@param string	$dep_amount,
*	@param string	$sss_no,
*	@param string	$phil_no,
*	@param string	$remark,
*	@param string	$date_regularization,
*	@param string	$date_est_regularization,
*	@param string	$regularization_status,
*	@param string	$rule_name,
*	@param string	$member_status,
*	@param string	$date_created
*
*   
*   return boolean    
*
*
*
*
*/			


public function ReActiveMem($dbname,$file_id,$batch_id,$emp_no,$hmo_no,$lastname,$firstname,$middlename,$ext,$gender,$dob,$maritalstatus,$category,$hmolevel,$site,$effectivedate,$end_date,$datehire,$joblevel,$suboffice,$subofficecode,$job_desc,$emp_eligibility,$dep_eligibility,$dateemp_eligibility,$datedep_eligibility,$emp_rom,$dep_rom,$emp_amount,$dep_amount,$sss_no,$phil_no,$remark,$date_regularization,$date_est_regularization,$regularization_status,$rule_name,$member_status,$date_created){

				$file_id = empty($file_id) ? "" : $file_id;
				$batch_id = empty($batch_id) ? "" : $batch_id;
				$hmo_no = empty($hmo_no) ? "" : $hmo_no;
				$lastname = empty($lastname) ? "" : $lastname;
				$firstname = empty($firstname) ? "" : $firstname;
				$middlename = empty($middlename) ? "" : $middlename;
				$ext = empty($ext) ? "" : $ext;
				$gender = empty($gender) ? "" : $gender;
				$dob = empty($dob) ? "" : $dob;
				$maritalstatus = empty($maritalstatus) ? "" : $maritalstatus;
				$category = empty($category) ? "" : $category;
				$hmolevel = empty($hmolevel) ? "" : $hmolevel;
				$site = empty($site) ? "" : $site;
				$effectivedate = empty($effectivedate) ? "" : $effectivedate;
				$end_date = empty($end_date) ? "" : $end_date;
				$datehire = empty($datehire) ? "" : $datehire;
				$joblevel = empty($joblevel) ? "" : $joblevel;
				$suboffice = empty($suboffice) ? "" : $suboffice;
				$subofficecode = empty($subofficecode) ? "" : $subofficecode;
				$job_desc = empty($job_desc) ? "" : $job_desc;
				$emp_eligibility = empty($emp_eligibility) ? "" : $emp_eligibility;
				$dep_eligibility = empty($dep_eligibility) ? "" : $dep_eligibility;
				$dateemp_eligibility = empty($dateemp_eligibility) ? "" : $dateemp_eligibility;
				$datedep_eligibility = empty($datedep_eligibility) ? "" : $datedep_eligibility;
				$emp_rom = empty($emp_rom) ? "" : $emp_rom;
				$dep_rom = empty($dep_rom) ? "" : $dep_rom;
				$emp_amount = empty($emp_amount) ? "" : $emp_amount;
				$dep_amount = empty($dep_amount) ? "" : $dep_amount;
				$sss_no = empty($sss_no) ? "" : $sss_no;
				$phil_no = empty($phil_no) ? "" : $phil_no;
				$remark = empty($remark) ? "" : $remark;				
				$date_regularization = empty($date_regularization) ? "" : $date_regularization;
				$date_est_regularization = empty($date_est_regularization) ? "" : $date_est_regularization;
				$regularization_status = empty($regularization_status) ? "" : $regularization_status;
				$rule_name = empty($rule_name) ? "" : $rule_name;
				$member_status = empty($member_status) ? "" : $member_status;
				$date_created = empty($date_created) ? "" : $date_created;

				if(date('Y-m-d',strtotime($date_regularization)) != date('Y-m-d')){
						$date_regularization = "";
				}
				

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

				$sql = "UPDATE mastermembertable SET hmo_no = '',dateendorsed = '',idreleaseddate = '',date_of_deactivation = '',date_hmoid_upload = '',file_id = :file_id,batch_id = :batch_id,hmo_no = :hmo_no,lastname = :lastname,firstname = :firstname,middlename = :middlename,ext = :ext,gender = :gender,dob = :dob,maritalstatus = :maritalstatus,category = :category,hmolevel = :hmolevel,site = :site,effectivedate = :effectivedate,end_date = :end_date,datehire = :datehire,joblevel = :joblevel,suboffice = :suboffice,subofficecode = :subofficecode,job_desc = :job_desc,emp_eligibility = :emp_eligibility,dep_eligibility = :dep_eligibility,dateemp_eligibility = :dateemp_eligibility,datedep_eligibility = :datedep_eligibility,emp_rom = :emp_rom,dep_rom = :dep_rom,emp_amount = :emp_amount,dep_amount = :dep_amount,sss_no = :sss_no,phil_no = :phil_no,remark = :remark,date_regularization = :date_regularization,date_est_regularization = :date_est_regularization,regularization_status = :regularization_status,rule_name = :rule_name,member_status = :member_status,date_created = :date_created where emp_no = :emp_no";

				$q = $this->conn->prepare($sql);

				$values =  array(':emp_no' => $emp_no,':file_id' => $file_id,':batch_id' => $batch_id,':hmo_no' => $hmo_no,':lastname' => $lastname,':firstname' => $firstname,':middlename' => $middlename,':ext' => $ext,':gender' => $gender,':dob' => $dob,':maritalstatus' => $maritalstatus,':category' => $category,':hmolevel' => $hmolevel,':site' => $site,':effectivedate' => $effectivedate,':end_date' => $end_date,':datehire' => $datehire,':joblevel' => $joblevel,':suboffice' => $suboffice,':subofficecode' => $subofficecode,':job_desc' => $job_desc,':emp_eligibility' => $emp_eligibility,':dep_eligibility' => $dep_eligibility,':dateemp_eligibility' => $dateemp_eligibility,':datedep_eligibility' => $datedep_eligibility,':emp_rom' => $emp_rom,':dep_rom' => $dep_rom,':emp_amount' => $emp_amount,':dep_amount' => $dep_amount,':sss_no' => $sss_no,':phil_no' => $phil_no,':remark' => $remark,':date_regularization' => $date_regularization,':date_est_regularization' => $date_est_regularization,':regularization_status' => $regularization_status,':rule_name' => $rule_name,':member_status' => $member_status,':date_created' => $date_created);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

       			return true;

     }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }
		

	}



/*
*
*   function DeletefromMaster
*       
*   it delete all data to mastermembertable table
*    
*	@param $dbname,
*	@param $emp_no
*
*   
*   return boolean    
*
*
*
*/


public function DeletefromMaster($dbname,$emp_no){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

					$sql = "DELETE FROM mastermembertable WHERE emp_no = :emp_no";

					$q = $this->conn->prepare($sql);

					$values = array(':emp_no'=>$emp_no);


		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

       			return true;

     }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }

						
			}



/*
*
*   function Insertnewdata_logs
*       
*   it insert an update into deletedtoactive_logs table
*    
*parameter $dbname
*parameter $file_id
*parameter $emp_no
*parameter $hmo_no_update
*parameter $lastname_update
*parameter $firstname_update
*parameter $middlename_update
*parameter $ext_update
*parameter $gender_update
*parameter $dob_update
*parameter $maritalstatus_update
*parameter $category_update
*parameter $site_update
*parameter $effectivedate_update
*parameter $datehire_update
*parameter $joblevel_update
*parameter $suboffice_update
*parameter $subofficecode_update
*parameter $job_desc_update
*parameter $emp_eligibility_update
*parameter $dep_eligibility_update
*parameter $dateemp_eligibility_update
*parameter $datedep_eligibility_update
*parameter $emp_rom_update
*parameter $dep_rom_update
*parameter $rule_name_update
*
*   
*   return boolean    
*
*
*
*/




public function Insertnewdata_logs($dbname,$file_id,$emp_no,$hmo_no_update,$lastname_update,$firstname_update,$middlename_update,$ext_update,$gender_update,$dob_update,$maritalstatus_update,$category_update,$site_update,$effectivedate_update,$datehire_update,$joblevel_update,$suboffice_update,$subofficecode_update,$job_desc_update,$emp_eligibility_update,$dep_eligibility_update,$dateemp_eligibility_update,$datedep_eligibility_update,$emp_rom_update,$dep_rom_update,$rule_name_update){

	try{

        if(!$this->OpenDB($dbname."_logs")){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

					$sql = "UPDATE deletedtoactive_logs SET hmo_no_update=:hmo_no_update,lastname_update=:lastname_update,firstname_update=:firstname_update,middlename_update=:middlename_update,ext_update=:ext_update,gender_update=:gender_update,dob_update=:dob_update,maritalstatus_update=:maritalstatus_update,category_update=:category_update,site_update=:site_update,effectivedate_update=:effectivedate_update,datehire_update=:datehire_update,joblevel_update=:joblevel_update,suboffice_update=:suboffice_update,subofficecode_update=:subofficecode_update,job_desc_update=:job_desc_update,emp_eligibility_update=:emp_eligibility_update,dep_eligibility_update=:dep_eligibility_update,dateemp_eligibility_update=:dateemp_eligibility_update,datedep_eligibility_update=:datedep_eligibility_update,emp_rom_update=:emp_rom_update,dep_rom_update=:dep_rom_update,rule_name_update=:rule_name_update WHERE emp_no = :emp_no";

					$q = $this->conn->prepare($sql);

					$values = array(':emp_no'=>$emp_no,':hmo_no_update'=>$hmo_no_update,':lastname_update'=>$lastname_update,':firstname_update'=>$firstname_update,':middlename_update'=>$middlename_update,':ext_update'=>$ext_update,':gender_update'=>$gender_update,':dob_update'=>$dob_update,':maritalstatus_update'=>$maritalstatus_update,':category_update'=>$category_update,':site_update'=>$site_update,':effectivedate_update'=>$effectivedate_update,':datehire_update'=>$datehire_update,':joblevel_update'=>$joblevel_update,':suboffice_update'=>$suboffice_update,':subofficecode_update'=>$subofficecode_update,':job_desc_update'=>$job_desc_update,':emp_eligibility_update'=>$emp_eligibility_update,':dep_eligibility_update'=>$dep_eligibility_update,':dateemp_eligibility_update'=>$dateemp_eligibility_update,':datedep_eligibility_update'=>$datedep_eligibility_update,':emp_rom_update'=>$emp_rom_update,':dep_rom_update'=>$dep_rom_update,':rule_name_update'=>$rule_name_update);


		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

       			return true;

     }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }
						
			}






/*
*
*   function Insertnewdata_logs
*       
*   it update mastermembertable table
*    
*parameter  string	$dbname
*parameter  string	$file_id
*parameter  string	$emp_no
*parameter  string	$lastname
*parameter  string	$firstname
*parameter  string	$middlename
*parameter  string	$ext
*parameter  string	$gender
*parameter  string	$dob
*parameter  string	$maritalstatus
*parameter  string	$category
*parameter  string	$hmolevel
*parameter  string	$site
*parameter  string	$effectivedate
*parameter  string	$end_date
*parameter  string	$datehire
*parameter  string	$joblevel
*parameter  string	$suboffice
*parameter  string	$subofficecode
*parameter  string	$job_desc
*parameter  string	$emp_eligibility
*parameter  string	$dep_eligibility
*parameter  string	$dateemp_eligibility
*parameter  string	$datedep_eligibility
*parameter  string	$emp_rom
*parameter  string	$dep_rom
*parameter  string	$emp_amount
*parameter  string	$dep_amount
*parameter  string	$date_regularization
*parameter  string	$date_est_regularization
*parameter  string	$regularization_status
*parameter  string	$rule_name
*parameter  string	$member_status
*parameter  string	$date_created
*
*return boolean
*/




			public function UpdateEmployee($dbname,$file_id,$emp_no,$lastname,$firstname,$middlename,$ext,$gender,$dob,$maritalstatus,$category,$hmolevel,$site,$effectivedate,$end_date,$datehire,$joblevel,$suboffice,$subofficecode,$job_desc,$emp_eligibility,$dep_eligibility,$dateemp_eligibility,$datedep_eligibility,$emp_rom,$dep_rom,$emp_amount,$dep_amount,$date_regularization,$date_est_regularization,$regularization_status,$rule_name,$member_status,$date_created){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }


				$sql = "UPDATE mastermembertable SET file_id=:file_id,emp_no=:emp_no,lastname=:lastname,firstname=:firstname,middlename=:middlename,ext=:ext,gender=:gender,dob=:dob,maritalstatus=:maritalstatus,category=:category,hmolevel=:hmolevel,
								site=:site,effectivedate=:effectivedate,end_date=:end_date,datehire=:datehire,joblevel=:joblevel,suboffice=:suboffice,subofficecode=:subofficecode,job_desc=:job_desc,emp_eligibility=:emp_eligibility,dep_eligibility=:dep_eligibility,dateemp_eligibility=:dateemp_eligibility,
								datedep_eligibility=:datedep_eligibility,emp_rom=:emp_rom,dep_rom=:dep_rom,emp_amount=:emp_amount,dep_amount=:dep_amount,date_regularization=:date_regularization,date_est_regularization=:date_est_regularization,regularization_status=:regularization_status,
								rule_name=:rule_name,member_status=:member_status,date_created=:date_created,hmo_no = '',remark = 'NOT ENDORSED' WHERE emp_no = :emp_no";

				$q = $this->conn->prepare($sql);

					$values = array(':file_id'=>$file_id,':emp_no'=>$emp_no,':lastname'=>$lastname,':firstname'=>$firstname,':middlename'=>$middlename,':ext'=>$ext,':gender'=>$gender,':dob'=>$dob,':maritalstatus'=>$maritalstatus,':category'=>$category,':hmolevel'=>$hmolevel,':site'=>$site,
						':effectivedate'=>$effectivedate,':end_date'=>$end_date,':datehire'=>$datehire,':joblevel'=>$joblevel,':suboffice'=>$suboffice,':subofficecode'=>$subofficecode,':job_desc'=>$job_desc,':emp_eligibility'=>$emp_eligibility,':dep_eligibility'=>$dep_eligibility,':dateemp_eligibility'=>$dateemp_eligibility,
						':datedep_eligibility'=>$datedep_eligibility,':emp_rom'=>$emp_rom,':dep_rom'=>$dep_rom,':emp_amount'=>$emp_amount,':dep_amount'=>$dep_amount,':date_regularization'=>$date_regularization,':date_est_regularization'=>$date_est_regularization,':regularization_status'=>$regularization_status,
						':rule_name'=>$rule_name,':member_status'=>$member_status,':date_created'=>$date_created);

		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

       			return true;

     }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }

						
			}


			




}

// ($_SESSION['fileid'],$_SESSION['batch_file_id'],$emp_no,$hmo_no,$lname,$fname,$mname,$extname,$gender,$dob,$mstatus,$category,$joblevel,$site,$efdate,$endate,$dateh,
// 	$joblevel,$suboffice,$subofficecode,$job_desc,$empeligibility,$depeligibility,$dateemp_eligibility,$datedep_eligibility,$emprom,$deprom,$empcoverage,$depcoverage,
// 	$sss_no,$phil_no,"NOT ENDORSED",$date_regularization,$regularization_dateest,$regstat,$_SESSION['rule_name'],"INACTIVE",$date_created)


?>