<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once('DbConnection.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');
$path = "DbQuery.php__=>";


class DbQuery extends DbConnection{

/*
*
*   function DbSelect
*       
*   it returns array of a select query 
*    
*   @param string $dbname,
*   @param string $sqlselect,

*   
*
*   
*   return array    
*
*
*
*
*/

public function DbSelect($dbname,$sqlselect){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }


        $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        $qselect = $this->conn->prepare($sqlselect);
        
        
        if(!$qselect->execute()){

                $errmsg = implode(" ", $qselect->errorInfo());
                $er = implode(" ", $this->conn->errorInfo());
                $emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                throw new Exception($emsg);

                return false;

        }

                $data = $qselect->fetchAll();
                return $data;
        

    }catch(Exception $e){

    	$err = "\n Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }

}



/*
*
*   function DbSelectUppath 
*       
*   it returns path of a file  
*    
*   @param string $dbname,
*   @param string $fileid,
*   @param string $user,

*   
*
*   
*   return string    
*
*
*
*
*/




public function DbSelectUppath($dbname,$fileid,$user){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
		// $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$dbname,$this->user,$this->pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

		$sqlselectpath = "SELECT * FROM uploadpath where file_id = '".$fileid."' and user ='".$user."'";

		$qselectpath  = $this->conn->query($sqlselectpath);
		
	

		        if(!$qselectpath->execute()){

                		$errmsg = implode(" ", $qselectpath->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			 }


					while($r = $qselectpath->fetch(PDO::FETCH_ASSOC)){
						$path = $r['pathvalue'];
					}

					return $path;

	}catch(Exception $e){

    	$err = "\n Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }				

		
}


/*
*
*   function Dbsqlquery 
*       
*   it execute an sql command 
*    
*   @param string $dbname,
*   @param string $sqlinsert,

*   
*
*   
*   return boolean    
*
*
*
*
*/



public function Dbsqlquery($dbname,$sqlinsert){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
		

		$q = $this->conn->prepare($sqlinsert);

		        if(!$q->execute()){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			 }		


		return true;
		
	}catch(Exception $e){

    	$err = "\n Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }		
		
		
}



/*
*
*   function DbinsertUp 
*       
*   it insert data value in tempmembertable table
*
*   @param string   $dbname,
*   @param string   $file_id,
*   @param string   $upid,
*   @param string   $user,
*   @param string   $date_created,
*   @param string   $status,
*   @param string   $emp_no,
*   @param string   $sss_no,
*   @param string   $phil_no,
*   @param string   $hmo_no,
*   @param string   $suboffice,
*   @param string   $subofficecode,
*   @param string   $site,
*   @param string   $lastname,
*   @param string   $firstname,
*   @param string   $middlename,
*   @param string   $ext,
*   @param string   $dob,
*   @param string   $age,
*   @param string   $gender,
*   @param string   $maritalstatus,
*   @param string   $category,
*   @param string   $job_desc,
*   @param string   $joblevel,
*   @param string   $effectivedate,
*   @param string   $datehire,
*   @param string   $process,
*   @param string   $rule_name,
*   @param string   $idreleaseddate
*   
*
*   
*   return boolean    
*
*
*
*
*/


public function DbinsertUp($dbname,$file_id,$upid,$user,$date_created,$status,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$age,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$datehire,$process,$rule_name,$idreleaseddate){
	
		
	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

            $dob = (empty($dob)) ? "0000-00-00" : $dob;
            $datehire = (empty($datehire)) ? "0000-00-00" : $datehire;
            $effectivedate = (empty($effectivedate)) ? "0000-00-00" : $effectivedate; 
            $idreleaseddate = (empty($idreleaseddate)) ? "0000-00-00" : $idreleaseddate; 



		$sql = "INSERT INTO tempmembertable (file_id,upid,user,date_created,status,emp_no,sss_no,phil_no,hmo_no,suboffice,subofficecode,site,lastname,firstname,middlename,ext,dob,age,gender,maritalstatus,category,job_desc,joblevel,effectivedate,datehire,process,rule_name,idreleaseddate) VALUES (:file_id,:upid,:user,:date_created,:status,:emp_no,:sss_no,:phil_no,:hmo_no,:suboffice,:subofficecode,:site,:lastname,:firstname,:middlename,:ext,:dob,:age,:gender,:maritalstatus,:category,:job_desc,:joblevel,:effectivedate,:datehire,:process,:rule_name,:idreleaseddate)";
											
		$q = $this->conn->prepare($sql);

		$values =  array(':file_id'=>$file_id,':upid'=>$upid,':user'=>$user,'date_created'=>$date_created,':status'=>$status,':emp_no'=>$emp_no,':sss_no'=>$sss_no,':phil_no'=>$phil_no,':hmo_no'=>$hmo_no,':suboffice'=>$suboffice,':subofficecode'=>$subofficecode,':site'=>$site,':lastname'=>$lastname,':firstname'=>$firstname,':middlename'=>$middlename,':ext'=>$ext,':dob'=>$dob,':age'=>$age,':gender'=>$gender,':maritalstatus'=>$maritalstatus,':category'=>$category,':job_desc'=>$job_desc,':joblevel'=>$joblevel,':effectivedate'=>$effectivedate,':datehire'=>$datehire,':process'=>$process,':rule_name'=>$rule_name,':idreleaseddate'=>$idreleaseddate);
		

		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}			
		

		return true;

	}catch(Exception $e){

    	$err = "\n Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }		
		

}



/*
*
*   function DbinsertUpLogs 
*       
*   it insert data value in upload_logs table
*
*   @param string   $dbname,
*   @param string   $details,
*   @param string   $employee_identification,
*   @param string   $date_of_birth,
*   @param string   $lastname,
*   @param string   $firstname,
*   @param string   $user

*   
*
*   
*   return boolean    
*
*
*
*
*/
	
public function DbinsertUpLogs($dbname,$details,$employee_identification,$date_of_birth,$lastname,$firstname,$user){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

		$sqllogs = "INSERT INTO upload_logs SET name=:name,details=:details,employee_identification=:employee_identification,date_of_birth=:date_of_birth,lastname=:lastname,firstname=:firstname,user=:user";
		$q = $this->conn->prepare($sqllogs);
		
		$values = array(':details'=>$details,':employee_identification'=>$employee_identification,':date_of_birth'=>$date_of_birth,':lastname'=>$lastname,':firstname'=>$firstname,':user'=>$user);


		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}


		return true;

	}catch(Exception $e){

    	$err = "\n Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }		
		


	}


 /*
*
*   function NumRow 
*       
*   it returns the count of sql command executed
*
*   @param string   $dbname,
*   @param string   $sqlnumrow,
*
*   
*
*   
*   return number    
*
*
*
*
*/   

	
public function NumRow($dbname,$sqlnumrow){
		
      try{


        
        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }


		$nrow = $this->conn->prepare($sqlnumrow);
		$nrow ->execute();



		     if(!$nrow->execute()){

                $errmsg = implode(" ", $nrow->errorInfo());
                $er = implode(" ", $this->conn->errorInfo());
                $emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                throw new Exception($emsg);

                return false;

        	}


		
		$num_rows = $nrow->rowCount();

		return $num_rows;

	

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }
}



 /*
*
*   function Dbsaveup 
*       
*   it insert a data into mastermembertable table
*
*   @param string   $dbname,
*   @param string   $activelink_id,
*   @param string   $file_id,
*   @param string   $batchid,
*   @param string   $emp_no,
*   @param string   $sss_no,
*   @param string   $phil_no,
*   @param string   $hmo_no,
*   @param string   $lastname,
*   @param string   $firstname,
*   @param string   $middlename,
*   @param string   $gender,
*   @param string   $dob,
*   @param string   $maritalstatus,
*   @param string   $category,
*   @param string   $hmolevel,
*   @param string   $site,
*   @param string   $effectivedate,
*   @param string   $end_date,
*   @param string   $datehire,
*   @param string   $joblevel,
*   @param string   $suboffice,
*   @param string   $subofficecode,
*   @param string   $ext,
*   @param string   $job_desc,
*   @param string   $emp_eligibility,
*   @param string   $dep_eligibility,
*   @param string   $emp_rom,
*   @param string   $dep_rom,
*   @param string   $emp_amount,
*   @param string   $dep_amount,
*   @param string   $dateemp_eligibility,
*   @param string   $datedep_eligibility,
*   @param string   $remark,
*   @param string   $date_created,
*   @param string   $date_regularizationest,
*   @param string   $date_regularization,
*   @param string   $regularization_status,
*   @param string   $rule_name,
*   @param string   $member_status 
*
*   
*
*   
*   return boolean    
*
*
*
*
*/  

public function Dbsaveup($dbname,$activelink_id,$file_id,$batchid,$emp_no,$sss_no,$phil_no,$hmo_no,$lastname,$firstname,$middlename,$gender,$dob,$maritalstatus,$category,$hmolevel,$site,$effectivedate,$end_date,$datehire,$joblevel,$suboffice,$subofficecode,$ext,$job_desc,$emp_eligibility,$dep_eligibility,$emp_rom,$dep_rom,$emp_amount,$dep_amount,$dateemp_eligibility,$datedep_eligibility,$remark,$date_created,$date_regularizationest,$date_regularization,$regularization_status,$rule_name,$member_status){
	
	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

		$sql =  "INSERT INTO mastermembertable (activelink_id,file_id,batch_id,emp_no,sss_no,phil_no,hmo_no,lastname,firstname,middlename,gender,dob,maritalstatus,category,hmolevel,site,effectivedate,end_date,datehire,joblevel,suboffice,subofficecode,ext,job_desc,emp_eligibility,dep_eligibility,emp_rom,dep_rom,emp_amount,dep_amount,dateemp_eligibility,datedep_eligibility,remark,date_created,date_est_regularization,date_regularization,regularization_status,rule_name,member_status) VALUES (:activelink_id,:file_id,:batchid,:emp_no,:sss_no,:phil_no,:hmo_no,:lastname,:firstname,:middlename,:gender,:dob,:maritalstatus,:category,:hmolevel,:site,:effectivedate,:end_date,:datehire,:joblevel,:suboffice,:subofficecode,:ext,:job_desc,:emp_eligibility,:dep_eligibility,:emp_rom,:dep_rom,:emp_amount,:dep_amount,:dateemp_eligibility,:datedep_eligibility,:remark,:date_created,:date_regularizationest,:date_regularization,:regularization_status,:rule_name,:member_status)";

		$q = $this->conn->prepare($sql);

		$values =  array(':activelink_id'=>$activelink_id,':file_id'=>$file_id,':batchid'=>$batchid,':emp_no'=>$emp_no,':sss_no'=>$sss_no,':phil_no'=>$phil_no,':hmo_no'=>$hmo_no,':lastname'=>$lastname,':firstname'=>$firstname,':middlename'=>$middlename,':gender'=>$gender,':dob'=>$dob,':maritalstatus'=>$maritalstatus,':category'=>$category,':hmolevel'=>$hmolevel,':site'=>$site,':effectivedate'=>$effectivedate,':end_date'=>$end_date,':datehire'=>$datehire,':joblevel'=>$joblevel,':suboffice'=>$suboffice,':subofficecode'=>$subofficecode,':ext'=>$ext,':job_desc'=>$job_desc,':emp_eligibility'=>$emp_eligibility,':dep_eligibility'=>$dep_eligibility,':emp_rom'=>$emp_rom,':dep_rom'=>$dep_rom,':emp_amount'=>$emp_amount,':dep_amount'=>$dep_amount,':dateemp_eligibility'=>$dateemp_eligibility,':datedep_eligibility'=>$datedep_eligibility,':remark'=>$remark,'date_created'=>$date_created,':date_regularizationest'=>$date_regularizationest,':date_regularization'=>$date_regularization,':regularization_status'=>$regularization_status,':rule_name'=>$rule_name,':member_status'=>$member_status);
					
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		

		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }		
		

}


/*
*
*   function DbLogs 
*       
*   it insert a data into updateupload_logs table
*
*   @param string   $dbname,
*   @param string   $file_id,
*   @param string   $upid,
*   @param string   $ran,
*   @param string   $user,
*   @param string   $date_created,
*   @param string   $status,
*   @param string   $emp_no,
*   @param string   $sss_no,
*   @param string   $phil_no,
*   @param string   $hmo_no,
*   @param string   $suboffice,
*   @param string   $subofficecode,
*   @param string   $site,
*   @param string   $lastname,
*   @param string   $firstname,
*   @param string   $middlename,
*   @param string   $ext,
*   @param string   $dob,
*   @param string   $age,
*   @param string   $gender,
*   @param string   $maritalstatus,
*   @param string   $category,
*   @param string   $job_desc,
*   @param string   $joblevel,
*   @param string   $effectivedate,
*   @param string   $datehire,
*   @param string   $process
*
*   
*
*   
*   return boolean    
*
*
*
*
*/ 


public function DbLogs($dbname,$file_id,$upid,$ran,$user,$date_created,$status,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$age,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$datehire,$process){
	
	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

		$sql = "INSERT INTO updateupload_logs (file_id,upid,ran,user,date_created,status,emp_no,sss_no,phil_no,hmo_no,suboffice,subofficecode,site,lastname,firstname,middlename,ext,dob,age,gender,maritalstatus,category,job_desc,joblevel,effectivedate,datehire,process) VALUES (:file_id,:upid,:ran,:user,:date_created,:status,:emp_no,:sss_no,:phil_no,:hmo_no,:suboffice,:subofficecode,:site,:lastname,:firstname,:middlename,:ext,:dob,:age,:gender,:maritalstatus,:category,:job_desc,:joblevel,:effectivedate,:datehire,:process)";
											
		$q = $this->conn->prepare($sql);

		$values =  array(':file_id'=>$file_id,':upid'=>$upid,':ran'=>$ran,':user'=>$user,':date_created'=>$date_created, ':status'=>$status,':emp_no'=>$emp_no,':sss_no'=>$sss_no,':phil_no'=>$phil_no,':hmo_no'=>$hmo_no,':suboffice'=>$suboffice,':subofficecode'=>$subofficecode,':site'=>$site,':lastname'=>$lastname,':firstname'=>$firstname,':middlename'=>$middlename,':ext'=>$ext,':dob'=>$dob,':age'=>$age,':gender'=>$gender,':maritalstatus'=>$maritalstatus,':category'=>$category,':job_desc'=>$job_desc,':joblevel'=>$joblevel,':effectivedate'=>$effectivedate,':datehire'=>$datehire,':process'=>$process);
				
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	
		

	}



/*
*
*   function DbupLogs 
*       
*   it insert a data into upload_logs table
*
*   @param string   $dbname,
*   @param string   $file_id,
*   @param string   $upid,
*   @param string   $user,
*   @param string   $date_created,
*   @param string   $status,
*   @param string   $emp_no,
*   @param string   $sss_no,
*   @param string   $phil_no,
*   @param string   $hmo_no,
*   @param string   $suboffice,
*   @param string   $subofficecode,
*   @param string   $site,
*   @param string   $lastname,
*   @param string   $firstname,
*   @param string   $middlename,
*   @param string   $ext,
*   @param string   $dob,
*   @param string   $age,
*   @param string   $gender,
*   @param string   $maritalstatus,
*   @param string   $category,
*   @param string   $job_desc,
*   @param string   $joblevel,
*   @param string   $effectivedate,
*   @param string   $datehire,
*   @param string   $process
*
*   
*
*   
*   return boolean    
*
*
*
*
*/   

public function DbupLogs($dbname,$file_id,$upid,$user,$date_created,$status,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$age,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$datehire,$process){
	
	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

		$sql = "INSERT INTO upload_logs (file_id,upid,user,date_created,status,emp_no,sss_no,phil_no,hmo_no,suboffice,subofficecode,site,lastname,firstname,middlename,ext,dob,age,gender,maritalstatus,category,job_desc,joblevel,effectivedate,datehire,process) VALUES (:file_id,:upid,:user,:date_created,:status,:emp_no,:sss_no,:phil_no,:hmo_no,:suboffice,:subofficecode,:site,:lastname,:firstname,:middlename,:ext,:dob,:age,:gender,:maritalstatus,:category,:job_desc,:joblevel,:effectivedate,:datehire,:process)";
											
		$q = $this->conn->prepare($sql);

		$values =  array(':file_id'=>$file_id,':upid'=>$upid,':user'=>$user,':date_created'=>$date_created,':status'=>$status,':emp_no'=>$emp_no,':sss_no'=>$sss_no,':phil_no'=>$phil_no,':hmo_no'=>$hmo_no,':suboffice'=>$suboffice,':subofficecode'=>$subofficecode,':site'=>$site,':lastname'=>$lastname,':firstname'=>$firstname,':middlename'=>$middlename,':ext'=>$ext,':dob'=>$dob,':age'=>$age,':gender'=>$gender,':maritalstatus'=>$maritalstatus,':category'=>$category,':job_desc'=>$job_desc,':joblevel'=>$joblevel,':effectivedate'=>$effectivedate,':datehire'=>$datehire,':process'=>$process);
				
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}	
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }		
		

}


/*
*
*   function DbsvupLogs 
*       
*   it insert a data into saveupload_logs table
*
* @param string     $dbname,
* @param string     $file_id,
* @param string     $upid,
* @param string     $user,
* @param string     $date_created,
* @param string     $emp_no,
* @param string     $sss_no,
* @param string     $phil_no,
* @param string     $hmo_no,
* @param string     $suboffice,
* @param string     $subofficecode,
* @param string     $site,
* @param string     $lastname,
* @param string     $firstname,
* @param string     $middlename,
* @param string     $ext,
* @param string     $dob,
* @param string     $gender,
* @param string     $maritalstatus,
* @param string     $category,
* @param string     $job_desc,
* @param string     $joblevel,
* @param string     $effectivedate,
* @param string     $enddate,
* @param string     $datehire,
* @param string     $rule_name,
* @param string     $process,
* @param string     $idreleaseddate
*
*   
*
*   
*   return boolean    
*
*
*
*
*/   


public function DbsvupLogs($dbname,$file_id,$upid,$user,$date_created,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$enddate,$datehire,$rule_name,$process,$idreleaseddate){
	
	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

		$sql = "INSERT INTO saveupload_logs (file_id,upid,user,date_created,emp_no,sss_no,phil_no,hmo_no,suboffice,subofficecode,site,lastname,firstname,middlename,ext,dob,gender,maritalstatus,category,job_desc,joblevel,effectivedate,end_date,datehire,rule_name,process,idreleaseddate) VALUES (:file_id,:upid,:user,:date_created,:emp_no,:sss_no,:phil_no,:hmo_no,:suboffice,:subofficecode,:site,:lastname,:firstname,:middlename,:ext,:dob,:gender,:maritalstatus,:category,:job_desc,:joblevel,:effectivedate,:enddate,:datehire,:rule_name,:process,:idreleaseddate)";
											
		$q = $this->conn->prepare($sql);

		$values =  array(':file_id'=>$file_id,':upid'=>$upid,':user'=>$user,':date_created'=>$date_created,':emp_no'=>$emp_no,':sss_no'=>$sss_no,':phil_no'=>$phil_no,':hmo_no'=>$hmo_no,':suboffice'=>$suboffice,':subofficecode'=>$subofficecode,':site'=>$site,':lastname'=>$lastname,':firstname'=>$firstname,':middlename'=>$middlename,':ext'=>$ext,':dob'=>$dob,':gender'=>$gender,':maritalstatus'=>$maritalstatus,':category'=>$category,':job_desc'=>$job_desc,':joblevel'=>$joblevel,':effectivedate'=>$effectivedate,':enddate'=>$enddate,':datehire'=>$datehire,':rule_name'=>$rule_name,':process'=>$process,':idreleaseddate'=>$idreleaseddate);
				
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }			
		

}



/*
*
*   function DbuperrLogs 
*       
*   it update a data into updateupload_logs table
*
*   @param string   $dbname,
*   @param string   $upid,
*   @param string   $ran,
*   @param string   $user,
*   @param string   $status,
*   @param string   $emp_no,
*   @param string   $sss_no,
*   @param string   $phil_no,
*   @param string   $hmo_no,
*   @param string   $suboffice,
*   @param string   $subofficecode,
*   @param string   $site,
*   @param string   $lastname,
*   @param string   $firstname,
*   @param string   $middlename,
*   @param string   $ext,
*   @param string   $dob,
*   @param string   $age,
*   @param string   $gender,
*   @param string   $maritalstatus,
*   @param string   $category,
*   @param string   $job_desc,
*   @param string   $joblevel,
*   @param string   $effectivedate,
*   @param string   $datehire	
*
*   
*
*   
*   return boolean    
*
*
*
*
*/  


public function DbuperrLogs($dbname,$upid,$ran,$user,$status,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$age,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$datehire){
	
	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

		// $sql = "INSERT INTO upload_logs (file_id,upid,user,status,emp_no,sss_no,phil_no,hmo_no,suboffice,subofficecode,site,lastname,firstname,middlename,ext,dob,age,gender,maritalstatus,category,job_desc,joblevel,effectivedate,datehire) VALUES (:file_id,:upid,:user,:status,:emp_no,:sss_no,:phil_no,:hmo_no,:suboffice,:subofficecode,:site,:lastname,:firstname,:middlename,:ext,:dob,:age,:gender,:maritalstatus,:category,:job_desc,:joblevel,:effectivedate,:datehire)";
		$sql = "UPDATE updateupload_logs SET status =:status,emp_no_update = :emp_no_update,sss_no_update = :sss_no_update,phil_no_update = :phil_no_update,hmo_no_update = :hmo_no_update,suboffice_update = :suboffice_update,subofficecode_update = :subofficecode_update,site_update = :site_update,lastname_update = :lastname_update,firstname_update = :firstname_update,middlename_update = :middlename_update,ext_update = :ext_update,dob_update = :dob_update,age_update = :age_update,gender_update = :gender_update,maritalstatus_update = :maritalstatus_update,category_update = :category_update,job_desc_update = :job_desc_update,joblevel_update = :joblevel_update,effectivedate_update = :effectivedate_update,datehire_update = :datehire_update where  upid = '".$upid."' and ran = '".$ran."' and user = '".$user."'";
		
		$q = $this->conn->prepare($sql);

		$values =  array(':status'=>$status,':emp_no_update'=>$emp_no,':sss_no_update'=>$sss_no,':phil_no_update'=>$phil_no,':hmo_no_update'=>$hmo_no,':suboffice_update'=>$suboffice,':subofficecode_update'=>$subofficecode,':site_update'=>$site,':lastname_update'=>$lastname,':firstname_update'=>$firstname,':middlename_update'=>$middlename,':ext_update'=>$ext,':dob_update'=>$dob,':age_update'=>$age,':gender_update'=>$gender,':maritalstatus_update'=>$maritalstatus,':category_update'=>$category,':job_desc_update'=>$job_desc,':joblevel_update'=>$joblevel,':effectivedate_update'=>$effectivedate,':datehire_update'=>$datehire);
				
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }		
		

	}

/*
*
*   function DbAddUserquery 
*       
*   it insert a new user into USER table
*
*   @param  string  $dbname,
*   @param  string  $firstname,
*   @param  string  $lastname,
*   @param  string  $username,
*   @param  string  $password,
*   @param  string  $email,
*   @param  string  $usertype,
*   @param  string  $accountname,
*   @param  string  $position
*
*   
*
*   
*   return boolean    
*
*
*
*
*/  
	
public function DbAddUserquery($dbname,$firstname,$lastname,$username,$password,$email,$usertype,$accountname,$position){
	
	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
		// $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$dbname,$this->user,$this->pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

		$sql = "INSERT INTO USER (firstname,lastname,username,password,email,usertype,accountname,position,status) VALUES (:firstname,:lastname,:username,:password,:email,:usertype,:accountname,:position,'active')";

		
		$q = $this->conn->prepare($sql);

		$values = array(':firstname'=>$firstname,':lastname'=>$lastname,':username'=>$username,':password'=>$password,':email'=>$email,':usertype'=>$usertype,':accountname'=>$accountname,':position'=>$position);

		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	


}

	public function DbUpdateUserquery($dbname,$firstname,$lastname,$username,$email,$accountname,$position,$id){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
		// $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$dbname,$this->user,$this->pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

		$sql =  "UPDATE user set username=:username, email=:email, accountname=:accountname, firstname=:firstname, lastname=:lastname, position=:position where id=:id";
		$q = $this->conn->prepare($sql);

		$values = array(':username'=>$username, ':email'=>$email, ':accountname'=>$accountname, ':firstname'=>$firstname, ':lastname'=>$lastname,':position'=>$position,':id'=>$id);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	

}


public function DbUpdateUserPasswordquery($dbname,$password,$id){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }


		$sql =  "UPDATE user set password=:password where id=:id";
		$q = $this->conn->prepare($sql);

		$values = array(':password'=>$password,':id'=>$id);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	

}

public function InsertDraftname($dbname,$user,$file_id,$account_name,$draftname){

    try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

        $sql = "INSERT INTO draft (user,file_id,account_name,draft_name) VALUES (:user,:file_id,:account_name,:draftname)";
        $q = $this->conn->prepare($sql);
        $values = array(':user'=>$user,':file_id'=>$file_id,':account_name'=>$account_name,':draftname'=>$draftname);

                if(!$q->execute($values)){

                        $errmsg = implode(" ", $q->errorInfo());
                        $er = implode(" ", $this->conn->errorInfo());
                        $emsg = "error code  :".$errmsg." || error code  : ".$er;   

                
                        throw new Exception($emsg);

                        return false;

                }

        return true;

    }catch(Exception $e){

        $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
        catcherror_log($err);



    }   
}


}


?>