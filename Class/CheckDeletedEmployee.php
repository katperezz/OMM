<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');

include_once('DbConnection.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');


class CheckDeletedEmployee extends Dbconnection{


/*
*
*   function CheckDeletedEmployeeNum 
*       
*   it check if its deleted to the database and it return the count
*
*   @param  string  $dbname,
*   @param  string  $emp_no,
*
*   
*
*   
*   return number    
*
*
*
*
*/ 


public function CheckDeletedEmployeeNum($dbname,$emp_no){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

			$sql = "SELECT emp_no,lastname,firstname,middlename,dob FROM mastermembertable WHERE member_status = 'deleted' and emp_no = :emp_no";
			
			$q = $this->conn->prepare($sql);
			
			$values = array(':emp_no'=>$emp_no);
			
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		$data = $q->rowCount();
		return $data;


    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	

}



/*
*
*   function CheckDeletedEmployeeName 
*       
*   it check if its deleted to the database and it return the count
*
*   @param  string  $dbname,
*   @param  string  $lastname,
*   @param  string  $firstname
*   @param  string  $dob
*
*   
*
*   
*   return number    
*
*
*
*
*/ 


public function CheckDeletedEmployeeName($dbname,$lastname,$firstname,$dob){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

			$sql = "SELECT emp_no,lastname,firstname,middlename,dob FROM mastermembertable WHERE member_status = 'deleted'  and lastname = :lastname and firstname = :firstname and dob = :dob";
			
			$q = $this->conn->prepare($sql);
			
			$values = array(':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob);
			
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		$data = $q->rowCount();
		return $data;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	

}


/*
*
*   function CheckDeletedEmployeeNameAndNum 
*       
*   it check if its deleted to the database and it return the count
*
*   @param  string  $dbname,
*   @param  string  $emp_no
*   @param  string  $lastname,
*   @param  string  $firstname
*   @param  string  $dob
*
*   
*
*   
*   return number    
*
*
*
*
*/ 
	
		
public function CheckDeletedEmployeeNameAndNum($dbname,$emp_no,$lastname,$firstname,$dob){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

				$sql = "SELECT emp_no,lastname,firstname,middlename,dob FROM mastermembertable WHERE member_status = 'deleted' and emp_no = :emp_no and lastname = :lastname and firstname = :firstname and dob = :dob";
				$q = $this->conn->prepare($sql);
				$values = array(':emp_no'=>$emp_no,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob);

		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		$data = $q->rowCount();
		return $data;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	



}

/*
*
*   function CheckEmployeeDeletedExist 
*       
*   it check if its deleted to the database and it return the status
*
*   @param  string  $dbname,
*   @param  string  $emp_no
*   @param  string  $lastname,
*   @param  string  $firstname
*   @param  string  $dob
*
*   
*
*   
*   return string    
*
*
*
*
*/


public function CheckEmployeeDeletedExist($dbname,$emp_no,$lastname,$firstname,$dob){

	try{		

			  if($this->CheckDeletedEmployeeNameAndNum($dbname,$emp_no,$lastname,$firstname,$dob) > 0){


			  		$status = "Employee No.,Birthday And Name same  Deleted member";

			  }else if($this->CheckDeletedEmployeeNum($dbname,$emp_no) > 0){


			  	
			  		$status = "Employee No. same  Deleted member"; 



			  }else if($this->CheckDeletedEmployeeName($dbname,$lastname,$firstname,$dob) > 0){

			 
			  		$status = "Employee Name and Birthday same Deleted member ";



			  }else{

			  		$status = "valid";

			  }
		// }

			  if(empty($status)){
			  		            throw new Exception("empty value status");
           						
			  }

			  return $status;

	}catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	


}
			
}



?>