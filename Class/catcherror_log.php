<?php

function catcherror_log($errmsg){

		date_default_timezone_set('Asia/Manila');
		
		$date = date("F j, Y, g:i a");
		$timezone = date_default_timezone_get();

		$errlog = "[ ".$date." ".$timezone." ]"." ".$errmsg."\n";

		error_log($errlog,3,$_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/error_log/errlogs.log');

}


class InnerException extends Exception{


	public function getErrorReportMessage() {

			return $this->getMessage();
	}

	public function getErrorReportFile() {

			return $this->getFile();
	}

	public function getErrorReportLine() {

			return $this->getLine();
	}

	public function getErrorReportTraceAsString() {

			return $this->getTraceAsString();
	}





}




?>