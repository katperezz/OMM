<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');

class DbConnection {

			public $host="localhost";
			public $user="mmdb_user";
			public $pass="mmdb_password";
			public $conn;
	
		public function OpenDB($dbname){


			try{


					$this->conn = new PDO("mysql:host=".$this->host.";dbname=mmdb_".$dbname,$this->user,$this->pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
					$this->conn->query('SET NAMES utf8');
				

					if(!$this->conn){

						$er = implode(" ", $this->conn->errorInfo());
						throw new Exception(" Database Connection Error :".$er);

							
					}

					return true;

						return true;

    		}catch(Exception $e){

    			$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    			catcherror_log($err);



    		}

			
	    }



	    public function OpenDB2(){


			try{


					$this->conndb2 = new PDO("mysql:host=".$this->host.";dbname=mmdb_mmdb",$this->user,$this->pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
					$this->conndb2->query('SET NAMES utf8');
				

					if(!$this->conndb2){

						$er = implode(" ", $this->conndb2->errorInfo());
						throw new Exception(" Database Connection 2 Error :".$er);

							
					}

					return true;

						return true;

    		}catch(Exception $e){

    			$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    			catcherror_log($err);



    		}

			
	    }



		


}




?>