<?php



function dev_log($errmsg){

		date_default_timezone_set('Asia/Manila');
		
		$date = date("F j, Y, g:i a");
		$timezone = date_default_timezone_get();

		$errlog = "[ ".$date." ".$timezone." ]"." ".$errmsg."\n";

		error_log($errlog,3,"errlogs/errlogs.log");

}



?>