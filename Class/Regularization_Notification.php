<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');

date_default_timezone_set('Asia/Manila');

$date = date('Y-m-d h:i:s');


class Regularization_Notification extends Notification{
	
	public $accountname;


public function __construct($accountname){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  

	$this->accountname = $accountname;



}


/*
*
*   function Select 
*       
*   it execute an sql select query and return array
*
*   @param  string  $dbname,
*   @param  string  $sql

*   
*
*   
*   return array    
*
*
*
*
*/	




public function Select($dbname,$sql){

    try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

 
        $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        $q = $this->conn->prepare($sql);
        
                if(!$q->execute()){

                        $errmsg = implode(" ", $q->errorInfo());
                        $er = implode(" ", $this->conn->errorInfo());
                        $emsg = "error code  :".$errmsg." || error code  : ".$er;   

                
                        throw new Exception($emsg);

                        return false;

                }

        		if($q->rowCount() == 0){

        			$data = array();
        			return $data;

        		}else{

                	$data = $q->fetchAll();
                	return $data;
                }

    }catch(Exception $e){

        $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
        catcherror_log($err);



    }   


}



/*
*
*   function SQLQuery 
*       
*   it execute an sql  query 
*
*   @param  string  $dbname,
*   @param  string  $sql

*   
*
*   
*   return boolean    
*
*
*
*
*/

public function SQLQuery($dbname,$sql){

    try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
 
        $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        $q = $this->conn->prepare($sql);
        
                if(!$q->execute()){

                        $errmsg = implode(" ", $q->errorInfo());
                        $er = implode(" ", $this->conn->errorInfo());
                        $emsg = "error code  :".$errmsg." || error code  : ".$er;   

                
                        throw new Exception($emsg);

                        return false;

                }


            return true;

    }catch(Exception $e){

        $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
        catcherror_log($err);



    }       


}



/*
*
*   function EligibilityDetailArray 
*       
*   it select the eligible employee to be regular   
*
*   
*   return array    
*
*
*
*
*/




public function EligibilityDetailArray(){

	try{

		$sql = "SELECT emp_no FROM mastermembertable WHERE date_est_regularization <= NOW() AND regularization_status = 'probationary' AND member_status != 'deleted'";

		 // to store the array value into single array
		
		$arr1 = array();

		if(empty($this->Select($this->accountname,$sql))){

				$arr1 = array();

		}else{

				foreach($this->Select($this->accountname,$sql) as $r){

					$arr1[] = $r['emp_no'];

				}
		}

	

		$sql2 = "SELECT detail from notification where category = 'regularization' and seen_status = 'inc'";

		$arr2 = array();

		if(empty($this->Select($this->accountname,$sql2))){

			$arr2 = array();
		}else{

			foreach($this->Select($this->accountname,$sql2) as $r2){

				$arr2[] = $r2['detail'];
				                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
			}

		}
		// implode to convert to string then explode convert it again into array to combine multiple array 
		$arrstr = implode(",", $arr2);	
		$arrdiff = explode(",", $arrstr); 


		$arr3 = array_diff($arr1, $arrdiff);
		


		return $arr3;

    }catch(Exception $e){

        $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
        catcherror_log($err);



    }   


}


/*
*
*   function InsertRegularizationNotification 
*       
*   it insert into a notification table which is eligible to be regular
*
*
*
*/

public function InsertRegularizationNotification(){

	try{

					$date = date('Y-m-d h:i:s');

					$arr_eli = $this->EligibilityDetailArray();

					$c = count($arr_eli);

					if($c != 0){
					
						$data = implode(",", $arr_eli);	

						$description = $c." employee(s) to be regular";

										
						if(!$this->InsertNotification($this->accountname,"system",$data,$description,"regularization",$date)){

							  throw new Exception("InsertNotification return false");
						}

					}

    }catch(Exception $e){

        $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
        catcherror_log($err);



     }          





}



}




?>