<?php

class JsonResponseAccntStat {

    public $success; 
    public $data; 
    public $total;
    public $message;
    public $totalendorsed;
    public $notendorse;
    public $sla;
    public $oversla;
    public $endothisweek;
    public $regularization;
    public $forregularization;
    public $processmembers;

    public $totalinclusion;
    public $totaldeletion;
    public $totalupdate;

    public $username;
    public $accountname;
    public $fullname;
    public $email;
    public $position;




    public function __construct($params = array()) {
        $this->success          = isset($params["success"]) ? $params["success"] : false;
        $this->totalendorsed    = isset($params["totalendorsed"]) ? $params["totalendorsed"] : '';
        $this->notendorse       = isset($params["notendorse"]) ? $params["notendorse"] : '';
        $this->sla              = isset($params["sla"]) ? $params["sla"] : '';
        $this->oversla          = isset($params["oversla"]) ? $params["oversla"] : '';
        $this->regularization   = isset($params["regularization"]) ? $params["regularization"] : '';
        $this->endothisweek      = isset($params["endothisweek"]) ? $params["endothisweek"] : '';
        $this->forregularization = isset($params["forregularization"]) ? $params["forregularization"] : '';
        $this->processmembers = isset($params["processmembers"]) ? $params["processmembers"] : '';

        $this->totalinclusion      = isset($params["totalinclusion"]) ? $params["totalinclusion"] : '';
        $this->totaldeletion     = isset($params["totaldeletion"]) ? $params["totaldeletion"] : '';
        $this->totalupdate      = isset($params["totalupdate"]) ? $params["totalupdate"] : '';

        $this->username         = isset($params["username"]) ? $params["username"] : '';
        $this->accountname      = isset($params["accountname"]) ? $params["accountname"] : '';
        $this->fullname         = isset($params["fullname"]) ? $params["fullname"] : '';
        $this->email            = isset($params["email"]) ? $params["email"] : '';
        $this->position         = isset($params["position"]) ? $params["position"] : '';

        $this->message          = isset($params["message"]) ? $params["message"] : '';
        $this->total            = isset($params["total"]) ? $params["total"] : '';
        $this->data             = isset($params["data"])    ? $params["data"]    : array();
    }

    public function to_json() {
        return json_encode(array(
            'success'         => $this->success,
            'totalendorsed'   => $this->totalendorsed,
            'notendorse'      => $this->notendorse,
            'sla'             => $this->sla,
            'oversla'         => $this->oversla,
            'regularization'  => $this->regularization,
            'endothisweek'     => $this->endothisweek,
            'forregularization'     => $this->forregularization,
            'totalinclusion' => $this->totalinclusion,
            'totaldeletion' => $this->totaldeletion,
            'totalupdate' => $this->totalupdate,
            'processmembers' => $this->processmembers,

            'username'        => $this->username,
            'accountname'     => $this->accountname,
            'fullname'        => $this->fullname,
            'email'           => $this->email,
            'position'        => $this->position,

            'message'         => $this->message,
            'total'           => $this->total,
            'data'            => $this->data
        ));
    }
}



?>