<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/PHPMailer/PHPMailerAutoload.php');
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');

class SendMail {

/*
*
*   function SendMailExcel
*       
*   it use to send an email with attached file     
*    
*   @param string $emailadd,
*   @param string $subj,
*   @param string $message,
*   @param string $attch,
*   @param string $cc
*   
*
*   
*   return boolean    
*
*
*
*
*/

	public function SendMailExcel($emailadd,$subj,$message,$attch,$cc){

      try { 

        $body = str_replace('\n','<BR>',$message);


        $mail = new PHPMailer;

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Port = 587;                                    // TCP port to connect to

        if($_SERVER['REMOTE_ADDR'] == '127.0.0.1'){
        $mail->Host = 'smtp.1and1.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'test@benefitsmadebetter.com';                 // SMTP username
        $mail->Password = 'activelink12345';                           // SMTP password
  
        // $mail->SMTPSecure = '';                            // Enable TLS encryption, `ssl` also accepted
        }else{
        $mail->Host = 'benefitsmadebetter.com';// main and backup SMTP servers

        $mail->SMTPAuth = true;                               // Enable SMTP authentication

        $mail->Username = 'test@benefitsmadebetter.com';                 // SMTP username
        $mail->Password = 'activelink12345';   
                                 // SMTP password
        // $mail->SMTPSecure = '';                            // Enable TLS encryption, `ssl` also accepted            
        }

        

       
        $mail->setFrom('test@benefitsmadebetter.com', 'Admin');
        $mail->AddReplyTo('test@benefitsmadebetter.com','Admin');

        $mail->addAddress($emailadd);     // Add a recipient

        $mail->addAttachment($attch);         // Add attachments
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $subj;
        $mail->Body    = $body;

            
        if(!empty($cc)){
            
            foreach($cc as $a){

                $mail->AddCC($a);

            } 
        }

        if(!$mail->send()) {
        
        	$msg = "Mailer Error: ".$mail->ErrorInfo;
            
            throw new Exception($msg);	
            return false;

        }      	
            
        return true;

    }catch(Exception $e){

        $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
        catcherror_log($err);



    }   
           
        


       
     }



}

?>