<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once('DbConnection.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');


date_default_timezone_set('Asia/Manila');

class AccountSetup extends DbConnection {

/*
*
*   function InsertRulename 
*       
*  it insert into account_rule for a new rulename
*
* @param  string $dbname,
* @param  string $account_id,
* @param  string $rule_name,
* @param  string $effective_date,
* @param  string $end_date,
* @param  string $emp_num,
* @param  string $hmo_provider,
* @param  string $hmo_num,
* @param  string $hmo_effective_date,
* @param  string $hmo_end_date,
* @param  string $status
*   
*
*   
* return boolean    
*
*
*
*
*/
 

public function InsertRulename($dbname,$account_id,$rule_name,$effective_date,$end_date,$emp_num,$hmo_provider,$hmo_num,$hmo_effective_date,$hmo_end_date,$status){

  try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

              $qin = "INSERT INTO account_rule (account_id,rule_name,date_created,effective_date,end_date,emp_num,hmo_provider,hmo_num,hmo_effective_date,hmo_end_date,status) VALUES (:account_id,:rule_name,:date_created,:effective_date,:end_date,:emp_num,:hmo_provider,:hmo_num,:hmo_effective_date,:hmo_end_date,:status)";
                                                 
               $date_created = date('Y-m-d h:i:s');             

              $q = $this->conn->prepare($qin);
              $q_array = array(':account_id'=> $account_id,':rule_name'=> $rule_name,':date_created'=>$date_created,':effective_date'=> $effective_date,':end_date'=> $end_date,':emp_num'=> $emp_num,':hmo_provider'=> $hmo_provider,':hmo_num'=> $hmo_num,':hmo_effective_date'=> $hmo_effective_date,':hmo_end_date'=> $hmo_end_date,':status'=>$status);


                if(!$q->execute($q_array)){

                $errmsg = implode(" ", $q->errorInfo());
                $er = implode(" ", $this->conn->errorInfo());
                $emsg = "error code  :".$errmsg." || error code  : ".$er; 

                
                throw new Exception($emsg);

                return false;

               }



             return true;

  }catch(Exception $e){

      $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
      catcherror_log($err);



  } 


}


/*
*
*   function InsertAccountId 
*       
*   it insert into account_setup for a new account
*
* @param  string $dbname,
* @param  string $account_name,
* @param  string $account_id,
*
*   
*   return boolean    
*
*
*
*
*/


public function InsertAccountId($dbname,$account_name,$account_id){

  try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

              $qin = "INSERT INTO account_setup (account_name,account_id) VALUES (:account_name,:account_id)";
                          

              $q = $this->conn->prepare($qin);
              $q_array = array(':account_name'=> $account_name,':account_id'=> $account_id);

            
                if(!$q->execute($q_array)){

                $errmsg = implode(" ", $q->errorInfo());
                $er = implode(" ", $this->conn->errorInfo());
                $emsg = "error code  :".$errmsg." || error code  : ".$er; 

                
                throw new Exception($emsg);

                return false;

               }



             return true;

  }catch(Exception $e){

      $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
      catcherror_log($err);



  } 


}


/*
*
*   function CreateDb 
*       
*   it create a new database and database for logs of new account
*
* @param  string $account_name,
*
*   
*   return boolean    
*
*
*
*
*/


public function CreateDb($account_name){

  try{


                        $account_name = "mmdb_".$account_name;

                        $pdo = new PDO("mysql:host=localhost","mmdb_user","mmdb_password");
                        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                        $dbname = "`".str_replace("`","``",$account_name)."`";

                      if($pdo->query("CREATE DATABASE IF NOT EXISTS $account_name")){

                        return true;
                      }else{
                        
                            $errmsg = implode(" ", $pdo->errorInfo());

                            $emsg = "error code  :".$errmsg; 
                            throw new Exception($emsg);

                            return false;
                                  
                       }

  }catch(Exception $e){

      $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
      catcherror_log($err);



  }                    

}


/*
*
*   function DropDb 
*       
*   it Drop/Delete the database and database for logs of  account
*
* @param  string $account_name,
*
*   
*   return boolean    
*
*
*
*
*/

public function DropDb($account_name){


  try{

                        $account_name = "mmdb_".$account_name;

                        $pdo = new PDO("mysql:host=localhost","mmdb_user","mmdb_password");
                        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                        $dbname = "`".str_replace("`","``",$account_name)."`";

                      if($pdo->query("DROP DATABASE $account_name")){
                        return true;
                       }else{
                        
                            $errmsg = implode(" ", $pdo->errorInfo());
                            $emsg = "error code  :".$errmsg; 
                            throw new Exception($emsg);

                            return false;                                      
                                  
                       }

  }catch(Exception $e){

      $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
      catcherror_log($err);



  }              

}




/*
*
*   function CreateTable 
*       
*   it create a table for database that create
*
* @param  string $dbname,
*
*   
*   return boolean    
*
*
*
*
*/


 public function CreateTable($dbname){

  try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }



                                $column = "CREATE TABLE IF NOT EXISTS `columntitle` (
  `file_id` varchar(250) NULL,
  `user` varchar(250) NULL,
  `rowtitle` varchar(250) NULL,
  `upload_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_ad` varchar(250) NULL
) ;";

          

                               $master = "CREATE TABLE IF NOT EXISTS `mastermembertable` (
                                          `id` int(250) NULL AUTO_INCREMENT,
                                          `activelink_id` varchar(250) NOT NULL,
                                          `file_id` varchar(250) DEFAULT NULL,
                                          `batch_id` varchar(250) DEFAULT NULL,
                                          `emp_no` varchar(250) DEFAULT NULL,
                                          `dep_no` int(250) DEFAULT NULL,
                                          `hmo_no` varchar(250) DEFAULT NULL,
                                          `lastname` varchar(250) DEFAULT NULL,
                                          `firstname` varchar(250) DEFAULT NULL,
                                          `middlename` varchar(250) DEFAULT NULL,
                                          `ext` varchar(250) DEFAULT NULL,
                                          `gender` varchar(250) DEFAULT NULL,
                                          `dob` date DEFAULT NULL,
                                          `age` int(250) DEFAULT NULL,
                                          `maritalstatus` varchar(250) DEFAULT NULL,
                                          `category` varchar(250) DEFAULT NULL,
                                          `hmolevel` varchar(250) DEFAULT NULL,
                                          `site` varchar(250) DEFAULT NULL,
                                          `effectivedate` date DEFAULT NULL,
                                          `end_date` date DEFAULT NULL,
                                          `datehire` date DEFAULT NULL,
                                          `dateendorsed` datetime DEFAULT NULL,
                                          `idreleaseddate` date DEFAULT NULL,
                                          `date_of_deactivation` datetime DEFAULT NULL,
                                          `joblevel` varchar(250) DEFAULT NULL,
                                          `suboffice` varchar(250) DEFAULT NULL,
                                          `subofficecode` varchar(250) DEFAULT NULL,
                                          `job_desc` varchar(250) DEFAULT NULL,
                                          `emp_eligibility` varchar(250) DEFAULT NULL,
                                          `dep_eligibility` varchar(250) DEFAULT NULL,
                                          `dateemp_eligibility` date DEFAULT NULL,
                                          `datedep_eligibility` date DEFAULT NULL,
                                          `emp_rom` varchar(250) DEFAULT NULL,
                                          `dep_rom` varchar(250) DEFAULT NULL,
                                          `emp_amount` varchar(250) DEFAULT NULL,
                                          `dep_amount` varchar(250) DEFAULT NULL,
                                          `sss_no` varchar(250) DEFAULT NULL,
                                          `phil_no` varchar(250) DEFAULT NULL,
                                          `remark` varchar(250) DEFAULT NULL,
                                          `date_hmoid_upload` datetime DEFAULT NULL,
                                          `date_regularization` datetime DEFAULT NULL,
                                          `date_est_regularization` date DEFAULT NULL,
                                          `regularization_status` varchar(250) DEFAULT NULL,
                                          `rule_name` varchar(250) DEFAULT NULL,
                                          `member_status` varchar(250) DEFAULT NULL,
                                          `date_created` datetime DEFAULT NULL,
                                          PRIMARY KEY (`id`)
                                        )   
                                          ";

        
                           $tempmem = "CREATE TABLE IF NOT EXISTS `tempmembertable` (
                                        `id` int(250) NULL AUTO_INCREMENT,
                                        `file_id` varchar(250) DEFAULT NULL,
                                        `upid` varchar(250) DEFAULT NULL,
                                        `user` varchar(250) DEFAULT NULL,
                                        `date_created` datetime DEFAULT NULL,
                                        `status` varchar(250) DEFAULT NULL,
                                        `emp_no` varchar(250) DEFAULT NULL,
                                        `sss_no` varchar(250) DEFAULT NULL,
                                        `phil_no` varchar(250) DEFAULT NULL,
                                        `hmo_no` varchar(250) DEFAULT NULL,
                                        `suboffice` varchar(250) DEFAULT NULL,
                                        `subofficecode` varchar(250) DEFAULT NULL,
                                        `site` varchar(250) DEFAULT NULL,
                                        `lastname` varchar(250) DEFAULT NULL,
                                        `firstname` varchar(250) DEFAULT NULL,
                                        `middlename` varchar(250) DEFAULT NULL,
                                        `ext` varchar(250) DEFAULT NULL,
                                        `dob` date DEFAULT NULL,
                                        `age` int(250) DEFAULT NULL,
                                        `gender` varchar(250) DEFAULT NULL,
                                        `maritalstatus` varchar(250) DEFAULT NULL,
                                        `category` varchar(250) DEFAULT NULL,
                                        `job_desc` varchar(250) DEFAULT NULL,
                                        `joblevel` varchar(250) DEFAULT NULL,
                                        `effectivedate` date DEFAULT NULL,
                                        `datehire` date DEFAULT NULL,
                                        `process` varchar(250) DEFAULT NULL, 
                                        `rule_name` varchar(250) DEFAULT NULL,
                                        `idreleaseddate` date DEFAULT NULL,
                                        PRIMARY KEY (`id`)
                                      )  
                                      ";

  

                        $draft = "CREATE TABLE IF NOT EXISTS `draftmembertable` (
  `id` int(250) NULL AUTO_INCREMENT,
  `file_id` varchar(250) NULL,
  `upid` varchar(250) NULL,
  `user` varchar(250) NULL,
  `date_created` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `status` varchar(250) NULL,
  `emp_no` varchar(250) NULL,
  `sss_no` varchar(250) NULL,
  `phil_no` varchar(250) NULL,
  `hmo_no` varchar(250) NULL,
  `suboffice` varchar(250) NULL,
  `subofficecode` varchar(250) NULL,
  `site` varchar(250) NULL,
  `lastname` varchar(250) NULL,
  `firstname` varchar(250) NULL,
  `middlename` varchar(250) NULL,
  `ext` varchar(250) NULL,
  `dob` date NULL,
  `age` int(250) NULL,
  `gender` varchar(250) NULL,
  `maritalstatus` varchar(250) NULL,
  `category` varchar(250) NULL,
  `job_desc` varchar(250) NULL,
  `joblevel` varchar(250) NULL,
  `effectivedate` date NULL,
  `datehire` date NULL,
  PRIMARY KEY (`id`)
)  

                                    ";

    
                          $error_upload = "CREATE TABLE IF NOT EXISTS `error_upload` (
  `id` int(250) NULL AUTO_INCREMENT,
  `file_id` varchar(250) NULL,
  `upid` varchar(250) NULL,
  `user` varchar(250) NULL,
  `error_dateupload` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `error_status` varchar(250) NULL,
  `emp_no` varchar(250) NULL,
  `sss_no` varchar(250) NULL,
  `phil_no` varchar(250) NULL,
  `hmo_no` varchar(250) NULL,
  `suboffice` varchar(250) NULL,
  `subofficecode` varchar(250) NULL,
  `site` varchar(250) NULL,
  `lastname` varchar(250) NULL,
  `firstname` varchar(250) NULL,
  `middlename` varchar(250) NULL,
  `ext` varchar(250) NULL,
  `dob` date NULL,
  `age` int(250) NULL,
  `gender` varchar(250) NULL,
  `maritalstatus` varchar(250) NULL,
  `category` varchar(250) NULL,
  `job_desc` varchar(250) NULL,
  `joblevel` varchar(250) NULL,
  `effectivedate` date NULL,
  `datehire` date NULL,
  `remarks` varchar(200) NULL,
  PRIMARY KEY (`id`)
)";        
                        
                        $notification = "CREATE TABLE IF NOT EXISTS `notification` (
                          
                          `id` int(250)  NULL AUTO_INCREMENT,
                          `user` varchar(250)  NULL,
                          `detail` text  NULL,
                          `description` varchar(250)  NULL,
                          `category` varchar(250)  NULL,
                            `user_seen` text NULL,
                            `user_all` text NULL,
                            `seen_status` varchar(250) DEFAULT NULL,
                          `datetime` datetime(6)  NULL,
                          PRIMARY KEY (`id`)

                        )";


                        $uploadpath = "CREATE TABLE IF NOT EXISTS `uploadpath` (
                          `id` int(250) NULL AUTO_INCREMENT,
                          `file_id` varchar(250) NULL,
                          `user` varchar(250) NULL,
                          `pathvalue` varchar(250) NULL,
                          `date_created` date DEFAULT NULL,
                          `ip_ad` varchar(250) NULL,
                          PRIMARY KEY (`id`)
                        )";

                        



                                      $col = $this->conn->prepare($column);                                       
                                      $mas = $this->conn->prepare($master);                                      
                                      $temp = $this->conn->prepare($tempmem);                                      
                                      $draft = $this->conn->prepare($draft);                                      
                                      $up = $this->conn->prepare($error_upload);                                  
                                      $notif = $this->conn->prepare($notification);                                      
                                      $uppath = $this->conn->prepare($uploadpath);
                                  
                                      if(!$draft->execute()){ 

                                            $errmsg = implode(" ", $draft->errorInfo());
                                            $er = implode(" ", $this->conn->errorInfo());
                                            

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;
                                                        
                                        
                                       }

                                      if(!$notif->execute()){ 

                                            $errmsg = implode(" ", $notif->errorInfo());
                                            $er = implode(" ", $this->conn->errorInfo());
 
                                             $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;                                          
                                                        
                                        
                                       }

                                     if(!$up->execute()){

                                              $errmsg = implode(" ", $up->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 

                                     if(!$temp->execute()){

                                              $errmsg = implode(" ", $temp->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 

                                     if(!$mas->execute()){

                                              $errmsg = implode(" ", $mas->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 

                                     if(!$uppath->execute()){

                                              $errmsg = implode(" ", $uppath->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;
                                       }  

                                     if(!$col->execute()){

                                              $errmsg = implode(" ", $col->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       }  


                                      // if($uppath->execute() && $col->execute() && $mas->execute() && $temp->execute() && $draft->execute() && $up->execute() && $notif->execute()){
                                      //     return true;
                                      // }

                      return true;    

    }catch(Exception $e){

      $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
      catcherror_log($err);



    }             

}



/*
*
*   function CreateTableLogs 
*       
*   it create a table for database logs that create
*
* @param  string $dbname,
*
*   
*   return boolean    
*
*
*
*
*/


public function CreateTableLogs($dbname){


  try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

            $importfile_logs = "CREATE TABLE IF NOT EXISTS `importfile_logs` (
                                `id` int(250) NULL AUTO_INCREMENT,
                                `user` varchar(250) NULL,
                                `steps` varchar(250) NULL,
                                `date_time` date DEFAULT NULL,
                                PRIMARY KEY (`id`)
                                )";

            $upload_logs = "CREATE TABLE IF NOT EXISTS `upload_logs` (
                                `id` int(250) NULL AUTO_INCREMENT,
                                `file_id` varchar(250) DEFAULT NULL,
                                `upid` varchar(250) DEFAULT NULL,
                                `user` varchar(250) DEFAULT NULL,
                                `date_created` datetime DEFAULT NULL,
                                `status` varchar(250) DEFAULT NULL,
                                `emp_no` varchar(250) DEFAULT NULL,
                                `sss_no` varchar(250) DEFAULT NULL,
                                `phil_no` varchar(250) DEFAULT NULL,
                                `hmo_no` varchar(250) DEFAULT NULL,
                                `suboffice` varchar(250) DEFAULT NULL,
                                `subofficecode` varchar(250) DEFAULT NULL,
                                `site` varchar(250) DEFAULT NULL,
                                `lastname` varchar(250) DEFAULT NULL,
                                `firstname` varchar(250) DEFAULT NULL,
                                `middlename` varchar(250) DEFAULT NULL,
                                `ext` varchar(250) DEFAULT NULL,
                                `dob` date DEFAULT NULL,
                                `age` int(250) DEFAULT NULL,
                                `gender` varchar(250) DEFAULT NULL,
                                `maritalstatus` varchar(250) DEFAULT NULL,
                                `category` varchar(250) DEFAULT NULL,
                                `job_desc` varchar(250) DEFAULT NULL,
                                `joblevel` varchar(250) DEFAULT NULL,
                                `effectivedate` date DEFAULT NULL,
                                `idreleaseddate` date DEFAULT NULL,
                                `datehire` date DEFAULT NULL,
                                `rule_name` varchar(250) DEFAULT NULL,
                                `process` varchar(250) DEFAULT NULL,
                                PRIMARY KEY (`id`)

                              )";


          
            $saveupload_logs = "CREATE TABLE IF NOT EXISTS `saveupload_logs` (
                                  `id` int(250) NULL AUTO_INCREMENT,
                                  `file_id` varchar(250) DEFAULT NULL,
                                  `upid` varchar(250) DEFAULT NULL,
                                  `user` varchar(250) DEFAULT NULL,
                                  `date_created` datetime DEFAULT NULL,
                                  `emp_no` varchar(250) DEFAULT NULL,
                                  `sss_no` varchar(250) DEFAULT NULL,
                                  `phil_no` varchar(250) DEFAULT NULL,
                                  `hmo_no` varchar(250) DEFAULT NULL,
                                  `suboffice` varchar(250) DEFAULT NULL,
                                  `subofficecode` varchar(250) DEFAULT NULL,
                                  `site` varchar(250) DEFAULT NULL,
                                  `lastname` varchar(250) DEFAULT NULL,
                                  `firstname` varchar(250) DEFAULT NULL,
                                  `middlename` varchar(250) DEFAULT NULL,
                                  `ext` varchar(250) DEFAULT NULL,
                                  `dob` date DEFAULT NULL,
                                  `gender` varchar(250) DEFAULT NULL,
                                  `maritalstatus` varchar(250) DEFAULT NULL,
                                  `category` varchar(250) DEFAULT NULL,
                                  `job_desc` varchar(250) DEFAULT NULL,
                                  `joblevel` varchar(250) DEFAULT NULL,
                                  `effectivedate` date DEFAULT NULL,
                                  `idreleaseddate` date DEFAULT NULL,
                                  `end_date` date DEFAULT NULL,
                                  `datehire` date DEFAULT NULL,
                                   `rule_name` varchar(250) DEFAULT NULL,
                                  `process` varchar(250) DEFAULT NULL,
                                  PRIMARY KEY (`id`)

                                )";

            
            $updateupload_logs = "CREATE TABLE IF NOT EXISTS `updateupload_logs` (
                                    `id` int(250) NULL AUTO_INCREMENT,
                                    `file_id` varchar(250) DEFAULT NULL,
                                    `upid` varchar(250) DEFAULT NULL,
                                    `ran` varchar(250) DEFAULT NULL,
                                    `user` varchar(250) DEFAULT NULL,
                                    `date_created` datetime DEFAULT NULL,
                                    `status` varchar(250) DEFAULT NULL,
                                    `emp_no` varchar(250) DEFAULT NULL,
                                    `emp_no_update` varchar(250) DEFAULT NULL,
                                    `sss_no` varchar(250) DEFAULT NULL,
                                    `sss_no_update` varchar(250) DEFAULT NULL,
                                    `phil_no` varchar(250) DEFAULT NULL,
                                    `phil_no_update` varchar(250) DEFAULT NULL,
                                    `hmo_no` varchar(250) DEFAULT NULL,
                                    `hmo_no_update` varchar(250) DEFAULT NULL,
                                    `suboffice` varchar(250) DEFAULT NULL,
                                    `suboffice_update` varchar(250) DEFAULT NULL,
                                    `subofficecode` varchar(250) DEFAULT NULL,
                                    `subofficecode_update` varchar(250) DEFAULT NULL,
                                    `site` varchar(250) DEFAULT NULL,
                                    `site_update` varchar(250) DEFAULT NULL,
                                    `lastname` varchar(250) DEFAULT NULL,
                                    `lastname_update` varchar(250) DEFAULT NULL,
                                    `firstname` varchar(250) DEFAULT NULL,
                                    `firstname_update` varchar(250) DEFAULT NULL,
                                    `middlename` varchar(250) DEFAULT NULL,
                                    `middlename_update` varchar(250) DEFAULT NULL,
                                    `ext` varchar(250) DEFAULT NULL,
                                    `ext_update` varchar(250) DEFAULT NULL,
                                    `dob` date DEFAULT NULL,
                                    `dob_update` date DEFAULT NULL,
                                    `age` int(250) DEFAULT NULL,
                                    `age_update` int(250) DEFAULT NULL,
                                    `gender` varchar(250) DEFAULT NULL,
                                    `gender_update` varchar(250) DEFAULT NULL,
                                    `maritalstatus` varchar(250) DEFAULT NULL,
                                    `maritalstatus_update` varchar(250) DEFAULT NULL,
                                    `category` varchar(250) DEFAULT NULL,
                                    `category_update` varchar(250) DEFAULT NULL,
                                    `job_desc` varchar(250) DEFAULT NULL,
                                    `job_desc_update` varchar(250) DEFAULT NULL,
                                    `joblevel` varchar(250) DEFAULT NULL,
                                    `joblevel_update` varchar(250) DEFAULT NULL,
                                    `effectivedate` date DEFAULT NULL,
                                    `effectivedate_update` date DEFAULT NULL,
                                    `idreleaseddate` date DEFAULT NULL,
                                    `idreleaseddate_update` date DEFAULT NULL,
                                    `datehire` date DEFAULT NULL,
                                    `datehire_update` date DEFAULT NULL,
                                    `process` varchar(250) DEFAULT NULL,
                                    PRIMARY KEY (`id`)


                                  )";

              
            $deletion_logs="CREATE TABLE IF NOT EXISTS `deletion_logs` (
                                    `id` int(250) NULL AUTO_INCREMENT,
                                    `user` varchar(250) DEFAULT NULL,
                                    `activelink_id` varchar(250) NOT NULL,
                                    `emp_no` varchar(250) DEFAULT NULL,
                                    `dep_no` int(250) DEFAULT NULL,
                                    `hmo_no` varchar(250) DEFAULT NULL,
                                    `lastname` varchar(250) DEFAULT NULL,
                                    `firstname` varchar(250) DEFAULT NULL,
                                    `middlename` varchar(250) DEFAULT NULL,
                                    `ext` varchar(250) DEFAULT NULL,
                                    `gender` varchar(250) DEFAULT NULL,
                                    `dob` date DEFAULT NULL,
                                    `age` int(250) DEFAULT NULL,
                                    `maritalstatus` varchar(250) DEFAULT NULL,
                                    `category` varchar(250) DEFAULT NULL,
                                    `hmolevel` varchar(250) DEFAULT NULL,
                                    `site` varchar(250) DEFAULT NULL,
                                    `effectivedate` date DEFAULT NULL,
                                    `datehire` date DEFAULT NULL,
                                    `dateendorsed` datetime DEFAULT NULL,
                                    `idreleaseddate` date DEFAULT NULL,
                                    `date_of_deactivation` datetime DEFAULT NULL,
                                    `joblevel` varchar(250) DEFAULT NULL,
                                    `suboffice` varchar(250) DEFAULT NULL,
                                    `subofficecode` varchar(250) DEFAULT NULL,
                                    `job_desc` varchar(250) DEFAULT NULL,
                                    `emp_eligibility` varchar(250) DEFAULT NULL,
                                    `dep_eligibility` varchar(250) DEFAULT NULL,
                                    `dateemp_eligibility` date DEFAULT NULL,
                                    `datedep_eligibility` date DEFAULT NULL,
                                    `emp_rom` varchar(250) DEFAULT NULL,
                                    `dep_rom` varchar(250) DEFAULT NULL,
                                    `emp_amount` varchar(250) DEFAULT NULL,
                                    `dep_amount` varchar(250) DEFAULT NULL,
                                    `sss_no` varchar(250) DEFAULT NULL,
                                    `phil_no` varchar(250) DEFAULT NULL,
                                    `remark` varchar(250) DEFAULT NULL,
                                    `date_hmoid_upload` datetime DEFAULT NULL,
                                    `regularization_date` datetime DEFAULT NULL,
                                    `regularization_status` varchar(250) DEFAULT NULL,
                                    `delete_datetime` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
                                    PRIMARY KEY (`id`)
                                  )"; 
              



              $endorsement_logs = "CREATE TABLE IF NOT EXISTS `endorsement_logs` (
                                     `id` int(250) NULL AUTO_INCREMENT,
                                      `user` varchar(250) NOT NULL,
                                      `file_id` varchar(250) DEFAULT NULL,
                                      `activelink_id` varchar(250) NOT NULL,
                                      `emp_no` varchar(250) DEFAULT NULL,
                                      `dep_no` int(250) DEFAULT NULL,
                                      `hmo_no` varchar(250) DEFAULT NULL,
                                      `lastname` varchar(250) DEFAULT NULL,
                                      `firstname` varchar(250) DEFAULT NULL,
                                      `middlename` varchar(250) DEFAULT NULL,
                                      `ext` varchar(250) DEFAULT NULL,
                                      `gender` varchar(250) DEFAULT NULL,
                                      `dob` date DEFAULT NULL,
                                      `age` int(250) DEFAULT NULL,
                                      `maritalstatus` varchar(250) DEFAULT NULL,
                                      `category` varchar(250) DEFAULT NULL,
                                      `position` varchar(250) DEFAULT NULL,
                                      `hmolevel` varchar(250) DEFAULT NULL,
                                      `site` varchar(250) DEFAULT NULL,
                                      `effectivedate` date DEFAULT NULL,
                                      `datehire` date DEFAULT NULL,
                                      `dateendorsed` datetime DEFAULT NULL,
                                      `idreleaseddate` date DEFAULT NULL,
                                      `date_of_deactivation` datetime DEFAULT NULL,
                                      `joblevel` varchar(250) DEFAULT NULL,
                                      `suboffice` varchar(250) DEFAULT NULL,
                                      `subofficecode` varchar(250) DEFAULT NULL,
                                      `account` varchar(250) DEFAULT NULL,
                                      `job_desc` varchar(250) DEFAULT NULL,
                                      `hmo_coverage` varchar(250) DEFAULT NULL,
                                      `emp_eligibility` varchar(250) DEFAULT NULL,
                                      `dep_eligibility` varchar(250) DEFAULT NULL,
                                      `dateemp_eligibility` date DEFAULT NULL,
                                      `datedep_eligibility` date DEFAULT NULL,
                                      `emp_rom` varchar(250) DEFAULT NULL,
                                      `dep_rom` varchar(250) DEFAULT NULL,
                                      `emp_amount` varchar(250) DEFAULT NULL,
                                      `dep_amount` varchar(250) DEFAULT NULL,
                                      `sss_no` varchar(250) DEFAULT NULL,
                                      `phil_no` varchar(250) DEFAULT NULL,
                                      `remark` varchar(250) DEFAULT NULL,
                                      `date_created` datetime DEFAULT NULL,
                                      `date_hmoid_upload` datetime DEFAULT NULL,
                                      `date_regularization` datetime DEFAULT NULL,
                                      `regularization_status` varchar(250) DEFAULT NULL,
                                      `endorsement_datetime` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
                                        PRIMARY KEY (`id`)
                                    )";


              $regularization_logs = "CREATE TABLE IF NOT EXISTS `regularization_logs` (
                                       `id` int(250) NULL AUTO_INCREMENT,
                                        `user` varchar(250) NOT NULL,
                                        `activelink_id` varchar(250) NOT NULL,
                                        `emp_no` varchar(250) NOT NULL,
                                        `sss_no` varchar(250) NOT NULL,
                                        `phil_no` varchar(250) NOT NULL,
                                        `lastname` varchar(250) NOT NULL,
                                        `firstname` varchar(250) NOT NULL,
                                        `middlename` varchar(250) NOT NULL,
                                        `ext` varchar(250) NOT NULL,
                                        `gender` varchar(250) NOT NULL,
                                        `dob` date NOT NULL,
                                        `maritalstatus` varchar(250) NOT NULL,
                                        `datetime_log` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
                                            PRIMARY KEY (`id`)
                                      )";




$force_endorsed_logs = "CREATE TABLE IF NOT EXISTS `force_endorsed_logs` (
                        `id` int(250) NULL AUTO_INCREMENT,
                        `user` varchar(250) DEFAULT NULL,
                        `activelink_id` varchar(250) NOT NULL,  
                        `emp_no` varchar(250) DEFAULT NULL,
                        `dep_no` int(250) DEFAULT NULL,
                        `hmo_no` varchar(250) DEFAULT NULL,
                        `lastname` varchar(250) DEFAULT NULL,
                        `firstname` varchar(250) DEFAULT NULL,
                        `middlename` varchar(250) DEFAULT NULL,
                        `ext` varchar(250) DEFAULT NULL,
                        `gender` varchar(250) DEFAULT NULL,
                        `dob` date DEFAULT NULL,
                        `age` int(250) DEFAULT NULL,
                        `maritalstatus` varchar(250) DEFAULT NULL,
                        `category` varchar(250) DEFAULT NULL,
                        `hmolevel` varchar(250) DEFAULT NULL,
                        `site` varchar(250) DEFAULT NULL,
                        `effectivedate` date DEFAULT NULL,
                        `datehire` date DEFAULT NULL,
                        `idreleaseddate` date DEFAULT NULL,
                        `date_of_deactivation` datetime DEFAULT NULL,
                        `joblevel` varchar(250) DEFAULT NULL,
                        `suboffice` varchar(250) DEFAULT NULL,
                        `subofficecode` varchar(250) DEFAULT NULL,
                        `job_desc` varchar(250) DEFAULT NULL,
                        `emp_eligibility` varchar(250) DEFAULT NULL,
                        `dep_eligibility` varchar(250) DEFAULT NULL,
                        `dateemp_eligibility` date DEFAULT NULL,
                        `datedep_eligibility` date DEFAULT NULL,
                        `emp_rom` varchar(250) DEFAULT NULL,
                        `dep_rom` varchar(250) DEFAULT NULL,
                        `emp_amount` varchar(250) DEFAULT NULL,
                        `dep_amount` varchar(250) DEFAULT NULL,
                        `sss_no` varchar(250) DEFAULT NULL,
                        `phil_no` varchar(250) DEFAULT NULL,
                        `remark` varchar(250) DEFAULT NULL,
                        `date_created` datetime DEFAULT NULL,
                        `date_hmoid_upload` datetime DEFAULT NULL,
                        `date_regularization` datetime DEFAULT NULL,
                        `date_est_regularization` datetime DEFAULT NULL,
                        `regularization_status` varchar(250) DEFAULT NULL,
                        `rule_name` varchar(250) DEFAULT NULL,
                        `force_endorsed_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        PRIMARY KEY (`id`)
                      )   
                                          ";






                               $force_regular_logs = "CREATE TABLE IF NOT EXISTS `force_regular_logs` (
                                                      `id` int(250) NULL AUTO_INCREMENT,
                                                      `user` varchar(250) DEFAULT NULL,
                                                      `activelink_id` varchar(250) NOT NULL,
                                                      `emp_no` varchar(250) DEFAULT NULL,
                                                      `dep_no` int(250) DEFAULT NULL,
                                                      `hmo_no` varchar(250) DEFAULT NULL,
                                                      `lastname` varchar(250) DEFAULT NULL,
                                                      `firstname` varchar(250) DEFAULT NULL,
                                                      `middlename` varchar(250) DEFAULT NULL,
                                                      `ext` varchar(250) DEFAULT NULL,
                                                      `gender` varchar(250) DEFAULT NULL,
                                                      `dob` date DEFAULT NULL,
                                                      `age` int(250) DEFAULT NULL,
                                                      `maritalstatus` varchar(250) DEFAULT NULL,
                                                      `category` varchar(250) DEFAULT NULL,
                                                      `hmolevel` varchar(250) DEFAULT NULL,
                                                      `site` varchar(250) DEFAULT NULL,
                                                      `effectivedate` date DEFAULT NULL,
                                                      `datehire` date DEFAULT NULL,
                                                      `dateendorsed` datetime DEFAULT NULL,
                                                      `idreleaseddate` date DEFAULT NULL,
                                                      `date_of_deactivation` datetime DEFAULT NULL,
                                                      `joblevel` varchar(250) DEFAULT NULL,
                                                      `suboffice` varchar(250) DEFAULT NULL,
                                                      `subofficecode` varchar(250) DEFAULT NULL,
                                                      `job_desc` varchar(250) DEFAULT NULL,
                                                      `emp_eligibility` varchar(250) DEFAULT NULL,
                                                      `dep_eligibility` varchar(250) DEFAULT NULL,
                                                      `dateemp_eligibility` date DEFAULT NULL,
                                                      `datedep_eligibility` date DEFAULT NULL,
                                                      `emp_rom` varchar(250) DEFAULT NULL,
                                                      `dep_rom` varchar(250) DEFAULT NULL,
                                                      `emp_amount` varchar(250) DEFAULT NULL,
                                                      `dep_amount` varchar(250) DEFAULT NULL,
                                                      `sss_no` varchar(250) DEFAULT NULL,
                                                      `phil_no` varchar(250) DEFAULT NULL,
                                                      `remark` varchar(250) DEFAULT NULL,
                                                      `date_est_regularization` date DEFAULT NULL,
                                                      `rule_name` varchar(250) DEFAULT NULL,
                                                      `force_regular_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                                      PRIMARY KEY (`id`)
                                                    )   
                                          ";


$deletedtoactive_logs = "CREATE TABLE IF NOT EXISTS `deletedtoactive_logs` (
`id` int(250) NULL AUTO_INCREMENT,
  `user` varchar(250) NOT NULL,
  `file_id` varchar(250) NOT NULL,
  `activelink_id` varchar(250) NOT NULL,
  `emp_no` varchar(250) DEFAULT NULL,
  `dep_no` int(250) DEFAULT NULL,
  `hmo_no` varchar(250) DEFAULT NULL,
  `lastname` varchar(250) DEFAULT NULL,
  `firstname` varchar(250) DEFAULT NULL,
  `middlename` varchar(250) DEFAULT NULL,
  `ext` varchar(250) DEFAULT NULL,
  `gender` varchar(250) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `maritalstatus` varchar(250) DEFAULT NULL,
  `category` varchar(250) DEFAULT NULL,
  `hmolevel` varchar(250) DEFAULT NULL,
  `site` varchar(250) DEFAULT NULL,
  `effectivedate` date DEFAULT NULL,
  `datehire` date DEFAULT NULL,
  `dateendorsed` datetime DEFAULT NULL,
  `idreleaseddate` date DEFAULT NULL,
  `joblevel` varchar(250) DEFAULT NULL,
  `suboffice` varchar(250) DEFAULT NULL,
  `subofficecode` varchar(250) DEFAULT NULL,
  `job_desc` varchar(250) DEFAULT NULL,
  `emp_eligibility` varchar(250) DEFAULT NULL,
  `dep_eligibility` varchar(250) DEFAULT NULL,
  `dateemp_eligibility` date DEFAULT NULL,
  `datedep_eligibility` date DEFAULT NULL,
  `emp_rom` varchar(250) DEFAULT NULL,
  `dep_rom` varchar(250) DEFAULT NULL,
  `emp_amount` varchar(250) DEFAULT NULL,
  `dep_amount` varchar(250) DEFAULT NULL,
  `sss_no` varchar(250) DEFAULT NULL,
  `phil_no` varchar(250) DEFAULT NULL,
  `rule_name` varchar(250) DEFAULT NULL,
  `dep_no_update` int(250) DEFAULT NULL,
  `hmo_no_update` int(250) DEFAULT NULL,
  `lastname_update` varchar(250) DEFAULT NULL,
  `firstname_update` varchar(250) DEFAULT NULL,
  `middlename_update` varchar(250) DEFAULT NULL,
  `ext_update` varchar(250) DEFAULT NULL,
  `gender_update` varchar(250) DEFAULT NULL,
  `dob_update` date DEFAULT NULL,
  `maritalstatus_update` varchar(250) DEFAULT NULL,
  `category_update` varchar(250) DEFAULT NULL,
  `hmolevel_update` varchar(250) DEFAULT NULL,
  `site_update` varchar(250) DEFAULT NULL,
  `effectivedate_update` date DEFAULT NULL,
  `datehire_update` date DEFAULT NULL,
  `dateendorsed_update` datetime DEFAULT NULL,
  `idreleaseddate_update` date DEFAULT NULL,
  `joblevel_update` varchar(250) DEFAULT NULL,
  `suboffice_update` varchar(250) DEFAULT NULL,
  `subofficecode_update` varchar(250) DEFAULT NULL,
  `job_desc_update` varchar(250) DEFAULT NULL,
  `emp_eligibility_update` varchar(250) DEFAULT NULL,
  `dep_eligibility_update` varchar(250) DEFAULT NULL,
  `dateemp_eligibility_update` date DEFAULT NULL,
  `datedep_eligibility_update` date DEFAULT NULL,
  `emp_rom_update` varchar(250) DEFAULT NULL,
  `dep_rom_update` varchar(250) DEFAULT NULL,
  `emp_amount_update` varchar(250) DEFAULT NULL,
  `dep_amount_update` varchar(250) DEFAULT NULL,
  `rule_name_update` varchar(250) DEFAULT NULL,
  `datetime_logs` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
)"; 


$renewal_logs = "CREATE TABLE IF NOT EXISTS `renewal_logs` (
`id` int(250)  NULL AUTO_INCREMENT,
  `file_id` varchar(250) DEFAULT NULL,
  `user` varchar(250) DEFAULT NULL,
  `activelink_id` varchar(250) NOT NULL,
  `emp_no` varchar(250) DEFAULT NULL,
  `hmo_no` varchar(250) DEFAULT NULL,
  `suboffice` varchar(250) DEFAULT NULL,
  `subofficecode` varchar(250) DEFAULT NULL,
  `site` varchar(250) DEFAULT NULL,
  `lastname` varchar(250) DEFAULT NULL,
  `firstname` varchar(250) DEFAULT NULL,
  `middlename` varchar(250) DEFAULT NULL,
  `ext` varchar(250) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(250) DEFAULT NULL,
  `maritalstatus` varchar(250) DEFAULT NULL,
  `category` varchar(250) DEFAULT NULL,
  `job_desc` varchar(250) DEFAULT NULL,
  `joblevel` varchar(250) DEFAULT NULL,
  `effectivedate` date DEFAULT NULL,
  `datehire` date DEFAULT NULL,
  `rule_name` varchar(250) DEFAULT NULL,
  `datetime_logs` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
)";



$sendfile_logs = "CREATE TABLE IF NOT EXISTS `sendfile_logs` (
`id` int(250)  NULL AUTO_INCREMENT,
  `user` varchar(250)  NULL,
  `detail` text  NULL,
  `description` varchar(250)  NULL,
  `category` varchar(250)  NULL,
  `send_to` varchar(250)  NULL,
  `remark` varchar(250)  NULL,
  `datetime` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),

  PRIMARY KEY (`id`)
)";




                                          
                                      $im = $this->conn->prepare($importfile_logs);                                       
                                      $up = $this->conn->prepare($upload_logs);
                                      $svup = $this->conn->prepare($saveupload_logs); 
                                      $upd = $this->conn->prepare($updateupload_logs);
                                      $del = $this->conn->prepare($deletion_logs);
                                      $endorselogs = $this->conn->prepare($endorsement_logs);
                                      $regulalogs =  $this->conn->prepare($regularization_logs);

                                      $freg = $this->conn->prepare($force_regular_logs); 
                                      $fen = $this->conn->prepare($force_endorsed_logs); 
                                      $renew = $this->conn->prepare($renewal_logs); 
                                  
                                      $dltact =  $this->conn->prepare($deletedtoactive_logs);

                                      $sendf = $this->conn->prepare($sendfile_logs);

                                      if(!$renew->execute()){ 

                                            $errmsg = implode(" ", $renew->errorInfo());
                                            $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;
                                                        
                                        
                                       }

                                       if(!$freg->execute()){

                                              $errmsg = implode(" ", $freg->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 


                                       if(!$fen->execute()){

                                              $errmsg = implode(" ", $fen->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 



                                       if(!$im->execute()){

                                              $errmsg = implode(" ", $im->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 



                                       if(!$up->execute()){

                                              $errmsg = implode(" ", $up->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 



                                       if(!$svup->execute()){

                                              $errmsg = implode(" ", $svup->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 



                                       if(!$upd->execute()){

                                              $errmsg = implode(" ", $upd->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 



                                       if(!$del->execute()){

                                              $errmsg = implode(" ", $del->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 



                                       if(!$endorselogs->execute()){

                                              $errmsg = implode(" ", $endorselogs->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 



                                       if(!$regulalogs->execute()){

                                              $errmsg = implode(" ", $regulalogs->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 

                                      if(!$dltact->execute()){

                                              $errmsg = implode(" ", $dltact->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 

                                       
                                      if(!$sendf->execute()){

                                              $errmsg = implode(" ", $dltact->errorInfo());
                                              $er = implode(" ", $this->conn->errorInfo());

                                            $emsg = "error code  :".$errmsg." || error code  : ".$er;                 
                                            throw new Exception($emsg);
                                            return false;

                                       } 



                      return true;    

    }catch(Exception $e){

      $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
      catcherror_log($err);



    }  
  }


  }

?>


