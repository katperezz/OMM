<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once('DbConnection.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');

class Logs extends DbConnection{
	


	public function __construct(){

			$this->dbq = new DbQuery();	

	}


	


public function LogsUp($dbname1,$dbname2,$user,$file_id){
			
try{

		$sql = "SELECT upid,status,emp_no,sss_no,phil_no,hmo_no,suboffice,subofficecode,site,lastname,firstname,middlename,ext,dob,age,gender,maritalstatus,category,job_desc,joblevel,effectivedate,datehire FROM tempmembertable WHERE file_id = '".$file_id."' AND USER= '".$user."'";  
		
			
			foreach($this->dbq->DbSelect($dbname1,$sql) as $row){

				
				$upids = $row['upid'];				
				$status = $row['status'];
				$emp_no = $row['emp_no'];
				$sss_no = $row['sss_no'];
				$phil_no = $row['phil_no'];
				$hmo_no = $row['hmo_no'];	
				$suboffice = $row['suboffice'];
				$subofficecode= $row['subofficecode'];
				$site = $row['site'];
				$lastname = $row['lastname'];
				$firstname = $row['firstname'];
				$middlename = $row['middlename'];
				$ext = $row['ext'];
				$dob = $row['dob'];
				$age = $row['age'];
				$gender = $row['gender'];
				$maritalstatus = $row['maritalstatus'];
				$category = $row['category'];
				$job_desc = $row['job_desc'];
				$joblevel = $row['joblevel'];
				$effectivedate = $row['effectivedate'];
				$datehire = $row['datehire'];


				if(!$this->dbq->DbupLogs($dbname2,$file_id,$upids,$user,$status,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$age,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$datehire)){
	
					    throw new Exception("erro DbupLogs return false");

                		

				}	
				

			}

		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }


	}



public function LogsUpderr($dbname1,$dbname2,$user,$upid,$ran,$process){

	try{
			
		$sql = "SELECT file_id,upid,user,date_created,status,emp_no,sss_no,phil_no,hmo_no,suboffice,subofficecode,site,lastname,firstname,middlename,ext,dob,age,gender,maritalstatus,category,job_desc,joblevel,effectivedate,datehire FROM tempmembertable WHERE upid = '".$upid."' AND USER= '".$user."'";  
		
			$i=0;

			foreach($this->dbq->DbSelect($dbname1,$sql) as $row){

				$file_ids = $row['file_id'];
				$upids = $row['upid'];
				$users= $row['user'];
				$date_created= $row['date_created'];
				$status = $row['status'];
				$emp_no = $row['emp_no'];
				$sss_no = $row['sss_no'];
				$phil_no = $row['phil_no'];
				$hmo_no = $row['hmo_no'];	
				$suboffice = $row['suboffice'];
				$subofficecode= $row['subofficecode'];
				$site = $row['site'];
				$lastname = $row['lastname'];
				$firstname = $row['firstname'];
				$middlename = $row['middlename'];
				$ext = $row['ext'];
				$dob = $row['dob'];
				$age = $row['age'];
				$gender = $row['gender'];
				$maritalstatus = $row['maritalstatus'];
				$category = $row['category'];
				$job_desc = $row['job_desc'];
				$joblevel = $row['joblevel'];
				$effectivedate = $row['effectivedate'];
				$datehire = $row['datehire'];


				if(!$this->dbq->DbLogs($dbname2,$file_ids,$upid,$ran,$user,$date_created,$status,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$age,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$datehire,$process)){
				    throw new Exception("erro DbupLogs return false");

                	
				}	
				
				$i++;

			}

		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }

}

public function UpdupLogs($dbname,$user,$fileid,$status,$emp_no,$sss_no,$phil_no,$lastname,$firstname,$dob){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

			if($status == "e1"){

				$sql = "UPDATE tempmembertable SET status = :status where emp_no = :emp_no and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no";
				$values =  array(':status'=> $s,':emp_no'=>$emp_no,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "e2"){

				$sql = "UPDATE tempmembertable SET status = :status where lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry employee name";
				$values =  array(':status'=> $s,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "e3"){

				$sql = "UPDATE tempmembertable SET status = :status where emp_no = :emp_no and lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no and employee name";
				$values =  array(':status'=> $s,':emp_no'=>$emp_no,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
			}

			if($status == "s1"){

				$sql = "UPDATE tempmembertable SET status = :status where sss_no = :sss_no and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no";
				$values =  array(':status'=> $s,':emp_no'=>$sss_no,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "s2"){

				$sql = "UPDATE tempmembertable SET status = :status where lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry employee name";
				$values =  array(':status'=> $s,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "s3"){

				$sql = "UPDATE tempmembertable SET status = :status where sss_no = :sss_no and lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no and employee name";
				$values =  array(':status'=> $s,':emp_no'=>$sss_no,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
			}

			if($status == "p1"){

				$sql = "UPDATE tempmembertable SET status = :status where phil_no = :phil_no and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no";
				$values =  array(':status'=> $s,':emp_no'=>$phil_no,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "p2"){

				$sql = "UPDATE tempmembertable SET status = :status where lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry employee name";
				$values =  array(':status'=> $s,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}


			}elseif($status == "p3"){

				$sql = "UPDATE tempmembertable SET status = :status where phil_no = :phil_no and lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no and employee name";
				$values =  array(':status'=> $s,':emp_no'=>$phil_no,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		        
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
			}


		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	
 }

}



?>