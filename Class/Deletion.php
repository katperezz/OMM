<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
// include('DbConnection.php');
// include('dev_log.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');
	include_once('DbConnection.php');

class Deletion extends DbConnection{



/*
*
*   function UpdateEmployeeDeleted 
*       
*   update employee to be deleted into mastermembertable table 
*
*   @param  string $dbname,
*   @param  string $file_id,
*   @param  string $user,
*
*
*   
*   return boolean    
*
*
*
*
*/


public function UpdateEmployeeDeleted($dbname,$file_id,$user){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

					$dnow = date('Y-m-d H:i:s');

					$sql = "UPDATE mastermembertable SET regularization_status = 'deleted',member_status = 'deleted', date_of_deactivation='".$dnow."' WHERE emp_no IN (SELECT emp_no FROM tempmembertable WHERE status  LIKE '%valid%' and file_id = :file_id and user= :user) ";
					$q = $this->conn->prepare($sql);

					$values = array(':file_id'=>$file_id,':user'=>$user);		
					
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	

		}



/*
*
*   function SelectInsertDeleteValueEmp_no 
*       
*    
*
*   @param  string  $dbname,
*   @param  string  $file_id,
*   @param  string  $upid,
*   @param  string  $user,
*   @param  string  $status,
*   @param  string  $emp_no,
*   @param  string  $lastname,
*   @param  string  $firstname,
*   @param  string  $middlename,
*   @param  string  $ext,
*   @param  string  $dob,
*   @param  string  $process
*
*   
*   return boolean    
*
*
*
*
*/        


		public function SelectInsertDeleteValueEmp_no($dbname,$file_id,$upid,$user,$status,$emp_no,$lastname,$firstname,$middlename,$ext,$dob,$process){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }



					$sql = "INSERT INTO tempmembertable (file_id,upid,user,status,emp_no,sss_no,phil_no,hmo_no,suboffice,subofficecode,site,lastname,firstname,middlename,ext,dob,gender,maritalstatus,category,job_desc,joblevel,effectivedate,datehire,process) 
												  SELECT :file_id,:upid,:user,:status,emp_no,sss_no,phil_no,hmo_no,suboffice,subofficecode,site,lastname,firstname,middlename,ext,dob,gender,maritalstatus,category,job_desc,joblevel,effectivedate,datehire,:process FROM mastermembertable WHERE emp_no = :emp_no and lastname = :lastname and firstname = :firstname and middlename = :middlename and ext = :ext and dob = :dob";

					$q = $this->conn->prepare($sql);
					$values = array(':file_id'=>$file_id,':upid'=>$upid,':user'=>$user,':status'=>$status,':emp_no'=>$emp_no,':lastname'=>$lastname,':firstname'=>$firstname,':middlename'=>$middlename,':ext'=>$ext,':dob'=>$dob,':process'=>$process);
		       
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }
											


		}


/*
*
*   function InsertDeleteValueEmp_no 
*       
*   insert into tempmembertable table deleted employee
*
*   @param  string  $dbname,
*   @param  string  $file_id,
*   @param  string  $upid,
*   @param  string  $user,
*   @param  string  $status,
*   @param  string  $emp_no,
*   @param  string  $lastname,
*   @param  string  $firstname,
*   @param  string  $middlename,
*   @param  string  $ext,
*   @param  string  $dob,
*   @param  string  $process   
*
*   
*   return boolean    
*
*
*
*
*/        

public function InsertDeleteValueEmp_no($dbname,$file_id,$upid,$user,$status,$emp_no,$lastname,$firstname,$middlename,$ext,$dob,$process){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }


					$sql = "INSERT INTO tempmembertable (file_id,upid,user,status,emp_no,lastname,firstname,middlename,ext,dob,process) VALUES(:file_id,:upid,:user,:status,:emp_no,:lastname,:firstname,:middlename,:ext,:dob,:process)";

					$q = $this->conn->prepare($sql);
					$values = array(':file_id'=>$file_id,':upid'=>$upid,':user'=>$user,':status'=>$status,':emp_no'=>$emp_no,':lastname'=>$lastname,':firstname'=>$firstname,':middlename'=>$middlename,':ext'=>$ext,':dob'=>$dob,':process'=>$process);

		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }			


		}


/*
*
*   function DeletionLogs 
*       
*   
*
*   @param  string  $dbname,
*   @param  string  $file_id,
*   @param  string  $user,
  
*
*   
*   return boolean    
*
*
*
*
*/ 


public function DeletionLogs ($dbname,$file_id,$user){

					$date_created = date('Y-m-d');

	try{

        if(!$this->OpenDB($dbname."_logs")){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }	

					$sql = "INSERT INTO deletion_logs (user,activelink_id,emp_no,sss_no,phil_no,hmo_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus,category,hmolevel,site,effectivedate,datehire,dateendorsed,idreleaseddate,joblevel,suboffice,subofficecode,job_desc,emp_eligibility,dep_eligibility,dateemp_eligibility,datedep_eligibility,emp_rom,dep_rom,emp_amount,dep_amount,remark,date_hmoid_upload,regularization_status,regularization_date)
 							SELECT '".$user."',activelink_id,emp_no,sss_no,phil_no,hmo_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus,category,hmolevel,site,effectivedate,datehire,dateendorsed,idreleaseddate,joblevel,suboffice,subofficecode,job_desc,emp_eligibility,dep_eligibility,dateemp_eligibility,datedep_eligibility,emp_rom,dep_rom,emp_amount,dep_amount,remark,date_hmoid_upload,regularization_status,date_regularization FROM mmdb_".$dbname.".mastermembertable WHERE emp_no IN ( 
 									SELECT emp_no FROM mmdb_".$dbname.".tempmembertable WHERE status  LIKE '%valid%' and file_id = :file_id and user= :user)";


 					$q = $this->conn->prepare($sql);
					$values = array(':file_id'=>$file_id,':user'=>$user);

		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }				

}


	// 	public function CheckDuplicateEntry($dbname,$file_id,$emp_no,$lname,$fname,$dob){


	// try{

 //        if(!$this->OpenDB($dbname."_logs")){

 //            throw new Exception("OPEN DB ERROR!");
 //            return false;
 //        }

	// 				$sql1 = "SELECT id from tempmembertable where emp_no = '".$emp_no."' and lastname = '".$lname."' and firstname = '".$fname."' and dob = '".$dob."' and file_id = '".$file_id."'";
	// 				$sql2 = "SELECT id from tempmembertable where emp_no = '".$emp_no."' and file_id = '".$file_id."' ";
	// 				$sql3 = "SELECT id from tempmembertable where lastname = '".$lname."' and firstname = '".$fname."' and dob = '".$dob."' and file_id = '".$file_id."'";
					
	// 				$q1 = $this->conn->prepare($sql1);
	// 				$q2 = $this->conn->prepare($sql2);
	// 				$q3 = $this->conn->prepare($sql3);

	// 				$q1->execute();
	// 				$q2->execute();
	// 				$q3->execute();

	// 				$qc1 = $q1->rowCount();
	// 				$qc2 = $q2->rowCount();
	// 				$qc3 = $q3->rowCount();


	// 				if($sql1 > 1){

	// 						$u1 = "UPDATE tempmembertable SET status = 'DUPLICATE ENTRY SAME EMPLOYEE NO. AND NAME' where emp_no = '".$emp_no."' and lastname = '".$lname."' and firstname = '".$fname."' and dob = '".$dob."' and file_id = '".$file_id."' ";
	// 						$qu1 = $this->conn->prepare($u1);

	// 						// if(!$qu1->execute()){	
		
	// 				 	//  			$errmsg = implode(" ", $qu1->errorInfo());
 //       //                   			$er = implode(" ", $this->conn->errorInfo());
 //       //                   			dev_log($errmsg."//".$er);
                         
	// 						// 		return false;

	// 						// }

	// 						if(!$qu1->execute()){

 //                					$errmsg = implode(" ", $qu1->errorInfo());
 //                					$er = implode(" ", $this->conn->errorInfo());
 //                					$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
 //                					throw new Exception($emsg);

 //                					return false;

 //       						}


	// 				}else if($sql2 > 1){

	// 						$u2 = "UPDATE tempmembertable SET status = 'DUPLICATE ENTRY SAME EMPLOYEE NO.' where emp_no = '".$emp_no."' ";
	// 						$qu2 = $this->conn->prepare($u2);


	// 						if(!$qu2->execute()){

 //                					$errmsg = implode(" ", $qu2->errorInfo());
 //                					$er = implode(" ", $this->conn->errorInfo());
 //                					$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
 //                					throw new Exception($emsg);

 //                					return false;

 //       						}


	// 				}else if($sql3 > 1){

	// 						$u3 = "UPDATE tempmembertable SET status = 'DUPLICATE ENTRY SAME EMPLOYEE NAME' where lastname = '".$lname."' and firstname = '".$fname."' and dob = '".$dob."' and file_id = '".$file_id."' ";
	// 						$qu3 = $this->conn->prepare($u3);


	// 						if(!$qu3->execute()){

 //                					$errmsg = implode(" ", $qu3->errorInfo());
 //                					$er = implode(" ", $this->conn->errorInfo());
 //                					$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
 //                					throw new Exception($emsg);

 //                					return false;

 //       						}


	// 				}
					
	// 	return true;

 //    }catch(Exception $e){

 //    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
 //    	catcherror_log($err);



 //    }	



	// 	}		


public function DeleteEmployee($dbname,$emp_no){

    try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

                    $dnow = date('Y-m-d H:i:s');

                    $sql = "UPDATE mastermembertable SET regularization_status = 'deleted',member_status = 'deleted', date_of_deactivation='".$dnow."' WHERE emp_no = :emp_no";
                    $q = $this->conn->prepare($sql);

                    $values = array(':emp_no'=>$emp_no);       
                    
                if(!$q->execute($values)){

                        $errmsg = implode(" ", $q->errorInfo());
                        $er = implode(" ", $this->conn->errorInfo());
                        $emsg = "error code  :".$errmsg." || error code  : ".$er;   

                
                        throw new Exception($emsg);

                        return false;

                }
        
        return true;

    }catch(Exception $e){

        $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
        catcherror_log($err);



    }   

        }



 public function DeleteEmployeeLogs ($dbname,$emp_no,$user){

                    $date_created = date('Y-m-d');

    try{

        if(!$this->OpenDB($dbname."_logs")){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }   

               $sql = "INSERT INTO deletion_logs (user,activelink_id,emp_no,sss_no,phil_no,hmo_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus,category,hmolevel,site,effectivedate,datehire,dateendorsed,idreleaseddate,joblevel,suboffice,subofficecode,job_desc,emp_eligibility,dep_eligibility,dateemp_eligibility,datedep_eligibility,emp_rom,dep_rom,emp_amount,dep_amount,remark,date_hmoid_upload,regularization_status,regularization_date)
                                        SELECT :user,activelink_id,emp_no,sss_no,phil_no,hmo_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus,category,hmolevel,site,effectivedate,datehire,dateendorsed,idreleaseddate,joblevel,suboffice,subofficecode,job_desc,emp_eligibility,dep_eligibility,dateemp_eligibility,datedep_eligibility,emp_rom,dep_rom,emp_amount,dep_amount,remark,date_hmoid_upload,regularization_status,date_regularization FROM mmdb_".$dbname.".mastermembertable WHERE emp_no = :emp_no";


                    $q = $this->conn->prepare($sql);
                    $values = array(':emp_no'=>$emp_no,':user'=>$user);

                if(!$q->execute($values)){

                        $errmsg = implode(" ", $q->errorInfo());
                        $er = implode(" ", $this->conn->errorInfo());
                        $emsg = "error code  :".$errmsg." || error code  : ".$er;   

                
                        throw new Exception($emsg);

                        return false;

                }
        
        return true;

    }catch(Exception $e){

        $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
        catcherror_log($err);



    }               

}       



}







?>