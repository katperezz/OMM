<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once('DbConnection.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');

Class ReportManagement extends DbConnection {
	
		public $totalcountoversla = 0;
		public $totalcountduesla = 0;
		

public function ReturnAllAccounts(){	

	try{

        if(!$this->OpenDB2()){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

					$sql = "SELECT account_name FROM  account_setup";		

					$qselect = $this->conndb2->query($sql);

							
		        if(!$qselect){

                		$errmsg = implode(" ", $qselect->errorInfo());
                		$er = implode(" ", $this->conndb2->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}	
					
		
							while($r = $qselect->fetch(PDO::FETCH_ASSOC)){
									$data[]=$r;
							}

							return $data;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	
						

}



public function ReturnTotalAndMemberNow($dbname,$year){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }
							$sql = "SELECT id FROM mastermembertable WHERE member_status != 'deleted' and YEAR(date_created) = ".$year;
							$qcount = $this->conn->prepare($sql);

		        if(!$qcount->execute()){

                		$errmsg = implode(" ", $qcount->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}	

									$count = $qcount->rowCount();
									return $count;

	}catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	
						
}		



public function Slamember($dbname,$year){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

						$sql="SELECT date_hmoid_upload,dateendorsed from mastermembertable where hmo_no!='' and hmo_no!='0' and  regularization_status!='deleted' and YEAR(date_created)='".$year."'";
						$q = $this->conn->prepare($sql);


		        if(!$q->execute()){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
										
           				 				$bsla = 0;
           								$asla = 0;

           								if($q->rowCount() == 0){
           										         $asla = 0;
           											     $bsla = 0;
           								}else{

     
           									while($row = $q->fetch(PDO::FETCH_ASSOC)){
    

           													 $upload_hmo_no = $row['date_hmoid_upload'];
           													 $dateendorsed = $row['dateendorsed'];

            												 $d1 = new DateTime($upload_hmo_no);
            												 $d2 = new DateTime($dateendorsed);
            												 $ddiff = $d2->diff($d1)->days;
         
            										if($ddiff > 5){

                												$asla ++;

            										}else{
               													 $bsla++;
           											 }    
    										} 
									}


					

					$this->totalcountoversla = $asla;
					$this->totalcountduesla = $bsla;
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



   }	

}


public function Provider($account){	


	try{

        if(!$this->OpenDB2()){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

					$sql = "SELECT hmo_provider FROM account_rule WHERE account_id = '".$account."'";	

					$qselect = $this->conndb2->query($sql);


		        if(!$qselect){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conndb2->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
							while($r = $qselect->fetch(PDO::FETCH_ASSOC)){
									$data[]=$r;
							}

							return $data;

	}catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	
			

}


				public function ReturnTotalAndHighMemberCount(){

						date_default_timezone_set('Asia/Manila');

						$poversla = 0;
						$pduesla = 0;
						$year = date('Y');
						$highcount = 0;
						$highacct = "";
						$highsla = 0;
						$hslaacct = "";
						// $totalcountoversla = 0;
						// $totalcountduesla = 0;

						

						foreach($this->ReturnAllAccounts() as $row){


								$account = $row['account_name'];
								$count = $this->ReturnTotalAndMemberNow($account,$year);

							
								$total[] = $count;

								if($count > $highcount){

									$highcount = $count;
									$highacct = $account;
								}

								$this->Slamember($account,$year);

								$t = $this->totalcountoversla + $this->totalcountduesla;

								
								if($this->totalcountoversla == 0){
									$aoversla =  0;
									$poversla =  0;
								}else{
									$aoversla =  $this->totalcountoversla/$t;
									$poversla =  $aoversla*100;
								}

								if($this->totalcountoversla == 0){
									$aduesla =  0;
									$pduesla =  0;
								}else{
									$aduesla = $this->totalcountduesla/$t;
									$pduesla = $aduesla*100;
								}


								if($pduesla > $highsla || $pduesla == $highsla){

									$highsla = $pduesla;
									$hslaacct = $account;


								}



								$countoversla[] = $this->totalcountoversla;
								$countduesla[] = $this->totalcountduesla;



						}

						if(empty($hslaacct)){

							$provider = "";

						}else{	

							foreach($this->Provider($hslaacct) as $row){

								$hmoprovider = $row['hmo_provider'];


							}					


						}
		

						$totalcount = array_sum($total);
						$totalcountoversla = array_sum($countoversla);
						$totalcountduesla = array_sum($countduesla);
						// $hhmo = implode(" ",$hslaacct);

						$data = array('totalcount'=>$totalcount,'highestaccount'=>$highacct,'totalcountofhighest'=>$highcount,'totalcountoversla'=>$totalcountoversla,'totalduesla'=>$totalcountduesla,'slapercent'=>$highsla,'highhmo'=>$hmoprovider);
						
						$jsondata = json_encode($data);

						return $jsondata;

				}		

				
}







// $a = new ReportManagement();

// print_r($a->Provider("testaccount"));
// echo "<BR>";
// echo $a->ReturnTotalAndHighMemberCount(date('Y'));





?>