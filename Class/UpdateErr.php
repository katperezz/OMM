<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/catcherror_log.php');
include_once('DbConnection.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/dev_log.php');





class UpdateErr extends DbConnection {


/*
*
*   function StatusData
*       
*   it returns the status of the tempmembertable     
*    
*   
*   @param string $dbname database name
*   @param string $file_id
*   @param string $user
*   @param string $tblname
*   @param string $id
*   
*   return array    return array data 
*
*
*
*
*/


public function StatusData($dbname,$file_id,$user,$id){


	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

			$sql = "SELECT status FROM tempmembertable WHERE id = '".$id."' AND file_id = '".$file_id."' AND user = '".$user."'";		
			$q = $this->conn->prepare($sql);
			

		        if(!$q->execute()){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

                $data = $q->fetchAll();
                return $data;


    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	
          
        	

}




/*
*
*   function IdData
*       
*   it returns the status,emp_no,lastname,firstname,dob of the tempmembertable     
*    
*   
*   @param string $dbname database name
*   @param string $file_id
*   @param string $user
*   @param string $tblname
*   @param string $id
*   
*   return array    return array data 
*
*
*
*
*/




	public function IdData($dbname,$file_id,$user,$id){


	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }


			$sql = "SELECT status,emp_no,lastname,firstname,dob FROM tempmembertable WHERE id = '".$id."' AND file_id = '".$file_id."' AND user = '".$user."'";		
			$q = $this->conn->prepare($sql);
			
		        if(!$q->execute()){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return array();

       			}

       		 	if($q->rowCount() == 0){

       		 		return array();
       		 	}else{		

                $data = $q->fetchAll();
                return $data;

            	}

     }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	              	
        	

}


/*
*
*   function UpdatingData
*       
*   it returns all data row of the tempmembertable     
*    
*   
*   @param string $dbname database name
*   @param string $file_id
*   @param string $user
*   @param string $tblname
*   @param string $id
*   
*   return array    return array data 
*
*
*
*
*/




public function UpdatingData($dbname,$file_id,$user,$id){


	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

		if(!$this->IdData($dbname,$file_id,$user,$id)){
			throw new Exception("ERROR IdData return false");
			return false;
		}

	foreach($this->IdData($dbname,$file_id,$user,$id) as $r){		

			if($r['status'] == "duplicate entry emp_no and employee name"){

				$sql1 = "SELECT * FROM tempmembertable WHERE emp_no = '".$r['emp_no']."' AND lastname = '".$r['lastname']."' AND firstname = '".$r['firstname']."' AND dob = '".$r['dob']."' AND file_id = '".$file_id."' AND user = '".$user."' AND id != '".$id."'";
				$q1 = $this->conn->prepare($sql1);
			

		        if(!$q1->execute()){

                		$errmsg = implode(" ", $q1->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return array();

       			}

            	    $data = $q1->fetchAll();
            	    return $data;
        	


			}else if($r['status'] == "duplicate entry employee name"){

				$sql2 = "SELECT * FROM tempmembertable WHERE lastname = '".$r['lastname']."' AND firstname = '".$r['firstname']."' AND dob = '".$r['dob']."' AND file_id = '".$file_id."' AND user = '".$user."' AND id != '".$id."'";
				$q2 = $this->conn->prepare($sql2);
			

		        if(!$q2->execute()){

                		$errmsg = implode(" ", $q2->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return array();

       			}

            	    $data = $q2->fetchAll();
            	    return $data;
        		


			}else if($r['status'] == "duplicate entry emp_no"){

				$sql3 = "SELECT * FROM tempmembertable WHERE emp_no = '".$r['emp_no']."' AND file_id = '".$file_id."' AND user = '".$user."' AND id != '".$id."'";
				$q3 = $this->conn->prepare($sql3);
			

		        if(!$q3->execute()){

                		$errmsg = implode(" ", $q3->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return array();

       			}

            	    $data = $q3->fetchAll();
            	    return $data;
        		


			}else{

				
					return array();
			}

	

	}

}catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }			

}




/*
*
*   function UpdateTemp
*       
*   it returns all data row of the tempmembertable     
*    
*   
*   @param string $dbname database name
*   @param string $file_id
*   @param string $user
*   @param string $tblname
*   @param string $id
*   
*   return array    return array data 
*
*
*
*
*/


	

public function UpdateTemp($dbname,$user,$fileid,$status,$emp_no,$sss_no,$phil_no,$lastname,$firstname,$dob){


	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

			if($status == "e1"){

				$sql = "UPDATE tempmembertable SET status = :status where emp_no = :emp_no and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no";
				$values =  array(':status'=> $s,':emp_no'=>$emp_no,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
				

			}elseif($status == "e2"){

				$sql = "UPDATE tempmembertable SET status = :status where lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry employee name";
				$values =  array(':status'=> $s,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
						


			}elseif($status == "e3"){

				$sql = "UPDATE tempmembertable SET status = :status where emp_no = :emp_no and lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no and employee name";
				$values =  array(':status'=> $s,':emp_no'=>$emp_no,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
						

			}


		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	

}	


public function UpdateErrorstat($dbname,$user,$upid,$status,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$age,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$datehire){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

			$sql = "UPDATE tempmembertable SET status = :status,emp_no = :emp_no,sss_no = :sss_no,phil_no = :phil_no,hmo_no = :hmo_no,suboffice = :suboffice,subofficecode = :subofficecode,site = :site,lastname = :lastname,firstname = :firstname,middlename = :middlename,ext = :ext,dob = :dob,age = :age,gender = :gender,maritalstatus = :maritalstatus,category = :category,job_desc = :job_desc,joblevel = :joblevel,effectivedate = :effectivedate,datehire = :datehire where upid = :upid and user = :user";
			$q = $this->conn->prepare($sql);
			$values =  array(':status'=>$status,':emp_no'=>$emp_no,':sss_no'=>$sss_no,':phil_no'=>$phil_no,':hmo_no'=>$hmo_no,':suboffice'=>$suboffice,':subofficecode'=>$subofficecode,':site'=>$site,':lastname'=>$lastname,':firstname'=>$firstname,':middlename'=>$middlename,':ext'=>$ext,':dob'=>$dob,':age'=>$age,':gender'=>$gender,':maritalstatus'=>$maritalstatus,':category'=>$category,':job_desc'=>$job_desc,':joblevel'=>$joblevel,':effectivedate'=>$effectivedate,':datehire'=>$datehire, ':upid' => $upid, ':user' => $user);

		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	

}


public function UpdateErrorstatdraft($dbname,$user,$upid,$status,$emp_no,$sss_no,$phil_no,$hmo_no,$suboffice,$subofficecode,$site,$lastname,$firstname,$middlename,$ext,$dob,$age,$gender,$maritalstatus,$category,$job_desc,$joblevel,$effectivedate,$datehire){



	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

			$sql = "UPDATE draftmembertable SET status = :status,emp_no = :emp_no,sss_no = :sss_no,phil_no = :phil_no,hmo_no = :hmo_no,suboffice = :suboffice,subofficecode = :subofficecode,site = :site,lastname = :lastname,firstname = :firstname,middlename = :middlename,ext = :ext,dob = :dob,age = :age,gender = :gender,maritalstatus = :maritalstatus,category = :category,job_desc = :job_desc,joblevel = :joblevel,effectivedate = :effectivedate,datehire = :datehire where upid = :upid and user = :user";
			$q = $this->conn->prepare($sql);
			$values =  array(':status'=>$status,':emp_no'=>$emp_no,':sss_no'=>$sss_no,':phil_no'=>$phil_no,':hmo_no'=>$hmo_no,':suboffice'=>$suboffice,':subofficecode'=>$subofficecode,':site'=>$site,':lastname'=>$lastname,':firstname'=>$firstname,':middlename'=>$middlename,':ext'=>$ext,':dob'=>$dob,':age'=>$age,':gender'=>$gender,':maritalstatus'=>$maritalstatus,':category'=>$category,':job_desc'=>$job_desc,':joblevel'=>$joblevel,':effectivedate'=>$effectivedate,':datehire'=>$datehire, ':upid' => $upid, ':user' => $user);

		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	

}


public function UpdateDraft($dbname,$user,$fileid,$status,$emp_no,$sss_no,$phil_no,$lastname,$firstname,$dob){


	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }


			if($status == "e1"){

				$sql = "UPDATE draftmembertable SET status = :status where emp_no = :emp_no and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no";
				$values =  array(':status'=> $s,':emp_no'=>$emp_no,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "e2"){

				$sql = "UPDATE draftmembertable SET status = :status where lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry employee name";
				$values =  array(':status'=> $s,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "e3"){

				$sql = "UPDATE draftmembertable SET status = :status where emp_no = :emp_no and lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no and employee name";
				$values =  array(':status'=> $s,':emp_no'=>$emp_no,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
			}
		
		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	
}



	
public function UpdateTempLogs($dbname,$user,$fileid,$status,$emp_no,$sss_no,$phil_no,$lastname,$firstname,$dob){


	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

			if($status == "e1"){

				$sql = "UPDATE upload_logs SET status = :status where emp_no = :emp_no and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no";
				$values =  array(':status'=> $s,':emp_no'=>$emp_no,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "e2"){

				$sql = "UPDATE upload_logs SET status = :status where lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry employee name";
				$values =  array(':status'=> $s,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "e3"){

				$sql = "UPDATE upload_logs SET status = :status where emp_no = :emp_no and lastname = :lastname and firstname = :firstname and dob = :dob and file_id = :file_id and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no and employee name";
				$values =  array(':status'=> $s,':emp_no'=>$emp_no,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':file_id' => $fileid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}

		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }

}



public function UpdatedupTempLogs($dbname,$user,$upid,$status,$emp_no,$sss_no,$phil_no,$lastname,$firstname,$dob){

	try{

        if(!$this->OpenDB($dbname)){

            throw new Exception("OPEN DB ERROR!");
            return false;
        }

			if($status == "e1"){

				$sql = "UPDATE updateupload_logs SET status = :status where emp_no_update = :emp_no and upid = :upid and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no";
				$values =  array(':status'=> $s,':emp_no'=>$emp_no,':upid' => $upid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "e2"){

				$sql = "UPDATE updateupload_logs SET status = :status where lastname_update = :lastname and firstname_update = :firstname and dob_update = :dob and upid = :upid and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry employee name";
				$values =  array(':status'=> $s,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':upid' => $upid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}

			}elseif($status == "e3"){

				$sql = "UPDATE updateupload_logs SET status = :status where emp_no_update = :emp_no and lastname_update = :lastname and firstname_update = :firstname and dob_update = :dob and upid = :upid and user = :user";
				$q = $this->conn->prepare($sql);
				$s = "duplicate entry emp_no and employee name";
				$values =  array(':status'=> $s,':emp_no'=>$emp_no,':lastname'=>$lastname,':firstname'=>$firstname,':dob'=>$dob,':upid' => $upid, ':user' => $user);
		
		        if(!$q->execute($values)){

                		$errmsg = implode(" ", $q->errorInfo());
                		$er = implode(" ", $this->conn->errorInfo());
                		$emsg = "error code  :".$errmsg." || error code  : ".$er;	

                
                		throw new Exception($emsg);

                		return false;

       			}
			}


		return true;

    }catch(Exception $e){

    	$err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
    	catcherror_log($err);



    }	
}


}


?>