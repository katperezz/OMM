/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('OMM.Application', {
    extend: 'Ext.app.Application',
    
    name: 'OMM',

    stores: [
        'Get_acc_list',
        'Get_providers_list',
        'modern.Get_dental_tkt'
        // TODO: add global / shared stores here
    ],
    
    launch: function () {
        // TODO - Launch the application
                //OMM Main Clock settings
        Ext.getCmp('omm_time').setValue(Ext.Date.format(new Date(), 'g:i:s A')+' |');
        // Check Session every 30 minutes
        setInterval(function(){
            Ext.create('OMM.view.main.MainController').onLoadMainView();
            // Ext.Ajax.request({url:'../data/session_checker.php',success:function(response,btn,e){var response=Ext.decode(response.responseText);if(response.success==true){Ext.MessageBox.show({title:'SESSION EXPIRED',msg:'You session has been expired!',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'},fn:function(btn){if(btn=='ok'){window.location.href='index.php'}}})}}});
        },300000);//5minutes
        // 300000 5minutes 1800000 30 minutes

        if(document.URL.indexOf("batchupload") >= 0)
        {
            Ext.create('OMM.view.batchupload.BatchUpload').show();
        }
        if(document.URL.indexOf("sla") >=0)
        {
            Ext.create('OMM.view.sla.Sla_management').show();
        }
        if(document.URL.indexOf("logs") >= 0)
        {
            Ext.create('OMM.view.logs.LogsWindow').show();
        }
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
