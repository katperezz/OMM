Ext.define('OMM.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'OMM.model'
    }
});
