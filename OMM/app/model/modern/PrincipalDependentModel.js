Ext.define('OMM.model.modern.PrincipalDependentModel',{
	extend:'OMM.model.Base',
	fields:[
		'al_dep_id',
		'al_prin_id',
		'dep_status',
		'status',
		'hmo_id',
		'fname',
		'mname',
		'lname',
		'ename',
		'gender',
		'bdate',
		'civil_status',
		'philhealth',
		'relationship',
		'date_of_enrollment',
		'last_update',
		'datecreated',
		'remark',
		'date_of_deletion',
		'dcompanyid',
		'dbranchid',
		'dmbr_status',
		'dplan_level'
	],

	validators 	:{			
		fname   :{
			type    :'presence',
			message :'Please provide first name'
		},
		lname   :{
			type    :'presence',
			message :'Please provide last name'
		},
		bdate   :{
			type    :'presence',
			message :'Please provide birthdate'
		},
		gender   :{
			type    :'presence',
			message :'Please provide gender'
		}

    }

});