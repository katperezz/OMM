Ext.define('OMM.model.modern.PrincipalModel',{
	extend 	:'Ext.data.Model',
	requires :[
		'Ext.data.validator.Presence'
	],

	fields:[
		'activelink_id',
		'empid',
		'fname',
		'mname',
		'lname',
		'ename',
		'fullname',
		'birthdate',
		'gender',
		'civil_status',
		'philhealth',
		'sss_no',
		'pag_ibig_no',
		'unified_id_no',
		'plan_id',
		'branch_id',
		'email',
		'mbr_status',
		'id_number',
		'company_id',
		'member_date',
		'designation',
		'tin',
		'reg_date',
		'inactive_date',
		'existing_flag',
		'inclusion_date',
		'emp_hire_date',
		'date_of_joining',
		'hr_inclusion_endorsement_date',
		'hmo_inclusion_date',
		'hr_deletion_endorsement_date',
		'hmo_deletion_date',
		'plan_desc'
	],

	validators 	:{	
          activelink_id   :{
            type    :'presence',
            message :'Please provide choose ID name'
          },
          fname   :{
            type    :'presence',
            message :'Please provide choose ID name'
          },
          lname   :{
            type    :'presence',
            message :'Please provide choose ID name'
          }
	}
	

});