Ext.define('OMM.store.Get_user_list',{
	extend 	:'Ext.data.Store',
	fields 	:['username', 'email'],
	proxy 	:{
		type 	:'ajax',
		api 	:{
			read 	:'../data/get_user_list.php'
		},
		reader  :{
			type 			:'json',
			successProperty :'success',
			messageProperty :'message',
			rootProperty 	:'data'
		}

	}

});

