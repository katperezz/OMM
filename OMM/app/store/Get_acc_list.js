Ext.define('OMM.store.Get_acc_list', {
    extend  : 'Ext.data.Store',
    storeId : 'Get_acc_list',
    sorters     :[{property:'companyname',direction:'ASC'}],
    sortRoot    :'companyname',
    sortOnLoad  :true,
    model   : 'OMM.model.Get_acc_list',
    proxy   : {
        type          : 'ajax',
        actionMethods : 'POST',
        api           : {read : '../data/get_acc_list.php'},
        reader        : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
			messageProperty : 'message'	
    }
}
});
