Ext.define('OMM.store.Get_dependent_req_documents',{
	extend:'Ext.data.Store',
	field:['doc_id','doc_name'],
	proxy:{
		type:'ajax',
		api:{read:'../data/get_dep_required_docu.php'},
		reader:{
			type:'json',
			rootProperty:'data',
			messageProperty:'message',
			successProperty:'success'
		}
	}
	
});