Ext.define('OMM.store.activity.Get_member_update_log',{
	extend 	: 'Ext.data.ArrayStore',
	alias 	:'OMM.principal.activity.get_member_update_log',
	model 	:'OMM.model.principal.PrincipalDependentModel',
	proxy 	:{
		type:'ajax',
		api :{
			read:'../data/get_member_update_log.php'
		},
		actionMethods 	:'GET',
		extraParams 	:{
			gid 	:'',
			al_id 	:'',
			acc_id 	:''
		},
		reader 			:{
			type 		   :'json',
			rootProperty   :'data',
			totalProperty  :'total',
			successProperty:'success'
		}
	}
});