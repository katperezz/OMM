Ext.define('OMM.store.Get_groupbase_plan',{
	extend:'Ext.data.Store',
	fields:['id', 'show_for'],
	proxy:{
		type:'ajax',
		api:{read:'../data/get_dep_group_planid.php'},
		reader:{
			type:'json',
			rootProperty:'data',
			successProperty:'success',
			messageProperty:'message'
		}
	}
});