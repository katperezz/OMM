Ext.define('OMM.store.Get_gender',{
	extend 	:'Ext.data.Store',
	storeId :'Get_gender',
	fields 	:['gender','description'],
	proxy 	:{
		type 		 :'ajax',
		actionMethods:'POST',
		api		 	 :{
			read 	 :'../data/get_gender.php'
		},
		reader 	 	 :{
			type 	   		:'json',
			rootProperty 	:'data',
			successProperty :'success',
			messageProperty :'message'
		}
	}
});