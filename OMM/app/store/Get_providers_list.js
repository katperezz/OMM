Ext.define('OMM.store.Get_providers_list', {
    extend      : 'Ext.data.Store',
    storeId     : 'Get_providers_list',
    sorters     :[{property:'provider_name',direction:'ASC'}],
    sortRoot    :'provider_name',
    sortOnLoad  :true,
    fields      :['provider_id','provider_name'],
    proxy   : {
        type          : 'ajax',
        actionMethods : 'POST',
        api           : {
            // read : '/Providersaccess/data/get_providers_list.php'
            read :'https://www.benefitsmadebetter.com/Providersaccess/data/get_providers_list.php'
        },
        reader        : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
			messageProperty : 'message'	
    }
}
});
