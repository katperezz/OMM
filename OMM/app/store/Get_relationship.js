Ext.define('OMM.store.Get_relationship',{
	extend 	:'Ext.data.Store',
	fields 	:['relationship'],
	proxy 	:{
		type 		:'ajax',
		api  		:{
			read 	:'../data/get_relationship.php'
		},
		actionMethods:'POST',
		reader 	 	 :{
			type 		  	:'json',
			rootProperty  	:'data',
			successProperty :'success',
			messageProperty :'message'
		}
	}
});