Ext.define('OMM.store.Get_activity_note', {
    extend  : 'Ext.data.ArrayStore',
    alias   : 'OMM.get_activity_note',
    storeId :'gridstore',
    sorters     :[{property:'date',direction:'ASC'}],
    sortRoot    :'date',
    sortOnLoad  :true,
    // model   : 'OMM.model.principal.PrincipalDependentModel',
    proxy   : {
        type    : 'ajax',
        timeout :2000000,
        api     : {
            read: '../data/activity_get_note.php'
        },
        actionMethods   : 'GET',
        extraParams     : {
            prin_al_id  : ''
        },
        reader          : {
            type            : 'json',
            rootProperty    : 'data',
            totalProperty   : 'totalCount',
            successProperty : 'success'
        },
        writer         : {
            type        : 'json',
            rootProperty: 'data',
            encode      : true
        }
    }
});