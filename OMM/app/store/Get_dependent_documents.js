Ext.define('OMM.store.Get_dependent_documents',{
	extend: 'Ext.data.ArrayStore',
	fields  :['fid','doc_id','doc_name','doc_stat','filename','filesignature','dateuploaded'],
	proxy 	:{
		type 	 		:'ajax',
		api 	 		:{read 		:'../data/get_dependent_documents.php'},
		actionMethods 	:'POST',
		extraParams 	:{
			dep_al_id 	:'',
			acc_id 		:''
		},
		reader 			:{
			type 			:'json',
			rootProperty	:'data',
			successProperty :'success',
			messageProperty :'message'
		}
	}
});