Ext.define('OMM.store.modern.Get_dental_tkt', {
    extend: 'Ext.data.Store',

    alias: 'store.cc_connect_msg_thread',

    fields:[
      { name: 'hf_ticket_id'},
      { name: 'ticket_title'},
      { name: 'company_id'},
      { name: 'created',
        convert:function(v,record){

            var todaysDate = Ext.Date.format(new Date(), 'Y-m-d'),
                value      = Ext.Date.format(new Date(v), 'Y-m-d');
            var yesterday_1 = new Date(new Date().setDate(new Date().getDate()-1)),
                yesterday_2 = new Date(new Date().setDate(new Date().getDate()-2)),
                yesterday_3 = new Date(new Date().setDate(new Date().getDate()-3)),
                yesterday_4 = new Date(new Date().setDate(new Date().getDate()-4)),
                yesterday_5 = new Date(new Date().setDate(new Date().getDate()-5)),
                yesterday_6 = new Date(new Date().setDate(new Date().getDate()-6)),
                yesterday_7 = new Date(new Date().setDate(new Date().getDate()-7)),
                lastdate_1    =  Ext.Date.format(yesterday_1, 'Y-m-d'),
                lastdate_2     =  Ext.Date.format(yesterday_2, 'Y-m-d');
                lastdate_3    =  Ext.Date.format(yesterday_3, 'Y-m-d'),
                lastdate_4    =  Ext.Date.format(yesterday_4, 'Y-m-d'),
                lastdate_5    =  Ext.Date.format(yesterday_5, 'Y-m-d'),
                lastdate_6    =  Ext.Date.format(yesterday_6, 'Y-m-d'),
                lastdate_7    =  Ext.Date.format(yesterday_7, 'Y-m-d'),

            console.log(todaysDate+'\n'+lastdate_1+'\n'+value)
            if(value == todaysDate) {
                return Ext.Date.format(new Date(v), 'g:i a');
            }else if(value == lastdate_1){
                return "Yesterday"
                // return Ext.Date.format(new Date(v), 'D');
            }else if(value == lastdate_2){
                return Ext.Date.format(new Date(v), 'D');
            }else if(value == lastdate_3){
                return Ext.Date.format(new Date(v), 'D');
            }else if(value == lastdate_4){
                return Ext.Date.format(new Date(v), 'D');
            }else if(value == lastdate_5){
                return Ext.Date.format(new Date(v), 'D');
            }else if(value == lastdate_6){
                return Ext.Date.format(new Date(v), 'D');
            }else if(value == lastdate_7){
                return Ext.Date.format(new Date(v), 'D');
            }else{
                return value;
            }
        }
        },
      { name: 'ownerId'},
      { name: 'ticket_disp_id'},
      { name: 'activelink_id'},
      { name: 'dental_hmo'},
      { name: 'table_name'},
      { name: 'id'},
      { name: 'updated'},
      { name: 'ticket_msg'},
      { name: 'objectId'}
    ],
    proxy: {
        type          : 'ajax',
        actionMethods : 'GET',
        extraParams     : {
            activelink_id   : ''
        },
        api           : {
            // read  : 'map.json',
            read:"https://www.benefitsmadebetter.com/mobile/dentalconnect/get_dental_tkt.php"
            // read:"https://www.benefitsmadebetter.com/devmod5/Activebmb/data/get_dental_tkt.php"
            // read:'resources/data/get_dental_tkt.php'
            // read: 'https://74.208.146.145/devmod5/Activebmb/data/cc_connect_msg_thread.php'
            // read:'map.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    autoLoad:true
});
