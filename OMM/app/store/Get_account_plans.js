Ext.define('OMM.store.Get_account_plans', {
    extend        : 'Ext.data.Store',
    fields        :['planid'],
    proxy         : {
        type          : 'ajax',
        actionMethods : 'POST',
        timeout       :2000000,
        api           : {read  : '../data/get_account_plans.php'},
        extraParams   : {
            acc_id      : '',
            branch_id   : ''
        },
        reader       : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
            messageProperty : 'message'	
        }
    }
});
