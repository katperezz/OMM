Ext.define('OMM.store.NavigationTree', {
    extend: 'Ext.data.TreeStore',

    storeId: 'NavigationTree',
    root: {
        expanded: true,
        children: [
            {
                text:   'Dashboard',
                view:   'dashboard.Dashboard',
                leaf:   true,
                iconCls: 'right-icon x-fa fa-desktop',
                routeId: 'dashboard'
            },
            {
                text:   'Principal',
                expanded: false,
                selectable: false,
                iconCls: 'right-icon x-fa fa-user',
                children: [
                    {
                        text:'All Members',
                        view:   'principal.Principal_management',
                        leaf:   true,
                        iconCls: 'right-icon x-fa fa-files-o',
                        routeId: 'principal_management'
                    }
                    // {
                    //     text:'Registered <br>Members',
                    //     view:'principal.RegisteredPrincipal',
                    //     leaf:true,
                    //     iconCls:'right-icon x-fa fa-user',
                    //     routeId:'registeredprincipal'
                    // },
                    // {
                    //     text :'Upload',
                    //     view:'principal.Principal_Inclusion',
                    //     leaf:true,
                    //     iconCls:'right-icon x-fa fa-files-o',
                    //     routeId:'principal_inclusion'
                    // }
                ]
            },
            {
                text:   'Dependent',
                expanded: false,
                selectable: false,
                iconCls: 'right-icon x-fa fa-user',
                children: [
                    {
                        text:'All Dependent',
                        view:'dependent.Dependent_management',
                        leaf:   true,
                        iconCls: 'right-icon x-fa fa-files-o',
                        routeId:'dependent_management'
                    },
                    {
                        text:'Upload',
                        leaf:true,
                        iconCls:'right-icon x-fa fa-files-o'
                    }
                ]
            },
            {
                text:   'Activities',
                view:    'activities.ActivitiesPanel',
                leaf:   true,
                iconCls: 'right-icon x-fa fa-files-o',
                routeId:'activitiespanel'
            },
            {
                text:   'Batch Upload',
                leaf:   true,
                iconCls: 'right-icon x-fa fa-files-o'
            }

        ]
    },
    fields: [
        {
            name: 'text'
        }
    ]
});
