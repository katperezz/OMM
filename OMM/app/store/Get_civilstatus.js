Ext.define('OMM.store.Get_civilstatus',{
	extend 	:'Ext.data.Store',
	storeId :'Get_civilstatus',
	fields  :['civil_status','description'],
	proxy 	:{
		type 	 	 :'ajax',
		actionMethods:'POST',
		api 		 :{
			read 	 :'../data/get_civilstatus.php'
		},
		reader 		 :{
			type 	  	    :'json',
			rootProperty    :'data',
			successProperty :'success',
			messageProperty :'message'
		} 		
	}
});