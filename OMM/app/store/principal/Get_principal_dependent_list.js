Ext.define('OMM.store.principal.Get_principal_dependent_list', {
    extend  : 'Ext.data.ArrayStore',
    alias   : 'OMM.principal.get_principal_dependent_list',
    storeId :'gridstore',
    model   : 'OMM.model.principal.PrincipalDependentModel',
    proxy   : {
        type    : 'ajax',
        timeout :2000000,
        api     : {
            read: '../data/get_principal_dependents.php'
        },
        actionMethods   : 'GET',
        extraParams     : {
            prin_al_id  : ''
        },
        reader          : {
            type            : 'json',
            rootProperty    : 'data',
            totalProperty   : 'totalCount',
            successProperty : 'success'
        },
        writer         : {
            type        : 'json',
            rootProperty: 'data',
            encode      : true
        }
    }
});