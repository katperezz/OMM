Ext.define('OMM.store.principal.Get_principal_deleted_dependent_list',{
	extend 	:'Ext.data.ArrayStore',
	alias 	:'OMM.principal.get_principal_deleted_dependent_list',
	model 	:'OMM.model.principal.PrincipalDependentModel',
	proxy 	:{
		type 	:'ajax',
		timeout :2000000,
		api 	:{
			read:'../data/get_principal_deleted_dependent.php'
		},
		actionMethos:'GET',
		extraParams :{
			prin_al_id :''
		},
		reader 		:{
			type 			:'json',
			rootProperty 	:'data',
			totalProperty 	:'total',
			successProperty :'success'
		},
		writer 		:{
			type 		:'json',
			rootProperty:'data',
			encode 		:true
		}
	}
});