Ext.define('OMM.store.principal.Get_principal_mem_logs', {
    extend  : 'Ext.data.ArrayStore',
    alias   : 'OMM.principal.get_principal_mem_logs',
    storeId :'gridstore',
    sorters     :[{property:'date',direction:'ASC'}],
    sortRoot    :'date',
    sortOnLoad  :true,
    // model   : 'OMM.model.principal.PrincipalDependentModel',
    proxy   : {
        type    : 'ajax',
        timeout :2000000,
        api     : {
            read: '../data/activity_get_note.php'
        },
        actionMethods   : 'GET',
        extraParams     : {
            prin_al_id  : ''
        },
        reader          : {
            type            : 'json',
            rootProperty    : 'data',
            totalProperty   : 'totalCount',
            successProperty : 'success'
        },
        writer         : {
            type        : 'json',
            rootProperty: 'data',
            encode      : true
        }
    }
});