Ext.define('OMM.store.logs.Activity_batchprocess_loadcolumn', {
    extend      : 'Ext.data.Store',
    sorters     :[{property:'id',direction:'ASC'}],
    sortRoot    :'id',
    sortOnLoad  :true,
    // remoteSort  :true,//server side sorting
    proxy       : {
        type          : 'ajax',
        actionMethods : 'POST',
        api           : {
            read : '../data/activity_batch_data.php'
        },
        reader      : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
            messageProperty : 'message'	
        }
    }
});
