Ext.define('OMM.store.logs.Activity_Store',{
	extend:'Ext.data.Store',
	// autoLoad: {params:{start:0, limit:10}},
	pageSize: 10,
	proxy:{
		type:'ajax',
		api :{
            read : '../data/activity_search_details.php'
		},
		reader:{
			type:'json',
			rootProperty:'data',
			successProperty:'success',
			messageProperty:'message'
		}
	}

	
});


