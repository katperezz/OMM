Ext.define('OMM.store.logs.Activity_member_update', {
    extend  : 'Ext.data.Store',
    // fields  : [       
    //     {name: 'action_category', mapping: 'action_category'},
    //     {name: 'logs_count', mapping: 'logs_count'}
    //    ],
    proxy   : {
        type    : 'ajax',
        actionMethods : 'POST',
        api     : {
            read : '../data/activity_member_update.php'
        },
        reader  : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
            messageProperty : 'message'	
        }
    }
});
