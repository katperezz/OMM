Ext.define('OMM.store.logs.Activity_action_category', {
    extend  : 'Ext.data.Store',
    fields  : [       
        {name: 'action_category', mapping: 'action_category'},
        {name: 'logs_count', mapping: 'logs_count'}
       ],
    proxy   : {
        type    : 'ajax',
        actionMethods : 'POST',
        api     : {
            read : '../data/activity_accnt_stat.php'
        },
        reader  : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
            messageProperty : 'message'	
        }
    }
});
