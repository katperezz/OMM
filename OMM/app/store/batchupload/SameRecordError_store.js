Ext.define('OMM.store.batchupload.SameRecordError_store',{
	extend :'Ext.data.Store',
	fields:[{name:'activelink_id',mapping:'activelink_id'},{name:'empid',mapping:'empid'},{name:'fname',mapping:'fname'},{name:'mname',mapping:'mname'},{name:'lname',mapping:'lname'},{name:'ename',mapping:'ename'},{name:'fullname',mapping:'fullname'},{name:'birthdate',mapping:'birthdate'},{name:'gender',mapping:'gender'},{name:'civil_status',mapping:'civil_status'},{name:'philhealth',mapping:'philhealth'},{name:'sss_no',mapping:'sss_no'},{name:'pag_ibig_no',mapping:'pag_ibig_no'},{name:'unified_id_no',mapping:'unified_id_no'},{name:'plan_id',mapping:'plan_id'},{name:'branch_id',mapping:'branch_id'},{name:'email',mapping:'email'},{name:'mbr_status',mapping:'mbr_status'},{name:'id_number',mapping:'id_number'},{name:'company_id',mapping:'company_id'},{name:'member_date',mapping:'member_date'},{name:'designation',mapping:'designation'},{name:'tin',mapping:'tin'},{name:'reg_date',mapping:'reg_date'},{name:'inactive_date',mapping:'inactive_date'},{name:'existing_flag',mapping:'existing_flag'},{name:'inclusion_date',mapping:'inclusion_date'},{name:'emp_hire_date',mapping:'emp_hire_date'},{name:'date_of_joining',mapping:'date_of_joining'},{name:'hr_inclusion_endorsement_date',mapping:'hr_inclusion_endorsement_date'},{name:'hmo_inclusion_date',mapping:'hmo_inclusion_date'},{name:'hr_deletion_endorsement_date',mapping:'hr_deletion_endorsement_date'},{name:'hmo_deletion_date',mapping:'hmo_deletion_date'},{name:'plan_desc',mapping:'plan_desc'}],
	proxy  :{
		type 	:'ajax',
		api  	:{
			read 	:'../data/batch_samerecord_error.php'
		},
		extraParams:{
			lname:'',
			fname:'',
			bdate:''
		},
		reader 	:{
			type 	 		:'json',
			rootProperty 	:'data',
			successProperty :'success',
			messageProperty :'message'
		},
		writer 	:{
			type 			:'json',
			rootProperty 	:'data',
			successProperty :'success',
			messageProperty :'message'
		}
	}
});