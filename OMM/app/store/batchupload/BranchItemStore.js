Ext.define('OMM.store.batchupload.BranchItemStore',{
	extend:'Ext.data.Store',
	fields:[
		{name:'branch_name', mapping:'branch_name'}
	],
	proxy:{
		type          : 'ajax',
		actionMethods : 'POST',
		api           : {
			read : '../data/get_tmpbranches.php'
		},
		reader:{
			type:'json',
			rootProperty:'data',
			messageProperty:'message',
			successProperty:'success'
		}
	}
});