Ext.define('OMM.store.batchupload.PlanSource_store',{
	extend:'Ext.data.Store',
	fields:['tmp_branch','tmp_plan', 'match_plan'],
	proxy 	:{
		type 	:'ajax',
		api 	:{
			read :'../data/plansource.php'
		},
		reader :{
			type 	:'json',
			rootProperty:'data',
			messageProperty:'message',
			successProperty:'success'
		},
		writer 	:{
			type 	:'json',
			rootProperty:'data',
			messageProperty :'message',
			successProperty:'success'
		}

	}

});