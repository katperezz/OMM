Ext.define('OMM.store.batchupload.FieldConfig', {
    extend  : 'Ext.data.Store',
    fields  : [       
        {name: 'fieldname', mapping: 'fieldname'}
       ],
    proxy   : {
        type    : 'ajax',
        actionMethods : 'POST',
        api     : {
            read : '../data/batch_tmp_getfields.php'
        },
        reader  : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
            messageProperty : 'message'	
        }
    }
});
