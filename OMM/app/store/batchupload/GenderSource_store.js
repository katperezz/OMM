Ext.define('OMM.store.batchupload.GenderSource_store',{
	extend :'Ext.data.Store',
	fields :['tmp_gender', 'match_gender'],
	proxy :{
		type 	:'ajax',
		api 	:{
			read 	:'../data/gender_source.php'
		},
		extraParams     : {
			acc_id       : '',
			filesignature:'',
			user_id 	 :'',
			field_name 	 :''
		},
		reader 	:{
			type  		 	 :'json',
			rootProperty 	 :'data',
			successProperty  :'success',
			messageProperty  :'message'
		},
		writer :{
			type 			:'json',
			rootProperty 	:'data',
			successProperty :'success',
			messageProperty :'message'
		}
	}
});