Ext.define('OMM.store.batchupload.BranchSource_store',{
    extend  : 'Ext.data.Store',
	alias 	:['store.BranchSource_store'],
	fields 	:['source_branch', 'match_branch'],
	proxy   : {
	    type    : 'ajax',
	    actionMethods : 'POST',
	    api     : {
	        read : '../data/branchsource.php'
	    },
		extraParams     : {
			acc_id       : '',
			filesignature:'',
			user_id 	 :'',
			field_name 	 :''
		},
	    reader  : {
	        type            : 'json',
	        rootProperty    : 'data',
	        successProperty : 'success',
	        messageProperty : 'message'	
	    },
	    writer :{
	    	type 			:'json',
	    	rootProperty 	:'data',
	    	successProperty :'success',
	    	messageProperty :'message'
	    }
	}

});