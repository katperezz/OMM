Ext.define('OMM.store.batchupload.TempTableStore_loaddata', {
    extend      : 'Ext.data.Store',
    sorters     :[{property:'id',direction:'ASC'}],
    sortRoot    :'id',
    sortOnLoad  :true,
    // remoteSort  :true,//server side sorting
    proxy       : {
        type          : 'ajax',
        actionMethods : 'POST',
        api           : {
            read : '../data/batch_tmptbl_data.php'
        },
        reader      : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
            messageProperty : 'message'	
        }
    }
});
