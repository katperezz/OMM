Ext.define('OMM.store.batchupload.TempTableStore', {
    extend: 'Ext.data.Store',
    fields  :[
        {name: 'field_name', mapping: 'field_name'}
    ],
    filters: [function(record, id){
        return (record.data.field_name != "status_match");
    },function(record, id){
        return (record.data.field_name != "status");
    },function(record, id){
        return (record.data.field_name != "status_msg");
    },function(record, id){
        return (record.data.field_name != "id")
    }
    ],
    // data    :[{
    //     field_name :'blank_field'
    // }],
    proxy : {
        type          : 'ajax',
        actionMethods : 'POST',
        api           : {
            read : '../data/batch_tmptbl_list.php'
        },
        reader      : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
            messageProperty : 'message'	
        }
    },

    listeners   :{
        load:function(store){
            console.log(store);
            store.add({field_name:'Blank_Field'}, true);
        }
    }
});
