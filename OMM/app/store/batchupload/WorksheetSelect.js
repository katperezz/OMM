Ext.define('OMM.store.batchupload.WorksheetSelect', {
    extend  : 'Ext.data.Store',
    fields  : [       
        {name: 'worksheet_name', mapping: 'worksheet_name'}
    ],
    proxy   : {
        type    : 'ajax',
        actionMethods : 'POST',
        api     : {
            read : '../data/get_worksheet.php'
        },
        reader  : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
            messageProperty : 'message'	
        }
    }
});
