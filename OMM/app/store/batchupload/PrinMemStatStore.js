Ext.define('OMM.store.batchupload.PrinMemStatStore',{
	extend :'Ext.data.Store',
	fields :['tmp_status', 'match_status'],
	proxy :{
		type 	:'ajax',
		api 	:{
			read 	:'../data/prin_memstat_source.php'
		},
		extraParams     : {
			acc_id       : '',
			filesignature:'',
			user_id 	 :'',
			field_name 	 :''
		},
		reader 	:{
			type  		 	 :'json',
			rootProperty 	 :'data',
			successProperty  :'success',
			messageProperty  :'message'
		},
		writer :{
			type 			:'json',
			rootProperty 	:'data',
			successProperty :'success',
			messageProperty :'message'
		}
	}
});