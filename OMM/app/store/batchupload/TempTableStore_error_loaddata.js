Ext.define('OMM.store.batchupload.TempTableStore_error_loaddata',{
	extend 	:'Ext.data.Store',
	proxy	:{
		type 	:'ajax',
		actionMethods 	:'POST',
		api 	:{
			read:'../data/batch_tmptbl_data_error.php'
		},
		reader 	:{
			type 	:'json',
			rootProperty 	:'data',
			successProperty :'true',
			messageProperty :'message'
		}
	}
	

})