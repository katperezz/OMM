Ext.define('OMM.store.Get_status',{
	extend 	:'Ext.data.Store',
	storeId :'Get_status',
	fields 	:['id','status'],
	proxy 	:{
		type 	:'ajax',
		timeout :2000000,
		api 	:{
			read :'../data/get_status.php'
		},
		reader :{
			type 			:'json',
			rootProperty 	:'data',
			successProperty :'success',
			messageProperty :'message'
		},
		writer :{
			type 			:'json',
			rootProperty 	:'data',
			successProperty :'success',
			messageProperty :'message'
		}
	}
});