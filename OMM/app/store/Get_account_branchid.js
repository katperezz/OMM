Ext.define('OMM.store.Get_account_branchid',{
	extend 	:'Ext.data.Store',
	fields 	:['branch_id','branch'],
	proxy   : {
        type            : 'ajax',
        actionMethods   : 'POST',
        timeout         :300000,
        api             : {read : '../data/get_account_branchid.php'},
        actionMethods   : 'post',
        extraParams     : {
            acc_id      : '',
            branch_id   : ''
        },
        reader          : {
            type            : 'json',
            rootProperty    : 'data',
            successProperty : 'success',
            messageProperty : 'message'	
        }
}
});