Ext.define('OMM.store.batchmonitor.BatchMonitor_Info_store',{
	extend:'Ext.data.Store',
	// autoLoad: {params:{start:0, limit:10}},
	// pageSize: 10,
	proxy:{
		type:'ajax',
		api :{
            read : '../data/batch_monitor_tmptbl_data.php'
		},
		reader:{
			type:'json',
			rootProperty:'data',
			successProperty:'success',
			messageProperty:'message'
		}
	}

	
});


