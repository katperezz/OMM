Ext.define('OMM.store.batchmonitor.BatchMonitor_info_columnStore',{
	extend :'Ext.data.Store',
	fields:[
		{name:'field_name', mapping:'field_name'}
	],
	// filters: [
	// 	function(record, id){
	// 	return (record.data.field_name != "status_match");
	// 	},function(record, id){
	// 	return (record.data.field_name != "status");
	// 	},function(record, id){
	// 	return (record.data.field_name != "status_msg");
	// 	},function(record, id){
	// 	return (record.data.field_name != "id")
	// 	}
	// ],
    filters:[
	function(record, id){
	return (record.data.field_name != "status_match");
	},function(record, id){
	return (record.data.field_name != "status");
	},function(record, id){
	return (record.data.field_name != "status_msg");
	},function(record, id){
	return (record.data.field_name != "id")
	},
    function(record, id){
    	return (record.data.field_name !="id");
    }
    ],
	proxy:{
		type:'ajax',
		api:{
            read : '../data/batch_monitor_tmptbl_column.php'
		},
		extraParams     : {
			filesignature : ''
		},
		reader:{
			type:'json',
			rootProperty:'data',
			successProperty:'success',
			messageProperty:'message'
		}
	}
});

