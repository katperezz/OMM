Ext.define('OMM.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    requires: [
        'Ext.window.MessageBox'
    ],

    routes : {
        'batchupload' : 'showBatchUpload'
    },

    onMainSetWith:function(){
        var menu_main = this.lookupReference('menu_main');
            menu_main.setWidth(35);
    },

    onToggleNavigationSize:function(){
        var menu_main = this.lookupReference('menu_main'),
            menu_main_w = menu_main.getWidth();

        console.log(menu_main_w);
        if(menu_main_w>=190){
            menu_main.setWidth(35);
        }else{
            menu_main.setWidth(190);
        }
    },

    onDashBeforerender:function(){
        var login_today      = this.lookupReference('login_today');
        var mem_update_today = this.lookupReference('mem_update_today');
        Ext.Ajax.request({
            url:'../data/post_dashboard.php',
            success:function(response){
                var response = Ext.decode(response.responseText);
                console.log(response.login_today +'\n'+response.mem_update_today)
                login_today.setValue(response.login_today);
                mem_update_today.setValue(response.mem_update_today)
            },
            failure:function(response){
                var response = Ext.decode(response.responseText);                   
                conso.log('beforerender dashboard failed '+ response.message);
            }
        });
    },
    onLoadMainView:function(){
        Ext.Ajax.request({
            url:'../data/post_username.php',
            success:function(response){
                var response = Ext.decode(response.responseText);
                if(response.username=='' || response.username==null){
                    if(document.URL.indexOf("batchupload") >= 0)
                    {
                        console.log("Can't check session batch upload on going")
                    }else{
                        Ext.MessageBox.show({
                            title   :'SESSION EXPIRED',
                            msg     :'You session has been expired!<br><a href="https://www.benefitsmadebetter.com/OMM"><b>Log In here</a>',
                            closable:false,
                            icon    :Ext.Msg.WARNING
                        }); 
                    }
                }else{
                    console.log('user session is active')
                }
            },
            failure:function(){}
        });
        // if(Ext.getCmp('username').getText()=='' || Ext.getCmp('username').getText()==null){}else{console.log('user session is active')}
    },        

    showDashboard:function(){
        var tabpanel    = Ext.getCmp("ops_tabpanel");
        var pTabId      = "dashboard-tab";
        
        if(Ext.getCmp(pTabId)){
            tabpanel.setActiveTab(pTabId);
        }else{
            tabpanel.add({id:pTabId,xtype:'dashboard'});
            tabpanel.setActiveTab(pTabId);    
        }
    },

    showPrincipalManagement: function(){
        var tabpanel = Ext.getCmp("ops_tabpanel");
        var pTabId   = "principal-management-tab"
        
        if(Ext.getCmp(pTabId)){
            tabpanel.setActiveTab(pTabId);
        }else{
            tabpanel.add({id:pTabId,xtype:'principal_management'});
            tabpanel.setActiveTab(pTabId);    
        }
    },

    showDependentManagement: function(tabPanel, tab){
        var tabpanel  = Ext.getCmp("ops_tabpanel");
        var pTabId    = "dependent-management-tab";
        var activeTab = tabpanel.getActiveTab();

        // if(tab.items.findIndex.id=='dependent-management-tab'){
        // if(tab.items.)
        //     principal_update_field_form.getForm().findField('al_id')
        //     tabPanel.items.findIndex('id', activeTab.id):
        if(Ext.getCmp(pTabId)){
            tabpanel.setActiveTab(pTabId);
        }else{
            tabpanel.add({id:pTabId,xtype:'dependent_management'});
            tabpanel.setActiveTab(pTabId);    
        }
    },

    showDependentDetails:function(){
        var tabpanel = Ext.getCmp("ops_tabpanel");
        var pTabId = "dependent-details-tab"
        
        if(Ext.getCmp(pTabId)){
            tabpanel.setActiveTab(pTabId);
        }else{
            tabpanel.add({id:pTabId,xtype:'dependent_info'});
            tabpanel.setActiveTab(pTabId);    
        }

    },

    showLogs:function(){
        if (!window.location.hash) {
            this.redirectTo("#logs");
        }
        // document.location.href = "#batchupload";
        if(document.URL.indexOf("logs") >= 0)
        {
        // alert("your url contains the name franky");
            Ext.create('OMM.view.logs.LogsWindow').show();
        }
    },

    showBatchUpload:function(){
        if (!window.location.hash) {
            this.redirectTo("#batchupload");
        }
        // document.location.href = "#batchupload";
        if(document.URL.indexOf("batchupload") >= 0)
            {
                 // alert("your url contains the name franky");
                Ext.create('OMM.view.batchupload.BatchUpload').show();
            }
    },

    showSLA:function(tabPanel, tab){
        if(!window.location.hash){
            this.redirectTo("#sla");
        }
        if(document.URL.indexOf("sla") >= 0){
            Ext.create('OMM.view.sla.Sla_management').show();
        }

        // var tabpanel  = Ext.getCmp('ops_tabpanel'),
        //     pTabId    ="sla-management-tab",
        //     activeTab = tabpanel.getActiveTab();

        //     if(Ext.getCmp(pTabId)){
        //         tabpanel/setActiveTab(pTabId);
        //     }else{
        //         tabpanel.add({id:pTabId,xtype:'sla_management'});
        //         tabpanel.setActiveTab(pTabId);
        //     }

    },

    showProviderAccess:function(){
        var tabPanel    = Ext.getCmp("ops_tabpanel");

        var isMinimized   = false;
        var win           = new Ext.Window({
            closeAction   :'destroy',
            width         :'30%',
            modal         :true,
            height        :200,
            layout        :{type:'vbox',align:'stretch'},
            title         :'Generate Key for Provider',
            reference     :'prov_win',
            controller    :'main',
            scope         :this,
            initCenter    :true,
            autoShow      :true,
            titleAlign    :'center',
            closable      :false,
            items         :[
            {xtype:'displayfield',fieldLabel:'session_user',reference:'session_user',hidden:true},
            {
              xtype       :'form',
              bodyPadding :20,
              defaults    :{anchor:'100%',labelWidth:120,editable:false,emptyText:'Select Column'},
              reference   :'providerkey_form',
              items       :[
                {
                    xtype        : 'combo',
                    fieldLabel   : 'Provider name',
                    displayField : 'provider_name',
                    valueField   : 'provider_id',
                    emptyText    :'Select Provider',
                    editable     :false,
                    width        :400,allowBlank:false,
                    name         :'provider_id',
                    triggerAction: 'all',
                    store        : 'Get_providers_list',
                    afterLabelTextTpl: [
                        '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                    ]
                }
              ]
            }],
            buttons   :[{text:'Generate Key',ui:'soft-red',handler:'onProvGenKey'},
            {text:'Cancel',ui:'blue',handler:function(button,form){button.up('window').close();}}],
            listeners       :{
              beforerender  :this.onWindowRender
            }
        });
    },

    onWindowRender:function(btn){
        var session_user    = this.lookupReference('session_user');

          Ext.Ajax.request({
            url     :'../data/post_username.php',
            success :function(response){
              var response = Ext.decode(response.responseText);

              console.log(response.username);
              if(response.username==''||response.username==null){
                Ext.MessageBox.show({
                  title:'SESSION EXPIRED',
                  msg:'Your session has been expired!',
                  closable:false,
                  icon:Ext.Msg.WARNING,
                  buttonText:{ok:'Reload'},
                  fn:function(btn){
                    if(btn=='ok'){
                      window.location.href = 'https://www.benefitsmadebetter.com/OMM';
                    }}
                });
            }else{
                session_user.setValue(response.username);
                console.log("\n session_user :"+session_user.getValue())
              }
            },failure:function(){
              console.log('Batch principal update beforerender failed')
            }
          });
    },

    onProvGenKey:function(btn,form){
        var providerkey_form   = this.lookupReference('providerkey_form'),
            form               = providerkey_form.getForm(),
            values             = form.getValues(),
            session_user       = this.lookupReference('session_user'),
            prov_win           = this.lookupReference('prov_win');

        if(form.isValid()){
            Ext.Ajax.request({
                // url   :'../data/generate_key.php',
                url     :'https://www.benefitsmadebetter.com/Providersaccess/data/generate_key.php',
                method  :'POST',
                params  :{provider_id:values['provider_id'],session_user:session_user.getValue()},
                success :function(response){
                    var response = Ext.decode(response.responseText);
                    Ext.MessageBox.show({
                        title       :'Success',
                        msg         :'<b>Generated Key : </b>'+response.genkey+'<br>Generated Key may only use and show once.',
                        icon        :Ext.Msg.INFO,
                        closable    :false,
                        buttonText  :{ok:'OK'},
                        fn          :function(btn){
                            if(btn=='ok'){
                                form.reset()// prov_win.hide(); // this.up('window').close();                               
                            }
                        }
                    });
                },
                failure:function(response){
                    var response = Ext.decode(response.responseText);
                    alert('Failed', response.message)
                }
            });
        }else{
            alert('FORM IS INVALID')
        }
    }
});
