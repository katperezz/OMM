Ext.define('OMM.view.main.Main', {
    extend      : 'Ext.container.Container',
    xtype       : 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'OMM.view.main.MainController',
        'OMM.view.main.MainModel',
        'OMM.view.main.hidden.Logout_main',
        'OMM.view.principal.Principal_management',
        'OMM.view.dependent.Dependent_management',
        'OMM.view.dependent.Dependent_details',
        'OMM.view.batchupload.BatchUpload',
        'OMM.view.sla.Sla_management',
        'OMM.view.main.Dashboard',
        'OMM.view.logs.Logs'
        // 'OMM.view.provider.Provider_access'
    ],
    controller  : 'main',

    cls         : 'quick-graph-panel shadow-panel',
    layout      : {type: 'border'},
    items       : [{
        xtype       : 'panel',
        reference   :'menu_main',
        region      : 'west',
        width       :190,
        margin      :'9 0 0 0',
        bodyStyle   :{"background-color":"#293f54"},
        layout      : 'vbox',
        layoutConfig: {
            align   : 'stretch',
            pack    : 'center'
        },
        defaults    :{xtype:'button',scale:'small',width:195,height:55,ui:'soft-menu',margin:'0 5 0 3',iconAlign: "left", textAlign:'left',cls: 'my-btn',pressedCls: 'my-btn-pressed'},// style:{"background-color":"#32404e"}
        items       :[
        // <span style="font-face:arial;font-weight:bold;font-size:18px;">Inclusion</span>
        {text:'<span style="font-face:arial;font-weight:normal;font-size:12px;">&nbsp;Dashboard</span>',tooltip:'Dashboard',margin:'3 5 0 3', handler:'showDashboard', iconCls:'x-fa fa-tachometer'},
        {text:'<span style="font-face:arial;font-weight:normal;font-size:12px;">&nbsp;Principal Management</span>',tooltip:'Principal Management',iconCls:'x-fa fa-user', handler:'showPrincipalManagement'},
        {text:'<span style="font-face:arial;font-weight:normal;font-size:12px;">&nbsp;Dependent Management</span>',tooltip:'Dependent Management',iconCls:'x-fa fa-users', handler:'showDependentManagement'},
        {text:'<span style="font-face:arial;font-weight:normal;font-size:12px;">&nbsp;Batch Upload</span>',tooltip:'Batch Upload',iconCls:'x-fa fa-upload', handler:'showBatchUpload'},
        {text:'<span style="font-face:arial;font-weight:normal;font-size:12px;">&nbsp;Account SLA</span>',tooltip:'Account SLA',iconCls:'x-fa fa-bar-chart',handler:'showSLA'},
        {text:'<span style="font-face:arial;font-weight:normal;font-size:12px;">&nbsp;OMM Logs</span>',tooltip:'OMM Logs',docked  :'bottom',iconCls:'x-fa fa-history', handler:'showLogs'},
        {text:'<span style="font-face:arial;font-weight:normal;font-size:12px;">&nbsp;Provider Access</span>',disabled:true, tooltip:'Provider Access (For CS only)',iconCls:'x-fa fa-h-square', handler:'showProviderAccess'}
        ]
    },{
        region      : 'north',
        height      :30,
        xtype       : 'container',
        bodyStyle   :{"background-color":"#4dacec"},
        items       :[
        {xtype  :'logout_main'},
        {
            xtype   :'toolbar',
            cls     :'sencha-dash-dash-headerbar toolbar-btn-shadow',
            style   :{"background-color":"#4dacec"},
            itemId  :'headerBar',
            items   :[
                {
                    xtype       :'component',
                    reference   :'senchaLogo',
                    cls         :'sencha-logo',
                    html        :'<div class="main-logo"><img src="resources/images/logo.png"></div>',
                    width       :200
                },
                {
                    margin: '0 0 0 8',
                    ui          :'default',
                    border      :false,
                    cls: 'delete-focus-bg',
                    iconCls:'x-fa fa-navicon',
                    id: 'main-navigation-btn',
                    handler: 'onToggleNavigationSize'
                },
                {xtype:'tbspacer',flex:1},
                {xtype:'displayfield', id:'main_note', style:'style="color: rgb(255, 0,0); font-weight:bold',value:''},                
                {
                    xtype       :'displayfield',
                    fieldStyle  :'margin-top:15px;font-size:18px;font-weight:bold;font-face:arial;color:#FFFFFF',
                    style       :{"background-color":"#4dacec"},
                    reference   :'omm_time',
                    id          :'omm_time'
                },
                {
                    xtype       :'button',
                    id          :'username',
                    reference   :'username',
                    iconCls     :'x-fa fa-user',
                    ui          :'default',
                    border      :false,
                    height      :45,
                    menu        :[
                    {
                        xtype   :'panel',
                        width   :250,
                        layout  :{type:'vbox',pack:'center',align:'center'},
                        bbar    :{
                         layout  :{type:'vbox',pack:'center',align:'center'},
                         items:[
                            // {text:'Profile',ui:'gray',iconCls:'x-fa fa-th-large',href:'#profile',hrefTarget:'_self'},
                            // '->',
                            {text:'Log Out',ui:'gray',iconCls:'x-fa fa-sign-out',width:'100%', listeners:{
                                render:function(component){component.getEl().on('click',function(e){console.log(component);var form=Ext.getCmp('logout_main').getForm();
                                if(form.isValid()) {
                                    form.submit({
                                        url : '../data/logout.php',
                                        method : 'POST',
                                        waitMsg : 'Logging Out',
                                        success : function (response) {
                                            window.location.href = 'https://www.benefitsmadebetter.com/OMM';
                                            window.location.reload();
                                        }
                                    });
                                }
                                })}}
                            }
                         ]
                        },
                        items   :[
                            {html:'<img src="resources/icons/userprofile.png"  height="60px" width="60px">',margin:2},
                            {xtype:'displayfield',id:'user_email'}
                        ]
                    }]
                }]
            }]
    },{
        region      : 'center',
        xtype       : 'tabpanel',
        id          : 'ops_tabpanel',
        layout      : 'fit',
        margin      :'11 0 0 0',
        ui          :'viewport-tab',
        closeAction : 'hide',
        items       :[{xtype:'dashboard'}],
        listeners   :{
            afterrender:function(){

            // this.addName();
            // this.updateLayout();
            }
        }
    }],

    name:null,
    addName:function(){
        this.add({
            xtype:'panel',
            layout:'fit',
            title:'TESTPanel'
        });
        console.log('testadd')
    },

    listeners           :{
        beforerender    :'onMainSetWith',
        afterrender     :function(){

            // Clock settings
            setInterval(function(){
                Ext.getCmp('omm_time').setValue(Ext.Date.format(new Date(), 'g:i:s A')+' |');// console.log( Ext.getCmp('omm_time').getValue())
            },1000);//1sec
            
            Ext.Ajax.request({
                url     :'../data/post_username.php',
                success :function(response){
                    var response = Ext.decode(response.responseText);
                    if(response.username=='' || response.username==null){
                        Ext.getCmp('main_note').setValue('<span style="color:  #FFFFFF; font-weight:normal;font-size:12px; padding:10px">User is not LogIn  <a href="https://www.benefitsmadebetter.com/OMM"><b>Log In here</a></span>');
                        Ext.getCmp('username').hide();  
                    }else{
                        Ext.getCmp('username').show();
                        Ext.getCmp('username').setText('<div style="color: #FFFFFF">'+response.username);
                        Ext.getCmp('user_email').setValue(response.username);                        
                    }
                }
            });
        }
    }
});
