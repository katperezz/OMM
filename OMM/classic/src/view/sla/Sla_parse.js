Ext.define('OMM.view.sla.Sla_parse',{
	extend 	 	:'Ext.Panel',
	xtype 	 	:'sla_parse',
	reference 	:'sla_parse',

	requires 	:[
	    'Ext.grid.selection.Replicator',
	    'Ext.grid.plugin.Clipboard',
	    'Ext.grid.selection.SpreadsheetModel',
	    'Ext.grid.filters.Filters'
	],

	layout 		:'fit',
	items 		:[
	{
		xtype 	 	:'gridpanel',
		autoScroll 	:true,
		title 		:'SLA Parse',
		itemId 		:'sla_parse_grid',
		reference 	:'sla_parse_grid',
		plugins 	:[
			'clipboard',
			'selectionreplicator'		
		],	
		selModel 	:{
			type 	 	 :'spreadsheet',
			columnSelect :true,
			pruneRemoved :false,
			extensible 	 :'y'
		},
		store 		:Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
		viewConfig 	:{preserveScrollOnRefresh:true,preserveScrollOnReload:true,deferEmptyText:true,emptyText:'<h1>No data found</h1>',getRowClass:function(record,index){var c=record.get('status');if(c!='SUCCESS'||c==''){return'empty'}else{return'valid'}}},
		columns 	:[],
		listeners 	:{
			beforerender :'onSlaParseBeforerender',
			afterrender  :'onSlaParseAfterrender'
		}


	}
	]

});