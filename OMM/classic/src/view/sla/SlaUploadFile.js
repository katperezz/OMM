Ext.define('OMM.view.sla.SlaUploadFile',{
    extend      : 'Ext.tab.Panel',
    alias       : 'widget.slauploadfile',
    xtype       :'slauploadfile',
    reference   :'slauploadfile',
    itemId      :'slauploadfile',
    defaultType : 'form',
    layout      : 'card',
    defaults    :{border:false,cls:'batchupload-page-inner-container'},
    requires    :[
        'OMM.store.batchupload.TempTableStore'
    ],
    items: [
        {
            itemId      : 'UF_step1',
            title       :'Step 1',
            reference   :'UF_step1',
            layout      :{type:'vbox',align:'stretch'},
            defaults    :{allowBlank:false, cls:'batchupload-page-desc'},
            bodyPadding :30,
            items       : [
                {xtype:'label',style:'<margin-bottom: 30px;>',cls:'batchupload-page-title-text',text:'FILE UPLOAD'},    
                {xtype:'combo',fieldLabel:'Account name',displayField:'companyname',valueField:'companyid',emptyText:'Select account',editable:false,reference:'acc_id',name:'acc_id',triggerAction:'all',store:'Get_acc_list',afterLabelTextTpl:['<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'],listeners:{select:function(value){this.selectedC=value.getValue();console.log('selectedValue: '+this.selectedC)}}},
                {xtype:'filefield',ui:'blue',name:'batch_up_file', icon:'x-fa fa-file-excel-o'},
                {xtype:'label',style:'font-weight:bold;font-face:arial',html:'Accepted file : <span style="color:#2f9fe9; font-size:13px;">.xlsx and .xls. </span>'},
                {xtype:'label',style:'font-weight:bold;font-face:arial;',html:'NOTE : <span style="color:#2f9fe9; font-size:13px;">The file that user uploads here will serve as renewal list and validate with the existing record from the database</span>'}
            ],
            bbar    :{
                overflowHandler: 'menu',
                style:'background-color:#4dacec;',defaults:{margin:5,ui:'blue',style:'color:#ffffff;'}, 
                items:[
                    {xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;">Determine first if the workbook worksheet you uploading is protected. Advice to remove the protection before proceeding. User may also advice to unhide all columns.</span>'},
                    '->',
                    // {text:'WINDOW',handler:'onSlaValidate'},
                    {text:'Reset',handler:function(button){button.up('form').getForm().reset()}},
                    {text:'Next',listeners:{click:'onUF_step1_next'}},
                    {text:'Cancel and Exit',ui:'soft-red',handler:function(button){button.up('.window').close();document.location.href=""}}
                ]}
        },
        {
            itemId      : 'UF_step2',
            title       :'Step 2',
            disabled    :true,
            reference   :'UF_step2',
            layout      :{type:'vbox',align:'stretch'},
            defaults    :{allowBlank:false, cls:'batchupload-page-desc'},
            autoScroll  :true,
            bodyPadding :30,
            items       : [
                {xtype:'label',cls:'batchupload-page-title-text',style:'<margin-bottom: 25px;>',text:'ROW SELECTION'},
                {xtype:'label',style:'font-weight:bold;font-face:arial;font-size:13px;',html:'Complete the following required fields.'},
                {xtype:'label',style:'color:#2f9fe9;font-weight:normal;font-face:arial;font-size:13px;margin-bottom: 25px;margin-left: 10px;font-style: oblique;line-height:15px;',html:'• Select WorkSheet <br> • Input the field row, that will serve as your reference for the next step <br> • Input the first row you want to start upload <br> • Input the last row you want to upload'},
                {xtype: 'combo',store: Ext.create("OMM.store.batchupload.WorksheetSelect"),displayField: 'worksheet_name',valueField: 'worksheet_name',editable:false,fieldLabel:'Work Sheet',fieldStyle: {width: '700px'},emptyText:'Select worksheet ',name:'worksheet_name',reference:'worksheet_name',listeners:{el:{click:'onUF_worksheet_click'},select: function(value){this.selectedC = value.getValue();console.log('selectedValue: ' + this.selectedC);}}},  
                {xtype: 'numberfield',style:'font-family:arial;color:red;font-size:20px;', name : 'column_num',fieldLabel: 'Column Name',value:'1', emptyText:'Input'},
                {xtype: 'numberfield',style:'font-family:arial;color:red;font-size:20px;', name : 'first_numrow',fieldLabel: 'First Row',value:'2', emptyText:'Input'},
                {xtype: 'numberfield',style:'font-family:arial;color:red;font-size:20px;', name : 'last_numrow',fieldLabel: 'Last Row',value:'10', emptyText:'Input'},
                {xtype: 'tbspacer',flex: 1}
            ],
            bbar    :{style:'background-color:#4dacec;',overflowHandler: 'menu',defaults:{margin:5,ui:'blue'},items:[{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;">Complete the following required fields.</span>'},'->',{text:'Reset',handler:function(button){button.up('form').getForm().reset()}},{text:'Next',listeners:{click:'onUF_step2_next'}},{text:'Cancel and Exit',ui:'soft-red',handler:function(button){button.up('.window').close();document.location.href=""}}]}
        },
        {
            itemId      : 'UF_step3',
            title       :'Step 3',
            disabled    :true,
            reference   :'UF_step3',
            bodyPadding :15,
            layout      :'fit',
            tbar        :[
            {xtype:'panel',layout:{type:'vbox',align:'stretch'},bodyPadding:5,items:[
            {xtype:'label',cls:'batchupload-page-title-text',text:'FIELD CONFIGURATION'},
            // {xtype:'label',style:'color:#57b3cb;font-weight:normal;font-face:arial',text:'Modify the following fields. Set proper data type in each field row'},
            {xtype:'label',style:'color:#e44959;font-weight:normal;font-face:arial;margin-top: 5px;margin-bottom: 3px;',text:'Default Type :Text field'}
            ]}],
            items       :[
            {xtype:'form',itemId:'UF_step3_form', reference:'UF_step3_form', overflowY:'scroll',autoScroll:true,layout:{type:'vbox',align:'stretch'},defaults:{cls:'batchupload-page-desc',labelWidth:200}},
            {xtype: 'tbspacer',flex: 1}
            ],
            bbar    :{style:'background-color:#4dacec;',overflowHandler: 'menu',defaults:{margin:5,ui:'blue'},
            items:[
            {xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;">Modify the following fields. Set proper data type in each field row.</span>'},
            '->',
            {text:'Reset Fields',ui:'blue',handler:function(button){button.up('form').getForm().reset()}},{text:'Next',ui:'blue',listeners:{click:'onUF_step3_next'}},{text:'Cancel and Exit',ui:'soft-red',handler:function(button){button.up('.window').close();document.location.href=""}}]}
        }
    ]
});