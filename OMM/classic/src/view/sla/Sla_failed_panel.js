Ext.define('OMM.view.sla.Sla_failed_panel',{
	extend  	:'Ext.Panel',
	xtype 	 	:'sla_failed_panel',
	reference  	:'sla_failed_panel',

	requires 	:[
		'Ext.grid.selection.Replicator',
		'Ext.grid.plugin.Clipboard',
		'Ext.grid.selection.SpreadsheetModel',
		'Ext.grid.filters.Filters',
		'OMM.view.sla.SlaController'
	],

	controller 		:'sla',
	autoDestroy 	:true,
	closeAction 	:'destroy',
	layout 			:'fit',
	cls 			:'Principal_update-page-container',
	tbar 			:{
		layout 		:{type:'vbox',align:'stretch',pack:'start'},
		items 		:[
		{
			xtype 	 	:'panel',
			bodyStyle 	:{"background-color":"#D1F2FA","border-raduis":"10px;"},
			layout 		:'hbox',
			defaultType :'displayfield',
			defaults 	:{fieldStyle:'color:2990A8;font-weight:bold;font-face:arial;font-size:20px;',anchor:'50%',labelWidth:100,style:'padding:10px 50px 10px 50px;'},
			items 		:[
				{reference:'acc_id', hidden:true},
				{reference:'filesignature', hidden:true},
				{reference:'session_userid', hidden:true},
				{xtype:'tbspacer',width:'15%'},
				{reference:'sla_total',fieldLabel:'<span style="font-face:arial;font-weight:bold;font-size:18px;">Total rows</span>'},
				{reference:'sla_success',fieldLabel:'<span style="font-face:arial;font-weight:bold;font-size:18px;">Success</span>'},
				{reference:'sla_failed',fieldLabel:'<span style="font-face:arial;font-weight:bold;font-size:18px">Failed</span>'},
				{reference:'sla_warning',fieldLabel:'<span style="font-face:arial;font-weight:bold;font-size:18px;">Warning</span>'}
			]
		}
		]
	},
	items 			:[
	{
		xtype 		:'gridpanel',
		autoScroll 	:true,
		ui 			:'light',
		width 		:'90%',
		title 		:'FAILED RECORDS ONLY',
		reference 	:'sla_failed_grid',
		itemId 		:'sla_failed_grid',
		store 		:Ext.create('OMM.store.batchupload.TempTableStore_error_loaddata'),
		plugins 	:['clipboard','selectionreplicator'],
		selModel 	:{type:'spreadsheet',columnSelect:true,pruneRemoved:false,extensible:'y'},
		viewConfig  :{
			preserveScrollOnRefresh	:true,
			preserveScrollOnReload 	:true,
			deferEmptyText 			:true,
			emptyText 				:'<h1>No data found</h1>',
			getRowClass 			:function(record,index){
				var c=record.get('status');
				if(c=='WARNING'){
					return'warning'
				}else if(c!='SUCCESS'||c==''){
					return'empty'
				}else{
					return'valid'}
				}
		},
		columns 	:[
		{header:'status',dataIndex:'status',width:120,tdCls:'x-change-cell'},
		{header:'status_msg',dataIndex:'status_msg',width:200},
		{header:'status_match',dataIndex:'status_match',width:90}
		],
		listeners 		:{
			beforerender:'onSlaFailedGrid_beforerender',
			afterrender:'onSlaFailedGrid_afterrender'

		}
	}
	]
});

