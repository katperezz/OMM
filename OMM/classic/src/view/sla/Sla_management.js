Ext.define('OMM.view.sla.Sla_management',{
	extend 		:'Ext.window.Window',
 	alias 		:'widget.sla',
	// extend  	:'Ext.Panel',
	xtype   	:'sla_management',

	requires :[
	 	'OMM.view.sla.SlaController',
	 	'OMM.view.sla.SlaUploadFile',
	 	'OMM.view.sla.Sla_failed_panel'
	],

	controller  :'sla',
	closable 	:false,
	title 		:'Operations Management Module - ACCOUNT SLA',
	titleAlign  :'center',
	maximized 	:true,
	modal 		:true,
	closeAction :'destroy',
	layout 		:{type:'vbox', align:'center',pack:'center'},
	defaults 	:{width:'100%'},
	// sla_toolbar,
    dockedItems : [
    {
        dock:'top',xtype:'toolbar',reference:'sla_toolbar',
        items:[
        {
            iconCls:null,ui:'soft-red',text:'ReUpload',xtype:'button', reference:'menu_upload',
            handler:'onSlaUploadfile'
        },
        {
        	iconCls:null, ui:'soft-green',text:'Process', xtype:'button',reference:'menu_validate',
        	handler:'onSlaValidate'
        },
        '->',
        {text:'Close',ui:'soft-red',reference:'back',handler:function(button,form){button.up('.window').close();document.location.href=""}}
        ]}
    ],
	items 		:[
	{
		xtype 	 	:'panel',
		cls 	 	:'batchupload-page-container',
		reference  	:'sla_main_panel',
		flex 		:1,
		layout 		:'fit'
	}
	],
	listeners 	:{
		beforerender:'onSlaBeforerender'
	}
});
