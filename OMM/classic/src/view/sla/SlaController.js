var sla_unique_key = Ext.create('Ext.data.Store',{
  fields:["field", "value"],
  data:[
  	{"field":"Employee ID *", "value":"empid"},
  	{"field":"First, Last Name and Birthdate", "value":"fn_ln_db"}
  ]
});

Ext.define('OMM.view.sla.SlaController',{
	extend 	:'Ext.app.ViewController',
	alias 	:'controller.sla',	

	requires 	:[
		'Ext.util.TaskRunner'
		// 'Ext.form.action.StandardSumbit'
	],

	onSlaBeforerender:function(){
		console.log(window.console);if(window.console||window.console.firebug){console.clear()}

		var sla_main_panel = this.lookupReference('sla_main_panel'),
        sla_parse 		 = this.lookupReference('sla_parse'),
        sla_toolbar 	 = this.lookupReference('sla_toolbar');

		sla_toolbar.hide();
		sla_main_panel.add({xtype:'slauploadfile'});
		sla_main_panel.remove(sla_parse,true);
			//sla_toolbar
			//sla_parse_table
	},

	onUF_step1_next:function(form){
		var me 		= this.lookupReference('slauploadfile'),
			step1 	= this.lookupReference('UF_step1'),
			form 	  = step1.getForm(),
			l 		  = me.getLayout();

		if(form.isValid()){
			form.submit({
				url  	 	 	      :'../data/batch_up_file.php',
				waitMsg         :'Processing your request. Please wait',
				headers         :{'Content-Type':'multipart/form-data; charset=UTF-8'},
				submitEmptyText :false,
				success         :function(form,action){
					Ext.MessageBox.show({title:'Success',msg:"<b>File Uploaded!</b><br><br>"+"File Name : "+action.result.file_name+"<br>"+"File Size : "+action.result.file_size+"<br>"+"Operator : "+action.result.operator+"<br>"+"Date Modified : "+action.result.timestamp, closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}});
					l.setActiveItem(1);
					form.reset();					
					me.down('#UF_step2').setDisabled(false);
					me.down('#UF_step1').setDisabled(true);
				},failure       :function(form,action){
					Ext.MessageBox.show({title:'Failed', msg:action.result.message,closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
				}
			});
		}else{
			Ext.MessageBox.show({title:'WARNING', msg:'Someting went wrong. Please check details of required fields or Contact IT for assistant', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
		}
	},

  onUF_worksheet_click:function(){
    var worksheet_name = this.lookupReference('worksheet_name');
    worksheet_name.getStore().load();
  },

	onUF_step2_next:function(form){
		var me 			 	  = this.lookupReference('slauploadfile'),
			step2 		 	  = this.lookupReference('UF_step2'),
			form 		    	= step2.getForm(),
			l 			 	    = me.getLayout(),
			FieldConfig 	= this.FieldConfig=Ext.create('OMM.store.batchupload.FieldConfig');

		if(form.isValid()){
			form.submit({
				method 	 		 	       :'POST',
				timeout  		 	       :2000000,
				scope 	 		 	       :this,
				waitMsg		 	         :'Processing you request. Please wait',
				url 	 		 	         :'../data/post_worksheet.php',
				submitEmptyText  	   :false,
				disableCaching 	 	   :true,
				observable 		 	     :false,
				useDefaultXhrHeader  :false,
				success 			       :function(form,action){
					FieldConfig.removeAll();
					FieldConfig.on('load', this.onStoreLoad, this,{single:true});
					FieldConfig.load();
					me.down('#UF_step3').setDisabled(false);
					form.reset();
					l.setActiveItem(2);
					step2.setDisabled(true);
				},
				failure			      	 :function(form,action){
					Ext.MessageBox.show({title:'Failed', msg:action.result.message, closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
				}
			})
		}else{
			Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please check details of required fields or contact IT for assistant', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
		}
	},

  onStoreLoad: function() {
    var me=this;
    var i=0;
    var UF_step3_form=this.lookupReference('UF_step3_form');

    this.FieldConfig.each(function(record){
      var field_name=record.get('field_name');

      if(UF_step3_form.getForm().findField(field_name)){
        var field_name=field_name+"_"+i;
        me.addCombo(me,field_name);
        i++;
      }else if(field_name=='status_match' || field_name=='Status_match'){
        var field_name = field_name+"_"+i;
        me.addCombo(me,field_name);
        i++;
      }else if(field_name=='status_msg' || field_name=='Status_msg'){
        var field_name = field_name+"_"+i;
        me.addCombo(me, field_name);
        i++;
      }else if(field_name=='status' || field_name=='Status'){
        var field_name = field_name+"_"+i;
        me.addCombo(me, field_name);
        i++;
      }else if(field_name=='fname'){
        var field_name=field_name+"_"+i;
        me.addCombo(me,field_name);
        i++
      }else if(field_name=='lname'){
        var field_name=field_name+"_"+i;
        me.addCombo(me,field_name);
        i++
      }else if(field_name=='mname'){
        var field_name=field_name+"_"+i;
        me.addCombo(me,field_name);
        i++
      }else if(field_name=='birthdate'){
        var field_name=field_name+"_"+i;
        me.addCombo(me,field_name);
        i++
      }else if(field_name==null){
        var field_name="Blank_Field_"+i;
        me.addCombo(me,field_name);
        i++
      }else if(field_name==''){
        var field_name="Blank_Field_"+i;
        me.addCombo(me,field_name);
        i++
      }else if(field_name=='ID'||field_name=='id'){
        var field_name="ID_"+i;
        me.addCombo(me,field_name);
        i++
      }else{
        me.addCombo(me,field_name)
      }
    });      
  },

  addCombo:  function(panel,field_name,total){
    var me    = this.lookupReference('slauploadfile');
    var step2 = this.lookupReference('UF_step2');
    var form  = step2.getForm();
    var l     = me.getLayout();
    console.log(field_name);

    if(field_name.indexOf("date")>=0||field_name.indexOf("Date")>=0||field_name.indexOf("DATE")>=0){

      me.down('#UF_step3_form').add({
        bodyPadding   :'5px 10px 5px 10px',
        fieldLabel    :field_name,
        xtype         :'combo',
        store         :field_dataType,
        name          :field_name,
        editable      :false,
        queryMode     :'local',
        valueField    :'value',
        displayField  :'field_dataType',
        value         :'date',
        displayValue  :'Date field',
        listeners     :{
          select  :function(combo,records){
            var value   = this.getValue();
            if(value=='delete_field'){
              this.setFieldStyle('background:#e44959')
            }else{
              this.setFieldStyle('background:#FFFFFF')
            }
          }}
      });

    }else{

      me.down('#UF_step3_form').add({
        bodyPadding   :'5px 10px 5px 10px',
        fieldLabel    :field_name,
        xtype         :'combo',
        store         :field_dataType,
        name          :field_name,
        editable      :false,
        queryMode     :'local',
        valueField    :'value',
        displayField  :'field_dataType',
        value         :'VARCHAR(250)',
        displayValue  :'Text field',
        listeners     :{
          select  :function(combo,records){
            var value   = this.getValue();
            if(value=='delete_field'){
              this.setFieldStyle('background:#e44959')
            }else{
              this.setFieldStyle('background:#FFFFFF')
            }
          }}
        });
    }
  },

	onUF_step3_next:function(){
    var me              =this.lookupReference('slauploadfile'),
        step3           =this.lookupReference('UF_step3'),
        form            =step3.getForm(),
        l               =me.getLayout(),
        sla_main_panel  =this.lookupReference('sla_main_panel'),
        sla_parse       =this.lookupReference('sla_parse'),
        sla_toolbar=this.lookupReference('sla_toolbar');

    if(form.isValid){
      form.getFields().each(function(field){
        if(field.rawValue=='Delete field'){
          console.log("Deleted daw "+field.rawValue);
          field.destroy();
        }else{
          console.log(field.rawValue)
        }
      });

      form.submit({
        url     :'../data/batch_create_tmptbl.php',
        waitMsg :'Processing your request. Please wait...',
        params  :{fieldname:Ext.encode(form.getFieldValues())},
        success :function(form,action){
          var result=action.result;
          if(result.success==true){
            Ext.MessageBox.show({title:'Success',msg:"<b>Temporary table created!</b><br></br>Total record :"+result.totalparse+"<br>",closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}});
            me.down('#UF_step1').setDisabled(false);
            me.down('#UF_step2').setDisabled(true);
            me.down('#UF_step3').setDisabled(true);
            l.setActiveItem(0);
            sla_toolbar.show();
            sla_main_panel.remove(me,true);
            sla_main_panel.add({xtype:'sla_parse'});
          }else{
            Ext.MessageBox.show({title:'WARNING',msg:response.message,closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}})
          }
        },failure :function(form,action){
          var result=action.result;
          Ext.MessageBox.show({title:'WARNING',msg:result.message,closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
        }
      })
      }else{
        Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check details of form or contact IT for assistant',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}})
      }    
	},

  onSlaParseBeforerender:function(){
    var me             = this.lookupReference('slauploadfile'),
        ColumnField    = this.ColumnField = Ext.create('OMM.store.batchupload.TempTableStore'),
        sla_parse 	 = this.lookupReference('sla_parse');

    ColumnField.removeAll();
    console.log('Temporary Grid clear store');
    ColumnField.on('load',function(store,record,successful,eOpts){
    ColumnField.each(function(record){
      var field_name=record.get('field_name');
        sla_parse.down('#sla_parse_grid').headerCt.add({header:field_name,dataIndex:field_name,editor: {xtype: 'textfield'}});              
      });
    console.log(sla_parse.down('#sla_parse_grid').headerCt.getGridColumns());
    var gridColumns = sla_parse.down('#sla_parse_grid').headerCt.getGridColumns();
    for (var i = 0; i < gridColumns.length; i++) {
      if (gridColumns[i].text == 'status_match') {
        sla_parse.down('#sla_parse_grid').headerCt.items.getAt(i).setVisible(false);
      }
      if (gridColumns[i].text == 'status') {
        sla_parse.down('#sla_parse_grid').headerCt.items.getAt(i).setVisible(false);
      }
      if (gridColumns[i].text == 'status_msg') {
        sla_parse.down('#sla_parse_grid').headerCt.items.getAt(i).setVisible(false);
      }
    }
    });
    ColumnField.load();    

  },

	onSlaParseAfterrender:function(record){
		var sla_parse_grid = this.lookupReference('sla_parse_grid').getStore();
		sla_parse_grid.load();
		console.log('Temporary Grid record : ',sla_parse_grid.data);   
	},
  //END PARSE DATA

  onSlaUploadfile:function(){
    var sla_parse      = this.lookupReference('sla_parse'),
        sla_toolbar    = this.lookupReference('sla_toolbar'),
        sla_main_panel = this.lookupReference('sla_main_panel');

    Ext.Msg.confirm('ACCOUNT SLA','Are you sure you want to cancel the process and upload another file', function(answer){
      if(answer=='yes'){
        console.log(window.console);if(window.console||window.console.firebug){console.clear()}
        sla_toolbar.hide();
        sla_main_panel.add({xtype:'slauploadfile'});
        sla_main_panel.remove(sla_parse, true);
      }

    });
  },

  onSlaUniquekey:function(value, paging, params){
    if(window.console||window.console.firebug){
      console.clear()
    }
    var unique_key_opt  = this.lookupReference('unique_key_opt');
    var sla_unique_form = this.lookupReference('sla_unique_form');
    
    this.selected_update_val         = value.getValue();
    this.selected_update_raw         = value.getRawValue();
    console.log('Unique key : '+this.selected_update_val);
    
    if(this.selected_update_val=='empid'){
      sla_unique_form.removeAll();
      sla_unique_form.add({
        bodyPadding   :'5px 10px 5px 10px',
        fieldLabel    :this.selected_update_raw,
        xtype         :'combo',
        store         :Ext.create("OMM.store.batchupload.TempTableStore"),
        name          :this.selected_update_val,
        editable      :false,
        valueField    :'field_name',
        displayField  :'field_name',
        closable      :true
      });
    }else if(this.selected_update_val=='al_id'){
      sla_unique_form.removeAll();
      sla_unique_form.add({fieldLabel:'ActiveLink ID',name:'al_id'});
    }else if(this.selected_update_val=='fn_ln_db'){
      sla_unique_form.removeAll();
      sla_unique_form.add({fieldLabel:'First Name',name:'fname'},{fieldLabel:'Last Name',name:'lname'},{fieldLabel:'Birthdate',name:'birthdate'})}
  },

	//BEFORERENDER : ALL BATCH PROCESS FUNCTIONS (INCLUSION, UPDATE, DELETION)
	onSla_response_render:function(){
	  var acc_id        = this.lookupReference('acc_id');
	  var filesignature = this.lookupReference('filesignature');
	  var session_userid= this.lookupReference('session_userid');

    console.log('render');
	  Ext.Ajax.request({
	    url     :'../data/batch_response_render.php',
	    success :function(response){
	      var response = Ext.decode(response.responseText);

	      console.log(response.user_id);
	    if(response.user_id==''||response.user_id==null){
	        Ext.MessageBox.show({
	          title:'SESSION EXPIRED',
	          msg:'Your session has been expired!',
	          closable:false,
	          icon:Ext.Msg.WARNING,
	          buttonText:{ok:'Reload'},
	          fn:function(btn){
	            if(btn=='ok'){
	              window.location.href = 'https://www.benefitsmadebetter.com/OMM';
	            }}
	        });
	    }else if(response.acc_id==''||response.filesignature==''||response.acc_id==null||response.filesignature==null){
	        Ext.MessageBox.show({
	          title:'Batch session expired',
	          msg:'Your batchupload session has been expired. Please reload and upload file again.',
	          closable:false,
	          icon:Ext.Msg.WARNING,
	          buttonText:{ok:'Reload'},
	          fn:function(btn){
	            if(btn=='ok'){
	              window.location.reload();
	              // Ext.create('OMM.view.batchupload.BatchUpload').show();
	            }
	          }
	        });
	      }else{
	        acc_id.setValue(response.acc_id);
	        filesignature.setValue(response.filesignature);
	        session_userid.setValue(response.user_id);
	        console.log("acc_id :"+acc_id.getValue()+"\n session_userid :"+session_userid.getValue()+"\n filesignature :"+filesignature.getValue())
	      }
	    },failure:function(){
	      console.log('Batch principal update beforerender failed')
	    }
	  });
	},

  onSlaValidate:function(){

    var win=new Ext.Window({
    	closeAction :'hide',
    	title 	   	:'SLA Option',
    	width       :'75%',
    	height 		  :650,
    	modal       :true,
      maximizable :true,
      maximized   :true,
    	layout      :'vbox',
    	autoShow    :true,
    	autoScroll  :true,
    	overflowY 	:'scroll',
    	// scope 		:'sla',
    	controller 	:'sla',
    	defaults 	:{border:false,bodyPadding:5, width:'100%'},
    	// cls 		: 'button-container',
    	items 		:[
    	{xtype:'displayfield',fieldLabel:'acc_id',text:'acc_id',reference:'acc_id', hidden:true},{xtype:'displayfield',fieldLabel:'user_id',text:'session_userid',reference:'session_userid',hidden:true},{xtype:'displayfield',fieldLabel:'filesignature',text:'filesignature',reference:'filesignature',hidden:true},
    	{
    		fieldLabel  :'<span style="color: rgb(255, 0, 0); padding-left: 10px; font-size:13px; font-weight:normal">Select Unique key *</span>',  
    		xtype       :'combo',
    		store       :sla_unique_key,
    		name        :'unique_key_opt',
    		reference   :'unique_key_opt',
    		emptyText   :'Select Unique field/s',
    		queryMode 	:'local',
    		valueField  :'value',
    		displayField:'field',
    		width 		  :'95%',
    		labelWidth 	:200,style:'margin-left:10px;margin-top:10px;',editable:false, emptyText:'Select column',
    		listeners   :{select:'onSlaUniquekey'}
    	},
    	{
    		defaults    :{bodyPadding:'5px 10px 5px 10px',anchor:'95%', labelWidth:200, editable:false, allowBlank:false,emptyText:'Select column', store:Ext.create("OMM.store.batchupload.TempTableStore"),valueField:'field_name',displayField:'field_name'},
    		defaultType :'combo',
    		xtype       :'form',
    		reference   :'sla_unique_form',
    		itemId      :'sla_unique_form',
    		bodyPadding :5,
    		tbar        :{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',text:'Unique Key'}]},
    		items       :[]
    	},
    	{
        xtype       :'form',
        reference   :'sla_field_form',
        cls         :'Principal_update-page-container',
        defaults    :{anchor:'95%',labelWidth:200,editable:false,allowBlank:false,style:'margin-left:10px',emptyText:'Select column',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
        defaultType :'combo',
        tbar        :{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',text:'Required fields',style:'margin-top:5px;color:#e44959;font-size:15px'}]},
        items 		  :[
    			{fieldLabel:'HR Endorsed Date',name:'hr_endorsed_date',allowBlank:false},
    			{fieldLabel:'OPS Endorsed Date',name:'ops_endorsed_date',allowBlank:false},
    			{fieldLabel:'Current Effetivity Date',name:'curr_effectivity_date',allowBlank:false},
          {fieldLabel:'Current Maturity Date',name:'curr_maturity_date',allowBlank:false}
    		],
    		dockedItems	:[
    		{
    			dock:'bottom',xtype:'toolbar',
    			items :[
    				{xtype:'label',style:'font-weight:bold;font-face:arial;margin-left:10px',html:'NOTE : <span style="color:#666666;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;">Modify the appropriate column/field for the required column/fields.</span>'},
    				'->',
    				{xtype:'button',text:'Reset fields', ui:'soft-blue', handler:'sla_resetform'}
    			]
    		}
    		]
    	}
    	],
    	listeners 	:{
    		beforerender :this.onSla_response_render
    	},
    	dockedItems : [
    		{
    		dock:'bottom',xtype:'toolbar', cls:'button-container',
    		defaults :{height:240,width:210,margin:'0 10 0 10',ui:'button-container'},		
    		items:[					
    		{
    			xtype 	:'panel',
          width   :'20%',
    			items 	:[
          {
            xtype       :'button',
            cls         :'d1-btn dashboard',
            margin      :'5 50 5 40',
            text        :'A',
            handler     :'onSlaOptA'
          },
    			{
    				xtype 	  	:'panel',
    				baseCls   	:'sla_note_panel',
    				height 	  	:95,
    				bodyPadding :10,
    				items 	  	:[
    				{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#666666;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;"><br>HMO ID NUMBER WILL CHANGE.</span>'}
    				]
    			}		
    			]
    		},
    		{
    			xtype 	:'panel',
          width   :'20%',
    			items 	:[
          {
            xtype       :'button',
            cls         :'d2-btn dashboard',
            margin      :'5 50 5 40',
            top         :210,
            text        :'B',
            handler     :'onSlaOptB'
          },
    			{
    				xtype 	  	:'panel',
    				baseCls   	:'sla_note_panel',
    				height 	  	:95,
    				bodyPadding :10,
    				items 	  	:[
    				{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#666666;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;"><br>NUMBER RETAINED BUT PHYSICAL ID FOR REPLACEMENT.</span>'}
    				]
    			}						
    			]
    		},
    		{
    			xtype 	:'panel',
          width   :'20%',
    			items 	:[
          {
            xtype       :'button',
            cls         :'d3-btn dashboard',
            margin      :'5 50 5 40',
            top         :210,
            text        :'C',
            handler     :'onSlaOptC'
          },
    			{
    				xtype 	  	:'panel',
    				baseCls   	:'sla_note_panel',
    				height 	  	:95,
    				bodyPadding :10,
    				items 	  	:[
    				{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#666666;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;"><br>NO CHANGE ON HMO ID NUMBER AND CARD</span>'}
    				]
    			}						
    			]
    		},

        {
          xtype   :'panel',
          width   :'20%',
          items   :[
          {
            xtype       :'button',
            cls         :'d1-btn dashboard',
            margin      :'5 50 5 40',
            top         :210,
            text        :'D',
            handler     :'onSlaOptD'
          },
          {
            xtype       :'panel',
            baseCls     :'sla_note_panel',
            height      :95,
            bodyPadding :10,
            items       :[
            {xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#666666;font-weight:normal;font-face:arial;font-style:oblique;font-size:11px;"><br>Overlapping endorsement, Member WITHOUT "HMO ID Number" on the previous contract period. Status/Stage Scheme: (1,2,3,4)</span>'}
            ]
          }           
          ]
        },
        {
          xtype   :'panel',
          width   :'20%',
          items   :[
          {
            xtype   :'button',
            cls     :'d2-btn dashboard',
            margin  :'0 50 5 40',
            top     :210,
            text    :'E',
            handler :'onSlaOptE'
          },
          {
            xtype       :'panel',
            baseCls     :'sla_note_panel',
            height      :95,
            bodyPadding :10,
            items       :[
            {xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#666666;font-weight:normal;font-face:arial;font-style:oblique;font-size:11px;"><br> Overlapping endorsement, Member WITH "HMO ID Number" BUT "HMO ID CARD" was not released on the previous contract period. Status Scheme: (1,3,4)</span>'}
            ]
          }
          ]
        }

    		]}
    	]
    });
  },

  sla_resetform:function(){
    var sla_unique_form   =this.lookupReference('sla_unique_form'),
        unique_key_form   =sla_unique_form.getForm(),
        sla_field_form    =this.lookupReference('sla_field_form'),
        field_form        =sla_field_form.getForm(),
        unique_key_opt    =this.lookupReference('unique_key_opt');

    unique_key_opt.reset();
    unique_key_form.reset();
    sla_unique_form.removeAll();
    field_form.reset();
  },

  onSlaOptA:function(){
    var unique_key_opt    =this.lookupReference('unique_key_opt'),
        sla_unique_form   =this.lookupReference('sla_unique_form'),
        unique_key_form   =sla_unique_form.getForm(),
        sla_field_form    =this.lookupReference('sla_field_form'),
        field_form        =sla_field_form.getForm(),
        fields            =field_form.getValues(),
        TempData          =this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
        me                =this,
        setup_type        ='A',
        id                =1;

    Ext.Msg.confirm('VALIDATE RECORD','Are you want to validate records? This process may take some time. Please dont close your window while process is not yet finished', function(answer){
      if(answer=='yes'){
        if(sla_unique_form.items.items.length==0){
          Ext.MessageBox.show({title:'WARNING', msg:'Please select first unique key option.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
        }else{
          if(unique_key_form.isValid()){
            if(field_form.isValid()){

             var win  = new Ext.Window({
                closeAction   :'hide',
                modal         :true,
                maximized     :true,
                autoShow      :true,
                autoScroll    :true,
                layout        :'vbox',
                controller    :'sla',
                // scope         :this,
                reference     :'win_validate',
                listeners     :{
                  close:me.onShowSlaResult
                }
              });

             TempData.load({
              callback:function(records,operation,success){
                var total_count   = records.length;
                 console.log(records.length);
                 me.sla_AjaxRequest_getId(id,total_count,setup_type)
               }
             });
            }else{
              Ext.MessageBox.show({title:'WARNING', msg:'Please select column/field name for required fields',icon:Ext.Msg.WARNING,closable:false, buttonText:{ok:'Ok'}});
            }
          }else{
            Ext.MessageBox.show({title:'WARNING',msg:'Please select column/fieldname for selected unique key option', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
          }
        }
      }
    });
  },

  onSlaOptB:function(){
    var sla_unique_form   =this.lookupReference('sla_unique_form'),
        unique_key_form   =sla_unique_form.getForm(),
        sla_field_form    =this.lookupReference('sla_field_form'),
        field_form        =sla_field_form.getForm(),
        TempData          =this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
        setup_type        ='B',
        me                =this,
        id                =1;

    Ext.Msg.confirm('VALIDATE RECORD','Are you want to validate records? This process may take some time. Please dont close your window while process is not yet finished', function(answer){
      if(answer=='yes'){
        if(sla_unique_form.items.items.length==0){
          Ext.MessageBox.show({title:'WARNING', msg:'Please select first unique key option', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
        }else{
          if(unique_key_form.isValid()){
            if(field_form.isValid()){
              var win = new Ext.Window({
                closeAction     :'hide',
                modal           :true,
                maximized       :true,
                autoShow        :true,
                autoScroll      :true,
                layout          :'vbox',
                controller      :'sla',
                reference       :'win_validate',
                listeners       :{
                  close:me.onShowSlaResult
                }
              });

              TempData.load({
                callback:function(records, operation, success){
                  var total_count = records.length;
                  console.log(records.length);
                  me.sla_AjaxRequest_getId(id, total_count, setup_type);
                }
              });
            }else{
              Ext.MessageBox.show({title:'WARNING', msg:'Please select column/field name for required fields', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
            }
          }else{
            Ext.MessageBox.show({title:'WARNING', msg:'Please select column/field name for selected unique key options', closable:false, icon:Ext.Msg.WARNING,  buttonText:{ok:'OK'}});

          }
        }
      }
    });
  },

  onSlaOptC:function(){
    var sla_unique_form   =this.lookupReference('sla_unique_form'),
        unique_key_form   =sla_unique_form.getForm(),
        sla_field_form    =this.lookupReference('sla_field_form'),
        field_form        =sla_field_form.getForm(),
        TempData          =this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
        setup_type        ='C',
        me                =this,
        id                =1;

    Ext.Msg.confirm('VALIDATE RECORD','Are you sure want to validate records? This process may take some time. Please dont close your window while process is not yet finished', function(answer){
      if(answer=='yes'){
        if(sla_unique_form.items.items.length==0){
          Ext.MessageBox.show({title:'WARNING', msg:'Please select first unique key option', closable:false, icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
        }else{
          if(unique_key_form.isValid()){
            if(field_form.isValid()){
             var win  = new Ext.Window({
                closeAction   :'hide',
                modal         :true,
                maximized     :true,
                autoShow      :true,
                autoScroll    :true,
                layout        :'vbox',
                controller    :'sla',
                // scope         :this,
                reference     :'win_validate',
                listeners     :{
                  close:me.onShowSlaResult
                }
              });
              TempData.load({
                callback:function(records, operation, success){
                  var total_count = records.length;
                  console.log(records.length);
                  me.sla_AjaxRequest_getId(id, total_count, setup_type);
                }
              });
            }else{
              Ext.MessageBox.show({title:'WARNING', msg:'Please select column/field name for required fields', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
            }
          }else{
            Ext.MessageBox.show({title:'WARNING', msg:'Please select column/field name for selected unique key options', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
          }
        }          
      }
    });
  },

  onSlaOptE:function(){
    var sla_unique_form   =this.lookupReference('sla_unique_form'),
        unique_key_form   =sla_unique_form.getForm(),
        sla_field_form    =this.lookupReference('sla_field_form'),
        field_form        =sla_field_form.getForm(),
        values            =field_form.getValues(),
        TempData          =this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
        setup_type        ='E',
        me                =this;
        id                =1;

    console.log(values['curr_maturity_date'])
    console.log(values['curr_effectivity_date'])
    Ext.Msg.confirm('VALIDATE RECORD','Are you sure you want to validate records? This process may take some time. Please dont close your window while process is not yet complete',function(answer){
      if(answer=='yes'){
        if(sla_unique_form.items.items.length==0){
          Ext.MessageBox.show({title:'WARNING',msg:'Please select first unique key option',closable:false,icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
        }else{
          if(unique_key_form.isValid()){
            if(values['curr_maturity_date'] =='' || values['curr_effectivity_date'] =='' || values['curr_maturity_date'] ==null || values['curr_effectivity_date'] ==null){
                Ext.MessageBox.show({title:'WARNING',msg:'Please select field for the field form. Current Effetivity and Maturity Date is required',closable:false, icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});           
            }else{
               var win  = new Ext.Window({
                  closeAction   :'hide',
                  modal         :true,
                  maximized     :true,
                  autoShow      :true,
                  autoScroll    :true,
                  layout        :'vbox',
                  controller    :'sla',
                  // scope         :this,
                  reference     :'win_validate',
                  listeners     :{
                    close:me.onShowSlaResult
                  }
                });

              TempData.load({
                callback:function(records,operation,success){
                  var total_count   = records.length;
                  console.log(total_count);
                  me.sla_AjaxRequest_getId(id, total_count, setup_type);
                }
              });            
             }   


          }else{
            Ext.MessageBox.show({title:'WARNING',msg:'Please select column/field for the selected unique key options',closable:false, icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
          }
        }
      }
    });

  },

  onSlaOptD:function(){
    var sla_unique_form   =this.lookupReference('sla_unique_form'),
        unique_key_form   =sla_unique_form.getForm(),
        sla_field_form    =this.lookupReference('sla_field_form'),
        field_form        =sla_field_form.getForm(),
        values            =field_form.getValues(),
        TempData          =this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
        setup_type        ='D',
        me                =this,
        id                =1;

    Ext.Msg.confirm('VALIDATE RECORD','Are you sure you want to validate records? This process may take some time. Please dont close your window while process is not yet complete',function(answer){
      if(answer=='yes'){
        if(sla_unique_form.items.items.length==0){
          Ext.MessageBox.show({title:'WARNING',msg:'Please select first unique key option',closable:false,icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
        }else{
          if(unique_key_form.isValid()){
            if(values['curr_maturity_date'] =='' || values['curr_effectivity_date'] =='' || values['curr_maturity_date'] ==null || values['curr_effectivity_date'] ==null){
                Ext.MessageBox.show({title:'WARNING',msg:'Please select field for the field form. Current Effetivity and Maturity Date is required',closable:false, icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});           
            }else{
               var win  = new Ext.Window({
                  closeAction   :'hide',
                  modal         :true,
                  maximized     :true,
                  autoShow      :true,
                  autoScroll    :true,
                  layout        :'vbox',
                  controller    :'sla',
                  // scope         :this,
                  reference     :'win_validate',
                  listeners     :{
                    close:me.onShowSlaResult
                  }
                });

              TempData.load({
                callback:function(records,operation,success){
                  var total_count   = records.length;
                  console.log(total_count);
                  me.sla_AjaxRequest_getId(id, total_count, setup_type);
                }
              });            
             }          
          }else{
            Ext.MessageBox.show({title:'WARNING',msg:'Please select column/field for the selected unique key options',closable:false, icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
          }
        }
      }
    });
  },

	sla_AjaxRequest_getId:function(id, total_count, setup_type){
		var win  	   	    =Ext.WindowManager.getActive(),
        me 			      =this,
        win_validate  =this.lookupReference('win_validate'),
        win_result    =this.lookupReference('win_result');

		console.log('total_count : '+total_count+' id : '+id);
    if(total_count>=id){
      win.add({
        xtype :'label',
        html  :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px;">Records: '+ id
      });
      me.sla_AjaxRequest_ProcessId(id, total_count, setup_type);
    }else{
      Ext.MessageBox.show({title:'ACCOUNT SLA', msg:'Process finished. Please check details posted in Window', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
      // console.log(win_result);
      me.sla_resetform();
    }
	},

	sla_AjaxRequest_ProcessId:function(id, total_count, setup_type){
    var unique_key_opt  =this.lookupReference('unique_key_opt'),
    sla_unique_form     =this.lookupReference('sla_unique_form'),
    unique_key_form     =sla_unique_form.getForm(),
    sla_field_form      =this.lookupReference('sla_field_form'),
    field_form          =sla_field_form.getForm(),
    fields              =field_form.getValues(),
    acc_id              =this.lookupReference('acc_id'),
    filesignature       =this.lookupReference('filesignature'),
    ops                 =this.lookupReference('session_userid'),
    me                  =this;

    var sParams         ='';
    if(setup_type=='D' || setup_type=='E'){
        sParams         ={key_opt:unique_key_opt.getValue(),acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),ops:ops.getValue(),keys:Ext.encode(unique_key_form.getFieldValues()),id:id,setup_type:setup_type,curr_effectivity_date:fields['curr_effectivity_date'],curr_maturity_date:fields['curr_maturity_date']};
      }else{
        sParams         ={key_opt:unique_key_opt.getValue(),acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),ops:ops.getValue(),keys:Ext.encode(unique_key_form.getFieldValues()),id:id,setup_type:setup_type,hr_endorsed_date:fields['hr_endorsed_date'],ops_endorsed_date:fields['ops_endorsed_date'],curr_effectivity_date:fields['curr_effectivity_date'],curr_maturity_date:fields['curr_maturity_date']};
      }

    Ext.Ajax.request({
      url             :'../data/sla_batch_setup.php',
      method          :'POST',
      params          :sParams,
      submitEmptyText :false,
      success         :function(response){
        var response  = Ext.decode(response.responseText),
            result    = response.data[0].result;

        console.log('ID :'+id+'\nResult : '+result);
        me.sla_AjaxRequest_CallSuccess(id,total_count,setup_type,result)
      },failure:function(response){
        var response  = Ext.decode(response.responseText);
        console.log(response);
        me.sla_AjaxRequest_CallSuccess(id,total_count,setup_type,result);
        console.log('Failed!');
      }
    });
	},

  sla_AjaxRequest_CallSuccess:function(id, total_count, setup_type,result){
    var win   =Ext.WindowManager.getActive(),
        me    =this;

    win.add({
      xtype   :'label',
      html    :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top:5px;margin-left:5px;">'+result
    });
    me.sla_AjaxRequest_getId(id + 1, total_count, setup_type);
  },

  onShowSlaResult:function(){
    var win_result = new Ext.Window({
      closeAction   :'hide',
      modal         :true,
      maximized     :true,
      autoScroll    :true,
      autoShow      :true,
      layout        :'fit',
      title         :'ACCOUNT SLA RESULT',
      reference     :'win_result',
      listeners     :{
        beforerender:function(){
          if(window.console||window.console.firebug){console.clear()}
          console.log(this);
          this.add({xtype :'sla_failed_panel'})
        }
      }
     });
  },
  //END SLA PROCESS

  //SLA FAILED RESULT RENDER
  onSlaFailedGrid_beforerender:function(){
    var ColumnField             =this.ColumnField = Ext.create('OMM.store.batchupload.TempTableStore');
        sla_failed_panel        =this.lookupReference('sla_failed_panel'),
        sla_failed_grid         =this.lookupReference('sla_failed_grid'),
        me                      =this;

      console.log('before');
      ColumnField.removeAll();
      console.log('TemTable error grid clear store');
      ColumnField.on('load',function(store, record, successful, eOpts){
        ColumnField.each(function(record){
          var field_name  =record.get('field_name');
          sla_failed_grid.headerCt.add({header:field_name, dataIndex:field_name,filter:'list', editor: {xtype: 'textfield'}});
          // sla_failed_panel.down('#sla_failed_grid').headerCt.add({header:field_name, dataIndex:field_name,filter:'list', editor: {xtype: 'textfield'}});
        });
      });
      ColumnField.load();
  },

  onSlaFailedGrid_afterrender:function(){
    var acc_id          =this.lookupReference('acc_id'),
        filesignature   =this.lookupReference('filesignature'),
        session_userid  =this.lookupReference('session_userid'),
        total           =this.lookupReference('sla_total'),
        success         =this.lookupReference('sla_success'),
        failed          =this.lookupReference('sla_failed'),
        warning         =this.lookupReference('sla_warning'),
        sla_failed_grid =this.lookupReference('sla_failed_grid').getStore();
        // sla_failed_panel = this.lookupReference('sla_failed_panel');    

    Ext.Ajax.request({
      url     :'../data/batch_response_render.php',
      success :function(response){
        var response=Ext.decode(response.responseText);
        console.log(response.user_id);
        if(response.user_id==''||response.user_id==null){
          Ext.MessageBox.show({
            title     :'SESSION EXPIRED',
            msg       :'Your session has been expired!',
            closable  :false,
            icon      :Ext.Msg.WARNING,
            buttonText:{ok:'Reload'},
            fn        :function(btn){
              if(btn=='ok'){
                window.location.href='https://www.benefitsmadebetter.com/OMM'
              }
            }
          });
        }else if(response.acc_id==''||response.filesignature==''||response.acc_id==null||response.filesignature==null){
          Ext.MessageBox.show({
            title      :'Batch session expired',
            msg        :'Your batchupload session has been expired. Please reload and upload file again.',
            closable   :false,
            icon       :Ext.Msg.WARNING,
            buttonText :{ok:'Reload'},
            fn         :function(btn){
              if(btn=='ok'){
                window.location.reload()
              }
            }
          });
        }else{
          acc_id.setValue(response.acc_id);
          filesignature.setValue(response.filesignature);
          session_userid.setValue(response.user_id);
          console.log("acc_id :"+acc_id.getValue()+"\n session_userid :"+session_userid.getValue()+"\n filesignature :"+filesignature.getValue());
          
          var sParams='';
              sParams={acc_id:acc_id.getValue(),session_userid:session_userid.getValue(),filesignature:filesignature.getValue()};

          Ext.Ajax.request({
            url     :'../data/batch_processed_summary.php',
            params  :sParams,
            success :function(response){
              var response=Ext.decode(response.responseText);
              total.setValue(response.total_rows);
              success.setValue(response.success_rows);
              failed.setValue(response.failed_rows);
              warning.setValue(response.warning_rows);
              sla_failed_grid.load({params:sParams})
            },failure:function(response){
              Ext.MessageBox.show({
                title     :'WARNING',
                msg       :'Cant show result',
                icon      :Ext.Msg.WARNING,
                closable  :false,
                buttonText:{ok:'OK'}
              });
            }
          });
          }
      },failure:function(){
        console.log('Beforerender failed')
      }
    });
  }
  //END SLA RENDER
	
});

