Ext.define('OMM.view.logs.Logs_batchprocess',{
	extend 		:'Ext.Panel',
	xtype 		:'logs_batchprocess',

	requires 	:[
	    'Ext.grid.selection.Replicator',
	    'Ext.grid.plugin.Clipboard',
	    'Ext.grid.selection.SpreadsheetModel',
	    'Ext.grid.filters.Filters'
    ],

	reference 	:'logs_batchprocess',
	layout 	  	:'fit',
	bodyPadding :1,

	items 		:[
	{
		xtype        :'gridpanel',
		autoScroll 	 :true,
		overflowY  	 :'scroll',	
		itemId 		 :'logs_batchprocess_grid',
		reference  	 :'logs_batchprocess_grid',
		ui           :'light',
		tools        :[
			{xtype:'displayfield',reference:'logs_batchprocess_ops',margin:5,labelWidth:25,fieldLabel:' ',beforeLabelTextTpl:['<i class="fa fa-user" style="font-size:15px;color:#467ead;"></i>']},
			{xtype:'displayfield',reference:'logs_batchprocess_date',margin:5,labelWidth:25,fieldLabel:' ',beforeLabelTextTpl:['<i class="fa fa-clock-o" style="font-size:15px;color:#467ead;"></i>']}
		],
		plugins 	 : [
			'clipboard',
			'selectionreplicator'
		],
		selModel 	 : {
			type: 'spreadsheet',
			columnSelect: true,
			pruneRemoved: false,
			extensible: 'y'
		},
		store 	 	 :Ext.create('OMM.store.logs.Activity_batchprocess_loaddata'),
		viewConfig 	 :{preserveScrollOnRefresh:true,preserveScrollOnReload:true,deferEmptyText:true,emptyText:'<h1>No data found</h1>',getRowClass:function(record,index){var c=record.get('status');if(c!='SUCCESS'||c==''){return'empty'}else{return'valid'}}},
		columns 	 :[],
	    listeners    :{
	        beforerender:'onLogProccessBeforerender',
	        afterrender:'onLogProccessAfterrender'
	    }
	}]
});