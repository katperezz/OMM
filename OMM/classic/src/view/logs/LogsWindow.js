Ext.define('OMM.view.logs.LogsWindow',{
	extend 		:'Ext.window.Window',
 	alias 		:'widget.batchupload',
 	xtype 		:'logswindow',
    routeId     :'logs',

 	requires 	:[
        'OMM.view.logs.LogsController'
 	],
    controller  :'logs',
    autoShow    :true,
    cls         :'batchupload-page-container',
    closable    :false,
    title       :'Operations Management Module - Auditrail',
    titleAlign  :'center',
    maximized   :true,
    modal       :true,
    closeAction :'destroy',
    layout      : {type: 'vbox',align: 'center',pack: 'center'},
    defaults    :{width:'100%'},
    tbar        :{
        layout  :{type:'vbox',align:'stretch',pack:'start'},
        items   :[
        {
            xtype       :'form',
            ui          :'light',
            defaultType :'displayfield',
            reference   :'act_accnt_stat_form',
            height      :85,
            items:[
            {
                xtype       :'panel',
                layout      :'hbox',
                defaults    :{labelWidth:120, margin:'3 2 1 2',fieldStyle:'color:#2990A8;font-weight:normal; font-face:arial; font-size:11px;',labelStyle:'color:#2990A8;font-weight:normal; font-face:arial; font-size:11px;'},
                items       :[
                {xtype:'tbspacer',width:'20%'},                
                {
                    fieldLabel  :'Start and End Date',
                    xtype       :'datefield',
                    value       :'1910-01-01',
                    minValue    : '1910-01-01',
                    maxValue    : new Date(),
                    name        :'start_date', 
                    format      :'Y-m-d H:i:s'
                },{
                    xtype       :'datefield',
                    value       :new Date(),
                    minValue    : '1910-01-01',
                    name        :'end_date', 
                    format      :'Y-m-d H:i:s'
                },
                {
                    xtype            : 'combo',
                    displayField     : 'companyname',
                    valueField       : 'companyid',
                    emptyText        :'Select account',
                    editable         :false,
                    name             :'acc_id',
                    // id               :'acc_id_act',
                    reference        :'acc_id_act',
                    triggerAction    : 'all',
                    value            :'All',
                    rawValue         :'All',
                    store            : 'Get_acc_list',  
                    listeners        :{
                        render: function (field) {
                            field.setValue('all');
                        },
                        el:{click:'onAct_accnt'}
                    }
                },
                {
                    xtype           :'button',
                    iconCls         :'x-fa fa-search',
                    handler         :'onSearchAccountStat'
                }
                ]
            },
            {
                xtype       :'panel',
                layout      : {type: 'hbox',align: 'center',pack: 'center'},
                defaultType :'displayfield',
                reference   :'log_action_cat_panel',
                // height      :55,// bodyPadding :'0px 10px 0px 10px',
                overflowX   :'scroll',
                defaults    :{fieldStyle:'color:#2990A8;font-weight:bold; font-face:arial; font-size:15px;',anchor:'100%',labelWidth:'100%',style:'padding:10px 15px 10px 15px; font-size:15px; font-weight:bold;'},
                items       :[
]
            }]
        }]
    },
    tools       : [
    {
        type    :'gear',
        tooltip :'Show/Hide Statistics',
        handler :'onShowHideStat'
    },
    {
        type    : 'close',
        handler :function(){
            this.up('window').close();
            document.location.href=""
        }
    }],
    items 		:[
    {
        xtype    :'panel',
        cls      :'batchupload-page-container',
        reference:'logs_main_panel',
        flex     :1,
        layout   :'fit'// overflowY:'scroll'
    }
    ],
    listeners   :{
        beforerender    :'onLogsWindow_render'
    }
});