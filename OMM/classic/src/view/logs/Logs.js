var itemsPerPage=10;
var me 	= this;
Ext.create('Ext.data.Store',{
    id      :'activity_store',
    // fields:['activelink_id','emp_id','fname','mname','lname','ename','fullname','bdate','gender','branch_id','mbr_status','hmo_id','company_id'],
    pageSize:itemsPerPage,
    proxy   :{
        type    :'ajax',
        url     :'../data/activity_search.php',
        reader  :{
            type            :'json',
            rootProperty    :'data',
            messageProperty :'message',
            successProperty :'success',
            totalProperty   :'total'
        }
    }
});

function onActmem_grid_add_column(){
	var actmem_search_tbar = Ext.getCmp('actmem_search_tbar'),
		actmem_grid 	   = Ext.getCmp('actmem_grid'),
		ops 			   = Ext.getCmp('ops'),
		form 			   = actmem_search_tbar.getForm(),
		values 			   = form.getValues();

	if(values['isPrincipal_opt']==1){
		console.log('principal')
		actmem_grid.show();
		actmem_grid.headerCt.removeAll();			
		actmem_grid.headerCt.add(
			{xtype:'rownumberer', width:45},
			{header:'activelink_id', dataIndex:'activelink_id', flex:1},
			{header:'emp_id', dataIndex:'emp_id', flex:1},
			{header:'fullname', dataIndex:'fullname', flex:1},
			{header:'branch_id', dataIndex:'branch_id', flex:1},
			{header:'mbr_status', dataIndex:'mbr_status', flex:1},
			{header:'company_id', dataIndex:'company_id', flex:1}
		);
		// actmem_grid.store.load({params:{page:actmem_grid.store.currentPage+1,search_val:values['search_val'], isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()}});
	}else if(values['isPrincipal_opt']==0){
		console.log('Dependent')
		actmem_grid.show();
		actmem_grid.headerCt.removeAll();
		actmem_grid.headerCt.add(
			{xtype:'rownumberer', width:45},
			{header:'dep_activelink_id', dataIndex:'activelink_id', flex:1},
			{header:'prin_activelink_id', dataIndex:'prin_activelinkid', flex:1},
			{header:'emp_id', dataIndex:'emp_id', flex:1},
			{header:'fullname', dataIndex:'fullname', flex:1},
			{header:'branch_id', dataIndex:'branch_id', flex:1},
			{header:'mbr_status', dataIndex:'mbr_status', flex:1},
			{header:'company_id', dataIndex:'company_id', flex:1},
			{header:'plan_level', dataIndex:'plan_level', flex:1}
		);
		// page:actmem_grid.store.currentPage,
		// actmem_grid.store.load({params:{page:actmem_grid.store.currentPage+1,search_val:values['search_val'], isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()}});
	}
}

Ext.define('OMM.view.logs.Logs',{
    extend      : 'Ext.tab.Panel',
	// title 	 	:'OMM LOGS',
	xtype 	 	:'logs_tab',
	reference  	:'logs_tab',
    defaultType : 'form',
	requires 	:[
		'OMM.view.logs.LogsController',
		'OMM.view.logs.Logs_details',
		'OMM.view.logs.Logs_batchprocess',
		'Ext.toolbar.Paging'
	],

	controller  :'logs',
	layout  	:'fit',
	items 	 	:[
    {
        itemId      : 'act_mem',
        title       :'Activity',
        reference   :'act_mem',
        border 		:false,
        layout 		:'fit',
        defaults    :{allowBlank:false, cls:'batchupload-page-desc'},            
		tbar 		:[
		{
			xtype 	:'panel',
			items 	:[
			{
			    xtype 	 	:'form',
			    layout 	 	:'hbox',
			    reference   :'actmem_search_tbar',
			    id 		    :'actmem_search_tbar',
		        bodyPadding :5,
		        defaults    :{anchor:'95%',labelWidth:60,allowBlank:false, labelAlign:'bottom', margin:5},
			    items 	 	:[
				{ 	fieldLabel 	 	:'Search',
					xtype 		 	:'textfield',
					name 		 	:'search_val',
					reference 	 	:'mem_search_val',
					emptyText 	 	:'Search Member',
					enableKeyEvents :true,
					triggers 		:{actmem_search:{cls:Ext.baseCSSPrefix+'form-search-trigger',handler:'onActMem_search'}},
					inputAttrTpl 	:" data-qtip='Search Value: <br> • emp_number <br> • lastname <br> • firstname <br> • middlename'",
					listeners 		:{keyup:'onActMem_search_enter'}
				},
				{
				    xtype       : 'fieldcontainer', 
				    allowBlank  :false,
				    width 	    :210,
				    defaultType :'radiofield',
				    defaults    :{flex:1},
				    layout      :'hbox',
				    items       :[
				    {
				        boxLabel    :'Principal',
				        margin      :'0 0 0 10',
				        checked 	:true,
				        name        :'isPrincipal_opt',
				        inputValue :'1'
				    },
				    {
				        boxLabel    :'Dependent',
				        name        :'isPrincipal_opt',
				        inputValue :'0'
				    }]
				}]
			},
			{xtype:'displayfield',reference:'ops', id:'ops',hidden:true},
			{xtype:'label',style:'font-weight:bold;font-size:11px;font-face:arial',html:'NOTE : <span style="color:#57b3cb;font-weight:bold;font-face:arial;font-size:11px;font-style:oblique;">Double click row to view member logs</span>'}
		]}
		],
		items 	:[
		{
			xtype 	 	 	:'gridpanel',
		    store    		:'activity_store',
			reference  	 	:'actmem_grid',
			hidden 	 	 	:true,
			border 	 		:false,
			overflowY 		:'scroll',
			columnLines  	:false,
			rowLines 	 	:true,
			headerBorders  	:false,
			plugins 	 	:[
				'gridfilters',
				'clipboard',
				'selectionreplicator'
			],
			selModel 	 	:{
				type 		:'spreadsheet',
				columnSelect:true,
				pruneRemoved:false,
				extensible 	:'y'
			},
			viewConfig    	:{preserveScrollOnRefresh:true,preserveScrollOnReload:true,deferEmptyText:true,emptyText:'<h1>No data found</h1>',trackOver: false}, 			
			columns   		:[],
			listeners  		:{
				rowdblclick :'onActMem_dbclick'
			},
			dockedItems : [{
			    xtype       : 'pagingtoolbar',
			    dock        : 'bottom',
			    store       :'activity_store',
			    displayInfo : true,
			    controller 	:'logs',
				// me.store.getProxy().extraParams={search_val:values['search_val'],isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()};
				moveNext : function(){
					var actmem_search_tbar = Ext.getCmp('actmem_search_tbar'),
						actmem_grid 	   = this.lookupReference('actmem_grid'),
						ops 			   = Ext.getCmp('ops'),
						form 			   = actmem_search_tbar.getForm(),
						values 			   = form.getValues(),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						next  			   = me.store.currentPage + 1;

						console.log(next);
					if (next <= total) {
					    if (me.fireEvent('beforechange', me, next) !== false) {			        
					        me.store.nextPage({params:{search_val:values['search_val'], isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()}});
					    	onActmem_grid_add_column
					    }
					}
				},
				movePrevious:function(){
					var actmem_search_tbar = Ext.getCmp('actmem_search_tbar'),
						actmem_grid 	   = this.lookupReference('actmem_grid'),
						ops 			   = Ext.getCmp('ops'),
						form 			   = actmem_search_tbar.getForm(),
						values 			   = form.getValues(),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						previous  		   = me.store.currentPage - 1;

						console.log(previous);
					if (previous <= total) {
					    if (me.fireEvent('beforechange', me, previous) !== false) {					              
					        me.store.previousPage({params:{search_val:values['search_val'], isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()}});
					    	onActmem_grid_add_column
					    }
					}
				},
				moveFirst:function(){
					var actmem_search_tbar = Ext.getCmp('actmem_search_tbar'),
						actmem_grid 	   = this.lookupReference('actmem_grid'),
						ops 			   = Ext.getCmp('ops'),
						form 			   = actmem_search_tbar.getForm(),
						values 			   = form.getValues(),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						current 		   = me.store.currentPage;

					console.log(current)
						me.store.getProxy().extraParams = {
							search_val:values['search_val'], 
							isPrincipal_opt:values['isPrincipal_opt'],
							ops:ops.getValue()
						};
						me.store.loadPage(1);
						onActmem_grid_add_column								
				},
				moveLast:function(){
					var actmem_search_tbar = Ext.getCmp('actmem_search_tbar'),
						actmem_grid 	   = this.lookupReference('actmem_grid'),
						ops 			   = Ext.getCmp('ops'),
						form 			   = actmem_search_tbar.getForm(),
						values 			   = form.getValues(),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						current 		   = me.store.currentPage;

					console.log(current)
					me.store.getProxy().extraParams = {
						search_val:values['search_val'], 
						isPrincipal_opt:values['isPrincipal_opt'],
						ops:ops.getValue()
					};
					me.store.loadPage(total);
					onActmem_grid_add_column
				},
				doRefresh : function(){
					var actmem_search_tbar = Ext.getCmp('actmem_search_tbar'),
						actmem_grid 	   = this.lookupReference('actmem_grid'),
						ops 			   = Ext.getCmp('ops'),
						form 			   = actmem_search_tbar.getForm(),
						values 			   = form.getValues(),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						current 		   = me.store.currentPage;

					console.log(current)
					if (me.fireEvent('beforechange', me, current) !== false) {
						me.store.getProxy().extraParams = {
							search_val:values['search_val'], 
							isPrincipal_opt:values['isPrincipal_opt'],
							ops:ops.getValue()
						};
						me.store.loadPage(current);
						onActmem_grid_add_column
					}
				}
			}]
		}]
    }
	],

	listeners   :{
		beforerender:'onLogs_render'
	}
});


