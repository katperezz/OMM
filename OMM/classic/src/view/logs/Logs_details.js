Ext.define('OMM.view.logs.Logs_details',{
	extend      : 'Ext.form.Panel',
    xtype       : 'logs_details',
    reference   : 'logs_details',
    ui          :'light',
    requires    :['OMM.view.logs.LogsController'],
	layout      :'fit',
	bodyPadding :5,
    border      :true,
    defaults    : {anchor: '100%'},
    controller  :'logs',
    listeners   :{
    }
});