var itemsPerPage=10;
var activity_store =Ext.create('Ext.data.Store',{
	pageSize:itemsPerPage,
    proxy   :{
    type    :'ajax',
    url     :'../data/activity_search.php',
    reader  :{
        type            :'json',
        rootProperty    :'data',
        messageProperty :'message',
        successProperty :'success',
        totalProperty   :'total'
    }}
});
var activity_search_details =Ext.create('Ext.data.Store',{
	pageSize:itemsPerPage,
    proxy   :{
    type    :'ajax',
    url     :'../data/activity_search_details.php',
    reader  :{
        type            :'json',
        rootProperty    :'data',
        messageProperty :'message',
        successProperty :'success',
        totalProperty   :'total'
    }}
});

Ext.define('OMM.view.logs.LogsController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.logs',

    onLogsWindow_render:function(btn){
		console.log(window.console);if(window.console||window.console.firebug){console.clear()}
		var logs_main_panel  	= this.lookupReference('logs_main_panel'),
		log_action_cat_panel 	= this.lookupReference('log_action_cat_panel'),
			me 				 	= this;

		logs_main_panel.add({xtype:'logs_tab'});
		me.onSearchAccountStat();
    },

    onShowHideStat:function(){
    	var act_accnt_stat_form = this.lookupReference('act_accnt_stat_form');

    	if(act_accnt_stat_form.hidden==true){
    		act_accnt_stat_form.show();
    	}else{
    		act_accnt_stat_form.hide();
    	}
    },

	onAct_accnt:function(value, paging, params){
		var acc_id_act = this.lookupReference('acc_id_act');		

		if(acc_id_act.getStore().data.items[0].data['companyname']!='All'){
			acc_id_act.getStore().add({companyname: 'All', companyid: 'All'},true); 
		}                         
	},

    onLogs_render:function(){
    	var ops  	 	= this.lookupReference('ops');

		Ext.Ajax.request({
			url     :'../data/post_username.php',
			success :function(response){
				var response 		 = Ext.decode(response.responseText),
		    		details  	 	 = {};
		    		details.ops 	 = response.username;

				ops.setValue(response.username);
				// console.log(ops.getValue());
				localStorage.setItem("OMM_logs_ops",Ext.encode(details));
			}
		});
    },

    onSearchAccountStat:function(btn){
		var form  			  	  = this.lookupReference('act_accnt_stat_form'),// btn.up('form'),
			values 			  	  = form.getValues(),
			OMM_logs_ops   		  = localStorage.getItem("OMM_logs_ops"),
			obj               	  = JSON.parse(OMM_logs_ops),
			log_action_cat_panel  = this.lookupReference('log_action_cat_panel'),
			Category  		 	  = this.Category=Ext.create('OMM.store.logs.Activity_action_category');

            // extraParams
            log_action_cat_panel.removeAll();
            Category.removeAll();
            Category.getProxy().extraParams = {ops:obj,start_date:values['start_date'], end_date:values['end_date'], acc_id:values['acc_id']};
			Category.on('load',function(store,record,successful,eOpts){
			  Category.each(function(record){
			    var action_category  = record.get('action_category'),
			    	logs_count   	 = record.get('logs_count');
			    log_action_cat_panel.add({
			    	fieldLabel:'<span style="font-face:arial;font-weight:bold;font-size:15px;">'+action_category+'</span>', value:logs_count
			    })
			  });
			});
			Category.load();
    },

    onActMem_search:function(t,e,o, btn){
		var actmem_search_tbar = this.lookupReference('actmem_search_tbar'),
			actmem_grid 	   = this.lookupReference('actmem_grid'),
			logs_details 	   = this.lookupReference('logs_details'),
			ops 			   = this.lookupReference('ops'),
			form 			   = actmem_search_tbar.getForm(),
			values 			   = form.getValues();

    	// console.log(memact_search_tbar.getForm().getValues());
		if(logs_details!=null){
			logs_details.hide();			
		}
		if(values['isPrincipal_opt']==1){
			actmem_grid.show();
			// actmem_grid.headerCt.add({header:'activelink_id',dataIndex:'activelink_id'}); // actmem_grid.setStore(activity_store);
			actmem_grid.headerCt.removeAll();			
			actmem_grid.headerCt.add(
			    {xtype:'rownumberer', width:45},
				{header:'activelink_id', dataIndex:'activelink_id', flex:1},
				{header:'emp_id', dataIndex:'emp_id', flex:1},
				{header:'fullname', dataIndex:'fullname', flex:1},
				{header:'branch_id', dataIndex:'branch_id', flex:1},
				{header:'mbr_status', dataIndex:'mbr_status', flex:1},
				{header:'company_id', dataIndex:'company_id', flex:1}
			);
			actmem_grid.store.load({params:{page:actmem_grid.store.currentPage,search_val:values['search_val'], isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()}});
		}else if(values['isPrincipal_opt']==0){
			actmem_grid.show();
			actmem_grid.headerCt.removeAll();
			actmem_grid.headerCt.add(
			    {xtype:'rownumberer', width:45},
				{header:'dep_activelink_id', dataIndex:'activelink_id', flex:1},
				{header:'prin_activelink_id', dataIndex:'prin_activelinkid', flex:1},
				{header:'emp_id', dataIndex:'emp_id', flex:1},
				{header:'fullname', dataIndex:'fullname', flex:1},
				{header:'branch_id', dataIndex:'branch_id', flex:1},
				{header:'mbr_status', dataIndex:'mbr_status', flex:1},
				{header:'company_id', dataIndex:'company_id', flex:1},
				{header:'plan_level', dataIndex:'plan_level', flex:1}
			);
			actmem_grid.store.load({params:{page:actmem_grid.store.currentPage,search_val:values['search_val'], isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()}});
		}		
		// actmem_grid.show();
		// actmem_grid.store.load({params:{page:actmem_grid.store.currentPage,search_val:values['search_val'], isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()}});
    },
    onActMem_search_enter:function(t,e,o){
    	var me  = this;
        if(e.getKey()==e.ENTER){
        	me.onActMem_search();
        }            
    },

    onActmem_grid_pagination:function(){
		var actmem_search_tbar = Ext.getCmp('actmem_search_tbar'),
			actmem_grid 	   = Ext.getCmp('actmem_grid'),
			ops 			   = Ext.getCmp('ops'),
			form 			   = actmem_search_tbar.getForm(),
			values 			   = form.getValues();

		// actmem_grid.store.load({params:{page:actmem_grid.store.currentPage,search_val:values['search_val'], isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()}});
		if(values['isPrincipal_opt']==1){
			// console.log('principal')
			actmem_grid.show();
			actmem_grid.headerCt.removeAll();			
			actmem_grid.headerCt.add(
				{xtype:'rownumberer', width:45},
				{header:'activelink_id', dataIndex:'activelink_id', flex:1},
				{header:'emp_id', dataIndex:'emp_id', flex:1},
				{header:'fullname', dataIndex:'fullname', flex:1},
				{header:'branch_id', dataIndex:'branch_id', flex:1},
				{header:'mbr_status', dataIndex:'mbr_status', flex:1},
				{header:'company_id', dataIndex:'company_id', flex:1}
			);
			actmem_grid.store.load({params:{page:actmem_grid.store.currentPage,search_val:values['search_val'], isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()}});
		}else if(values['isPrincipal_opt']==0){
			// console.log('Dependent')
			actmem_grid.show();
			actmem_grid.headerCt.removeAll();
			actmem_grid.headerCt.add(
				{xtype:'rownumberer', width:45},
				{header:'dep_activelink_id', dataIndex:'activelink_id', flex:1},
				{header:'prin_activelink_id', dataIndex:'prin_activelinkid', flex:1},
				{header:'emp_id', dataIndex:'emp_id', flex:1},
				{header:'fullname', dataIndex:'fullname', flex:1},
				{header:'branch_id', dataIndex:'branch_id', flex:1},
				{header:'mbr_status', dataIndex:'mbr_status', flex:1},
				{header:'company_id', dataIndex:'company_id', flex:1},
				{header:'plan_level', dataIndex:'plan_level', flex:1}
			);
			actmem_grid.store.load({params:{page:actmem_grid.store.currentPage,search_val:values['search_val'], isPrincipal_opt:values['isPrincipal_opt'],ops:ops.getValue()}});
		}
    },

    onActMem_dbclick:function(row, r, tr, rowIndex, e, eOpts,grid,record,colIndex){
    	var act_mem      	 	 = this.lookupReference('act_mem'),
			actmem_grid      	 = this.lookupReference('actmem_grid'),
			actmem_search_tbar 	 = this.lookupReference('actmem_search_tbar'),
			form 			     = actmem_search_tbar.getForm(),
			values 				 = form.getValues(),
			me 					 = this;
			fullname 			 = r.data['fullname'];

    	actmem_grid.hide();
    	act_mem.add({
    		xtype 	 	:'logs_details',
    		title 	 	:'Member Details',
			controller    :'logs',
			scope         :this,
			tools       : [
			{
			    text 	:'Back',
			    xtype 	:'button',
			    scale 	:'small',
			    ui 		:'blue',
			    handler :function(){
			        this.up('panel').hide();
			        actmem_grid.show();
			    }
			}],
			tbar        :{
				layout  :{type:'vbox',align:'stretch',pack:'start'},
				items   :[
				{
					xtype 	 :'panel',
					bodyStyle:{"background-color":"#D1F2FA"},
					layout 	 :'hbox',defaultType:'displayfield',
					defaults :{fieldStyle:'color:#2990A8;font-weight:normal; font-face:arial; font-size:13px;',labelWidth:80,style:'padding:5px 10px 5px 10px;'},
					items:[
						{fieldLabel:'<b>Fullname</b>', value:r.data['fullname']},
						{fieldLabel:'<b>EmpId</b>', value:r.data['emp_id']},
						{fieldLabel:'<b>Company</b>', value:r.data['company_id']},
						{fieldLabel:'<b>Principal Name</b>',labelWidth:120, value:r.data['principal_name '], reference:'log_details_prin_name'}
					]
				}]
			},
			items:[
			{
				xtype 	 	 	:'gridpanel',
			    store 	 	 	:activity_search_details,
			    reference  	 	:'act_mem_grid_details',
			    // id 		  	 	:'act_mem_grid_details',
				rowLines 	 	:true,
				headerBorders  	:false,
				viewConfig    	:{
					preserveScrollOnRefresh :true,
					preserveScrollOnReload 	:true,
					deferEmptyText 			:true,
					trackOver 				:false,
					emptyText 				:'<h1>No OMM logs Found</h1>',
					getRowClass 			:function(record,index){
						var c 	=record.get('action_category');
						if(c=='member update' || c=='Member Update' || c=='MEMBER UPDATE'){
							return 'update'
						}else if(c=='hmo_id update' || c=='Hmo_id update' || c=='HMO_ID UPDATE'){
							return 'hmo_update'
						}else if(c=='inclusion' || c=='Inclusion' || c=='INCLUSION'){
							return 'valid'
						}else if(c=='deletion' || c=='Deletion' || c=='DELETION'){
							return 'empty'
						}else{
							return 'warning'
						}
					}
				}, 		
				columns  	:[
					{xtype:'rownumberer', width:45},
					{header:'Action', dataIndex:'action_category',  tdCls:'x-change-cell',flex:1},
					{header:'Date', dataIndex:'date', flex:1},
					{header:'Ops', dataIndex:'operator', flex:1},
					{header:'Action Id', dataIndex:'gen_idx', flex:1},
					{header:'Batch File Name', dataIndex:'batch_filename', flex:1},					
					{width:45,
						renderer: function (v, m, r) {
							var id         	 	 		= Ext.id(),
								batch_file_id  	 		= r.get('batch_file_id'),
								batch_filename 	 		= r.get('batch_filename'),
								company_id  	 		= r.get('company_id'),
								action_category  	   	= r.get('action_category'),
								gen_idx  	 	 	   	= r.get('gen_idx'),
								operator 				= r.get('operator'),
								OMM_logs_ops     		= localStorage.getItem("OMM_logs_ops"),
								ops              		= JSON.parse(OMM_logs_ops),
								details  	 	  	   	= {};
								details.batch_file_id  	= batch_file_id,
								details.company_id 	   	= company_id,
								details.action_category = action_category,
								details.ops 	 		= operator,
								details.date 	 		= r.data['date'];

							if(batch_file_id!='-'){
								console.log(batch_file_id+' '+company_id+' '+ops)
							 Ext.defer(function (batch_file_id,company_id,ops) {
							   Ext.widget('button', {
							     renderTo 	: id,
							     ui 		:'soft-green',
							     height 	:30,
							     margin 	:'-8 0 0 0',
							     tooltip 	:'Click icon to view batch process',
							     // renderTo    : Ext.getBody(),
							     iconCls 	:'x-fa fa-file-o',
							     handler 	:function(){
									localStorage.setItem("OMM_logs_batchfile",Ext.encode(details));
									var win           = new Ext.Window({
										closeAction   :'hide',
										width         :'70%',
										modal         :true,
										height        :650,
										layout 		  :'fit',				
										title         :batch_filename,
										controller    :'logs',
										scope         :this,
										initCenter    :true,
										autoShow      :true,
										maximized 	  :true,
										titleAlign    :'center',
										closable      :true,
										reference  	  :'mem_update_win',
										items         :[
										{
											xtype 	 :'panel',
											layout 	 :'fit',
											reference:'mem_update_batch_win'
										}
										],
										listeners  	:{
										  beforerender  :me.onWinBatchprocessBeforerender
										}
									});       

							     }
							     // handler	:function(batch_file_id,company_id,ops){}
							   });
							 }, 5);
							 return Ext.String.format('<div id="{0}"></div>', id);             
							}            
						}
					}
				],
				listeners  	 	:{
					rowdblclick 	:'onActMem_update_dbclick'
				},
				dockedItems  	: [{
				    xtype       : 'pagingtoolbar',
				    dock        : 'bottom',
				    // store       :'activity_store',
			    	store 		:activity_search_details,
				    displayInfo : true,
					moveNext : function(){
						var OMM_logs_ops=localStorage.getItem("OMM_logs_ops"),obj=JSON.parse(OMM_logs_ops),me=this,total=me.getPageData().pageCount,next=me.store.currentPage+1;
							console.log(next);
						if(next<=total){
							if(me.fireEvent('beforechange',me,next)!==false){
								me.store.nextPage({params:{ops:obj,activelink_id:r.data['activelink_id']}});
							}
						}
					},
					movePrevious:function(){
						var OMM_logs_ops=localStorage.getItem("OMM_logs_ops"),obj=JSON.parse(OMM_logs_ops),me=this,total=me.getPageData().pageCount,previous=me.store.currentPage-1;
	
						// console.log(previous);
						if(previous<=total){if(me.fireEvent('beforechange',me,previous)!==false){me.store.previousPage({params:{ops:obj,activelink_id:r.data['activelink_id']}});}}
					},
					moveFirst:function(){
						var OMM_logs_ops=localStorage.getItem("OMM_logs_ops"),obj=JSON.parse(OMM_logs_ops),me=this;

						me.store.getProxy().extraParams = {ops:obj,activelink_id:r.data['activelink_id']};
						me.store.loadPage(1);
					},
					moveLast:function(){
						var OMM_logs_ops=localStorage.getItem("OMM_logs_ops"),obj=JSON.parse(OMM_logs_ops),me=this;total=me.getPageData().pageCount;

						me.store.getProxy().extraParams = {ops:obj,activelink_id:r.data['activelink_id']};
						me.store.loadPage(total);
					},

					doRefresh : function(){
						var OMM_logs_ops=localStorage.getItem("OMM_logs_ops"),
						obj=JSON.parse(OMM_logs_ops),
						me=this;
						total=me.getPageData().pageCount;
						current=me.store.currentPage;
						if(me.fireEvent('beforechange',me,current)!==false){
							me.store.getProxy().extraParams={ops:obj,activelink_id:r.data['activelink_id']};
							me.store.loadPage(current);
						}
					},
					listeners:{
						beforerender:function(){
							var OMM_logs_ops=localStorage.getItem("OMM_logs_ops"),
								obj=JSON.parse(OMM_logs_ops),
								me=this;
								total=me.getPageData().pageCount;
								current=me.store.currentPage;
							me.store.getProxy().extraParams={ops:obj,activelink_id:r.data['activelink_id']};
							me.store.loadPage(current);
							console.log(current)
							console.log(total);							

						}
					}
				}
				]
			}],
			listeners 	:{
				beforerender 	:function(){
					var act_mem_grid_details  = this.lookupReference('act_mem_grid_details');
						OMM_logs_ops   		  = localStorage.getItem("OMM_logs_ops"),
						obj               	  = JSON.parse(OMM_logs_ops),
						log_details_prin_name = this.lookupReference('log_details_prin_name');

					if(values['isPrincipal_opt']==1){
						log_details_prin_name.hide()
					}else{
						log_details_prin_name.show();
					}
					act_mem_grid_details.store.load({
						params:{ops:obj,activelink_id:r.data['activelink_id']}
					});
				}
			}
    	});
    },

	onWinBatchprocessBeforerender: function(){
		var mem_update_batch_win = this.lookupReference('mem_update_batch_win');

		mem_update_batch_win.removeAll();
		mem_update_batch_win.add({xtype:'logs_batchprocess'});
	},

	onLogProccessBeforerender:function(){
		var	OMM_logs_ops     	   = localStorage.getItem("OMM_logs_ops"),
			OMM_logs_batchfile     = localStorage.getItem("OMM_logs_batchfile"),
			ops       	     	   = JSON.parse(OMM_logs_ops),
			obj       	     	   = JSON.parse(OMM_logs_batchfile),
			logs_batchprocess  	   = this.lookupReference('logs_batchprocess'),
			logs_batchprocess_grid = this.lookupReference('logs_batchprocess_grid').getStore(),
			ColumnField    	 	   = this.ColumnField = Ext.create('OMM.store.logs.Activity_batchprocess_loaddata');

		ColumnField.removeAll();
		ColumnField.load();
		ColumnField.getProxy().extraParams = {ops:ops,filesignature:obj['batch_file_id'], acc_id:obj['company_id']};
		ColumnField.on('load',function(store,record,successful,eOpts){
			var field = Object.keys(ColumnField.data.items[0].data);
			// console.log(ColumnField.data.items[0].data);// console.log(Object.keys(ColumnField.data.items[0].data));
			Ext.Array.each(field, function(name, index, countriesItSelf) {
				// console.log(name);// editor: {xtype: 'textfield'}
				logs_batchprocess.down('#logs_batchprocess_grid').headerCt.add({header:name,dataIndex:name, filter:'list'});
			});
		});
		ColumnField.load();
	},

	onLogProccessAfterrender:function(record){
		var	OMM_logs_ops     	   = localStorage.getItem("OMM_logs_ops"),
			OMM_logs_batchfile     = localStorage.getItem("OMM_logs_batchfile"),
			ops       	     	   = JSON.parse(OMM_logs_ops),
			obj       	     	   = JSON.parse(OMM_logs_batchfile),
			store  				   = this.lookupReference('logs_batchprocess_grid').getStore(),
			logs_batchprocess_grid = this.lookupReference('logs_batchprocess_grid'),
			logs_batchprocess_ops  = this.lookupReference('logs_batchprocess_ops'),
			logs_batchprocess_date = this.lookupReference('logs_batchprocess_date');
		
		logs_batchprocess_ops.setValue(obj['ops']);
		// logs_batchprocess_date.setValue(obj['ops']);
		logs_batchprocess_date.setValue(obj['date']);
		logs_batchprocess_grid.setTitle('<p style="border-left:solid #9cc96b 5px;">&nbsp;&nbsp;Batch Process : '+obj['action_category']);
		store.load({params:{ops:ops,filesignature:obj['batch_file_id'], acc_id:obj['company_id']}});
		console.log('Batch Grid record : ',store.data);   

	},

	onActMem_update_dbclick:function(row, r, tr, rowIndex, e, eOpts,grid,record,colIndex){
		var mem_type  		 	   	= r.data['mem_type'],
			action_category  	   	= r.data['action_category'];
			company_id  	 	   	= r.data['company_id'];
			gen_idx  	 	 	   	= r.data['gen_idx'];
			batch_file_id    	   	= r.data['batch_file_id'];
			batch_filename 	 	   	= r.data['batch_filename'];
			operator 				= r.data['operator'];
			OMM_logs_ops     	   	= localStorage.getItem("OMM_logs_ops");
			ops       	     	   	= JSON.parse(OMM_logs_ops),
			me 				 	   	= this;
			mem_update       	   	= this.mem_update=Ext.create('OMM.store.logs.Activity_member_update'),		
			details  	 	  	   	= {},
			details.batch_file_id  	= batch_file_id,
			details.company_id 	   	= company_id,
			details.action_category = action_category,
			details.ops 	 		= operator,
			details.date 	 		= r.data['date'],
			me  				 	= this;
					
		if(action_category=='MEMBER UPDATE'){
			var ad   		= new Ext.Window({
				title 		:fullname,
				controller 	:'logs',
				autoShow 	:true,
				width 		:'60%',
				height 		:650,
				overflowY   :'scroll',
				maximizable :true,
				modal       :true,
				layout 		:'fit',
				bodyPadding :5,
				reference  	:'mem_update_win',
				defaultType :'displayfield',
				items 		:[
				{
					xtype 		:'form',
					reference 	:'mem_update_panel',
					id 		 	:'mem_update_panel',
					defaultType :'displayfield',
		        	overflowY   :'scroll',
					listeners 	:{
						afterrender :onMem_update_panel_beforerender
					}
				}]
			});
			ad.show();
		}else if(action_category=='HMO_ID UPDATE'){
			var win 		= new Ext.Window({
				title 		:fullname,
				controller 	:'logs',
				autoShow 	:true,
				width 		:'60%',
				maximizable :true,
				modal 		:true,
				layout 		:'fit',
				bodyPadding :5,
				reference 	:'mem_hmoid_update_win',
				defaultType :'displayfield',
				items 		:[
				{
					xtype 	 	:'form',
					reference  	:'mem_hmoid_update_panel',
					id 			:'mem_hmoid_update_panel',
					defaultType :'displayfield',
					overflowY 	:'scroll',
					listeners 	:{
						afterrender :onMem_hmoid_update_panel_afterrender
					}
				}
				]
			});
		}

		function onMem_hmoid_update_panel_afterrender(){
			var mem_hmoid_update_panel 	= Ext.getCmp('mem_hmoid_update_panel'),
				mem_update       	    = this.mem_update=Ext.create('OMM.store.logs.Activity_member_update'),
			 	OMM_logs_ops 			= localStorage.getItem("OMM_logs_ops"),
			 	ops 					= JSON.parse(OMM_logs_ops);

			 mem_update.removeAll();
			 mem_update.getProxy().extraParams = {ops:ops, mem_type:mem_type, company_id:company_id, gen_idx:gen_idx},
			 mem_update.on('load',function(store,record,successful,eOpts){
			 mem_update.each(function(record,key){	
				var field 	= record.data;
					
				mem_hmoid_update_panel.add({
					xtype 		:'panel',
					ui 			:'light',
					title 		:'ActiveLink ID : '+field['activelink_id'],
					tbar       :[{xtype:'displayfield',value:field['modified_by'],labelWidth:25,fieldLabel:' ',beforeLabelTextTpl:['<i class="fa fa-user" style="font-size:15px;color:#467ead;"></i>'], margin:'0 5 0 5'},{xtype:'displayfield',value:field['modified_date'],labelWidth:25,fieldLabel:' ',beforeLabelTextTpl:['<i class="fa fa-clock-o" style="font-size:15px;color:#467ead;"></i>']}],
					items 	 	:[{xtype:'label',margin:'0 10 0 10',style:'font-weight:bold;font-size:11px;font-face:arial',html:'NOTE : <span style="color:#57b3cb;font-weight:bold;font-face:arial;font-size:11px;font-style:oblique;">Update/s made to member</span>'}]
				});
				
					

			 });
			 });
			 mem_update.load();
		}

		function onMem_update_panel_beforerender(){
			// var mem_update_panel = this.lookupReference('mem_update_panel'),
			var mem_update_panel = Ext.getCmp('mem_update_panel'),
				mem_update       = this.mem_update=Ext.create('OMM.store.logs.Activity_member_update'),
				OMM_logs_ops     = localStorage.getItem("OMM_logs_ops"),
				ops              = JSON.parse(OMM_logs_ops);

			mem_update.removeAll();
			mem_update.getProxy().extraParams = {ops:ops,mem_type:mem_type, company_id:company_id, gen_idx:gen_idx};
			mem_update.on('load',function(store,record,successful,eOpts){
			mem_update.each(function(record,key){
				var field = record.data;
				mem_update_panel.add({
					xtype 		:'panel',
					ui 			:'light',
					title 		:'ActiveLink ID : '+field['activelink_id'],
					tbar       :[{xtype:'displayfield',value:field['modified_by'],labelWidth:25,fieldLabel:' ',beforeLabelTextTpl:['<i class="fa fa-user" style="font-size:15px;color:#467ead;"></i>'], margin:'0 5 0 5'},{xtype:'displayfield',value:field['modified_date'],labelWidth:25,fieldLabel:' ',beforeLabelTextTpl:['<i class="fa fa-clock-o" style="font-size:15px;color:#467ead;"></i>']}],
					items 	 	:[{xtype:'label',margin:'0 10 0 10',style:'font-weight:bold;font-size:11px;font-face:arial',html:'NOTE : <span style="color:#57b3cb;font-weight:bold;font-face:arial;font-size:11px;font-style:oblique;">Update/s made to member</span>'}]
				});
				mem_update_panel.add(
					{xtype:'form',title:'Employee number',itemId:'empid_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'empid_o',value:field['empid_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'empid_n',value:field['empid_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['empid_o']==values['empid_n']){mem_update_panel.down('#empid_panel').hide();}else if(values['empid_o']!=values['empid_n'] && values['empid_n']=='-'){mem_update_panel.down('#empid_panel').hide();}}}},
					{xtype:'form',title:'First name',itemId:'fname_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',reference:'fname_o',name:'fname_o',value:field['fname_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'fname_n',value:field['fname_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['fname_o']==values['fname_n']){mem_update_panel.down('#fname_panel').hide();}else if(values['fname_o']!=values['fname_n'] && values['fname_n']=='-'){mem_update_panel.down('#fname_panel').hide();}}}},
					{xtype:'form',title:'Middle name',itemId:'mname_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'mname_o',value:field['mname_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'mname_n',value:field['mname_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['mname_o']==values['mname_n']){mem_update_panel.down('#mname_panel').hide();}else if(values['mname_o']!=values['mname_n'] && values['mname_n']=='-'){mem_update_panel.down('#mname_panel').hide();}}}},
					{xtype:'form',title:'Last name',itemId:'lname_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'lname_o',value:field['lname_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'lname_n',value:field['lname_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['lname_o']==values['lname_n']){mem_update_panel.down('#lname_panel').hide();}else if(values['lname_o']!=values['lname_n'] && values['lname_n']=='-'){mem_update_panel.down('#lname_panel').hide();}}}},
					{xtype:'form',title:'Extension name',itemId:'ename_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'ename_o',value:field['ename_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'ename_n',value:field['ename_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['ename_o']==values['ename_n']){mem_update_panel.down('#ename_panel').hide();}else if(values['ename_o']!=values['ename_n'] && values['ename_n']=='-'){mem_update_panel.down('#ename_panel').hide();}}}},
					{xtype:'form',title:'Birthdate',itemId:'birthdate_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'birthdate_o',value:field['birthdate_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'birthdate_n',value:field['birthdate_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['birthdate_o']==values['birthdate_n']){mem_update_panel.down('#birthdate_panel').hide();}else if(values['birthdate_o']!=values['birthdate_n'] && values['birthdate_n']=='-'){mem_update_panel.down('#birthdate_panel').hide();}}}},
					{xtype:'form',title:'Gender',itemId:'gender_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'gender_o',value:field['gender_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'gender_n',value:field['gender_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['gender_o']=='F'&&values['gender_n']=='Fe'||values['gender_o']=='M'&&values['gender_n']=='Ma'){mem_update_panel.down('#gender_panel').hide();}else if(values['gender_o']==values['gender_n']){mem_update_panel.down('#gender_panel').hide();}else if(values['gender_o']!=values['gender_n'] && values['gender_n']=='-'){mem_update_panel.down('#gender_panel').hide();}}}},
					{xtype:'form',title:'Civil Status',itemId:'civil_status_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'civil_status_o',value:field['civil_status_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'civil_status_n',value:field['civil_status_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['civil_status_o']==values['civil_status_n']){mem_update_panel.down('#civil_status_panel').hide();}else if(values['civil_status_o']!=values['civil_status_n'] && values['civil_status_n']=='-'){mem_update_panel.down('#civil_status_panel').hide();}}}},
					{xtype:'form',title:'Philhealth',itemId:'philhealth_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'philhealth_o',value:field['philhealth_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'philhealth_n',value:field['philhealth_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['philhealth_o']==values['philhealth_n']){mem_update_panel.down('#philhealth_panel').hide();}else if(values['philhealth_o']!=values['philhealth_n'] && values['philhealth_n']=='-'){mem_update_panel.down('#philhealth_panel').hide();}}}},
					{xtype:'form',title:'SSS',itemId:'sss_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'sss_o',value:field['sss_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'sss_n',value:field['sss_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['sss_o']==values['sss_n']){mem_update_panel.down('#sss_panel').hide();}else if(values['sss_o']!=values['sss_n'] && values['sss_n']=='-'){mem_update_panel.down('#sss_panel').hide();}}}},
					{xtype:'form',title:'Pag-ibig',itemId:'pagibig_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'pagibig_o',value:field['pagibig_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'pagibig_n',value:field['pagibig_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['pagibig_o']==values['pagibig_n']){mem_update_panel.down('#pagibig_panel').hide();}else if(values['pagibig_o']!=values['pagibig_n'] && values['pagibig_n']=='-'){mem_update_panel.down('#pagibig_panel').hide();}}}},
					{xtype:'form',title:'Plan Id',itemId:'plan_id_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'plan_id_o',value:field['plan_id_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'plan_id_n',value:field['plan_id_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['plan_id_o']==values['plan_id_n']){mem_update_panel.down('#plan_id_panel').hide();}else if(values['plan_id_o']!=values['plan_id_n'] && values['plan_id_n']=='-'){mem_update_panel.down('#plan_id_panel').hide();}}}},
					{xtype:'form',title:'Branch Id',itemId:'branch_id_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'branch_id_o',value:field['branch_id_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'branch_id_n',value:field['branch_id_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['branch_id_o']==values['branch_id_n']){mem_update_panel.down('#branch_id_panel').hide();}else if(values['branch_id_o']!=values['branch_id_n'] && values['branch_id_n']=='-'){mem_update_panel.down('#branch_id_panel').hide();}}}},
					{xtype:'form',title:'Member Status',itemId:'mbr_status_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'mbr_status_o',value:field['mbr_status_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'mbr_status_n',value:field['mbr_status_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['mbr_status_o']==values['mbr_status_n']){mem_update_panel.down('#mbr_status_panel').hide();}else if(values['mbr_status_o']!=values['mbr_status_n'] && values['mbr_status_n']=='-'){mem_update_panel.down('#mbr_status_panel').hide();}}}},
					{xtype:'form',title:'HMO Id',itemId:'hmo_id_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'hmo_id_o',value:field['hmo_id_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'hmo_id_n',value:field['hmo_id_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['hmo_id_o']==values['hmo_id_n']){mem_update_panel.down('#hmo_id_panel').hide();}else if(values['hmo_id_o']!=values['hmo_id_n'] && values['hmo_id_n']=='-'){mem_update_panel.down('#hmo_id_panel').hide();}}}},
					{xtype:'form',title:'Designation',itemId:'designation_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'designation_o',value:field['designation_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'designation_n',value:field['designation_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['designation_o']==values['designation_n']){mem_update_panel.down('#designation_panel').hide();}else if(values['designation_o']!=values['designation_n'] && values['designation_n']=='-'){mem_update_panel.down('#designation_panel').hide();}}}},
					{xtype:'form',title:'TIN',itemId:'tin_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'tin_o',value:field['tin_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'tin_n',value:field['tin_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['tin_o']==values['tin_n']){mem_update_panel.down('#tin_panel').hide();}else if(values['tin_o']!=values['tin_n'] && values['tin_n']=='-'){mem_update_panel.down('#tin_panel').hide();}}}},
					{xtype:'form',title:'Current Effective Date',itemId:'current_effective_date_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'current_effective_date_o',value:field['current_effective_date_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'current_effective_date_n',value:field['current_effective_date_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['current_effective_date_o']==values['current_effective_date_n']){mem_update_panel.down('#current_effective_date_panel').hide();}else if(values['current_effective_date_o']!=values['current_effective_date_n'] && values['current_effective_date_n']=='-'){mem_update_panel.down('#current_effective_date_panel').hide();}}}},
					{xtype:'form',title:'Orig Effective Date',itemId:'orig_effective_date_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'orig_effective_date_o',value:field['orig_effective_date_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'orig_effective_date_n',value:field['orig_effective_date_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['orig_effective_date_o']==values['orig_effective_date_n']){mem_update_panel.down('#orig_effective_date_panel').hide();}else if(values['orig_effective_date_o']!=values['orig_effective_date_n'] && values['orig_effective_date_n']=='-'){mem_update_panel.down('#orig_effective_date_panel').hide();}}}},
					{xtype:'form',title:'Maturity Date',itemId:'maturity_date_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'maturity_date_o',value:field['maturity_date_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'maturity_date_n',value:field['maturity_date_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['maturity_date_o']==values['maturity_date_n']){mem_update_panel.down('#maturity_date_panel').hide();}else if(values['maturity_date_o']!=values['maturity_date_n'] && values['maturity_date_n']=='-'){mem_update_panel.down('#maturity_date_panel').hide();}}}},
					{xtype:'form',title:'Emp hire Date',itemId:'emp_hire_date_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'emp_hire_date_o',value:field['emp_hire_date_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'emp_hire_date_n',value:field['emp_hire_date_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['emp_hire_date_o']==values['emp_hire_date_n']){mem_update_panel.down('#emp_hire_date_panel').hide();}else if(values['emp_hire_date_o']!=values['emp_hire_date_n'] && values['emp_hire_date_n']=='-'){mem_update_panel.down('#emp_hire_date_panel').hide();}}}},		
					{xtype:'form',title:'Hospital HMO',itemId:'hospital_hmo_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'hospital_hmo_o',value:field['hospital_hmo_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'hospital_hmo_n',value:field['hospital_hmo_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['hospital_hmo_o']==values['hospital_hmo_n']){mem_update_panel.down('#hospital_hmo_panel').hide();}else if(values['hospital_hmo_o']!=values['hospital_hmo_n'] && values['hospital_hmo_n']=='-'){mem_update_panel.down('#hospital_hmo_panel').hide();}}}},
					{xtype:'form',title:'Dental HMO',itemId:'dental_hmo_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'dental_hmo_o',value:field['dental_hmo_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'dental_hmo_n',value:field['dental_hmo_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['dental_hmo_o']==values['dental_hmo_n']){mem_update_panel.down('#dental_hmo_panel').hide();}else if(values['dental_hmo_o']!=values['dental_hmo_n'] && values['dental_hmo_n']=='-'){mem_update_panel.down('#dental_hmo_panel').hide();}}}},
					//additional field for dependent
					{xtype:'form',title:'Dependent Status',itemId:'dep_status_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'dep_status_o',value:field['dep_status_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'dep_status_n',value:field['dep_status_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['dep_status_o']==values['dep_status_n']){mem_update_panel.down('#dep_status_panel').hide();}else if(values['dep_status_o']!=values['dep_status_n']&&values['dep_status_n']=='-'){mem_update_panel.down('#dep_status_panel').hide();}}}},
					{xtype:'form',title:'Dependent Plan Level',itemId:'dep_plan_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'dplan_level_o',value:field['dplan_level_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'dplan_level_n',value:field['dplan_level_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['dplan_level_o']==values['dplan_level_n']){mem_update_panel.down('#dep_plan_panel').hide();}else if(values['dplan_level_o']!=values['dplan_level_n']&&values['dplan_level_n']=='-'){mem_update_panel.down('#dep_plan_panel').hide();}}}},
					{xtype:'form',title:'Dependent Type',itemId:'dep_type_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'dep_type_o',value:field['dep_type_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'dep_type_n',value:field['dep_type_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['dep_type_o']==values['dep_type_n']){mem_update_panel.down('#dep_type_panel').hide();}else if(values['dep_type_o']!=values['dep_type_n']&&values['dep_type_n']=='-'){mem_update_panel.down('#dep_type_panel').hide();}}}},
					{xtype:'form',title:'Relationship',itemId:'relationship_panel',ui:'light',defaultType:'textfield',defaults:{anchor:'70%',editable:false},bodyPadding:10,items:[{style:'border-left:solid #2eadf5 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">OLD</span>',name:'relationship_o',value:field['relationship_o']},{style:'border-left:solid #9cc96b 8px;',fieldLabel:'&nbsp;&nbsp;<span style="font-face:arial;font-weight:bold;">NEW</span>',name:'relationship_n',value:field['relationship_n']}],listeners:{afterrender:function(btn){var form=this.up('form').getForm(),values=form.getValues();if(values['relationship_o']==values['relationship_n']){mem_update_panel.down('#relationship_panel').hide();}else if(values['relationship_o']!=values['relationship_n']&&values['relationship_n']=='-'){mem_update_panel.down('#relationship_panel').hide();}}}}
				);
			});
			});
			mem_update.load();
		}
	}

 });