Ext.define('OMM.view.principal.Principal_details',{
	extend     : 'Ext.form.Panel',
	title      : 'Principal Details',
	xtype      : 'principal_details',
    requires   :['OMM.view.principal.PrincipalController'],
	controller :'principal',
    
    closable   :true,
    closeAction:'hide',
    layout     :'border',
	bodyPadding:5,
    defaults   : {anchor: '100%'},
    bbar       :{
        overflowHandler     :'menu',
        items               :[
         {
            xtype           :'panel',
            defaults        :{anchor:'100%',labelWidth:100},
            titleCollapse   :true,
            collapsible     :true,
            collapsed       :true,
            border          :true,
            animate         :true,
            hideCollapseTool: true,
            layout          :'anchor',
            bodyPadding     :5,
            dock            : 'bottom',
            width           :'100%',
            title           :'<b>Notes/Logs</b>',
            reference       :'prin_note_panel',
            items           :[
            {
                xtype       :'form',
                height      :300,
                overflowY   :'scroll',
                reference   :'prin_note_thread',
                defaults    :{border:false,autoHeight:true,collapsed:false,titleCollapse:false,hideCollapseTool:true,ui:'thread-panel-action',bodyStyle:{"background-color":"#f4f3d7","padding":"10px"}}
            },
            {
                xtype      : 'textareafield',
                margin     : '15 10 10 10',
                reference  :'prin_note_text',
                grow       : true,
                name       : 'note',
                fieldLabel : 'Note',
                width      :'100%',
                flex       :1
            }],
            bbar    :[
            {
                text    :'Submit',
                ui      :'soft-blue',
                handler :'onSubmitNote'
            },
            {
                text    :'Cancel',
                handler :'onCancelNote'
            }]
        }]
    },
    dockedItems: [
    {
        dock    : 'bottom',
        xtype   : 'toolbar',
        items   : [
        {
            xtype   :'panel',
            defaults:{
                labelStyle  :'font-size:20px;font-style :font-weight:bold;font-face:arial',
                fieldStyle:'font-size:20px;font-weight:bold;font-face:arial;color:#FF0000',
                labelWidth:130
            },
            items   :[
            {
                xtype       :'displayfield',
                fieldLabel  :'BMB Status',
                value       :'Not register',
                reference   :'prin_bmb_status'
            }
            ]
        },'->',
        {
            text        :'Reactivate',
            reference   :'prin_rectv',
            ui          :'soft-red',
            handler     :function(){
                var form         = this.up('form').getForm();
                var values       = form.getValues();
                var view         = Ext.getCmp("ops_tabpanel");
                var cur_tab_id   = "prin"+values['activelink_id'];
                var g            = Ext.getCmp('principal_grid');

                // if(values['mbr_status']=='ACTIVE'){alert('User cant set member status as active')}else{alert('set active')}Ext.Msg.confirm('Member Status','This function may set ',function(answer){if(answer=='yes'){}});                 
                Ext.Msg.confirm('Set Member Active','Are you sure you want to set Member active', function(answer){
                    if(answer=='yes'){
                        var waitMsg = Ext.MessageBox.wait('Processing your request. Please wait...');
                        Ext.Ajax.request({
                            url             :'../data/prin_set_active.php',
                            submitEmptyText :false,
                            params          :{acc_id:values['company_id'], session_userid:values['session_userid'], activelink_id:values['activelink_id']},
                            method          :'POST',
                            success         :function(response){
                                var response    = Ext.decode(response.responseText);
                                if(response.success == true){

                                    var jsonData = Ext.encode(Ext.pluck(g.store.data.getAt(0), 'data'));
                                    console.log(jsonData);

                                    waitMsg.hide();
                                    g.store.load();
                                    view.remove(cur_tab_id);
                                    Ext.MessageBox.show({title:'SUCCESS', msg:response.message, icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
                                }else{
                                    waitMsg.hide();
                                    g.store.load();
                                    Ext.MessageBox.show({title:'FAILED', msg:response.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}})
                                }
                            },failure    :function(response){
                                waitMsg.hide();
                                g.store.load();
                                Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please contact your IT for assistance', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                            }
                        });
                    }
                });
            }
        },
        {
            text        :'HMO Deletion',
            reference   :'prin_del',
            ui          :'soft-red',
            handler     :function(){
                var form            = this.up('form').getForm();
                var values          = form.getValues();
                var view            = Ext.getCmp("ops_tabpanel");
                var cur_tab_id      = "prin"+values['activelink_id'];
                var g               = Ext.getCmp('principal_grid');
                
                Ext.Msg.confirm('HMO Deletion','This function may set member as deleted, HMO status as Inactive and reset BMB account. Are you sure you want to delete Principal Member', function(answer){
                    if(answer=='yes'){

                        if(values['fname']=='' || values['lname']=='' || values['empid']=='' || values['fname']==null || values['lname']==null || values['empid']==null){
                            Ext.MessageBox.show({title:'WARNING', msg:'Cant delete principal member if either First, Last Name and Employee id is null or empty', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});                            
                        }else{
                            var waitMsg = Ext.MessageBox.wait('Processing your request. Please wait...');
                            Ext.Ajax.request({
                                url             :'../data/prin_single_del.php',
                                submitEmptyText :false,
                                params          :{acc_id:values['company_id'],session_userid:values['session_userid'],al_id:values['activelink_id'],empid:values['empid'], fname:values['fname'], lname:values['lname']},
                                method          :'POST',
                                success         :function(response){
                                    var response = Ext.decode(response.responseText);
                                    if(response.success == true){
                                        waitMsg.hide();
                                        g.store.load();
                                        view.remove(cur_tab_id);
                                        Ext.MessageBox.show({title:'SUCCESS', msg:response.message, icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
                                    }else{
                                        waitMsg.hide();
                                        g.store.load();
                                        Ext.MessageBox.show({title:'FAILED', msg:response.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                                    }
                                },failure       :function(response){
                                    waitMsg.hide();
                                    g.store.load();
                                    Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please contact your IT for assistance', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                                }
                            });

                        }
                    }
                });
            }
        },
        {
            text        :'User Reset',
            reference   :'prin_reset',
            ui          :'soft-red',
            handler     :function(){
                var form        = this.up('form').getForm();
                var values      = form.getValues();
                var view        = Ext.getCmp("ops_tabpanel");
                var cur_tab_id  = "prin"+values['activelink_id'];
                var g           = Ext.getCmp('principal_grid');

                Ext.Msg.confirm('User Reset','Are you sure you want to reset Principal member registration information',function(answer){
                    if(answer=='yes'){
                        var waitMsg = Ext.MessageBox.wait('Processing your request. Please wait...');
                        Ext.Ajax.timeout= 600000; //10 minutes 
                        Ext.Ajax.request({
                            url             :'../data/get_principal_reset.php',
                            submitEmptyText :false,     
                            timeout         : 600000, //10 minutes
                            params          :{empid:values['empid'],email:values['email'], p_al_id:values['activelink_id'], acc_id:values['company_id']},
                            method          : 'POST',
                            success         :function(response){
                                var response = Ext.decode(response.responseText);
                                if(response.success == true){
                                    waitMsg.hide();
                                    g.store.load();
                                    view.remove(cur_tab_id);
                                    Ext.MessageBox.show({title:'SUCCESS',msg:response.message,icon:Ext.Msg.INFO,closable:false,buttonText:{ok:'OK'}});
                                }else{
                                    waitMsg.hide();
                                    g.store.load();
                                    Ext.MessageBox.show({title:'FAILED',msg:response.message,closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                }
                            },failure       :function(response){
                                waitMsg.hide();
                                g.store.load();
                                Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please contact your IT for assistance',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                            }
                        });
                    }
                });
            }
        },
        {
            text        : 'Save Changes',
            reference   :'prin_update',
            ui          :'soft-red',
            handler     : function(){
                var form        = this.up('form').getForm();
                var values      = form.getValues();
                var fields      = form.getFields();
                var view        = Ext.getCmp("ops_tabpanel");
                var cur_tab_id  = "prin"+values['activelink_id'];
                var g           = Ext.getCmp('principal_grid');

                Ext.Msg.confirm('Update Member','Are you sure you want to update members information',function(answer){
                    if(answer=='yes'){
                        // if(values['empid']=='-'||values['fname']=='-'||values['mname']=='-'||values['lname']=='-'||values['ename']=='-'||values['birthdate']=='-'||values['gender']=='-'||values['philhealth']=='-'||values['civil_status']=='-'||values['sss_no']=='-'||values['pagibig']=='-'||values['unified_id']=='-'||values['plan_id']=='-'||values['branch_id']=='-'||values['mbr_status']=='-'||values['id_number']=='-'||values['designation']=='-'||values['tin']=='-'||values['orig_effectivity_date']=='-'||values['curr_effectivity_date']=='-'||values['maturity_date']=='-'||values['emp_hire_date']=='-'){
                        //     Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please make sure that enter valid value',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                        // }else{}
                        
                            //Other information setValue
                            Ext.each(fields.items, function (f) {
                            var field = f.getName(),
                                value = f.getValue();
                            if(field=='philhealth' && value=='-' || field=='sss_no' && value=='-' || field=='tin' && value=='-' || field=='unified_id_no' && value=='-' || field=='pag_ibig_no' && value=='-'){
                                console.log(field);
                                console.log(value);
                            }else{
                                if(value=='-'){
                                    // console.log(field); // console.log(form.findField(field))
                                    form.findField(field).setValue('');
                                }
                            }
                            });

                            //Gender Subtring 1
                            // Ext.each(fields.items, function (f) {
                            // var field = f.getName(),
                            //     value = f.getValue();
                            //     length= field.length;
                            // if(field=='gender' && value=='female' || field=='gender' && value=='male' || field=='gender' && value=='Female' || field=='gender' && value=='Male'){
                            //     form.findField(field).setValue(value.substr(-20,1));
                            // }
                            // });

                            //SLA updates delete fields
                            form.getFields().each(function(field){
                                if(field.name=='hmoid_no' || field.name=='hmoid_released_date' || field.name=='card_received' || field.name=='card_transmit' || field.name=='batch_code' || field.name=='sla_option' || field.name=='email1' || field.name=='address1' || field.name=='contact1' || field.name=='contact2' || field.name=='inclusion_date' ||field.name=='deactivation_date'){
                                  console.log("deleted field :"+field.fieldLabel);
                                  field.destroy();
                                }else{
                                    console.log('Fields : '+field.name);
                                }
                            });

                            if(form.isValid()){
                                form.submit({
                                    url             :'../data/principal_update.php',
                                    waitMsg         :'Processing your request. Please wait...',
                                    submitEmptyText :false,
                                    timeout         : 120000, //2minutes
                                    method          :'POST',
                                    success         :function(form,action){
                                        g.store.load();
                                        view.remove(cur_tab_id);
                                        Ext.MessageBox.show({title:'SUCCESS',msg:'Principal update saved',closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}});
                                    },failure       :function(form,action){
                                        g.store.load();
                                        Ext.MessageBox.show({title:'FAILED',msg:'Principal update failed',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                    }
                                })
                            }else{
                                Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please contact your IT for assistance',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                            }
                    }
                });
            }
        }]
    }],
    listeners:{
        afterrender:'onAfterrenderPrinTab'
    }
});