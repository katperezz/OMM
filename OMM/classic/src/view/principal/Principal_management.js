var itemsPerPage=25;Ext.create('Ext.data.Store',{id:'principalStore',autoLoad:false,fields:[{name:'activelink_id',mapping:'activelink_id'},{name:'empid',mapping:'empid'},{name:'fname',mapping:'fname'},{name:'mname',mapping:'mname'},{name:'lname',mapping:'lname'},{name:'ename',mapping:'ename'},{name:'fullname',mapping:'fullname'},{name:'birthdate',mapping:'birthdate'},{name:'gender',mapping:'gender'},{name:'civil_status',mapping:'civil_status'},{name:'philhealth',mapping:'philhealth'},{name:'sss_no',mapping:'sss_no'},{name:'pag_ibig_no',mapping:'pag_ibig_no'},{name:'unified_id_no',mapping:'unified_id_no'},{name:'plan_id',mapping:'plan_id'},{name:'branch_id',mapping:'branch_id'},{name:'email',mapping:'email'},{name:'mbr_status',mapping:'mbr_status'},{name:'id_number',mapping:'id_number'},{name:'company_id',mapping:'company_id'},{name:'member_date',mapping:'member_date'},{name:'designation',mapping:'designation'},{name:'tin',mapping:'tin'},{name:'reg_date',mapping:'reg_date'},{name:'inactive_date',mapping:'inactive_date'},{name:'existing_flag',mapping:'existing_flag'},{name:'inclusion_date',mapping:'inclusion_date'},{name:'emp_hire_date',mapping:'emp_hire_date'},{name:'date_of_joining',mapping:'date_of_joining'},{name:'hr_inclusion_endorsement_date',mapping:'hr_inclusion_endorsement_date'},{name:'hmo_inclusion_date',mapping:'hmo_inclusion_date'},{name:'hr_deletion_endorsement_date',mapping:'hr_deletion_endorsement_date'},{name:'hmo_deletion_date',mapping:'hmo_deletion_date'},{name:'plan_desc',mapping:'plan_desc'}],pageSize:itemsPerPage,proxy:{type:'ajax',timeout:2000000,url:'../data/get_principal_list.php',actionMethods:'POST',
	extraParams:{search_value:''},
	reader:{type:'json',rootProperty:'data',totalProperty:'total',successProperty:'success'},writer:{type:'json',rootProperty:'data',encode:true}}});

function calcAge(dateString) {
  var birthday = +new Date(dateString);
  return ~~((Date.now() - birthday) / (31557600000));
}
var myTemplate = new Ext.XTemplate('<div class="tpl_details"><strong><pre>Email             : </strong> {email}</p>', '<p><strong>Contact no1       : </strong> {contact1}</p>', '<p><strong>Contact no2       : </strong> {contact2}</p>', '<p><strong>Address           : </strong> {address1}</p>', '<p><strong>Inclusion Date    : </strong> {inclusion_date}</p>', '<p><strong>Deactivation Date : </strong> {deactivation_date}</p>');

Ext.define('OMM.view.principal.Principal_management',{
	extend 	 	: 'Ext.Panel',
	title 	 	: 'Principal Management',
	xtype 	 	: 'principal_management',
    requires 	:[
    'Ext.grid.selection.Replicator',
    'Ext.grid.plugin.Clipboard',
    'Ext.grid.selection.SpreadsheetModel',
    'Ext.grid.filters.Filters',

    'Ext.toolbar.Paging',
    'OMM.view.principal.Principal_details',
    'OMM.view.principal.PrincipalDependent_info',
    'OMM.view.principal.PrincipalController'
    ],
    controller  :'principal',

	closable 	:true,
	layout 	  	:'fit',
	bodyPadding :1,
	tbar:{
		items:[
		{
			xtype 	:'panel',
			items 	:[
			{
				xtype 	:'panel',
				defaults:{anchor:'100%',labelWidth:110,fieldStyle:'font-size:12px;',labelStyle:'font-size:12px;'},
				layout 	:'hbox',
				items 	:[
				{
				    xtype      		 : 'combo',
				    fieldLabel    	 : 'Account name',
				    displayField 	 : 'companyname',
				    valueField   	 : 'companyid',
				    emptyText    	 :'Select account',
				    width     		 :400,
				    editable  		 :false,
				    name         	 :'companyname',
				    id           	 :'prin_companyname',
				    triggerAction 	 : 'all',
				    store         	 : 'Get_acc_list',
				    afterLabelTextTpl: [
				        '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
				    ],
				    listeners     	 :{
				        select   	 :'onPrinSelectAccount'                                
				    }
				},
				{xtype:'button',text:'Export',margin:'0 5 0 5',ui:'soft-green',handler:'onPrinExportXLS'},
					{xtype:'panel',defaults:{labelStyle:'font-size:25px;font-style :font-weight:bold;font-face:arial',fieldStyle:'font-size:25px;font-weight:bold;font-face:arial'},items:[{xtype:'displayfield',fieldLabel:'TOTAL',labelWidth:85,margin:'0 0 0 5',id:'principal_total'}]}
				]
			},
			{xtype:'label',style:'font-weight:bold;font-size:11px;font-face:arial',html:'NOTE : <span style="color:#2f9fe9;font-weight:bold;font-face:arial;font-size:11px;font-style:oblique;">Double click row to view Principal information or click the group icon to view Principal Dependent/s</span>'}
			]
		},
		'->',
		{
			xtype 		:'panel',
			reference   :'notif_panel',
			layout  	:'hbox',
			items  	 	:[]
		},
		{xtype:'textfield',id:'prin_search_value',emptyText:'Search Employee',enableKeyEvents:true,
		triggers:{prin_search_value:{cls:Ext.baseCSSPrefix+'form-search-trigger',handler:'onPrinSearchEmp'}},
		inputAttrTpl:" data-qtip='Search Value: <br> • emp_number <br> • lastname <br> • firstname <br> • middlename'",listeners:{keyup:'onPrinSearchEmp_enter'}}
		]
	},
	items 		:[
	{
		xtype 		 :'grid',
		id 			 :'principal_grid',
		border 		 :false,
		autoScroll 	 :true,
		columnLines  :false,
		store        :'principalStore',
		headerBorders:false,
		rowLines     :true,
		plugins 	 : [
			'gridfilters',
			'clipboard',
			'selectionreplicator',
			'rowexpander'
		],
		animCollapse:true,
		plugins: [{
			ptype 				: 'rowexpander',
			rowBodyTpl: new Ext.XTemplate('<p>&nbsp;</p>'),
			expandOnDblClick 	:false
		}],
		layout:{
			type:'fit',
			align:'stretch',
			pack:'start'
		},
		onRowExpand : function(rowNode, record, expandRow, body,expandNode) {
        	var waitMsg = Ext.MessageBox.wait('Processing your request. Please wait...');
        	var theTd =  Ext.get(expandRow); // Here I get the td
	        theTd.mask('Loading...'); // I mask it, in case the process takes long

			Ext.Ajax.request({
			url:'../data/get_pmember_adddetails.php',
			method:'GET',
			params :{p_al_id:record.get('activelink_id'), acc_id:Ext.getCmp('prin_companyname').getValue()},
			success: function ( result, request ) {
			    var jsonData = Ext.util.JSON.decode(result.responseText);
				if(jsonData.success==true){
				    console.log(jsonData.data[0].lname);
					theTd.update(myTemplate.apply({
						email  			  : jsonData.data[0].email,
						address1  		  : jsonData.data[0].address1,
				    	contact1 		  : jsonData.data[0].contact1,
				    	contact2 		  : jsonData.data[0].contact2,
				    	inclusion_date 	  : jsonData.data[0].inclusion_date,
				    	deactivation_date : jsonData.data[0].deactivation_date
					}));
					// Remove the mask
					waitMsg.hide();				
				}else{
					waitMsg.hide();
					theTd.update(myTemplate.apply({
					}));
					theTd.unmask();
		            Ext.MessageBox.show({title:'WARNING', msg:jsonData.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
				}
			},
			failure: function ( result, request) {
				waitMsg.hide();
				theTd.update(myTemplate.apply({
				}));
				theTd.unmask();
				Ext.MessageBox.show({title:'WARNING', msg:result.responseText, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
			    return false;
			}
			});
		},
		selModel: {
			type 		: 'spreadsheet',
			columnSelect: true,
			pruneRemoved: false,
			extensible 	: 'y'
		},
		viewConfig   :{
			preserveScrollOnRefresh :true,
			preserveScrollOnReload 	:true,
			deferEmptyText 			:true,
			emptyText 				:'<h1>No data found</h1>',
			getRowClass 			:function(record,index){
				var c 	=record.get('email');
				if(c=='' || c=='-' || c=='-_DEL'){
					return 'empty'
				}else{
					return 'valid'
				}
			}
		},	
		columns 	 : [
		    {xtype:'rownumberer', width:45},
		    {header:'ActiveLink ID',dataIndex:'activelink_id',filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},
		    {header:'Employee no',dataIndex:'empid',filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},
			{header:'Full Name', dataIndex:'fullname', flex:1, filter:{type:'string', itemDefaults:{emptyText:'Search for...'}}},			    
			{header:'Age',width:80, filter:'list',
			renderer : function(value, metaData, record, rowIdx, colIdx, store, view){
                    return calcAge(record.get('birthdate'));
                }
			},
		    {header:'HMO/Job level',dataIndex:'plan_id',width:120,filter:'list'},
		    {header:'Branch ID',dataIndex:'branch_id',filter:'list'},
		    {header:'Status',dataIndex:'mbr_status',filter:'list'},
			{header:'BMB Registration', width:125,tdCls:'x-change-cell',
				renderer : function(value, metaData, record, rowIdx, colIdx, store, view){
					var email 	=record.get('email');
					if(email=='' || email=='-' || email=='-_DEL'){
						return 'UNREGISTERED'
					}else{
						return 'REGISTERED'
					}
				}

			}
		],
		listeners 	:{
			beforerender 	:function(form,btn){
				Ext.getCmp('principal_grid').getStore().removeAll();
				console.log(window.console);if(window.console||window.console.firebug){console.clear()}
			},
			afterrender: function(me) {
				me.getView().on('expandbody', me.onRowExpand);
			},
			rowdblclick:'onPrincipalDetails'
		},
		dockedItems 	: [{
				xtype 	 	: 'pagingtoolbar',
				dock 	 	: 'bottom',
				store 		:'principalStore',
	    		displayInfo : true,
				moveNext : function(){
					var search_value 	   = Ext.getCmp('prin_search_value').getValue(),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						next  			   = me.store.currentPage + 1;

						console.log(next);
					if (next <= total) {
					    if (me.fireEvent('beforechange', me, next) !== false) {	        
					        me.store.nextPage({params:{search_value:search_value}});
					    }
					}
				},
				movePrevious:function(){
					var search_value 	   = Ext.getCmp('prin_search_value').getValue(),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						previous  		   = me.store.currentPage - 1;

						console.log(previous);
					if (previous <= total) {
					    if (me.fireEvent('beforechange', me, previous) !== false) {
					        me.store.previousPage({params:{search_value:search_value}});
					    }
					}
				},
				moveFirst:function(){
					var search_value 	   = Ext.getCmp('prin_search_value').getValue(),
						me  			   = this;

					me.store.getProxy().extraParams = {search_value:search_value};
					me.store.loadPage(1);
				},
				moveLast:function(){
					var search_value 	   = Ext.getCmp('prin_search_value').getValue(),
						me  			   = this,
						total  			   = me.getPageData().pageCount;

					me.store.getProxy().extraParams = {search_value:search_value};
					me.store.loadPage(total);
				},

				doRefresh : function(){
					var search_value 	   = Ext.getCmp('prin_search_value').getValue(),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						current 		   = me.store.currentPage;

					console.log(current)
					if (me.fireEvent('beforechange', me, current) !== false) {
						me.store.getProxy().extraParams = {search_value:search_value};
						me.store.loadPage(current);
					}
				}
			}]
		}]
});