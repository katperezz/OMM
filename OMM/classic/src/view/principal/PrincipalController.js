
var mbr_status=Ext.create('Ext.data.Store',{fields:['mbr_status'],data:[{"mbr_status":"ACTIVE"},{"mbr_status":"INACTIVE"}]});
var provider= Ext.create('Ext.data.Store', {
    fields:['prov_code','prov_name'],
    data:[
    {"prov_code":"HMO_1WORLD","prov_name":"1-World Wide Health"},
    {"prov_code":"HMO_ASIANLIFE","prov_name":"AsianLife"},
    {"prov_code":"HMO_COCOLIFE","prov_name":"Cocolife"},
    {"prov_code":"HMO_HMI","prov_name":"Health Maintenance, Inc"},
    {"prov_code":"HMO_INSULAR","prov_name":"INSULAR HEALTH CARE INC."},
    {"prov_code":"HMO_INTELLICARE","prov_name":"Intellicare"},
    {"prov_code":"HMO_MAXI","prov_name":"Maxicare"},
    {"prov_code":"HMO_MEDICARD","prov_name":"MediCard Philippines"},
    {"prov_code":"HMO_PHILCARE","prov_name":"PhilCare Health Care Service"}
    ]
});

var dep_remarks = Ext.create('Ext.data.Store',{
    fields:['r_code', 'r_name'],
    data:[
        {"r_code":"0","r_name":"Qualified for Enrollment"},
        {"r_code":"1","r_name":"Under Age"},
        {"r_code":"2", "r_name":"Over Age"},
        // {"r_code":"3", "r_name":"Qualified Over Age"},
        {"r_code":"4","r_name":"Skipped for Enrollment"}
    ]
});

var dep_relationship = Ext.create('Ext.data.Store',{
    fields:['relationship'],
    data:[
        {"relationship":"Husband"},
        {"relationship":"Wife"},
        {"relationship":"Father"},
        {"relationship":"Mother"},
        {"relationship":"Brother"},
        {"relationship":"Sister"},
        {"relationship":"Son"},
        {"relationship":"Daughter"},
        {"relationship":"Aunt"},
        {"relationship":"Uncle"},
        {"relationship":"Grandmother"},
        {"relationship":"Grandfather"}
        // {"relationship":"Spouse"},{"relationship":"Parent"},{"relationship":"Sibling"},{"relationship":"Child"},{"relationship":"Extended"},{"relationship":"Cousin"},{"relationship":"Aunt"},{"relationship":"Uncle"},
    ]
});

Ext.define('OMM.view.principal.PrincipalController', {
    extend  : 'Ext.app.ViewController',
    alias   : 'controller.principal',

    requires: [
        'Ext.util.TaskRunner',        
        'Ext.form.action.StandardSubmit'
    ],

    calcAge:function (dateString) {
        var birthday = +new Date(dateString);
        return ~~((Date.now() - birthday) / (31557600000));
    },

    onClearNotif:function(ops_endo_count,ops_endo_count_label,id_numR_count,id_numR_count_label,id_cardR_count,id_cardR_count_label,id_cardT_count,id_cardT_count_label){
        ops_endo_count.setVisible(false);
        id_numR_count.setVisible(false);
        id_cardR_count.setVisible(false);
        id_cardT_count.setVisible(false);
        ops_endo_count_label.setHidden(true);
        id_numR_count_label.setHidden(true);
        id_cardR_count_label.setHidden(true);
        id_cardT_count_label.setHidden(true);
    },

    //ComboBox - Select account
    onPrinSelectAccount:function(value, paging, params){

        console.log(window.console);if(window.console||window.console.firebug){console.clear()}
        this.selected_db = value.getValue();
        console.log('Principal select account : ' + this.selected_db);  

        var g                    = Ext.getCmp('principal_grid'),
            prin_search_value    = Ext.getCmp('prin_search_value'),
            ops_endo_count       = this.lookupReference('ops_endo_count'),
            id_numR_count        = this.lookupReference('id_numR_count'),
            id_cardR_count       = this.lookupReference('id_cardR_count'),
            id_cardT_count       = this.lookupReference('id_cardT_count'),
            ops_endo_count_label = this.lookupReference('ops_endo_count_label'),
            id_numR_count_label  = this.lookupReference('id_numR_count_label'),
            id_cardR_count_label = this.lookupReference('id_cardR_count_label'),
            id_cardT_count_label = this.lookupReference('id_cardT_count_label'),
            notif_panel          = this.lookupReference('notif_panel'),
            me                   = this;

        if(this.selected_db!=='') {
            Ext.Ajax.request({
                url        : '../data/session_prin_acc.php',
                params     :{session_prin_acc:this.selected_db},
                timeout    : 300000, //5minutes
                waitMsg    :    'Processing your request',
                method     : 'POST',
                success    : function () {
                    // Ext.getCmp('principal_grid').filters.clearFilters();
                    // prin_paging_toolbar.getStore().load({start:1, page:1});
                    // console.log('Principal current page :'+g.store.currentPage);
                    Ext.getCmp('principal_grid').getStore().removeAll();
                    Ext.getCmp('principal_grid').getStore().load({params:{page:g.store.currentPage}});
                    prin_search_value.setValue('');
                    console.log(g.getStore().data);

                    Ext.Ajax.request({
                        url     :'../data/post_username.php',
                        success :function(response){
                            var response         = Ext.decode(response.responseText);
                                details          = {};
                                details.ops      = response.username;
                            localStorage.removeItem('OMM_logs_ops');
                            localStorage.removeItem('OMM_thread_notes');
                            localStorage.setItem("OMM_logs_ops",Ext.encode(details));
                        }
                    });

                    Ext.Ajax.request({url:'../data/get_principal_total.php',success:function(response){var response=Ext.decode(response.responseText);Ext.getCmp('principal_total').setValue(response.total)},failure:function(response){}});
                }
            });

            //Notification goes here
            notif_panel.removeAll();   

            }else{Ext.MessageBox.show({title:'WARNING',msg:'Select Account',closable:false,icon:Ext.Msg.WARNING,buttons:Ext.Msg.WARNING,buttonText:{ok:'OK'}});}
    },

    //Textfield - Keyup search employee
    onPrinSearchEmp_enter:function(t,e,o){
        var me  = this;
        if(e.getKey()==e.ENTER){
            me.onPrinSearchEmp();
        }              
    },

    //Button - search employee
    onPrinSearchEmp:function(t,e,o){
        var prin_companyname=Ext.getCmp("prin_companyname"),g=Ext.getCmp("principal_grid");console.log(prin_companyname),null==prin_companyname.value?(alert("No account selected"),prin_companyname.focus(),prin_companyname.expand()):(console.log(prin_companyname.getValue()),g.store.clearFilter(prin_companyname),g.store.load({params:{search_value:Ext.getCmp("prin_search_value").getValue()}}));
    },

    onPrinBeforeChangePage:function(){
        var g = Ext.getCmp('principal_grid');
        console.log(g.store.currentPage," ",Ext.getCmp("prin_search_value").getValue())
        // console.log(g.store.currentPage);

        g.on('beforeload',function(store, operation,eOpts){
            operation.params={
                page:g.store.currentPage,search_value:Ext.getCmp("prin_search_value").getValue()
            };
        },this);
        console.log('load')
    },

    //Button - export list of principal
    onPrinExportXLS:function(){

        if(Ext.getCmp('prin_companyname').getValue()==null){
            Ext.MessageBox.show({title:'WARNING',msg:'No account selected',closable:false,icon:Ext.Msg.WARNING,buttons:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
        }else{
            var win    =   new Ext.Window({
                width       :'80%',
                height      :350,
                modal       :true,
                closeAction :'hide',
                bodyPadding :5,
                title       :'Set Filter for downloads',
                layout      : {type:'accordion',titleCollapse:true,animate:true, activeOnTop:true},// activeOnTop  : true
                items       :[
                {
                    xtype         :'form',
                    title         :'Based on Inclusion Date',
                    defaultType   :'datefield',
                    bodyPadding   :5,
                    defaults      :{anchor:'100%',labelWidth:390,allowBlank:false,fieldStyle:'font-size:12px;',labelStyle:'font-size:12px;'},
                    url           :'../data/download_principal_members.php',
                    standardSubmit:true,
                    items           :[
                    {fieldLabel:'Start Date and Time (YYYY-MM-DD hh:mm:ss) ',value:'1910-01-01 00:00:00', minValue: '1910-01-01',maxValue: new Date(),name:'start_date', reference:'prin_start_date',format:'Y-m-d H:i:s',afterLabelTextTpl:['<span style="color:red;font-weight:bold" data-qtip="Required">* Default value</span>']},
                    {fieldLabel:'End Date and Time (YYYY-MM-DD hh:mm:ss)',value: new Date(),minValue:'1910-01-01', name:'end_date',reference:'prin_end_date',format:'Y-m-d H:i:s',afterLabelTextTpl:['<span style="color:red;font-weight:bold" data-qtip="Required">*</span>']}
                    // maxValue: new Date(),
                    ],
                    bbar            :{
                        items       :[
                        {
                            text    :'Export to XLS',
                            ui      :'soft-purple',
                            handler :function(){
                                var form    = this.up('form').getForm();
                                var values      = form.getValues();
                                console.log("Start Date :"+values['start_date']+"\n"+"End Date :"+values['end_date'])                                            
                                if(form.isValid()){
                                    if(values['start_date']>=values['end_date']){
                                        Ext.MessageBox.show({title:'WARNING',msg:'Date and time is not valid',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                    }else{                                             
                                        form.submit({
                                            params           :{acc_id      :Ext.getCmp('prin_companyname').getValue()},
                                            wait             :true,
                                            waitMsg          :'Processing your request',
                                            waitConfig       :{duration:3000, increment:3, text:'Loading...', scope:this, fn:function(){Ext.MessageBox.hide();}},
                                            submitEmptyText  :false,
                                            // waitMsg :'Processing your request',// timeout : 60000,//1 minute
                                            success          :function(){
                                                // window.open('app/data/dashboard/masterlist_file.php','report')
                                                Ext.MessageBox.show({title:'Export',msg:'Dowload Success',closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}});
                                                form.reset();
                                            },
                                            failure          :function(form, action){
                                                
                                                if (action.failureType === Ext.form.action.Action.CONNECT_FAILURE) {
                                                    Ext.MessageBox.show({title:'Export',msg:'Donwload Failed',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                                }
                                                if (action.failureType === Ext.form.action.Action.SERVER_INVALID){
                                                Ext.MessageBox.show({title:'Export',msg:'Donwload Failed',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                                }
                                            }
                                        });
                                    }
                                }else{Ext.MessageBox.show({title:'WARNING',msg:'Date and time is not valid',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});}
                            }
                        }]
                    }
                },{
                    xtype:'form',title:'Registered Members',bodyPadding:10,flex:1,url:'../data/download_registered_members.php',standardSubmit:true,
                    items:[
                    {xtype:'label',style:'color:#2f9fe9;font-weight:bold;font-face:arial',text:'Check if you dont want to include inactive members',iconCls:'x-fa fa-info-circle'},
                    {xtype:'checkbox',boxLabel:'Do not include inactive Members',name:'stat_opt',anchor:'100%',checked:false,inputValue:'1',uncheckedValue:'0'}
                    ],
                    bbar    :{
                        items   :[
                        {
                            xtype   :'button',
                            ui      :'soft-purple',
                            text    :'Export to XLS',
                            handler :function(){
                                var form    = this.up('form').getForm();
                                var values  = form.getValues();
                                form.submit({
                                    params          :{stat_opt:values['stat_opt'],acc_id:Ext.getCmp('prin_companyname').getValue()},
                                    submitEmptyText :false,
                                    success         :function(){
                                        Ext.MessageBox.show({title:'Success',msg:'Download Success',closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}});
                                        form.reset();
                                    },failure       :function(form, action){

                                        if (action.failureType === Ext.form.action.Action.CONNECT_FAILURE) {
                                            Ext.MessageBox.show({title:'Export',msg:'Donwload Failed',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                        }
                                        if (action.failureType === Ext.form.action.Action.SERVER_INVALID){
                                        Ext.MessageBox.show({title:'Export',msg:'Donwload Failed',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                        }
                                    }
                                });
                            }
                        }]
                    }
                }],
                bbar     :{
                    items:[
                        {xtype:'label',style:'color:#e44959;font-weight:bold;font-face:arial', margin:5,text:'NOTE : Download is not successful if page redirect or reload the window',iconCls:'x-fa fa-info-circle'}
                    ]}
            });win.show(this);
        }
    },


    //ComboBox - onClick Principal Plan level
    onPrinCompanyID:function(){
        var prin_acc_id     = this.lookupReference('prin_acc_id');
        var prin_planLevel  = this.lookupReference('prin_planLevel');
        var prin_branch_id  = this.lookupReference('prin_branch_id');

        if(prin_branch_id.getValue()=='' || prin_branch_id.getValue()== null ){
            Ext.MessageBox.show({title:'WARNING',msg:'Please select Branch ID first', icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
        }else{prin_planLevel.getStore().load({params:{acc_id:prin_acc_id.getValue(),branch_id:prin_branch_id.getValue()}});}       
    },

    //ComboBox - onChange Principal Plan level
    onPrinPlanChange:function(){

        var prin_acc_id     = this.lookupReference('prin_acc_id');
        var prin_planLevel  = this.lookupReference('prin_planLevel');
        var prin_branch_id  = this.lookupReference('prin_branch_id');
        var prin_plan_desc  = this.lookupReference('prin_plan_desc');

        console.log(prin_branch_id.getValue());

        Ext.Ajax.request({
            url     :'../data/get_principal_plan.php',
            params  :{acc_id:prin_acc_id.getValue(),plan_id:prin_planLevel.getValue(),branch_id:prin_branch_id.getValue()},
            success :function(response){
                var response = Ext.decode(response.responseText);
                prin_plan_desc.setValue(response.description);
            },failure:function(){
                Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please contact IT for assistance',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
            }
        });

    },

    //ComboBox - onClick Principal Branch
    onPrinBranchID:function(){
        var prin_acc_id    = this.lookupReference('prin_acc_id');
        var prin_branch    = this.lookupReference('prin_branch');
        prin_branch.getStore().load({params:{acc_id:prin_acc_id.getValue()}});
    },

    //ComboBox - onChange Principal Branch
    onPrinBranchChange:function(){
        var prin_acc_id     = this.lookupReference('prin_acc_id');
        var prin_branch     = this.lookupReference('prin_branch');
        var prin_branch_id  = this.lookupReference('prin_branch_id');

        Ext.Ajax.request({
            url     :'../data/get_newsite_id.php',
            params  :{acc_id:prin_acc_id.getValue(), branch_name:prin_branch.getValue()},
            success :function(response){
                var response    = Ext.decode(response.responseText);
                console.log(response.message[0].branch_id)
                // prin_branch_id.setValue(response.branch_id);
                prin_branch_id.setValue(response.message[0].branch_id);
            },failure:function(){
                Ext.MessageBox.show({title:'WARNING',msg:'Couldnt load ID of branch name',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
            }
        });
    },

    onDepCompanyID:function(){
        var dep_acc_id      = this.lookupReference('dep_acc_id');
        var dep_planLevel   = this.lookupReference('dep_planLevel');
        var dep_branch_id   = this.lookupReference('dep_branch_id');

        if(dep_branch_id.getValue()=='' || dep_branch_id.getValue()== null ){
            Ext.MessageBox.show({title:'WARNING', msg:'Please select Branch ID first', icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});            
        }else{
            dep_planLevel.getStore().load({params:{acc_id:dep_acc_id.getValue(),branch_id:dep_branch_id.getValue()}});            
        }
    },

    onDepPlanChange:function(){

        var dep_acc_id      = this.lookupReference('dep_acc_id');
        var dep_planLevel   = this.lookupReference('dep_planLevel');
        var dep_branch_id   = this.lookupReference('dep_branch_id');
        var dep_plan_desc   = this.lookupReference('dep_plan_desc');

        Ext.Ajax.request({
            url     :'../data/get_principal_plan.php',
            params  :{acc_id:dep_acc_id.getValue(),plan_id:dep_planLevel.getValue(),branch_id:dep_branch_id.getValue()},
            success:function(response){
                var response    = Ext.decode(response.responseText);
                dep_plan_desc.setValue(response.plan_desc)
            },failure:function(){
                Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please contact IT for assistance',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
            }
        });
 
    },

    onDepBranchID:function(){
        var dep_acc_id     = this.lookupReference('dep_acc_id');
        var dep_branch_id    = this.lookupReference('dep_branch_id');
        dep_branch_id.getStore().load({params:{acc_id:dep_acc_id.getValue()}});
    },


    //Afterrender - principal_details panel
    onAfterrenderPrinTab:function(btn){
        var id                    = 1,
            prin_email            = this.lookupReference('prin_email'),
            prin_bmb_status       = this.lookupReference('prin_bmb_status'),
            prin_reset            = this.lookupReference('prin_reset'),
            prin_update           = this.lookupReference('prin_update'),
            prin_note_thread      = this.lookupReference('prin_note_thread'),
            activity_note         = this.activity_note=Ext.create('OMM.store.Get_activity_note'),
            OMM_logs_ops          = localStorage.getItem("OMM_logs_ops"),
            obj                   = JSON.parse(OMM_logs_ops),
            OMM_thread_notes      = localStorage.getItem("OMM_thread_notes"),
            obj2                  = JSON.parse(OMM_thread_notes),
            me                    = this;

        console.log('prinTab render :'+prin_note_thread);

        if(prin_email.getValue()=='' || prin_email.getValue()=='-' || prin_email.getValue()=='-_DEL'){
            prin_bmb_status.setValue('Unregistered');prin_reset.setDisabled(true);prin_update.setDisabled(false);
        }else{
            prin_bmb_status.setValue('Registered');prin_reset.setDisabled(false);prin_update.setDisabled(false);
        }
        activity_note.removeAll();
        activity_note.getProxy().extraParams = {activelink_id:obj2.activelink_id, acc_id:obj2.acc_id, ops:obj};
        activity_note.load({
        callback: function(records, operation, success) {
          var total_count  = records.length,
              rec          = records,
              i            = 0;

          console.log(records.length);
          me.activity_note_AjaxRequest_getId(id, total_count,rec)
        }
        });
        // activity_note.load();
    },


    //Reload Principal Activity Logs
    activity_note_AjaxRequest_getId:function(id, total_count,rec){
        var me               = this,
            i                = 0,
            prin_note_thread = this.lookupReference('prin_note_thread');

        this.activity_note.each(function(record){
            var note     = record.get('note'),
                operator = record.get('operator'),
                date     = record.get('date');

            if(note=='DELETION' || note=='INCLUSION' || note=='MEMBER REACTIVATION' || note=='MEMBER RESET' || note=='MEMBER UPDATE' || note=='MEMBER DEP DOCUMENT' || note=='RESET DEP DOCUMENT' || note=='USER RESET'){
                prin_note_thread.add({
                bodyStyle    :{"background-color":"#f4f3d7"},
                xtype        :'panel',
                margin       :10,
                bodyPadding  :'0px 0px 15px 0px',
                title        :operator,
                html         :'&nbsp;&nbsp;&nbsp;&nbsp;'+note,
                tools        :[
                {xtype:'displayfield',value:date,labelWidth:25,fieldLabel:' ',beforeLabelTextTpl:['<i class="fa fa-clock-o" style="font-size:15px;color:#467ead;"></i>']}]
            });
            }else{
                prin_note_thread.add({
                bodyStyle    :{"background-color":"#f4f3d7"},
                xtype        :'panel',
                ui           :'thread-panel-note',
                bodyStyle    :{"background-color":"#f6f7f9","padding":"15px"},
                margin       :5,
                bodyPadding  :'0px 0px 15px 0px',
                title        :operator,
                html         :'&nbsp;&nbsp;&nbsp;&nbsp;'+note,
                tools        :[
                {xtype:'displayfield',value:date,labelWidth:25,fieldLabel:' ',beforeLabelTextTpl:['<i class="fa fa-clock-o" style="font-size:15px;color:#467ead;"></i>']}]
            });
            }
            i++;
        });
    },    

    //SLA Updates HMO ID
    onHmoIdUpdate:function(btn){
        var form                = btn.up('form').getForm(),
            values              = form.getValues(),
            OMM_thread_notes    = localStorage.getItem('OMM_thread_notes'),
            OMM_logs_ops        = localStorage.getItem('OMM_logs_ops'),
            obj                 = JSON.parse(OMM_thread_notes),
            obj2                = JSON.parse(OMM_logs_ops),
            cur_tab_id          ="prin"+obj['activelink_id'],
            me                  =this,
            prin_info           = this.lookupReference('prin_info').getForm(),
            prin_val            = prin_info.getValues();

        console.log(values);

        var view        = Ext.getCmp("ops_tabpanel");
        var myMask  = new Ext.LoadMask({
            msg     :'Processing your request. Please wait...',
            target  :view
        });
        myMask.show();

        if(values['hmoid_no']==''||values['hmoid_released_date']==''){
            Ext.MessageBox.show({title:'WARNING', msg:'Please fill up required fields.', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
            myMask.hide();
        }else{
            Ext.Ajax.request({
                url     :'../data/prin_hmoid_update.php',
                // url     :'https://www.benefitsmadebetter.com/devmod5/Activebmb/data/getDependentList.php',
                params  :{ops:obj2['ops'],activelink_id:prin_val['activelink_id'],hmoid_no:values['hmoid_no'],hmoid_released_date:values['hmoid_released_date']},
                method  :'POST',
                scope   :me,
                success :function(response){
                    var response = Ext.decode(response.responseText);
                    if(response.success==true){
                        myMask.hide();
                        Ext.MessageBox.show({title:'SUCCESS',msg:response.message, icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
                        this.onSlaUpdate_beforerender()
                        // view.remove(cur_tab_id);
                    }else{
                        myMask.hide();
                        Ext.MessageBox.show({title:'FAILED', msg:response.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                    }
                },
                failure :function(){
                    myMask.hide();
                    Ext.MessageBox.show({title:'FAILED', msg:'500 Internal Server Error', icon:Ext.msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                }
            });
        }
    },

    //SLA Updates HMO Card Received
    onSaveCardReceived:function(btn){
        var form             = btn.up('form').getForm(),
            values           = form.getValues(),
            view             = Ext.getCmp('ops_tabpanel'),
            OMM_logs_ops     = localStorage.getItem('OMM_logs_ops'),
            OMM_thread_notes = localStorage.getItem('OMM_thread_notes'),
            obj              = JSON.parse(OMM_logs_ops),
            obj2             = JSON.parse(OMM_thread_notes),
            me               = this,
            prin_info        = this.lookupReference('prin_info').getForm(),
            prin_val         = prin_info.getValues();

        var myMask      = new Ext.LoadMask({
            msg         : 'Processing your request. Please wait...',
            target      :view
        });
        myMask.show();

        if(values['card_received']==''){
            Ext.MessageBox.show({title:'WARNING', msg:'Please fill up required fields', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
            myMask.hide();
        }else{
            Ext.Ajax.request({
                url         :'../data/sla_pid_released_update.php',
                // url     :'https://www.benefitsmadebetter.com/devmod5/Activebmb/data/getDependentList.php',
                params      :{ops:obj['ops'],acc_id:prin_val['company_id'],activelink_id:prin_val['activelink_id'],phys_id_released_date:values['card_received']},
                method      :'POST',
                scope       :me,
                success     :function(response){
                    var response = Ext.decode(response.responseText);
                    if(response.success==true){
                        myMask.hide();                        
                        Ext.MessageBox.show({title:'SUCCESS', msg:response.message, icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});                        
                        this.onSlaUpdate_beforerender()
                    }else{
                        myMask.hide();
                        Ext.MessageBox.show({title:'FAILED', msg:response.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                    }
                },
                failure     :function(){
                    myMask.hide();
                    Ext.MessageBox.show({title:'FAILED',msg:'500 Internal Server Error',icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});

                }
            });
        }
    },
 
    //SLA Updates HMO Card Transmittal
    onSaveCardTransmit:function(btn){
        var form             = btn.up('form').getForm(),
            values           = form.getValues(),
            view             = Ext.getCmp('ops_tabpanel'),
            OMM_logs_ops     = localStorage.getItem('OMM_logs_ops'),
            OMM_thread_notes = localStorage.getItem('OMM_thread_notes'),
            obj              = JSON.parse(OMM_logs_ops),
            obj2             = JSON.parse(OMM_thread_notes),
            me               = this,
            prin_info        = this.lookupReference('prin_info').getForm(),
            prin_val         = prin_info.getValues();

        var myMask  = new Ext.LoadMask({
            msg     :'Processing your request. Please wait...',
            target  :view
        });
        myMask.show();

        if(values['card_transmit']==''){
            Ext.MessageBox.show({title:'WARNING', msg:'Please fill up required fields.', icon:Ext.Msg.WARNING,closable:false, buttonText:{ok:'OK'}});
            myMask.hide();
        }else{
            Ext.Ajax.request({                
                url         :'../data/sla_pid_transmittal_update.php',
                // url     :'https://www.benefitsmadebetter.com/devmod5/Activebmb/data/getDependentList.php',
                params  :{ops:obj['ops'],acc_id:prin_val['company_id'],activelink_id:prin_val['activelink_id'],phys_id_transmittal_date:values['card_transmit']},
                method  :'POST',
                scope   :me,
                success :function(response){
                    var response    = Ext.decode(response.responseText);
                    if(response.success==true){
                        myMask.hide();
                        Ext.MessageBox.show({title:'SUCCESS',msg:response.message, icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
                        this.onSlaUpdate_beforerender()
                    }else{
                        myMask.hide();
                        Ext.MessageBox.show({title:'FAILED', msg:response.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}})
                    }
                },
                failure :function(){
                    myMask.hide();
                    Ext.MessageBox.show({title:'FAILED',msg:'500 Internal Server Error', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                }
            });

        }
    },


    //Beforerender - SLA update panel
    onSlaUpdate_beforerender:function(){
        var OMM_logs_ops        = localStorage.getItem("OMM_logs_ops"),
            OMM_thread_notes    = localStorage.getItem('OMM_thread_notes'),
            obj                 = JSON.parse(OMM_logs_ops),
            obj2                = JSON.parse(OMM_thread_notes),
            view                = Ext.getCmp("ops_tabpanel"),
            sla_hmoid           = this.lookupReference('sla_hmoid'),
            sla_cardr           = this.lookupReference('sla_cardr'),
            sla_cardt           = this.lookupReference('sla_cardt'),
            prin_info           = this.lookupReference('prin_info').getForm(),
            prin_val            = prin_info.getValues();

        var form                = this.lookupReference('sla_form').getForm(),
            values              = form.getValues();
        var myMask              = new Ext.LoadMask({
            msg                 : 'Loading content. Please wait...',
            target              :view
        });
        myMask.show();                            

        Ext.Ajax.request({
            url     :'../data/sla_validator.php',
            params  :{activelink_id:prin_val['activelink_id'],acc_id:prin_val['company_id'],ops:obj['ops']},
            success :function(response){
                var response    = Ext.decode(response.responseText);

                if(response.success==true){
                    sla_hmoid.setDisabled(false);
                    sla_cardr.setDisabled(false);
                    sla_cardt.setDisabled(false);
                }else{
                    sla_hmoid.setDisabled(true);
                    sla_cardr.setDisabled(true);
                    sla_cardt.setDisabled(true);
                }
            },failure:function(response){}
        });

        Ext.Ajax.request({
            url :'../data/get_prin_hmoid.php',
            params  :{ops:obj['ops'],activelink_id:prin_val['activelink_id']},
            success     :function(response){
                var response = Ext.decode(response.responseText),
                    data     = response.data;
                form.setValues({hmoid_no:data})
            },failure:function(){
                Ext.MessageBox.show({title:'FAILED', msg:'Something went wrong. Please contact IT for assistant.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
            }
        });

        Ext.Ajax.request({
            url     :'../data/get_prin_id_released_date.php',
            params  :{ops:obj['ops'], activelink_id:prin_val['activelink_id']},
            success :function(response){
                var response = Ext.decode(response.responseText),
                    data     = response.data;

                    form.setValues({hmoid_released_date:data})
            },failure:function(){
                Ext.MessageBox.show({title:'FAILED', msg:'Something went wrong. Please contact IT for assistant.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
            }
        });

        Ext.Ajax.request({
            url     :'../data/get_prin_pid_released_date.php',
            params  :{ops:obj['ops'], activelink_id:prin_val['activelink_id']},
            success :function(response){
                var response = Ext.decode(response.responseText),
                    data     = response.data;

                    form.setValues({card_received:data})
            },failure:function(){
                Ext.MessageBox.show({title:'FAILED', msg:'Something went wrong. Please contact IT for assistant.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
            }
        });

        Ext.Ajax.request({
            url     :'../data/get_prin_pid_transmittal_date.php',
            params  :{ops:obj['ops'],activelink_id:prin_val['activelink_id']},
            success :function(response){
                var response = Ext.decode(response.responseText),
                    data     = response.data;

                    form.setValues({card_transmit:data});

            },failure:function(){
                Ext.MessageBox.show({title:'FAILED',msg:'Something went wrong. Please contact IT for assistant.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
            }
        });

        Ext.Ajax.request({
            url     :'../data/sla_getDetails.php',
            params  :{ops:obj['ops'], activelink_id:prin_val['activelink_id']},
            success :function(response){
                var response   = Ext.decode(response.responseText),
                    sla_option = response.sla_option,
                    batch_code = response.batch_code;

                    form.setValues({sla_option:sla_option,batch_code:batch_code});
            },failure:function(){
                Ext.MessageBox.show({title:'FAILED', msg:'Something went wrong. Please contact IT for assistant.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
            }
        });
        myMask.hide();

    },

    //rowdblclick - principal grid
    onPrincipalDetails:function(row, r, tr, rowIndex, e, eOpts,grid,record,colIndex){
        console.log('Principal Details : '+r.data);
        var view                    = Ext.getCmp("ops_tabpanel"),
            cur_tab_id              = "prin"+r.data['activelink_id']
            details                 = {},
            details.activelink_id   = r.data['activelink_id'],
            details.acc_id          = r.data['company_id'],
            me                      = this;

        localStorage.setItem("OMM_thread_notes",Ext.encode(details));

        if(Ext.getCmp(cur_tab_id)){
            view.setActiveTab(cur_tab_id)
        }else{
            var myMask = new Ext.LoadMask({
                    msg    : 'Processing your request. Please wait...',
                    target : view
            });
            myMask.show();

            view.add({
                id       :cur_tab_id,
                xtype    :'principal_details',
                 defaults :{fieldStyle:'font-size:12px;',labelStyle:'font-size:12px;'},
                title    :'Principal Info - '+r.data['fname']+' '+r.data['lname'],
                items    :[                
                {
                    title       :'SLA Updates',
                    itemId      :'sla_form',
                    reference   :'sla_form',
                    xtype       :'form',
                    region      :'east',
                    autoScroll  :true,
                    width       : '21%',
                    collapsible : true,
                    split       :true,
                    defaultType :'textfield',
                    defaults    :{width:'90%',labelAlign:'top',margin:'0 0 0 10'},
                    items       :[
                    {
                        fieldLabel :'HMO ID number', name:'hmoid_no'
                    },
                    {
                        fieldLabel:'HMO ID number Release Date', xtype:'datefield',minValue:'1900-01-01',maskRe: /[0-9\/]/,format:'Y-m-d',name:'hmoid_released_date'
                    },
                    {
                        xtype   :'button',
                        text    :'Save',
                        margin  :'5 0 5 10',
                        width   :90,
                        height  :30,
                        ui      :'soft-green',
                        itemId  :'sla_hmoid',
                        reference  :'sla_hmoid',
                        handler :'onHmoIdUpdate'
                    },
                    {
                        fieldLabel:'Card Received', xtype:'datefield',minValue:'1900-01-01',format:'Y-m-d',name:'card_received'
                    },
                    {
                        xtype   :'button',
                        text    :'Save',
                        margin  :'5 0 5 10',
                        width   :90,
                        height  :30,
                        ui      :'soft-green',
                        itemId  :'sla_cardr',
                        reference  :'sla_cardr',
                        handler :'onSaveCardReceived'  
                    },
                    {
                        fieldLabel:'Card Transmitted', xtype:'datefield',minValue:'1900-01-01',format:'Y-m-d',name:'card_transmit'
                    },
                    {
                        xtype   :'button',
                        text    :'Save',
                        margin  :'5 0 5 10',
                        width   :90,
                        height  :30,
                        ui      :'soft-green',
                        itemId  :'sla_cardt',
                        reference  :'sla_cardt',
                        handler :'onSaveCardTransmit'  
                    },
                    {
                        fieldLabel  :'Current Contract Year',
                        margin      :'0 0 -10 10',
                        labelAlign  :'left',
                        labelWidth  :140,
                        name        :'batch_code',
                        xtype       :'displayfield'
                    },
                    {
                        fieldLabel  :'SLA Option',
                        margin      :'-15 0 -10 10',
                        labelAlign  :'left',
                        labelWidth  :80,
                        name        :'sla_option',
                        xtype       :'displayfield'
                    },
                    {
                        fieldLabel  :'HR endorsed date',
                        margin      :'-15 0 -10 10',
                        labelAlign  :'left',
                        labelWidth  :140,
                        name        :'hr_endorsed_date',
                        xtype       :'displayfield'
                    },
                    {
                        fieldLabel  :'OPS endorsed date',
                        margin      :'-15 0 -10 10',
                        labelAlign  :'left',
                        labelWidth  :140,
                        name        :'ops_endorsed_date',
                        xtype       :'displayfield'
                    }
                    ],
                    listeners:{
                        beforerender:function(){
                        
                            console.log(this);
                            var OMM_logs_ops        = localStorage.getItem("OMM_logs_ops"),
                                OMM_thread_notes    = localStorage.getItem('OMM_thread_notes'),
                                obj                 = JSON.parse(OMM_logs_ops),
                                obj2                = JSON.parse(OMM_thread_notes),
                                view                = Ext.getCmp("ops_tabpanel"),
                                sla_hmoid           = this.down('#sla_hmoid'),
                                sla_cardr           = this.down('#sla_cardr'),
                                sla_cardt           = this.down('#sla_cardt');


                            var form                = this.up('form').getForm(),
                                values              = form.getValues();
                            var myMask              = new Ext.LoadMask({
                                msg                 : 'Loading content. Please wait...',
                                target              :view
                            });
                            myMask.show();                            

                            Ext.Ajax.request({
                                url     :'../data/sla_validator.php',
                                params  :{activelink_id:obj2['activelink_id'],acc_id:obj2['acc_id'],ops:obj['ops']},
                                success :function(response){
                                    var response    = Ext.decode(response.responseText);

                                    if(response.success==true){
                                        sla_hmoid.setDisabled(false);
                                        sla_cardr.setDisabled(false);
                                        sla_cardt.setDisabled(false);
                                    }else{
                                        sla_hmoid.setDisabled(true);
                                        sla_cardr.setDisabled(true);
                                        sla_cardt.setDisabled(true);
                                    }
                                },failure:function(response){}
                            });

                            Ext.Ajax.request({
                                url :'../data/get_prin_hmoid.php',
                                params  :{ops:obj['ops'],activelink_id:obj2['activelink_id']},
                                success     :function(response){
                                    var response = Ext.decode(response.responseText),
                                        data     = response.data;
                                    form.setValues({hmoid_no:data})
                                },failure:function(){
                                    Ext.MessageBox.show({title:'FAILED', msg:'Something went wrong. Please contact IT for assistant.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
                                }
                            });

                            Ext.Ajax.request({
                                url     :'../data/get_prin_id_released_date.php',
                                params  :{ops:obj['ops'], activelink_id:obj2['activelink_id']},
                                success :function(response){
                                    var response = Ext.decode(response.responseText),
                                        data     = response.data;

                                        form.setValues({hmoid_released_date:data});                                        
                                },failure:function(){
                                    Ext.MessageBox.show({title:'FAILED', msg:'Something went wrong. Please contact IT for assistant.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
                                }
                            });

                            Ext.Ajax.request({
                                url     :'../data/get_prin_pid_released_date.php',
                                params  :{ops:obj['ops'], activelink_id:obj2['activelink_id']},
                                success :function(response){
                                    var response = Ext.decode(response.responseText),
                                        data     = response.data;

                                        form.setValues({card_received:data})
                                },failure:function(){
                                    Ext.MessageBox.show({title:'FAILED', msg:'Something went wrong. Please contact IT for assistant.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
                                }
                            });

                            Ext.Ajax.request({
                                url     :'../data/get_prin_pid_transmittal_date.php',
                                params  :{ops:obj['ops'],activelink_id:obj2['activelink_id']},
                                success :function(response){
                                    var response = Ext.decode(response.responseText),
                                        data     = response.data;

                                        form.setValues({card_transmit:data});

                                },failure:function(){
                                    Ext.MessageBox.show({title:'FAILED',msg:'Something went wrong. Please contact IT for assistant.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
                                }
                            });

                            Ext.Ajax.request({
                                url     :'../data/sla_getDetails.php',
                                params  :{ops:obj['ops'], activelink_id:obj2['activelink_id']},
                                success :function(response){
                                    var response          = Ext.decode(response.responseText),
                                        sla_option        = response.sla_option,
                                        batch_code        = response.batch_code;
                                        hr_endorsed_date  = response.hr_endorsed_date;
                                        ops_endorsed_date = response.ops_endorsed_date;

                                        form.setValues({sla_option:sla_option,batch_code:batch_code,hr_endorsed_date:hr_endorsed_date,ops_endorsed_date:ops_endorsed_date});
                                },failure:function(){
                                    Ext.MessageBox.show({title:'FAILED', msg:'Something went wrong. Please contact IT for assistant.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
                                }
                            });
                            myMask.hide();

                        }
                    }
                },
                {
                    xtype       :'form',
                    reference   :'prin_info',
                    region      :'center',
                    layout      :'hbox',                 
                    overflowY   :'scroll',
                    items       :[
                    {
                        xtype       :'panel',
                        width       :'45%',
                        bodyPadding :5,
                        items       :[
                        {
                            xtype       :'fieldset',
                            title       :'<b>Personal Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%',labelWidth:130, allowBlank:true},
                            layout      :'anchor',
                            items       :[
                                {name:'session_userid',reference:'session_userid', hidden:true},
                                {fieldLabel:'First Name',name:'fname', allowBlank:false},
                                {fieldLabel:'Middle Name',name:'mname'},
                                {fieldLabel:'Last Name',name:'lname',allowBlank:false},
                                {fieldLabel:'Suffix Name',name:'ename'},
                                {fieldLabel:'Gender',xtype:'combo',store:Ext.create("OMM.store.Get_gender"),name:'gender',editable:false,emptyText:'Select Gender',valueField:'gender',displayField:'gender'},
                                {xtype:'datefield',fieldLabel:'Birthdate',name:'birthdate'},
                                {fieldLabel:'Civil Status',xtype:'combo',store:Ext.create("OMM.store.Get_civilstatus"),name:'civil_status',editable:false,emptyText:'Select Civil Status',valueField:'civil_status',displayField:'civil_status'}
                            ]
                        },
                        {
                            xtype       :'fieldset',
                            title       :'<b>Company Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%',labelWidth:140, allowBlank:true},
                            layout      :'anchor',
                            items       :[
                                {fieldLabel:'ActiveLink ID',name:'activelink_id',allowBlank:false,readOnly:true,hidden:true},
                                {fieldLabel:'Employee ID',name:'empid',reference:'prin_empid'},
                                {fieldLabel:'Email',name:'email',reference:'prin_email', value:r.data['email'], editable:false,hidden:true},
                                {fieldLabel:'Company ID',name:'company_id',reference:'prin_acc_id',readOnly:true},                                
                                {fieldLabel:'Branch',xtype:'combo',name:'branch',emptyText:'Select Plan level',store:Ext.create("OMM.store.Get_account_branchid"),reference:'prin_branch',emptyText:'Select Branch',editable:false,valueField:'branch',displayField:'branch',listeners:{el:{click:'onPrinBranchID'}, select:'onPrinBranchChange'}},
                                {fieldLabel:'Branch ID',name:'branch_id',reference:'prin_branch_id', hidden:true},
                                {fieldLabel:'Designation',name:'designation'},
                                {fieldLabel:'Last Member Update',name:'member_date',readOnly:true},
                                {fieldLabel:'Registration Date',name:'reg_date',readOnly:true},
                                {fieldLabel:'Existing Flag',name:'existing_flag',readOnly:true},
                                {fieldLabel:'Inclusion Date',name:'inclusion_date',readOnly:true},
                                {fieldLabel:'Deactivation Date',name:'deactivation_date'},
                                {fieldLabel:'Hire Date',name:'emp_hire_date',xtype:'datefield',format:'Y-m-d H:i:s',readOnly:true}
                            ]
                        }
                        ]
                    },
                    {
                        xtype       :'panel',
                        width       :'55%',
                        bodyPadding :5,
                        items:[
                        {
                            xtype       :'fieldset',
                            columnWidth :0.5,
                            title       :'<b>HMO Information</b>',
                            collapsible :true,
                            layout      :'anchor',
                            defaultType :'textfield', 
                            defaults    :{anchor:'95%', labelWidth:150, allowBlank:true},
                            items       :[
                                {fieldLabel:'Member Status', name:'mbr_status',reference:'prin_mbr_status', readOnly:true},
                                {xtype:'form',defaultType :'textfield',defaults:{width:'100%',labelWidth:150, allowBlank:true},margin :'10 0 0 0',bodyStyle   :{"background-color":"#f4f3d7"}},
                                {fieldLabel:'HMO Provider',name:'hospital_hmo',editable:false, xtype:'combo', store:provider, queryMode:'local', displayField:'prov_name', valueField:'prov_code'},
                                {fieldLabel:'Dental Provider',name:'dental_hmo', editable:false, xtype:'combo', store:provider, queryMode:'local', displayField:'prov_name', valueField:'prov_code'},
                                {fieldLabel:'Principal Plan level',xtype:'combo',name:'plan_id',reference:'prin_planLevel',emptyText:'Select Plan level',store:Ext.create("OMM.store.Get_account_plans"),displayField:'planid',valueField:'planid',editable:false,listeners:{el:{click:'onPrinCompanyID'},select:'onPrinPlanChange'}},
                                {fieldLabel:'Benefit limit',name:'benefit_limit',readOnly:true},
                                {fieldLabel:'Plan Description',name:'plan_desc',readOnly:true, reference:'prin_plan_desc'},
                                {fieldLabel:'Original Effective Date',xtype:'datefield',minValue:'1910-01-01',format:'Y-m-d',name:'orig_eff_date'},
                                {fieldLabel:'Current Effective Date', xtype:'datefield',minValue:'1910-01-01',format:'Y-m-d',name:'cur_eff_date'},
                                {fieldLabel:'Maturity Date',name:'maturity_date', xtype:'datefield',minValue:'1910-01-01',format:'Y-m-d'}                
                            ]
                        },
                        {
                            xtype       :'fieldset',
                            title       :'<b>Contact  Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%',labelWidth:140, allowBlank:true,editable:false},
                            layout      :'anchor',
                            items       :[
                                {xtype:'label',style:'font-weight:bold;font-size:11px;font-face:arial',html:'NOTE : <span style="color:#2f9fe9;font-weight:bold;font-face:arial;font-size:11px;font-style:oblique;">User cannot change the information below. For viewing purposes only. </span>'},
                                {fieldLabel:'Email',name:'email1'},
                                {fieldLabel:'Address',name:'address1'},
                                {fieldLabel:'Contact 1', name :'contact1'},
                                {fieldLabel:'Contact 2',name:'contact2'}
                            ]
                        },
                        {
                            xtype       :'fieldset',
                            title       :'<b>Additional Information</b> e.g.(PhilHealth,SSS no, Pag-ibig no, etc.)',
                            collapsible :true,
                            collapsed   :true,
                            animate      :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%',labelWidth:140, allowBlank:true},
                            layout      :'anchor',// overflowY   :'scroll',
                            items       :[
                                {fieldLabel:'PhilHealth',name:'philhealth'},
                                {fieldLabel:'SSS no',name:'sss_no'},
                                {fieldLabel:'Pag-ibig no',name:'pag_ibig_no'},  
                                {fieldLabel:'Unified ID no',name:'unified_id_no'},
                                {fieldLabel:'TIN',name:'tin'}
                            ]
                        },
                        {
                            xtype        :'gridpanel',
                            title        :'<b>Dependent/s</b>',
                            reference    :'prin_dep_grid_p',
                            itemId       :'prin_dep_grid_p',
                            ui           :'light',
                            autoScroll   :true,
                            plugins      :'gridfilters',
                            headerBorders:false,
                            rowLines     :true,
                            bodyPadding  :10,
                            titleCollapse:true,
                            animate      :true,
                            layout       :'fit',
                            stripeRows   :false,
                            trackMouseOver:false,
                            viewConfig   :{preserveScrollOnRefresh:true,preserveScrollOnReload:true,deferEmptyText:true,emptyText:'<h1>No data found</h1>'},
                            store        :Ext.create("OMM.store.principal.Get_principal_dependent_list"),
                            tbar         :[//57b3cb
                            {xtype:'label', style:'font-weight:bold;font-size:11px:font-face:arial', html:'NOTE : <span style="color:#2f9fe9;font-size:11px;font-style:oblique;">Double click row to view information</span>'},
                            '->',
                            {xtype:'button',iconCls:'x-fa fa-refresh',ui:'soft-green',handler:'onPrinDepGrid_refresh'},
                            {xtype:'button',iconCls:'x-fa fa-user-plus',text:'Add Dependent',ui:'soft-blue',handler:'onAddDeps'}
                            ],
                            columns      :[
                            {xtype:'rownumberer', width:30},
                            {header:'Name(Surname, Firstname, Middlename)',dataIndex:'fullname',filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}, width:270},
                            {header:'Relationship',dataIndex:'relationship',filter:'list', width:100},{header:'Plan Level',dataIndex:'dplan_level',filter:'list', width:100},
                            {header:'Status', dataIndex:'dmbr_status'}
                            ],
                            listeners    :{
                                rowdblclick     :'onPrincipalDependentDetails'                        
                            }
                        }]
                    }]
                }],
                listeners       :{
                    beforerender:function(component, eOpts){

                        var prin_mbr_status = this.lookupReference('prin_mbr_status');
                        var prin_dep_grid   = this.lookupReference('prin_dep_grid_p');
                        var prin_del        = this.lookupReference('prin_del');
                        var prin_rectv      = this.lookupReference('prin_rectv');
                        var prin_reset      = this.lookupReference('prin_reset');
                        var prin_update     = this.lookupReference('prin_update'),
                            me              = this;

                        var session_userid        = this.lookupReference('session_userid');

                        //Contact Info
                        var email1         =component.getForm().findField('email1'),
                            address1       =component.getForm().findField('address1'),
                            contact1       =component.getForm().findField('contact1'),
                            contact2       =component.getForm().findField('contact2'),
                            inclusion_date =component.getForm().findField('inclusion_date'),
                            deactivation_date =component.getForm().findField('deactivation_date'); 

                        Ext.Ajax.request({
                        url     :'../data/post_username.php',
                        success :function(response){
                            var response=Ext.decode(response.responseText);
                            session_userid.setValue(response.username);
                            console.log(session_userid.getValue())
                        }
                        });

                        Ext.Ajax.request({
                            url     :'../data/get_pmember_details.php',
                            headers :{'Content-Type': 'application/json'},
                            params  :{p_al_id:r.get('activelink_id'), acc_id:r.get('company_id')},
                            method  :'GET',
                            success :function(response){
                                var principal       = Ext.create('OMM.model.principal.PrincipalModel');
                                var response        = Ext.decode(response.responseText),
                                prin_form           = Ext.getCmp(cur_tab_id);

                                console.log(response.data[0])
                                if (response.data[0]) {
                                    principal.set(response.data[0]);
                                    console.log(principal)
                                    prin_form.loadRecord(principal);
                                    prin_dep_grid.getStore().load({params:{prin_al_id:r.get('activelink_id')}});

                                    myMask.hide();
                                    var values      = prin_form.getForm().getValues();
                                    // if(values['fname'].indexOf("_DEL")>=0){
                                    if(values['mbr_status']!='ACTIVE'){
                                        prin_del.hide();
                                        prin_rectv.show();
                                        prin_update.setDisabled(true);
                                    }else{
                                        prin_del.show();
                                        prin_rectv.hide();
                                        prin_update.setDisabled(false);
                                    }
                                }
           
                                Ext.Ajax.request({
                                url:'../data/get_pmember_adddetails.php',
                                method:'GET',
                                params :{p_al_id:r.get('activelink_id'), acc_id:r.data['company_id']},
                                success: function ( result, request ) {
                                    var jsonData = Ext.util.JSON.decode(result.responseText);
                                    if(jsonData.success==true){
                                        // console.log(jsonData.data[0].lname);
                                        email1.setValue(jsonData.data[0].email)
                                        address1.setValue(jsonData.data[0].address1)
                                        contact1.setValue(jsonData.data[0].contact1)
                                        contact2.setValue(jsonData.data[0].contact2)
                                        inclusion_date.setValue(jsonData.data[0].inclusion_date)
                                        deactivation_date.setValue(jsonData.data[0].deactivation_date) 
                                    }else{
                                        Ext.MessageBox.show({title:'WARNING', msg:jsonData.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                                    }
                                },
                                failure: function ( result, request) {
                                    Ext.MessageBox.show({title:'WARNING', msg:result.responseText, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                                    return false;
                                }
                                });
                            },
                            failure:function(response){ var response    = Ext.decode(response.responseText);Ext.Msg.alert('WARNING',response.message);myMask.hide();}
                        });
                }
            }
            });
            view.setActiveTab(cur_tab_id)
        }
    },

    onPrinDepGrid_refresh:function(btn){
        var prin_dep_grid   = this.lookupReference('prin_dep_grid_p');
        var form            = btn.up('form').getForm();
        var values          = form.getValues();

        prin_dep_grid.getStore().load({params:{prin_al_id:values['activelink_id']}});
    },

    onAddDeps:function(btn, e){
        var form         = btn.up('form').getForm();
        var values       = form.getValues();
        var now          = new Date();

        Ext.Ajax.request({
            url     :'../data/get_dependent_count.php',
            params  :{session_userid:values['session_userid'],acc_id:values['company_id'],p_al_id:values['activelink_id']},
            success :function(response){
                var response  = Ext.decode(response.responseText);
                
                var d         = new Date();
                var n         = d.getFullYear();
                var Yr        = n.toString().substring(2);
                var al_dep_id = values['activelink_id']+'D'+Yr+Ext.Date.format(new Date(),'md')+'A00'+response.count;
                console.log(values['activelink_id']+'D'+Ext.Date.format(new Date(),'Ymd')+'A00'+response.count);

                var win = new Ext.Window({
                    closeAction     :'destroy',
                    width           :'60%',
                    controller      :'principal',
                    modal           :true,
                    maximized       :true,
                    closable        :false,
                    layout          :'fit',
                    autoShow        :true,
                    bodyPadding     :5,
                    title           :values['fname']+' '+values['lname']+' (New Dependent)',
                    items           :[
                    {
                        xtype           :'form',
                        reference       :'dep_add_form',
                        layout          :'border',
                        items:[
                        {
                            xtype        :'gridpanel',
                            title        :'<b>Dependent/s</b>',
                            reference    :'prin_dep_grid',
                            region       :'south',
                            collapsible: true,
                            autoScroll   :true,
                            plugins      :'gridfilters',
                            headerBorders:false,
                            rowLines     :true,
                            bodyPadding  :10,
                            titleCollapse:true,
                            animate      :true,
                            layout       :'fit',
                            stripeRows   :false,
                            trackMouseOver:false,
                            viewConfig   :{preserveScrollOnRefresh:true,preserveScrollOnReload:true,deferEmptyText:true,emptyText:'<h1>No data found</h1>'},
                            store        :Ext.create("OMM.store.principal.Get_principal_dependent_list"),
                            columns      :[
                            {xtype:'rownumberer', width:30},
                            {header:'Name(Surname, Firstname, Middlename)',dataIndex:'fullname', flex:1},
                            {header:'Relationship',dataIndex:'relationship',filter:'list', width:100},{header:'Plan Level',dataIndex:'dplan_level', flex:1},
                            {header:'Status', dataIndex:'dmbr_status', flex:1}
                            ]
                        },
                        {
                            xtype    :'panel',
                            region   :'center',
                            layout   :'hbox',
                            overflowY:'scroll',
                            defaults :{margin:10},
                            items    :[                    
                            {
                                xtype       :'fieldset',
                                columnWidth :0.3,
                                title       :'<b>Personal Information</b>',
                                defaultType :'textfield',
                                width       :'32%',
                                defaults    :{anchor:'95%',labelWidth:100, allowBlank:true},
                                layout      :'anchor',
                                items       :[
                                    {name:'activelink_id',reference:'activelink_id', value:values['activelink_id'], hidden:true},
                                    {name:'session_userid',reference:'session_userid',value:values['session_userid'], hidden:true},
                                    {name:'mbr_status',value:values['mbr_status'],hidden:true},
                                    {name:'plan_id', value:values['plan_id'], hidden:true},
                                    {name:'branch_id', value:values['branch_id'], hidden:true},
                                    {fieldLabel:'Relationship',xtype:'combo',name:'relationship',allowBlank:false, store:dep_relationship,queryMode:'local',displayField:'relationship',valueField:'relationship',editable:false},
                                    {fieldLabel:'First Name',name:'fname', allowBlank:false},
                                    {fieldLabel:'Middle Name',name:'mname'},
                                    {fieldLabel:'Last Name',name:'lname',allowBlank:false},
                                    {fieldLabel:'Suffix Name',name:'ename'},
                                    {fieldLabel:'Gender',xtype:'combo',name:'gender',allowBlank:false, store:Ext.create("OMM.store.Get_gender"),valueField:'gender',displayField:'gender',editable:false,emptyText:'Select Gender'},
                                    {xtype:'datefield',fieldLabel:'Birthdate',name:'bdate', allowBlank:false},
                                    {fieldLabel:'Civil Status',xtype:'combo',name:'civil_status',store:Ext.create("OMM.store.Get_civilstatus"),valueField:'civil_status',displayField:'civil_status',emptyText:'Select Civil Status',editable:false}
                                ]
                            },
                            {
                                xtype       :'panel',
                                width       :'35%',
                                defaultType :'fieldset',
                                defaults    :{layout:'anchor', collapsible:true},
                                items       :[
                                {
                                    title   :'<b>Enrollment Setup</b>',
                                    defaults:{anchor:'95%', labelWidth:130},
                                    items   :[
                                        {fieldLabel:'Enrollment Status',flex:1,xtype:'combo',name:'dep_stat',reference:'dep_enroll_stat',allowBlank:false,store:Ext.create("OMM.store.Get_status"),valueField:'id',displayField:'status',emptyText:'Select dependent status',editable:false},
                                        {xtype: 'numberfield',name : 'dep_hierarchy',fieldLabel: 'Hierarchy',allowBlank:false,maxValue: 7,minValue:0,validator: function(value) {return (value > 0) ? true : 'No. must be greater than the 0.'}},
                                        {fieldLabel:'Group Plan ID', xtype:'combo', hidden:true,reference:'dep_group_plan', name:'dep_group_plan', store:Ext.create("OMM.store.Get_groupbase_plan"), valueField:'id', displayField:'show_for', emptyText:'Select Plan', editable:false,listeners:{el:{click:'onDepGroupBasePlan_change'}}},
                                        {fieldLabel:'Dependent type',xtype:'combo',allowBlank:false, name:'dep_type',reference:'dep_type', store:Ext.create("OMM.store.Get_deptype"),valueField:'id',displayField:'dep_type',emptyText:'Select type',editable:false,listeners:{el:{click:'onDepType_change'}}},
                                        // {fieldLabel:'Dependent Qualified',allowBlank:false, name:'dep_qualified', xtype:'numberfield', minValue:0, maxValue:1},
                                        {
                                            fieldLabel  :'Dependent Qualified',
                                            xtype       : 'fieldcontainer', 
                                            allowBlank  :false,
                                            defaultType :'radiofield',
                                            defaults    :{flex:1},
                                            layout      :'hbox',
                                            items       :[
                                            {
                                                boxLabel    :'Yes',
                                                margin      :'0 0 0 10',
                                                name        :'dep_qualified',
                                                inputValue :'1'
                                            },
                                            {
                                                boxLabel    :'No',
                                                name        :'dep_qualified',
                                                inputValue :'0'
                                            }
                                            ]
                                        },
                                        {xtype:'label', style:'font-weight:bold;font-size:11px:font-face:arial', html:'NOTE : <span style="color:#2f9fe9;font-size:11px;font-style:oblique;"><font color=red>0</font> means dependent is qualified and <font color=red>1</font> means dependent is not qualified</span>'}
                                    ]
                                },
                                {
                                    title       :'<b>Remark/s</b>',
                                    defaultType :'textfield',
                                    defaults    :{anchor:'95%',labelWidth:130},
                                    items       :[
                                    {fieldLabel:'PhilHealth', name:'philhealth'},
                                    {fieldLabel:'Remark',xtype:'combo',name:'dep_remarks',allowBlank:false, reference:'dep_remarks',store:dep_remarks, valueField:'r_name', displayField:'r_name', queryMode:'local',emptyText:'Select dependent status',editable:false, listeners:{change:'onChangeRemark'}},
                                    {
                                        xtype     :'form',
                                        reference :'skip_enroll_panel',
                                        ui        :'light',
                                        hidden    :true,
                                        flex      :1,
                                        defaultType:'checkbox',
                                        defaults  :{checked:false,anchor:'100%'},
                                        items     :[]
                                      }]
                                }]
                            },
                            {
                                xtype       :'panel',
                                width       :'32%',
                                defaultType :'fieldset',
                                defaults    :{layout:'anchor'},
                                items       :[
                                {         
                                    xtype       :'fieldset',
                                    columnWidth :0.3,
                                    title       :'<b>Dependent generated ActiveLink ID</b>',
                                    defaultType :'textfield',
                                    layout      :'anchor',
                                    defaults    :{anchor:'95%',allowBlank:true,margin:'0 5 0 10', labelWidth:130,editable:false,
                                    labelStyle  :'font-size:16px;font-style :font-weight:bold;font-face:arial',
                                    fieldStyle  :'font-size:16px;font-weight:bold;font-face:arial;color:#FF0000'
                                    },
                                    items       :[                           
                                    {value:al_dep_id,name:'al_dep_id'}
                                    ]
                                },     
                                {
                                    xtype       :'fieldset',
                                    columnWidth :0.3,
                                    title       :'<b>Required Documents</b>',
                                    defaultType :'textfield',
                                    defaults    :{anchor:'95%',labelWidth:140, allowBlank:true},
                                    layout      :'anchor',
                                    items       :[
                                    {
                                        fieldLabel  :'Company ID', labelWidth:90,hidden:true,
                                        name        :'company_id',
                                        value       :values['company_id']
                                    },
                                    {fieldLabel:'Required Documents',reference:'dep_req_docs',multiSelect: true,triggerAction: 'all', xtype:'combo',name:'req_docs',store:Ext.create("OMM.store.Get_dependent_req_documents"),valueField:'doc_id',displayField:'doc_name',emptyText:'Select Document/s',editable:false,listeners:{el:{click:'onDepReqDoc_change'}}}
                                    ]
                                }]
                            }]
                        }],

                        bbar:{
                            defaults:{ui:'soft-red', margin:5},
                            items:[
                            {
                            xtype       :'panel',
                            reference   :'prin_info_panel',
                            title       :'<b>Principal Information</b>',
                            defaultType :'displayfield',
                            defaults    :{anchor:'95%',allowBlank:true, margin:'0 5 0 0',
                            labelStyle  :'font-size:12px;font-style :font-weight:bold;font-face:arial',
                            fieldStyle  :'font-size:12px;font-weight:bold;font-face:arial;color:#FF0000'
                            },
                            layout      :'hbox',
                            items       :[
                            {
                                fieldLabel  :'Employee No',labelWidth:90,
                                value       :values['empid']
                            },{
                                fieldLabel  :'Company ID', labelWidth:90,
                                value       :values['company_id']
                            },
                            {
                                fieldLabel  :'Civil status',labelWidth:80, 
                                value       :values['civil_status']
                            },
                            {
                                fieldLabel  :'Gender',labelWidth:65, 
                                value       :values['gender']
                            },
                            {
                                fieldLabel  :'Birthdate',labelWidth:70, 
                                value       :values['birthdate']
                            }
                            ]
                            },
                                '->',
                            {text:'Add Dependent',handler:'onAddDeps_submit'},
                            {text:'Cancel',handler:function(btn){btn.up('window').hide();}}
                            ]
                        }
                    }
                    ],
                    listeners:{
                        afterrender:function(){
                            var prin_dep_grid   =this.lookupReference('prin_dep_grid'),
                                prin_info_panel =this.lookupReference('prin_info_panel'),
                                dep_group_plan  =this.lookupReference('dep_group_plan');

                            if(values['session_userid']==''){
                                Ext.MessageBox.show({
                                    title:'SESSION EXPIRED',
                                    msg:'You session has been expired!<br><a href="https://www.benefitsmadebetter.com/OMM"><b>Log In here</a>',
                                    closable:false,icon:Ext.Msg.WARNING})
                            }else{
                                this.lookupReference('prin_dep_grid').getStore().clearFilter(true);
                                prin_dep_grid.getStore().load({params:{prin_al_id:values['activelink_id']}});

                                if(values['company_id']=='QBE'){
                                    dep_group_plan.show();
                                }else{
                                    dep_group_plan.hide()
                                }
                            }
                        }
                    }
                });
            },failure:function(){
                Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please contact IT for your assistance', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
            }
        })
    },

    onClosewin:function(){
        var win               = Ext.WindowManager.getActive();
        win.hide();
    },

    //ComboBox - OnChange Dependent Group Plan ID
    onDepGroupBasePlan_change:function(){
        var dep_add_form    = this.lookupReference('dep_add_form'),
            form            = dep_add_form.getForm(),
            values          = form.getValues(),
            dep_group_plan  = this.lookupReference('dep_group_plan');

        dep_group_plan.getStore().load({params:{account:values['company_id'], operator:values['session_userid']}});
    },

    //ComboBox - Onchange Dependent type
    onDepType_change_dep_form:function(btn){
        var dep_type       = this.lookupReference('dep_type'),
            acc_id         = this.lookupReference('dep_acc_id'),
            session_userid = this.lookupReference('session_userid');

        dep_type.getStore().load({params:{account:acc_id.getValue(), operator:session_userid.getValue()}})
    },


    //ComboBox - Onchange Dependent type
    onDepType_change:function(btn){
        var dep_add_form = this.lookupReference('dep_add_form'),
            form         = dep_add_form.getForm(),
            values       = form.getValues(),
            dep_type     = this.lookupReference('dep_type');

        dep_type.getStore().load({params:{account:values['company_id'], operator:values['session_userid']}})
    },

    //ComboBox - Onchange Dependent required docs modified
    onDepReqDoc_change:function(){
        var dep_add_form = this.lookupReference('dep_add_form'),
            form         = dep_add_form.getForm(),
            values       = form.getValues(),
            dep_req_docs = this.lookupReference('dep_req_docs');

        dep_req_docs.getStore().load({params:{account:values['company_id'], operator:values['session_userid']}});

    },

    //Button - Save Principal dependet information        
    onAddDeps_submit:function(btn){
        var form              = btn.up('form').getForm(),
            skip_enroll_panel = this.lookupReference('skip_enroll_panel'),
            skip_enroll_form  = skip_enroll_panel.getForm(),
            values            = form.getValues(),
            dep_req_docs      = this.lookupReference('dep_req_docs'),
            prin_d_grid       = "prin"+values['activelink_id'];

            console.log('dep_add : '+prin_d_grid)

            if(form.isValid()){

                console.log(dep_req_docs.getValue());
                skip_enroll_form.getFields().each(function(field){
                    if(field.rawValue==false){
                        console.log("delete checkbox : "+field.boxLabel);
                        field.destroy();
                    }else{
                        console.log(field.name +" "+ field.boxLabel);
                    }
                });
                console.log(skip_enroll_form.getFieldValues(true));

                form.submit({
                url             :'../data/dep_inclusion.php',
                submitEmptyText :false,
                params          :{mbr_status:values['mbr_status'],plan_level:values['plan_id'],branch_id:values['branch_id'],session_userid:values['session_userid'],dep_skip_enroll:Ext.encode(skip_enroll_form.getValues()),dep_req_docs:Ext.encode(dep_req_docs.getValue())},
                success         :function(){
                    var win               = Ext.WindowManager.getActive();
                    Ext.MessageBox.show({title:'Success', msg:'Dependent added', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'},
                        fn:function(btn){
                        if(btn=='ok'){
                            win.hide();
                            Ext.getCmp(prin_d_grid).down('#prin_dep_grid').getStore().load({params:{prin_al_id:values['activelink_id']}});
                            console.log('Load store principal depedent grid');
                    }}});
                },failure       :function(form, action){
                    var win         = Ext.WindowManager.getActive();
                    Ext.MessageBox.show({title:'Failed', msg:action.result.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'},
                        fn:function(btn){if(btn=='ok'){
                         // win.hide();
                         skip_enroll_panel.removeAll();
                         skip_enroll_panel.add(
                        {boxLabel:'Employed',name:'dep_employed',inputValue:'Employed',uncheckedValue:'0'},
                        {boxLabel:'With Existing HMO',name:'dep_withhmo',inputValue:'With Existing HMO',uncheckedValue:'0'},
                        {boxLabel:'Overseas',name:'dep_overseas',inputValue:'Overseas',uncheckedValue:'0'},
                        {boxLabel:'Same address',name:'dep_sameaddress',inputValue:'Same address',uncheckedValue:'0'},
                        {boxLabel:'Overage',name:'dep_overage',anchor:'100%',inputValue:'Overage',uncheckedValue:'0'},
                        {boxLabel:'Underage',name:'dep_underage',inputValue:'Underage',uncheckedValue:'0'},
                        {boxLabel:'Separated/Not living Together',name:'dep_separated',inputValue:'Separated/Not living Together',uncheckedValue:'0'},
                        {boxLabel:'Other Reason',name:'dep_otherreason',inputValue:'Other Reason',uncheckedValue:'0'},
                        {boxLabel:'Skipped Other',name:'dep_skippedother',inputValue:'Skipped Other',uncheckedValue:'0'},
                        {boxLabel:'Withdrawal Reason',name:'dep_withdrawal_reason',inputValue:'Withdrawal Reason',uncheckedValue:'0'});
                     }}});
                }
                });

            }else{
                Ext.MessageBox.show({title:'WARNING', msg:'Please check required fields', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
            }
    },

    onAddDep_beforerender:function(){
        var prin_dep_grid = this.lookupReference('prin_dep_grid');
        prin_dep_grid.getStore().load({params:{prin_al_id:r.get('activelink_id')}});
    },


    //ComboBox - onChange Dependent remarks
    onChangeRemark:function(value, paging, params){
        var skip_enroll_panel = this.lookupReference('skip_enroll_panel');
        this.selected_val = value.getValue();
        console.log('dependent remark: ' + this.selected_val);
        if(this.selected_val=='Skipped for Enrollment'){
            skip_enroll_panel.show();
            skip_enroll_panel.add(               
            {boxLabel:'Employed',name:'dep_employed',inputValue:'Employed',uncheckedValue:'0'},
            {boxLabel:'With Existing HMO',name:'dep_withhmo',inputValue:'With Existing HMO',uncheckedValue:'0'},
            {boxLabel:'Overseas',name:'dep_overseas',inputValue:'Overseas',uncheckedValue:'0'},
            {boxLabel:'Same address',name:'dep_sameaddress',inputValue:'Same address',uncheckedValue:'0'},
            {boxLabel:'Overage',name:'dep_overage',anchor:'100%',inputValue:'Overage',uncheckedValue:'0'},
            {boxLabel:'Underage',name:'dep_underage',inputValue:'Underage',uncheckedValue:'0'},
            {boxLabel:'Separated/Not living Together',name:'dep_separated',inputValue:'Separated/Not living Together',uncheckedValue:'0'},
            {boxLabel:'Other Reason',name:'dep_otherreason',inputValue:'Other Reason',uncheckedValue:'0'},
            {boxLabel:'Skipped Other',name:'dep_skippedother',inputValue:'Skipped Other',uncheckedValue:'0'},
            {boxLabel:'Withdrawal Reason',name:'dep_withdrawal_reason',inputValue:'Withdrawal Reason',uncheckedValue:'0'});
        }else{
            skip_enroll_panel.hide();
            skip_enroll_panel.removeAll();
        }
    },

    // Principal view click dependent/s information
    onPrincipalDependentDetails:function( row, r, tr, rowIndex, e, eOpts,grid,record,colIndex){
        console.log('Dependent details :'+ r.data);
        var view                    =Ext.getCmp("ops_tabpanel"),
            cur_tab_id              ="dep"+r.data['al_dep_id'];
            details                 = {},
            details.activelink_id   = r.data['al_dep_id'],
            details.acc_id          = r.data['dcompanyid'];

        localStorage.setItem("OMM_thread_notes",Ext.encode(details));


        if(Ext.getCmp(cur_tab_id)){
            view.setActiveTab(cur_tab_id)
        }else{
            var myMask = new Ext.LoadMask({
                    msg    : 'Processing your request. Please wait...',
                    target : view
            });
            myMask.show();

            view.add({
                id       :cur_tab_id,
                xtype    :'dependent_details',
                reference:'dependent_details',
                title    :'Dep Info - '+r.data['fname']+' '+r.data['lname'],
                items    :[
                {
                    xtype    :'panel',
                    layout   :'hbox',
                    overflowY:'scroll',
                    items    :[
                    {
                        xtype       :'panel',
                        width       :'45%',
                        bodyPadding :5,
                        items       :[
                        {
                            xtype       :'fieldset',
                            columnWidth :0.5,
                            title       :'<b>Personal Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%',labelWidth:130, allowBlank:true},
                            layout      :'anchor',
                            items       :[
                                {name:'session_userid',reference:'session_userid',hidden:true},
                                {fieldLabel:'First Name',name:'fname',value:r.data['fname'], allowBlank:false},{fieldLabel:'Middle Name',name:'mname',value:r.data['mname']},{fieldLabel:'Last Name',name:'lname',allowBlank:false,value:r.data['lname']},{fieldLabel:'Suffix Name',name:'ename',value:r.data['ename']},
                                {fieldLabel:'Gender',xtype:'combo',name:'gender',value:r.data['gender'],store:Ext.create("OMM.store.Get_gender"),valueField:'gender',displayField:'gender',editable:false,emptyText:'Select Gender'},
                                {xtype:'datefield',fieldLabel:'Birthdate',name:'bdate',value:r.data['bdate']},
                                {fieldLabel:'Age', name:'age', readOnly:true,
                                    renderer : function(value, metaData, r, rowIdx, colIdx, store, view){
                                    calcAge(r.data['bdate']);
                                }
                                },
                                {fieldLabel:'Civil Status',xtype:'combo',name:'civil_status',value:r.data['civil_status'],store:Ext.create("OMM.store.Get_civilstatus"),valueField:'civil_status',displayField:'civil_status',emptyText:'Select Civil Status',editable:false},
                                {fieldLabel:'Relationship',xtype:'combo',name:'relationship',value:r.data['relationship'],store:Ext.create("OMM.store.Get_relationship"),displayField:'relationship',valueField:'relationship',editable:false},
                                {fieldLabel:'PhilHealth',name:'philhealth',value:r.data['philhealth']}
                            ]
                        },
                        {
                            xtype       :'fieldset',
                            columnWidth :0.5,
                            title       :'<b>Company Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%',labelWidth:130, allowBlank:true},
                            layout      :'anchor',
                            items       :[
                                {fieldLabel:'Principal AL ID',name:'al_prin_id',readOnly:true,value:r.data['al_prin_id'],hidden:true},
                                {fieldLabel:'Dependent AL ID',name:'al_dep_id',allowBlank:false,readOnly:false,value:r.data['al_dep_id'],hidden:true,id:'dep_al_dep_id'+r.data['al_dep_id']},
                                {fieldLabel:'Company ID',name:'dcompanyid',value:r.data['dcompanyid'],editable:false,reference:'dep_acc_id',id:'dep_acc_id'+r.data['al_dep_id']},
                                {fieldLabel:'Branch',name:'dbranchid',value:r.data['dbranchid'],readOnly:true,reference:'dep_branch_id'}
                            ]
                        },                        
                        {
                            xtype       :'panel',
                            title       :'<b>Required Documents</b>',
                            defaults    :{anchor:'100%',labelWidth:130},
                            ui          :'light',
                            titleCollapse :true,
                            collapsible :true,
                            animate     :true,
                            layout      :'anchor',
                            bodyPadding :5,
                            tbar        :[
                            {
                                text    :'Modify',
                                ui      :'soft-green',
                                handler :function(btn, grid, record, index, eOpts, rowIndex, colIndex, b){
                                    var g        = Ext.getCmp('dep-required-doc-'+r.data['activelink_id']).getStore();
                                    var array_id = new Array();
                                    var i        = 0;
                                    var file_name=0;

                                    g.each(function (record){
                                        if(record.get('active')===true){
                                            array_id[i]         = record.get('doc_id');
                                            file_name           = record.get('filename');
                                            console.log('Dependent document ID :'+ record.get('doc_id') +' File name : '+record.get('filename'));
                                            var data = record.get('doc_id');
                                            i++;// file_name++;
                                        }
                                    });

                                    if (i<1){
                                        Ext.MessageBox.show({title:'WARNING',msg:'Please select document to modify',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
                                    }else if(i>1){
                                        Ext.MessageBox.show({title:'WARNING',msg:'Please select less than 1 document to modify',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                    }else{// alert(file_name.length); console.log(file_name);
                                        if(file_name.length<=0){
                                           
                                            var f   = new Ext.form.Panel({
                                            layout  :'anchor',
                                            defaults:{anchor:'100%',labelWidth:130, allowBlank:false},
                                            items   :[
                                                {xtype:'filefield',name:'doc_file',fieldLabel:'File',msgTarget:'side'}
                                            ],
                                            bbar    :{
                                                items :[
                                                '->',
                                                {
                                                    text    :'Save Document',
                                                    ui      :'soft-green',
                                                    handler :function(){
                                                        var form = this.up('form').getForm();
                                                        if(form.isValid()){
                                                            form.submit({
                                                                url     :'../data/dependent_save_docs.php',
                                                                params  :{al_prin_id:r.data['al_prin_id'],al_dep_id:r.data['al_dep_id'], acc_id:r.data['dcompanyid'], doc_id:array_id},
                                                                waitMsg :'Please wait...',
                                                                method  :'POST',// timeout : 120000, //2minutes
                                                                headers :{'Content-Type':'multipart/form-data; charset=UTF-8'},
                                                                success:function(form,action){
                                                                    if(action.result.message=="ok"){
                                                                        
                                                                        var al_dep_id   = Ext.getCmp('dep_al_dep_id'+r.data['al_dep_id']);
                                                                        var dep_acc_id  = Ext.getCmp('dep_acc_id'+r.data['al_dep_id']);
                                                                        
                                                                        Ext.MessageBox.show({title:'Success', msg:'Success saving documents', closable:false, icon:Ext.Msg.INFO, buttonText:{ok:'OK'}});
                                                                        Ext.getCmp('dep-required-doc-'+r.data['activelink_id']).getStore().load({params:{dep_al_id:al_dep_id.getValue(),acc_id:dep_acc_id.getValue()}});
                                                                        console.log('Dependent document grid load');
                                                                        ad.close(); 
                                                                    }
                                                                },failure:function(form, action){
                                                                    Ext.MessageBox.show({title:'WARNING',msg:action.result.message,closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                                                }
                                                            });
                                                        }else{
                                                            Ext.MessageBox.show({title:'Failed', msg:'Failed saving document',icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}})
                                                        }
                                                    }
                                                }]
                                            }
                                            });
                                            var ad  = new Ext.Window({title:'Add/Replace Required Document',width:500,modal:true,layout:'fit',bodyPadding:5,items:[f]});
                                            ad.show();
                                        
                                        }else{
                                            Ext.MessageBox.show({title:'WARNING',msg:'Reset document first before you modify object',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                        }
                                    }}
                            },
                            {
                            text    :'Reset check document',
                            ui      :'soft-green',
                            handler :function(btn,grid, record, index, eOpts,rowIndex, colIndex){
                                var g = Ext.getCmp('dep-required-doc-'+r.data['activelink_id']).getStore();
                                // var al_dep_id   = Ext.getCmp('dep_al_dep_id'+r.data['al_dep_id']);var dep_acc_id = Ext.getCmp('dep_acc_id'+r.data['al_dep_id']);
                                var array_id = new Array();
                                var i = 0;   

                                g.each(function (record) {

                                if (record.get('active') === true) {
                                    array_id[i] = record.get('doc_id');
                                    console.log('Dependent check document: '+record.get('doc_id'));
                                    var data = record.get('doc_id');
                                    i++;
                                }
                                });

                                if(i<1){
                                    Ext.MessageBox.show({title:'WARNING',msg:'Please select document to reset',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                }else{
                                    Ext.Msg.confirm('Reset Document','Are you sure you want to reset dependent document/s', function(answer){
                                        if(answer==='yes'){
                                            Ext.Ajax.request({
                                                url     :'../data/get_dependent_document_reset.php',
                                                params  :{doc_id:Ext.encode(array_id), al_dep_id:r.data['al_dep_id'], acc_id:r.data['dcompanyid']},
                                                waitMsg :'Processing your request',
                                                success :function(){
                                                    Ext.MessageBox.show({title:'Success',msg:'Document reset success!',icon:Ext.Msg.INFO,closable:false,buttonText:{ok:'OK'}});
                                                    g.removeAll();
                                                    g.load({params:{dep_al_id:r.data['al_dep_id'],acc_id:r.data['dcompanyid']}});
                                                },
                                                failure :function(){
                                                    Ext.MessageBox.show({title:'Failed',msg:'Document reset failed. Please contact IT for your assistance',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                                }
                                            });
                                        }
                                    });                                
                                }
                            }
                            }],
                            items   :[
                            {
                                xtype        :'gridpanel',
                                plugins      :'gridfilters',
                                ui           :'light',
                                headerBorders:false,
                                rowLines     :true,
                                bodyPadding  :10,
                                id           :'dep-required-doc-'+r.data['activelink_id'],
                                viewConfig   :{preserveScrollOnRefresh:true,preserveScrollOnReload:true,deferEmptyText:true,emptyText:'<h1>No data found</h1>'},
                                store        :Ext.create("OMM.store.Get_dependent_documents"),
                                columns      :[
                                    {xtype: 'checkcolumn',dataIndex: 'active',width:30, locked:true},    // {xtype:'rownumberer'},
                                    {header:'Doc ID',dataIndex:'doc_id'},
                                    {header:'Doc name',dataIndex:'doc_name', flex:1},
                                    {header:'Status',dataIndex:'doc_stat', flex:1},
                                    {
                                        header      : 'File name',
                                        dataIndex   : 'fid',
                                        flex        : 1,
                                        renderer    : function (val, meta, record,view, cell, cellIndex, row, rowIndex, e) {
                                        return '<a href=https://www.benefitsmadebetter.com/member/activelink_admins/doc-files.php?fid='+ val  +'>' + record.get('filename') + '</a>';
                                        }},
                                    {header:'Date modified',dataIndex:'modified_on', flex:1}
                                ],
                                listeners   :{
                                    beforerender    :function(){// var al_dep_id   = Ext.getCmp('dep_al_dep_id'+r.data['al_dep_id']);var dep_acc_id = Ext.getCmp('dep_acc_id'+r.data['al_dep_id']);
                                    Ext.getCmp('dep-required-doc-'+r.data['activelink_id']).getStore().load({params:{dep_al_id:r.data['al_dep_id'],acc_id:r.data['dcompanyid']}});
                                    }// rowclick:function(grid,record,rowIndex,colIndex){alert(record.get('filename'))}
                                }
                            }]
                        }]
                    },
                    {
                        xtype       :'panel',
                        width       :'55%',
                        bodyPadding :5,
                        items       :[
                        {
                            xtype       :'fieldset',
                            columnWidth :0.5,
                            title       :'<b>Enrollment Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%',labelWidth:130, allowBlank:true},
                            layout      :'anchor',
                            items       :[
                                {fieldLabel:'HMO ID',name:'hmo_id',value:r.data['hmo_id'],maxLength:50},
                                {fieldLabel:'Dependent type',xtype:'combo',allowBlank:false, name:'dep_type',value:r.data['dep_type'],reference:'dep_type', store:Ext.create("OMM.store.Get_deptype"),valueField:'dep_type',displayField:'dep_type',emptyText:'Select type', editable:false,listeners:{el:{click:'onDepType_change_dep_form'}}},
                                {fieldLabel:'DEPSETTINGSIDX',name:'dep_settings_idx',value:r.data['idx'],hidden:true},
                                {xtype:'label',style:'color:#2f9fe9;font-weight:bold;font-style:oblique;font-face:arial',text:'Enrollment status will automatically set as Active if user fill the valid HMO ID'},
                                {fieldLabel:'Enrollment Status',flex:1,xtype:'combo',name:'status',reference:'dep_enroll_stat',store:Ext.create("OMM.store.Get_status"),value:r.data['status'],valueField:'status',displayField:'status',emptyText:'Select dependent status',editable:false},
                                {xtype:'checkbox',boxLabel:'Enrollment status option : Check if you want to update status of dependent',labelAlign:'right',labelStyle:'color:#2f9fe9;font-weight:bold;font-face:arial;font-size:12px',labelWidth:480,name:'dep_stat_opt',reference:'dep_stat_opt',checked:false,padding:'-10 0 0 0',inputValue:'1',uncheckedValue:'0',listeners:{change:'onDepChangeEnrollStat'}},
                                {xtype:'datefield',fieldLabel:'Date of Enrollment',name:'date_of_enrollment',value:r.data['date_of_enrollment'],editable:false},
                                {xtype:'datefield',fieldLabel:'Date Created',name:'datecreated',value:r.data['datecreated'],editable:false,format:'Y-m-d H:i:s'},
                                {xtype:'datefield',fieldLabel:'Last Updated On',name:'last_update',value:r.data['last_update'],editable:false,format:'Y-m-d H:i:s'},
                                {fieldLabel:'Remarks',name:'remark',value:r.data['remark'],editable:false}
                            ]
                        },
                        {
                            xtype       :'fieldset',
                            title       :'<b>HMO Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%', labelWidth:160, allowBlank:true},
                            layout      :'anchor',
                            items       :[       
                            {
                                xtype      :'form',
                                reference  :'opt_plan',
                                // id          :'opt_plan',
                                defaultType :'textfield',
                                defaults    :{anchor:'100%', labelWidth:170, width:'100%'},
                                items       :[                                    
                                    {fieldLabel:'Principal Member Status',name:'pmbr_stat',value:r.data['pmbr_stat'],editable:false},
                                    {fieldLabel:'Principal Plan (Level)',name:'pplan_id',value:r.data['pplan_id'],editable:false},
                                    {
                                        xtype       :'fieldset',
                                        title       :'<b>Plan Information</b>',
                                        defaultType :'textfield',
                                        hidden      :true,
                                        defaults    :{anchor:'100%', labelWidth:170, width:'100%'},
                                        layout      :'anchor',
                                        items       :[
                                                {fieldLabel:'HMO Provider',name:'hospital_hmo',readOnly:true},
                                                {fieldLabel:'Dependent Member Status',name:'dmbr_stat',value:r.data['dmbr_stat'],editable:false},
                                                {fieldLabel:'level',name:'dplan_id',reference:'dep_planLevel',value:r.data['dplan_id'],editable:false},
                                                {fieldLabel:'Plan Description',name:'plan_desc',readOnly:true,reference:'dep_plan_desc'},
                                                {fieldLabel:'Benefit limit',name:'benefit_limit',readOnly:true},
                                                {xtype:'label',style:'font-weight:bold;font-face:arial;margin-bottom:10px;',html:'NOTE : <span style="color:#e44959;font-weight:bold;font-face:arial;font-size:11px;font-style:oblique;">Advise to double check the benefit limit of the member. Value was based on Principal plan information</span>'},
                                                {
                                                    xtype       :'panel',
                                                    reference   :'dep_opt_panel',
                                                    defaultType :'textfield',
                                                    defaults    :{anchor:'100%', labelWidth:160, width:'100%'},
                                                    items       :[
                                                        {fieldLabel:'Option',name:'opt',readOnly:true},
                                                        {xtype:'label',name:'opt_plan_desc', reference:'opt_plan_desc', style:'color:#2f9fe9;font-weight:bold;font-face:arial',html:'Option description : '}
                                                    ]
                                                }
                                        ]
                                    }]
                            }]
                        }]
                    }]
                }],
                listeners       :{
                    beforerender:function(){
                        var dep_enroll_stat  = this.lookupReference('dep_enroll_stat'),// var dep_enroll_stat  = Ext.getCmp('dep_enroll_stat'+r.data['al_dep_id']);
                            dep_prin_name    = this.lookupReference('dep_prin_name'),
                            dep_prin_emp_num = this.lookupReference('dep_prin_emp_num'),
                            session_userid   = this.lookupReference('session_userid'),
                            opt_plan         = this.lookupReference('opt_plan'),
                            opt_plan_desc    = this.lookupReference('opt_plan_desc'),
                            dep_opt_panel    = this.lookupReference('dep_opt_panel'),
                            dep_plandetails  = Ext.create('OMM.model.Dependent_plandetails_Model');

                        dep_enroll_stat.setDisabled(true);
                        myMask.hide();
                        Ext.Ajax.request({
                        url     :'../data/post_username.php',
                        success :function(response){
                            var response=Ext.decode(response.responseText);
                            session_userid.setValue(response.username);
                            console.log(session_userid.getValue());                        
                            Ext.Ajax.request({
                                url:'../data/get_dependent_plandetails.php',
                                params:{ops:session_userid.getValue(),dep_alid:r.data['al_dep_id'],acc_id:r.data['dcompanyid']},
                                success:function(response){
                                    var response=Ext.decode(response.responseText);                                    
                                    dep_plandetails.set(response.data[0]);
                                    console.log(dep_plandetails);
                                    // console.log(session_userid.getValue())
                                    opt_plan.getForm().loadRecord(dep_plandetails);
                                    opt_plan_desc.setHtml('Option description : '+response.data[0].opt_plan_desc);
                                    console.log(opt_plan.getForm().getValues());
                                },failure:function(response){
                                    var response=Ext.decode(response.responseText);                                                                       
                                    opt_plan_desc.setHtml('Something went wrong. cant view plan description. Please contact your IT for assistance');

                                }
                            });
                        
                            if(r.data['dcompanyid']!='QBE'){
                                dep_opt_panel.hide();
                            }else{}
                        }
                        });
                        Ext.Ajax.request({
                            url:'../data/get_dependent_principal.php',
                            params:{al_prin_id:r.data['al_prin_id'],acc_id:r.data['dcompanyid']},
                            success:function(response){var response=Ext.decode(response.responseText);dep_prin_name.setValue(response.prin_name);dep_prin_emp_num.setValue(response.prin_emp_num);}
                        }); 
                    }
                }
            });
            view.setActiveTab(cur_tab_id)
        }
    },


    //Button - close principal note panel
    onCancelNote:function(btn){
        var me              = this,
            form            = btn.up('form').getForm(),
            prin_note_panel = this.lookupReference('prin_note_panel');

        form.reset();
        prin_note_panel.setCollapsed(true);
    },

    //Button - submit principal note
    onSubmitNote:function(btn){
        var me                    = this,
            form                  = btn.up('form').getForm(),
            values                = form.getValues(),
            OMM_logs_ops          = localStorage.getItem("OMM_logs_ops"),
            obj                   = JSON.parse(OMM_logs_ops),
            OMM_thread_notes      = localStorage.getItem("OMM_thread_notes"),
            obj2                  = JSON.parse(OMM_thread_notes),
            activity_note         = this.activity_note=Ext.create('OMM.store.Get_activity_note'),
            prin_note_text        = this.lookupReference('prin_note_text');

        var myMask = new Ext.LoadMask({
                msg    : 'posting...',
                target : btn.up('form')
        });
        myMask.show();

        console.log(values['activelink_id'])
        if(prin_note_text.getValue()!='' || prin_note_text.getValue()!=null){
            Ext.Ajax.request({
                url:'../data/activity_post_note.php',
                params:{activelink_id:values['activelink_id'],acc_id:values['company_id'], ops:obj, note:prin_note_text.getValue()},
                success:function(){
                    activity_note.removeAll();
                    activity_note.getProxy().extraParams = {activelink_id:values['activelink_id'],acc_id:values['company_id'], ops:obj};
                    activity_note.load({
                    callback: function(records, operation, success) {
                        var total_count   = records.length,
                        rec           = records;

                        prin_note_text.setValue('');
                        myMask.hide();
                        me.activity_note_AjaxRequest_getId(id, total_count,rec);
                    }
                    });
                },failure:function(){
                    Ext.MessageBox.show({title:'WARNING', msg:'Undifined', icon:Ext.msg.WARNING, buttonText:{ok:'OK'}})
                }
            });
        }else{
            Ext.MessageBox.show({title:'WARNING', msg:'User is advise to atleast input any text', icon:Ext.Msg.WARNING});
            myMask.hide();
        }
    },

    onShow_opsendo_count:function(){

        var isMinimized   = false;
        var win           = new Ext.Window({
            closeAction     :'destroy',
            width           :'75%',
            height          :450,
            modal           :true,
            // maximized       :true,
            maximizable     :true,
            autoShow        :true,
            scope           :this,
            controller      :'principal',
            titleAlign      :'center',
            title           :'Operations Endorsement'
        });
    }
    //end dependent information
 });