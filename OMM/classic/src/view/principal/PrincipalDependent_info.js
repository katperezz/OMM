Ext.define('OMM.view.principal.PrincipalDependent_info',{
	extend     : 'Ext.form.Panel',
	title      : 'Principal Dependent Information',
	xtype      : 'principaldependent_info',
	closable   :true,
	layout     :'fit',
	bodyPadding:5,
    defaults   : {anchor: '100%'}   	
});