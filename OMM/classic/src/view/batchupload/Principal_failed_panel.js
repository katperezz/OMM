Ext.define('OMM.view.batchupload.Principal_failed_panel',{
	extend 		:'Ext.Panel',
	requires 	:[
	    'Ext.grid.selection.Replicator',
	    'Ext.grid.plugin.Clipboard',
	    'Ext.grid.selection.SpreadsheetModel',
	    'Ext.grid.filters.Filters'
	],
	xtype 		:'principal_failed_panel',
	autoDestroy :true,
	closeAction :'destroy',
	reference 	:'principal_failed_panel',
	defaults 	:{width:'100%'},
	layout 		:'fit',
	cls 		:'Principal_update-page-container',
	tbar 		:{
		layout 	:{type:'vbox',align:'stretch',pack:'start'},
		items 	:[
			{xtype:'panel',bodyStyle:{"background-color":"#D1F2FA","border-radius":"10px;"},layout:'hbox',defaultType:'displayfield',defaults:{fieldStyle:'color:#2990A8;font-weight:bold; font-face:arial; font-size:20px;',anchor:'50%',labelWidth:100,style:'padding:10px 50px 10px 50px; font-size:20px; font-weight:bold;'},items:[{xtype:'tbspacer',width:'15%'},
			{reference:'prin_total',fieldLabel:'<span style="font-face:arial;font-weight:bold;font-size:18px;">Total rows</span>'},
			{reference:'prin_success',fieldLabel:'<span style="font-face:arial;font-weight:bold;font-size:18px;">Success</span>'},
			{reference:'prin_failed',fieldLabel:'<span style="font-face:arial;font-weight:bold;font-size:18px;">Failed</span>'},
			{reference:'prin_warning',fieldLabel:'<span style="font-face:arial;font-weight:bold;font-size:18px;">Warning</span>'}
			]}
		]
	},
	items:[
	{
		xtype 		:'gridpanel',
		autoScroll 	:true,
		ui 			:'light',
		title 		:'FAILED RECORDS ONLY',
		reference 	:'principal_failed_grid',
		itemId 		:'principal_failed_grid',
		store 		:Ext.create('OMM.store.batchupload.TempTableStore_error_loaddata'),
		plugins 	:['clipboard','selectionreplicator'],
		selModel 	:{type:'spreadsheet',columnSelect:true,pruneRemoved:false,extensible:'y'},
		viewConfig  :{
			preserveScrollOnRefresh	:true,
			preserveScrollOnReload 	:true,
			deferEmptyText 			:true,
			emptyText 				:'<h1>No data found</h1>',
			getRowClass 			:function(record,index){
				var c=record.get('status');
				if(c=='WARNING'){
					return'warning'
				}else if(c!='SUCCESS'||c==''){
					return'empty'
				}else{
					return'valid'}
				}
			},
		columns 	:[		
	    // {menuDisabled:true,sortable:false,xtype:'actioncolumn',align:'center', items:[{iconCls:'x-fa fa-eye',reference:'ac_viewdetails', tooltip:'View Error Details'}]},
		{header:'status',dataIndex:'status',width:120,tdCls:'x-change-cell'},
		{header:'status_msg',dataIndex:'status_msg',width:200},
		{header:'status_match',dataIndex:'status_match',width:90}
		],
		listeners 	:{
			beforerender:'onPrin_failed_grid_beforerender',
			afterrender:'onPrin_failed_grid_afterrender',
			rowdblclick:function(grid, record, rowIndex, colIndex,model){
				var r = record.get('status_msg');
				var fname = record.get('fname')
					lname = record.get('lname')
					bdate = record.get('birthdate');

				console.log(record.data);

				if(r=='Found same existing record'){
					Ext.create('Ext.window.Window',{
						title 		:record.get('status_msg'),
						reference 	:'win_failed_panel',
						titleAlign 	:'center',
						autoShow 	:true,
						cls 		:'batchupload-page-container',
						maximized 	:true,
						modal 		:true,
						closeAction :'destroy',
						layout 		:{type:'vbox',align:'center',pack:'center'},
						defaults 	:{width:'100%',anchor:'95%',labelWidth:200,bodyPadding:10},
						controller 	:'batchupload',
						overflowY 	:'scroll',
						items 		:[
						{
							xtype 		:'gridpanel',
							reference 	:'gp_samerecorderror',
							height 		:210,
							store 		:Ext.create('OMM.store.batchupload.SameRecordError_store'),
							columns 	:[
								{xtype:'rownumberer',width:45},{header:'ActiveLink ID',dataIndex:'activelink_id',filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},{header:'Employee no',dataIndex:'empid',filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},{header:'Last name',dataIndex:'lname',flex:1,filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},{header:'First name',dataIndex:'fname',flex:1,filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},{header:'Middle name',dataIndex:'mname',flex:1,filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},{header:'Suffix',dataIndex:'ename',filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},{header:'HMO/Job level',dataIndex:'plan_id',flex:1,filter:'list'},{header:'Branch',dataIndex:'branch_id',filter:'list'},{header:'Status',dataIndex:'mbr_status',filter:'list'}
							],
							listeners 	:{selectionchange:'onSamerecordError_SelectionChange'}
						},
						{
							xtype 		:'panel',
							title 		:record.get('status_msg')+' Details',
							cls 		:'batchupload-page-container',
							reference 	:'batch_main_panel',
							flex 		:1,
							layout 		:'fit',
							items 		:[
							{
								xtype 		:'form',
								reference 	:'frm_samerecorderror',
								layout 		:'hbox',
								overflowY 	:'scroll',
								items 		:[
									{
										xtype:'panel',width:'45%',bodyPadding:5,
										items:[
										{xtype:'fieldset',title:'<b>Personal Information</b>',collapsible:true,defaultType:'textfield',defaults:{anchor:'95%',labelWidth:130,allowBlank:true},layout:'anchor',items:[{fieldLabel:'First Name',name:'fname',allowBlank:false},{fieldLabel:'Middle Name',name:'mname'},{fieldLabel:'Last Name',name:'lname',allowBlank:false},{fieldLabel:'Suffix Name',name:'ename'},{fieldLabel:'Gender',xtype:'combo',store:Ext.create("OMM.store.Get_gender"),name:'gender',editable:false,emptyText:'Select Gender',valueField:'gender',displayField:'gender'},{xtype:'datefield',fieldLabel:'Birthdate',allowBlank:false, name:'birthdate'},{fieldLabel:'Civil Status',xtype:'combo',store:Ext.create("OMM.store.Get_civilstatus"),name:'civil_status',editable:false,emptyText:'Select Civil Status',valueField:'civil_status',displayField:'civil_status'}]},
										{xtype:'fieldset',title:'<b>Company Information</b>',collapsible:true,defaultType:'textfield',defaults:{anchor:'95%',labelWidth:130,allowBlank:true},layout:'anchor',items:[{fieldLabel:'ActiveLink ID',name:'activelink_id',allowBlank:false,readOnly:true,hidden:true},{fieldLabel:'Employee ID',name:'empid',reference:'prin_empid'},{fieldLabel:'Company ID',name:'company_id',reference:'prin_acc_id',readOnly:true},{fieldLabel:'Branch',xtype:'combo',name:'branch',emptyText:'Select Plan level',store:Ext.create("OMM.store.Get_account_branchid"),reference:'prin_branch',emptyText:'Select Branch',editable:false,valueField:'branch',displayField:'branch',listeners:{el:{click:'onPrinBranchID'},select:'onPrinBranchChange'}},{fieldLabel:'Branch ID',name:'branch_id',reference:'prin_branch_id',hidden:true},{fieldLabel:'Designation',name:'designation'},{fieldLabel:'Member Date',name:'member_date',readOnly:true},{fieldLabel:'Registration Date',name:'reg_date',readOnly:true},{fieldLabel:'Inactive Date',name:'inactive_date',readOnly:true},{fieldLabel:'Existing Flag',name:'existing_flag',readOnly:true},{fieldLabel:'Inclusion Date',name:'inclusion_date',readOnly:true},{fieldLabel:'Hire Date',name:'emp_hire_date',xtype:'datefield',format:'Y-m-d H:i:s',editable:true}]}
										]
									},{
										xtype:'panel',width:'55%',bodyPadding:5,
										items:[
										{xtype:'fieldset',columnWidth:0.5,title:'<b>HMO Information</b>',collapsible:true,defaultType:'textfield',layout:'anchor',defaults:{anchor:'95%',labelWidth:150,allowBlank:true},items:[{fieldLabel:'Member Status',xtype:'combo',store:mbr_status,name:'mbr_status',allowBlank:false,editable:false,queryMode:'local',valueField:'mbr_status',displayField:'mbr_status'},{fieldLabel:'HMO ID Number',name:'id_number',minLength:5,maxLength:30},{fieldLabel:'HMO Provider',name:'hmo_provider',readOnly:true},{fieldLabel:'Dental Provider',name:'dental_provider',readOnly:true},{fieldLabel:'Plan level',xtype:'combo',name:'plan_id',reference:'prin_planLevel',emptyText:'Select Plan level',store:Ext.create("OMM.store.Get_account_plans"),displayField:'planid',valueField:'planid',editable:false,listeners:{el:{click:'onPrinCompanyID'},select:'onPrinPlanChange'}},{fieldLabel:'Plan Description',name:'plan_desc',readOnly:true,reference:'prin_plan_desc'},{fieldLabel:'Original Effective Date',xtype:'datefield',minValue:'1910-01-01',format:'Y-m-d',name:'orig_eff_date'},{fieldLabel:'Current Effective Date',xtype:'datefield',minValue:'1910-01-01',format:'Y-m-d',name:'cur_eff_date'},{fieldLabel:'Maturity Date',name:'maturity_date',xtype:'datefield',minValue:'1910-01-01',format:'Y-m-d'}]},
										{xtype:'fieldset',title:'<b>Additional Information</b>',collapsible:true,animate:true,defaultType:'textfield',defaults:{anchor:'95%',labelWidth:130,allowBlank:true},layout:'anchor',items:[{fieldLabel:'PhilHealth',name:'philhealth'},{fieldLabel:'SSS no',name:'sss_no'},{fieldLabel:'Pag-ibig no',name:'pag_ibig_no'},{fieldLabel:'Unified ID no',name:'unified_id_no'},{fieldLabel:'TIN',name:'tin'}]}
										]
									}]
							}]
						}],
						buttons 	:[
							{ui:'soft-red',text:'Save Changes',handler:'onSamerecordError_save'}
						],
						listeners 	:{
							beforerender 	:function(){
								var gp_samerecorderror=this.lookupReference('gp_samerecorderror');
								gp_samerecorderror.getStore().load({params  :{fname:fname, lname:lname, bdate:bdate}})
							}
						}
					}).show();					
				}else{
					console.log(r);
				}
			}
		}}]
});
