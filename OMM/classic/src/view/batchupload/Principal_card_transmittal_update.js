var update_selection  = Ext.create('Ext.data.Store',{
	fields 	:["field","value"],
	data 	:[
	{"field":"Physical ID Received Date","value":"phys_id_recv_date"},
	{"field":"Physical ID Transmittal Date","value":"phys_id_transmittal_date"}
	]
});

var update_unique_key = Ext.create('Ext.data.Store',{
	fields:["field","value"],
	data 	:[
	{"field":"Employee ID *","value":"empid"},
	{"field":"First, Last Name and Birthdate", "value":"fn_ln_db"}
	]
});

var update_action = Ext.create('Ext.data.Store',{
	
	fields :["field","action"],
	data   :[
	{"field":"new", "action":"new"},
	{"field":"data_correction", "action":"data_correction"},
	{"field":"ID Replacement <span style='color : rgb(255, 0, 0); padding-left:10px;padding-top:10px; font-size:13px; font-weight:normal'>Not available. Processing function </span>","action":'replacement'}
	]
});


Ext.define('OMM.view.batchupload.Principal_card_transmittal_update',{
	extend :'Ext.window.Window',
	xtype 	:'principal_card_transmittal_update',
	reference :'principal_card_transmittal_update',

	requires 	:[
		'OMM.view.batchupload.BatchUploadController'
	],

	controller 	:'batchupload',
	cls 		:'Principal_update-page-container',
	initCenter 	:true,
	autoShow 	:true,
	maximized  	:true,
	title 		:'SLA Update - Physical ID Transmittal',
	titleAlign  :'center',
	closeAction :'destroy',
	layout 		:'fit',
	tools       :[{type:'restore',hidden:true,handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.expand('',true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type:'minimize',handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.collapse();winWidth=window.getWidth();window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();isMinimized=true}}],

	items 		:[
	{
		xtype 	 	:'tabpanel',
		defaultType :'form',
		layout 		:'card',
		reference 	:'principal_card_transmittal_update_tab',
		defaults 	:{border:false, bodyPadding:10},
		items 		:[
		{
			title 	:'1 : Unique Key', reference :'PCTU_step1',itemId:'PCTU_step1', bodyPadding:10,
			defaults :{anchor:'95%', labelWidth:200,editable:false,queryModel:'local',valueField:'value',displayField:'field'},
			items 	:[
			{xtype:'displayfield',fieldLabel:'acc_id',name:'acc_id',reference:'acc_id',hidden:true},{xtype:'displayfield',fieldLabel:'session_userid',name:'session_userid',reference:'session_userid',hidden:true},{xtype:'displayfield',fieldLabel:'filesignature',name:'filesignature',reference:'filesignature',hidden:true},
			{
				xtype:'combo',
				fieldLabel:'<span style="color: rgb(255,0,0); padding-left:10px;padding-top:10px;font-size:13px;font-weight:normal">Select Unique Key * </span>',
				store :update_unique_key,
				margin:10,
				name 	:'unique_key_opt',
				reference :'unique_key_opt',
				emptyText:'Select Unique field/s',
				listeners :{select:'onUpdateCardTransmitUniqueKey'}
			},
			{
				xtype 	:'form',
				defaults :{bodyPadding:'5px 10px',anchor:'95%',labelWidth:200,editable:false,allowBlank:false,emptyText:'Select column', store:Ext.create('OMM.store.batchupload.TempTableStore'),valueField:'field_name',displayField:'field_name'},
				defaultType:'combo',
				reference :'principal_card_transmittal_update_unique_form',
				itemId  	:'principal_card_transmittal_update_unique_form',
				bodyPadding:5,
				tbar		:{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',text:'Unique Key'}]},
				items 		:[]
			},
			{
				xtype 	:'panel',
				html 	:'<hr align=2 color=#333333>'
			},
			{
				defaults:{bodyPadding:'5px 10px',anchor:'95%',labelWidth:200,editable:false,allowBlank:false, emptyText:'Select column',store:Ext.create('OMM.store.batchupload.TempTableStore'),valueField:'field_name',displayField:'field_name'},
				defaultType :'combo',
				xtype 	:'form',
				reference :'principal_card_transmittal_update_field_form',
				itemId 	  :'principal_card_transmittal_update_field_form',
				cls 	  :'Principal_update-page-container',
				tbar 	  :{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',text:'Field Selection'}]},
				items 	  :[
				{
					fieldLabel 	:'Physical ID Transmittal Date',
					allowBlank	:false,
					name 		:'card_transmittal_date'
				}
				]
			}
			],
			listeners	:{
				beforerender :'onBatch_response_render'
			},
			bbar      :{style:'background-color:#4dacec;',defaults:{margin:5,ui:'blue'},items:[{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;">Modify the appropriate column/field for the required column/fields</span>'},'->',{text:'Reset Fields',handler:function(button){button.up('form').getForm().reset()}},{text:'Next',ui:'blue',handler:'onCardTransmittalFieldSelectNext'}]}
		},
		{
			title 	:'2 : Result', disabled:true, reference:'PCTU_step2',itemId:'PCTU_step2',defaults:{width:'100%'},layout:'fit',bodyPadding:5,
			tbar      :{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',style:'margin-bottom:5px; margin-top:10px',text:'UPDATE RESULT'}]},
			bbar      :{style:'background-color:#4dacec;',defaults:{margin:5,ui:'soft-red'},items:[{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;"> Suggest to download error for your reference because it only shows once</span>'},'->',{text:'Download Error to XLS', disabled:true,handler:function(){window.open('../data/download_error.php','DOWNLOAD ERROR')}}]}

		}
		]
	}
	]


});