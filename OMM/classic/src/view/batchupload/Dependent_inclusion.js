Ext.define('OMM.view.batchupload.Dependent_inclusion',{
	extend 	 	:'Ext.window.Window',
	xtype 	 	:'dependent_inclusion',
	requires 	:[
	 	'OMM.view.batchupload.BatchUploadController'
	],
	controller 	:'batchupload',
	initCenter 	:true,
	autoShow 	:true,	
	modal 	 	:true,
	maximized 	:true,
	height 		:500,
	width 		:'90%',
	title 		:'Dependent Inclusion',
	titleAlign 	:'center',
	closeAction :'destroy',
	 
	layout 	 	:'fit',
	cls 	 	:'Principal_update-page-container',
	tools 	 	:[{type:'restore',hidden:true, handler:function(evt,toolEl,owner,tool){var window = owner.up('window');window.expand('', true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type 	:'minimize',handler :function(evt, toolEl, owner,tool){var window = owner.up('window');window.collapse();winWidth=window.getWidth();window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();isMinimized=true}}],	
	items 	 	:[
	{xtype:'displayfield',fieldLabel:'acc_id',text:'acc_id',reference:'acc_id', hidden:true},{xtype:'displayfield',fieldLabel:'user_id',text:'session_userid',reference:'session_userid',hidden:true},{xtype:'displayfield',fieldLabel:'filesignature',text:'filesignature',reference:'filesignature',hidden:true},
	{
		xtype 		:'panel',
		layout 		:'card',
		reference  	:'dependent_inclusion_tab',
		defaults  	:{border:false, bodyPadding:5},
		items  		:[
		{
			xtype  		:'form',
			reference 	:'dependent_inclusion_field_form',
			cls 		:'Principal_update-page-container',
			defaults  	:{anchor:'95%', labelWidht:200, style:'margin-left:10px', editable:false, emptyText:'Select column'},
			defaultType :'combo',
			overflowY 	:'scroll',
			tbar 	 	:{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',text:'FIELD CONFIGURATION',style:'margin-top:20px'}]},
			items 		:[
			{
				defaults 	:{anchor:'95%', labelWidth:200, editable:false, style:'margin-left:10px', emptyText:'Select column', store:Ext.create('OMM.store.batchupload.TempTableStore'), displayField:'field_name', valueField:'field_name'},
				defaultType :'combo',
				xtype 		:'fieldset',
				collapsible :true,
				reference 	:'dependent_inclusion_branch_form',
				itemId 		:'dependent_inclusion_branch_form',
				cls 		:'Principal_update-page-container',
				title 		:'Branch ID',
				items 		:[
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Branch ID*</span>', name:'branch_id', reference:'branch', allowBlank:false},
				{xtype:'button', text:'Convert Column Values to Branch Identifier', ui:'soft-green', listeners:{el:{click:'onPrinCvrtBranch'}}}
				]			  
			},
			{
				defaults 	:{anchor:'95%', labelWidth:200, editable:false, style:'margin-left:10px', emptyText:'Select column',store:Ext.create('OMM.store.batchupload.TempTableStore'), displayField:'field_name', valueField:'field_name'},
				defaultType :'combo',
				xtype 		:'fieldset',
				collapsible :true,
				reference 	:'dependent_inclusion_plan_form',
				itemId 		:'dependent_inclusion_plan_form',
				cls 		:'Principal_update-page-container',
				title 		:'Plan ID',
				items 		:[
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Plan ID*</span>', name:'plan_id', reference:'plan_id', allowBlank:false},
				{xtype:'button', text:'Convert Column Values to Plan Identifier', ui:'soft-green', listeners:{el:{click:'onPrinCvrtPlan'}}}
				]
			},	
			{
				defaults 	:{anchor:'95%',labelWidth:200, editable:false, style:'margin-left:10px',emptyText:'Select column', store:Ext.create('OMM.store.batchupload.TempTableStore'), displayField:'field_name', valuefield:'field_name'},
				defaultType :'combo',
				xtype 		:'fieldset',
				collapsible :true,
				reference 	:'dependent_inclusion_field_form',
				itemId 	 	:'dependent_inclusion_field_form',
				cls 		:'Principal_update-page-container',
				title 	 	:'Field Selection',
				items 	:[
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Employee ID*</span>', name:'activelink_id', allowBlank:false},
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Principal Employee ID*</span>', name:'prin_empid', allowBlank:false},
				{fieldLabel:'Dependent HMO ID number', name:'dep_id_number'},
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Dependent First name*</span>', name:'fname',allowBlank:false},
				{fieldLabel:'Dependent Middle name', name:'mname'},
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Dependent Last name', name:'lname', allowBlank:false},
				{fieldLabel:'Dpendent Extension name', name:'ename'},
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Dependent Date of Birth', name:'dob', allowBlank:false},
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Dependent Gender', name:'gender', allowBlank:false},
				{xtype:'button', margin:'0 0 20 10', text:'Convert Column Values to Gender Identifier', ui:'soft-green', listeners:{el:{click:'onCvrtGender'}}},
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Dependent Civil Status', name:'civil_status', allowBlank:false},
				{xtype:'button', margin:'0 0 20 10', text:'Convert Column Values to Civil Status Identifier', ui:'soft-green', listeners:{el:{click:'onCvrtCivil_stat'}}},
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Dependent Relationship', name:'relationship', allowBlank:false},
				{fieldLabel:'<span style="color: rgb(255,0,0); font-weight:bold;">Dependent Type', name:'dep_type', allowBlank:false},
				{fieldLabel:'Philhealth', name:'philhealth'}
				]
			}
			],
			bbar:{
				style:'background-color:#4dacec;', defaults:{margin:5, ui:'blue'}, items:[
				{
					xtype:'label',
					style:'font-weight:bold; font-face:arial',
					html:'NOTE : <span style="color:#ffffff; font-weight:normal; font-face:arial; font-style:oblique;">Fill the required fields.</span>'
				},
				'->',
				{text:'Upload',handler:'onDepInc_upload'}
				]
			}
		},
		{
			defaults:{width:'100%'}, layout:'fit', bodyPadding:5,reference:'dependent_inclusion_result',
			tbar:{layout:{type:'vbox', align:'stretch', pack:'start'}, items:[{xtype:'label', cls:'Principal_update-page-title-text',style:'margin-bottom:5px; margin-top:10px', text:'INCLUSION RESULT'}]},
			bbar:{style:'background-color:#4dacec;', defaults:{margin:5, ui:'soft-red'}, items:[
			{xtype:'label', style:'font-weight:bold;font-face:arial', html:'NOTE : <span style ="color:#ffffff; font-weight:normal; font-style:oblique; font-size:13px"> Suggest to download error fore your reference because it only shows once</span'},
			'->',
			{
				text:'Download Error to XLS',
				handler:function(){
					window.open('../data/download_error.php', 'DOWNLOAD ERROR');
				},
				disabled:true
			}
			]
			}
		}
		]
	}
	],
	listeners :{
		beforerender:'onBatch_response_render'
	}

});