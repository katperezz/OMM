Ext.define('OMM.view.batchupload.BatchUpload',{
	extend 		:'Ext.window.Window',
 	alias 		:'widget.batchupload',
 	xtype 		:'batchupload',
    routeId     :'batchupload',

 	requires 	:[
        'OMM.view.batchupload.BatchUploadController',
        'OMM.view.batchupload.UploadFile',
        'OMM.view.batchupload.TempTable_list'
 	],
    controller  :'batchupload',
    autoShow    :true,
    cls         :'batchupload-page-container',
    closable    :false,
    title       :'Operations Management Module - Batch Upload',
    titleAlign  :'center',
    maximized   :true,
    modal       :true,
    closeAction :'destroy',
    layout      : {type: 'vbox',align: 'center',pack: 'center'},
    defaults    :{width:'100%'},
    dockedItems : [
    {
        dock:'top',xtype:'toolbar',reference:'batch_toolbar',
        items:[
        {
            iconCls:null,ui:'soft-red',text:'ReUpload',xtype:'button', reference:'menu_upload',
            handler:'onBatchUploadfile'
        },
        '-',
        // {xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#2f9fe9;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;">Validating records let the user inform whose existing and new member.</span>'},

        {
            iconCls:null,ui:'soft-blue',text:'PRINCIPAL',xtype:'button', reference:'menu_prin',
            menu    :{
                xtype       :'menu',
                layout      :'fit',
                bodyStyle   :{"background-color":"#4dacec"},
                items       :[
                {
                    xtype:'container',layout:'vbox',defaultType:'button',defaults:{width:180}, 
                    items   :[
                    {
                        style  :{"background-color":"#167abc","color":"#167abc","border":"#167abc"},
                        text   :'Inclusion',
                        handler:'onPrinInclusion_menu'
                    },
                    {
                        style  :{"background-color":"#167abc","color":"#167abc","border":"#167abc"},
                        text   :'Update',
                        handler:'onPrinUpdate_menu'    
                    },
                    {
                        style  :{"background-color":"#167abc","color":"#167abc","border":"#167abc"},
                        text   :'Deletion',
                        handler:'onPrinDeletion_menu'
                    },
                    // {style   :{"background-color":"#167abc","color":"#167abc","border":"#167abc"},text    :'Physical ID Update',handler :'onPrinUpdateCard_menu'},
                    {
                        style   :{"background-color":"#167abc","color":"#167abc","border":"#167abc"},
                        text    :'SLA Updates',
                        menu    :{
                            // layout  :'fit',
                            xtype     :'menu',
                            bodyStyle :{"background-color":"#4dacec"},
                            items     :[
                            {
                                xtype :'container',layout:'vbox',defaultType:'button',defaults:{width:180,style   :{"background-color":"#167abc","color":"#167abc","border":"#167abc"}},
                                items    :[
                                {text:'Physical ID Released',handler:'onPrinUpdateCard_menu'},
                                {text:'Physical ID Transmittal',handler:'onPrinUpdateCardTransmittal_menu'},
                                {text:'Member Endorsement',handler:'onPrinUpdateEndorsement_menu'}
                                    // {text    :'Endorsement Dates',handler:'onPrinUpdateEndorsedate_menu'},
                                    // {text    :'Physical ID',handler:'onPrinUpdateCard_menu'}
                                    // {text    :'HMO ID/ Policy Number', handler:'onPrinUpdateHMOID_menu'}
                                ]
                            }]                            
                        }
                        // menu    :[
                        // {
                        //     text    :'Physical ID',
                        //     style   :{"background-color":"#167abc","color":"#167abc","border":"#167abc"}
                        // },
                        // {
                        //     text    :'Endorsement Dates',
                        //     style   :{"background-color":"#167abc","color":"#167abc","border":"#167abc"}
                        // },
                        // {
                        //     text    :'HMO ID/ Policy Number',
                        //     style   :{"background-color":"#167abc","color":"#167abc","border":"#167abc"}
                        // }
                        // ]
                    }
                    ]
                }]
            }
        },
        '-',
        {
            iconCls:null,ui:'soft-blue',text:'DEPENDENT',xtype:'button', reference:'menu_dep',
            menu        :{
                xtype    :'menu',
                layout   :'fit',
                bodyStyle:{"background-color":"#2eadf5"},
                items    :[
                {
                    xtype:'container',layout:'vbox',defaultType:'button',defaults:{width:120}, 
                    items   :[
                    // {style   :{"background-color":"#167abc","color":"#167abc","border":"#167abc"},text    :'Inclusion',handler :'onDepInclusion_menu'},
                    {
                        style   :{"background-color":"#167abc","color":"#167abc","border":"#167abc"},
                        text    :'Update',
                        handler :'onDepUpdate_menu'
                    },
                    {
                        style   :{"background-color":"#167abc","color":"#167abc","border":"#167abc"},
                        text    :'Deletion',
                        handler :'onDepDeletion_menu'
                    }
                    ]
                }]
            }
        },
        '->',
        {text:'Close',ui:'soft-red',reference:'back',handler:function(button,form){button.up('.window').close();document.location.href=""}}
        ]}
    ],
    items 		:[
    {
        xtype    :'panel',
        cls      :'batchupload-page-container',
        reference:'batch_main_panel',
        flex     :1,
        layout   :'fit'// overflowY:'scroll'
    }
    ],
    listeners   :{beforerender:'onBatchUploadBeforerender'}
});