var field_dataType=Ext.create('Ext.data.Store',{fields:['field_dataType', 'value'],data:[{"field_dataType":"Text field", "value":"VARCHAR(250)"},{"field_dataType":"Numeric field", "value":"numeric"},{"field_dataType":"Date field","value":"date"}]});
  // ,{"field_dataType":"Delete field","value":"delete_field"}
Ext.define('OMM.view.batchupload.BatchUploadController', {
    extend  : 'Ext.app.ViewController',
    alias   : 'controller.batchupload',

    requires: [
        'Ext.util.TaskRunner',        
        'Ext.form.action.StandardSubmit'
    ],

    //FUNCTIONS

    //BEFORERENDER : ALL BATCH PROCESS FUNCTIONS (INCLUSION, UPDATE, DELETION)
    onBatch_response_render:function(){
      var acc_id        = this.lookupReference('acc_id');
      var filesignature = this.lookupReference('filesignature');
      var session_userid= this.lookupReference('session_userid');

      Ext.Ajax.request({
        url     :'../data/batch_response_render.php',
        success :function(response){
          var response = Ext.decode(response.responseText);

          console.log(response.user_id);
        if(response.user_id==''||response.user_id==null){
            Ext.MessageBox.show({
              title:'SESSION EXPIRED',
              msg:'Your session has been expired!',
              closable:false,
              icon:Ext.Msg.WARNING,
              buttonText:{ok:'Reload'},
              fn:function(btn){
                if(btn=='ok'){
                  window.location.href = 'https://www.benefitsmadebetter.com/OMM';
                }}
            });
        }else if(response.acc_id==''||response.filesignature==''||response.acc_id==null||response.filesignature==null){
            Ext.MessageBox.show({
              title:'Batch session expired',
              msg:'Your batchupload session has been expired. Please reload and upload file again.',
              closable:false,
              icon:Ext.Msg.WARNING,
              buttonText:{ok:'Reload'},
              fn:function(btn){
                if(btn=='ok'){
                  window.location.reload();
                  // Ext.create('OMM.view.batchupload.BatchUpload').show();
                }
              }
            });
          }else{
            acc_id.setValue(response.acc_id);
            filesignature.setValue(response.filesignature);
            session_userid.setValue(response.user_id);
            console.log("acc_id :"+acc_id.getValue()+"\n session_userid :"+session_userid.getValue()+"\n filesignature :"+filesignature.getValue())
          }
        },failure:function(){
          console.log('Batch principal update beforerender failed')
        }
      });
    },

    //CONVERT : CIVIL STATUS WINDOW
    onCvrtCivil_stat:function(){
      var isMinimized   = false;
      var win           = new Ext.Window({
        closeAction   :'hide',
        width         :'70%',
        modal         :true,
        height        :350,
        layout        :{type:'vbox',align:'stretch'},
        title         :'Matched Civil Status',
        controller    :'batchupload',
        scope         :this,
        defaultType   :{anchor:'100%',labelWidth:200,editable:false,emptyText:'Select column'},
        initCenter    :true,
        autoShow      :true,
        titleAlign    :'center',
        closable      :false,
        // tools         :[{type:'restore',hidden:true,handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.expand('',true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type:'minimize',handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.collapse();winWidth=window.getWidth().window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();isMinimized=true}}],
        items         :[
        {xtype:'displayfield',fieldLabel:'acc_id',reference:'acc_id',hidden:true},{xtype:'displayfield',fieldLabel:'filesignature',reference:'filesignature',hidden:true},{xtype:'displayfield',fieldLabel:'session_userid',reference:'session_userid',hidden:true},
        {
          xtype     :'form',
          defaults  :{anchor:'100%',labelWidth:250,editable:false,emptyText:'Select Column'},
          reference :'civilstat_form',
          items     :[
          {xtype:'combo',fieldLabel:'Civil Status/ Name source column',name:'civilstat_source',reference:'civilstat_source',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name',margin:5,listeners:{change:'onCivilStat_source_change'}},
          {
            xtype     :'form',
            width     :'100%',
            items     :[
            {
              xtype     :'gridpanel',
              title     :'Source Matching',
              reference :'civilstat_source_grid',
              autoScroll:true,
              height    :300,
              selModel  :'cellmodel',
              plugins   :{ptype:'cellediting',clicksToEdit:1},
              store     :Ext.create("OMM.store.batchupload.CivilstatSource_store"),
              viewConfig:{getRowClass:function(record,index){var c=record.get('match_civilstat');if(c==null||c==''){return'empty'}else{return'valid'}}},
              columns   :[
              {header:'Column Value',dataIndex:'tmp_civilstat',flex:1},
              {header:'Match to Civil Status',dataIndex:'match_civilstat',reference:'match_civilstat',flex:1,tdCls:'x-change-cell',
              editor:new Ext.form.field.ComboBox({triggerAction:'all',editable:false,reference:'civilstat_source_grid_combo',store:Ext.create("OMM.store.Get_civilstatus"),valueField:'civil_status',displayField:'civil_status'})}
              ]
            }]
          }]
        }],
        buttons   :[{text:'Convert Civil Status',ui:'blue',handler:'onConvertCivilStat'},{text:'Cancel',ui:'blue',scope:this,handler:function(button,form){button.up('window').close();}}],
        listeners :{
          beforerender  :this.onBatch_response_render
        }
      });
    },

    onCivilStat_source_change:function(value, paging, params){
      var acc_id                = this.lookupReference('acc_id');
      var filesignature         = this.lookupReference('filesignature');
      var session_userid               = this.lookupReference('session_userid');
      var civilstat_source_grid = this.lookupReference('civilstat_source_grid');
      var civilstat_source      = this.lookupReference('civilstat_source');
      civilstat_source_grid.getStore().load({params:{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),user_id:session_userid.getValue(),field_name:civilstat_source.getValue()}});    
    },

    onConvertCivilStat :function(button,form,grid,record,rowIndex,colIndex){
      var acc_id                      = this.lookupReference('acc_id');
      var filesignature               = this.lookupReference('filesignature');
      var session_userid                     = this.lookupReference('session_userid');
      var civilstat_source            = this.lookupReference('civilstat_source');
      var civilstat_source_grid       = this.lookupReference('civilstat_source_grid');
      var grid                        = civilstat_source_grid.getStore();
      var match_civilstat             = this.lookupReference('match_civilstat');
      var civilstat_source_grid_combo = this.lookupReference('civilstat_source_grid_combo');
      var civilstat_form              = this.lookupReference('civilstat_form');
      var i=0;

      if(civilstat_source.getValue()==null){
        Ext.MessageBox.show({title:'WARNING',msg:'Cant convert Civil Status if required field is null or empty',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
      }else{
        Ext.Msg.confirm('Convert Gender','Are you sure you want to convert Gender',function(answer){
          if(answer=='yes'){

            var win      = new Ext.Window({
              closeAction :'hide',
              width       :'70%',
              modal       :true,
              maximized   :true,
              layout      :'vbox',
              autoShow    :true,
              items       :[
              ]
            });
            // win.show();
            grid.each(function(record){
              var civilstat_old   = record.get('tmp_civilstat');
              var civilstat_new   = record.get('match_civilstat');
              var waitMsg = Ext.MessageBox.wait('Processing your request. Please wait...');
              Ext.Ajax.request({
                url             :'../data/batch_convert_civilstat.php',
                submitEmptyText :false,
                params          :{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),user_id:session_userid.getValue(),civilstat_old:civilstat_old,civilstat_new:civilstat_new,civilstatcol:civilstat_source.getValue()},
                success         :function(response){
                  var response = Ext.decode(response.responseText);
                  if(response.success==true){
                    console.log('Civil status update '+i);
                    i++
                  }else{
                    console.log('Something went wrong cant update Civil status '+i);
                    i++
                  }

                  civilstat_source_grid.getStore().removeAll();
                  waitMsg.hide();
                  button.up('window').close();
                },failure:function(){waitMsg.hide();alert('Something went wrong. Please contact you IT for assistant')}
              });
            });
          }
        });
      }
    },
    //END FUNCTION : CONVERSION

    //CONVERT : GENDER
    onCvrtGender:function(){
      var isMinimized     = false;
      var win             = new Ext.Window({
        closeAction :'hide',
        width       :'70%',
        modal       :true,
        height      :350,
        layout      :{type:'vbox',align:'stretch'},
        title       :'Matched Gender',
        controller  :'batchupload',
        scope       :this,
        defaultType :{anchor:'100%',labelWidth:200,editable:false,emptyText:'Select column'},
        initCenter  :true,
        autoShow    :true,
        titleAlign  :'center',
        closable    :false,
        items       :[
        {xtype:'displayfield',fieldLabel:'acc_id',reference:'acc_id',hidden:true},{xtype:'displayfield',fieldLabel:'filesignature',reference:'filesignature',hidden:true},{xtype:'displayfield',fieldLabel:'session_userid',reference:'session_userid',hidden:true},
        {
          xtype     :'form',
          defaults  :{anchor:'100%',labelWidth:200,editable:false,emptyText:'Select Column'},
          reference :'gender_form',
          items     :[
          {xtype:'combo',fieldLabel:'Gender/ Name source column',name:'gender_source',reference:'gender_source',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name',margin:5,listeners:{change:'onGender_source_change'}},
          {
            xtype  :'form',
            width  :'100%',
            items  :[
            {
              xtype     :'gridpanel',
              title     :'Source Matching',
              reference :'gender_source_grid',
              autoScroll:true,
              height    :300,
              selModel  :'cellmodel',
              plugins   :{ptype:'cellediting',clicksToEdit:1},
              store     :Ext.create("OMM.store.batchupload.GenderSource_store"),
              viewConfig:{getRowClass:function(record,index){var c=record.get('match_gender');if(c==null||c==''){return'empty'}else{return'valid'}}},
              columns   :[
              {header:'Column Value',dataIndex:'tmp_gender',flex:1},
              {header:'Match to Gender',dataIndex:'match_gender',reference:'match_gender',flex:1,tdCls:'x-change-cell',editor:new Ext.form.field.ComboBox({triggerAction:'all',editable:false,reference:'gender_source_grid_combo',store:Ext.create("OMM.store.Get_gender"),valueField:'gender',displayField:'gender'})}
              ]
            }]
          }]
        }],
        buttons   :[{text:'Convert Gender',ui:'blue',handler:'onConvertGender'},{text:'Cancel',ui:'blue',scope:this,handler:function(button,form){button.up('window').close();}}],
        listeners :{
          beforerender :this.onBatch_response_render
        }
        });
    },

    onGender_source_change:function(value, paging, params){
      var acc_id             = this.lookupReference('acc_id');
      var filesignature      = this.lookupReference('filesignature');
      var session_userid            = this.lookupReference('session_userid');
      var gender_source_grid = this.lookupReference('gender_source_grid');
      var gender_source      = this.lookupReference('gender_source');
      gender_source_grid.getStore().load({params:{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),user_id:session_userid.getValue(),field_name:gender_source.getValue()}});
    },

    onConvertGender:function(button, form, grid, record, rowIndex, colIndex){
      var acc_id                   = this.lookupReference('acc_id');
      var filesignature            = this.lookupReference('filesignature');
      var session_userid                  = this.lookupReference('session_userid');
      var gender_source            = this.lookupReference('gender_source');
      var gender_source_grid       = this.lookupReference('gender_source_grid');
      var grid                     = gender_source_grid.getStore();
      var match_gender             = this.lookupReference('match_gender');
      var gender_source_grid_combo = this.lookupReference('gender_source_grid_combo');
      var gender_form              = this.lookupReference('gender_form');
      var i                        = 0;

      if(gender_source.getValue()==null){
        Ext.MessageBox.show({title:'WARNING', msg:'Cant convert Gender if required field is null or empty', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
      }else{
        Ext.Msg.confirm('Convert Gender','Are you sure you want to convert Gender',function(answer){
          if(answer=='yes'){
            grid.each(function(record){
              var gender_old  = record.get('tmp_gender');
              var gender_new  = record.get('match_gender');
              var waitMsg     = Ext.MessageBox.wait('Processing your request. Please wait...');

              Ext.Ajax.request({
                url            :'../data/batch_convert_gender.php',
                submitEmptyText:false,
                params         :{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),user_id:session_userid.getValue(),gender_old:gender_old,gender_new:gender_new,gendercol:gender_source.getValue()},
                success        :function(response){
                  var response  = Ext.decode(response.responseText);
                  if(response.success==true){
                    console.log('Gender update '+i);
                    i++
                  }else{
                    console.log('Something went wrong cant update gender '+i);
                    i++
                  }
                  gender_source_grid.getStore().removeAll();
                  waitMsg.hide();
                  button.up('window').close();
                },failure:function(){waitMsg.hide();alert('Something went wrong. Please contact you IT for assistant');}
              });
            });
            }
          });
      }
    },
    //END FUNCTION : CONVERSION FOR GENDER

    //CONVERT : PRINCIPAL MEMBER STATUS
    onCvrtMemStat:function(){
      var isMinimized=false;
      var win=new Ext.Window({
        title         :'Matched Member Status',
        titleAlign    :'center',
        controller    :'batchupload',
        scope         :this,
        closeAction   :'hide',
        width         :'70%',
        modal         :true,
        height        :350,
        closable      :false,
        initCenter    :true,
        autoShow      :true,
        layout        :{type:'vbox',align:'stretch'},
        defaultType   :{anchor:'100%',labelWidth:200,editable:false,emptyText:'Select column'},
        // tools         :[{type:'restore',hidden:true,handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.expand('',true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type:'minimize',handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.collapse();winWidth=window.getWidth().window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();isMinimized=true}}],
        items         :[
        {xtype:'displayfield',fieldLabel:'acc_id',reference:'acc_id',hidden:true},{xtype:'displayfield',fieldLabel:'filesignature',reference:'filesignature',hidden:true},{xtype:'displayfield',fieldLabel:'session_userid',reference:'session_userid',hidden:true},
        {
          xtype     :'form',
          defaults  :{anchor:'100%',labelWidth:250,editable:false,emptyText:'Select Column'},
          reference :'prin_update_member_status_form',
          items     :[
          {xtype:'combo',fieldLabel:'Member Status/ Name source column',name:'prin_update_member_status_source',reference:'prin_update_member_status_source',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name',margin:5,listeners:{change:'onPrinUpdate_member_status_source_change'}},
          {
            xtype   :'form',width:'100%',
            items   :[
            {
              xtype     :'gridpanel',
              title     :'Source Matching',
              reference :'prin_update_member_status_source_grid',
              autoScroll:true,
              height    :300,
              selModel  :'cellmodel',
              plugins   :{ptype:'cellediting',clicksToEdit:1},
              store     :Ext.create("OMM.store.batchupload.PrinMemStatStore"),
              viewConfig:{getRowClass:function(record,index){var c=record.get('match_status');if(c==null||c==''){return'empty'}else{return'valid'}}},
              columns   :[
                {header:'Column Value',dataIndex:'tmp_status',flex:1},
                {header:'Match to Member status',dataIndex:'match_status',reference:'match_status',flex:1,tdCls:'x-change-cell',editor:new Ext.form.field.ComboBox({triggerAction:'all',editable:false,reference:'prin_update_member_status_source_grid_combo',store:mbr_status,valueField:'mbr_status',displayField:'mbr_status'})}
              ]
            }]
          }]
        }],
        buttons   :[{text:'Convert Member Status',ui:'blue',handler:'onConvertMemStat'},{text:'Cancel',ui:'blue',scope:this,handler:function(button,form){button.up('window').close();}}],
        listeners :{beforerender:this.onBatch_response_render
        }
      });
    },

    onPrinUpdate_member_status_source_change:function(value, paging, params){
      var acc_id                                = this.lookupReference('acc_id');
      var filesignature                         = this.lookupReference('filesignature');
      var session_userid                               = this.lookupReference('session_userid');
      var prin_update_member_status_source_grid = this.lookupReference('prin_update_member_status_source_grid');
      var prin_update_member_status_source      = this.lookupReference('prin_update_member_status_source');
      prin_update_member_status_source_grid.getStore().load({params:{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),user_id:session_userid.getValue(),field_name:prin_update_member_status_source.getValue()}});
    },

    onConvertMemStat:function(button, form, grid, record, rowIndex, colIndex){
      var acc_id                                       = this.lookupReference('acc_id');
      var filesignature                                = this.lookupReference('filesignature');
      var session_userid                                      = this.lookupReference('session_userid');
      var prin_update_member_status_source             = this.lookupReference('prin_update_member_status_source');
      var prin_update_member_status_source_grid        = this.lookupReference('prin_update_member_status_source_grid');
      var grid                                         = prin_update_member_status_source_grid.getStore();
      var match_status                                 = this.lookupReference('match_status');
      var prin_update_member_status_source_grid_combo  = this.lookupReference('prin_update_member_status_source_grid_combo');
      var prin_update_member_status_form               = this.lookupReference('prin_update_member_status_form');
      var i                                            = 0;

      if(prin_update_member_status_source.getValue()==null){
        Ext.MessageBox.show({title:'WARNING', msg:'Cant convert Member Status if required field is null or empty', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
      }else{
        Ext.Msg.confirm('Convert Member Status','Are you sure you want to convert Member Status',
          function(answer){
            if(answer=='yes'){
              grid.each(function(record){
                var memstat_old   = record.get('tmp_status');
                var memstat_new   = record.get('match_status');
                var waitMsg       = Ext.MessageBox.wait('Processing your request. Please wait...');
                Ext.Ajax.request({
                  url            :'../data/batch_convert_member_status.php',
                  submitEmptyText:false,
                  params         :{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),user_id:session_userid.getValue(),memstat_old:memstat_old,memstat_new:memstat_new,memstatcol:prin_update_member_status_source.getValue()},
                  success        :function(response){
                    var response  = Ext.decode(response.responseText);
                    if(response.success==true){
                      console.log('Principal Member status update '+i);
                      i++
                    }else{
                      console.log('Something went wrong cant update principal member status '+i);
                      i++
                    }
                    prin_update_member_status_source_grid.getStore().removeAll();
                    waitMsg.hide();
                    button.up('window').close();
                  },failure:function(){waitMsg.hide();alert('Something went wrong. Please contact you IT for assistant')}
                });
              });
              }
            });
      }
    },
    //END FUNCTION : CONVERSION FOR PRINCIPAL MEMBER STATUS

    //CONVERT BRANCH
    onPrinCvrtBranch:function(){
      var isMinimized = false;
      var win   = new Ext.Window({
        scope:this,closeAction :'hide',width:'70%',modal:true,height:350,layout:{type:'vbox',align:'stretch'},title:'Matched Branch Id',controller:'batchupload',defaults:{anchor:'100%',labelWidth:200,editable:false,emptyText:'Select column'},initCenter:true,autoShow:true,titleAlign:'center',closable:false,
        // tools       :[{type:'restore',hidden:true,handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.expand('',true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type:'minimize',handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.collapse();winWidth=window.getWidth();window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();isMinimized=true}}],
        items       :[
        {xtype:'displayfield', fieldLabel:'acc_id', reference:'acc_id', hidden:true},{xtype:'displayfield', fieldLabel:'filesignature', reference:'filesignature', hidden:true},{xtype:'displayfield', fieldLabel:'session_userid', reference:'session_userid', hidden:true},
        {
          xtype:'form', defaults:{anchor:'100%', labelWidth:200, editable:false,emptyText:'Select Column'}, reference:'prin_update_branch_form',
          items:[
          {xtype:'combo',fieldLabel:'Branch Id/ Name source column',name:'prin_update_branch_source',reference:'prin_update_branch_source',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name',margin:5,listeners:{change:'onPrinUpdate_branch_source_change'}},
          {
            xtype   :'form',width   :'100%',
            items   :[
            {
              xtype       :'gridpanel',
              title       :'Source Matching',
              reference   :'prin_update_branch_source_grid',
              autoScroll  :true,
              height      :300,
              selModel    :'cellmodel',
              plugins     :{ptype:'cellediting',clicksToEdit:1},
              store       :Ext.create("OMM.store.batchupload.BranchSource_store"),
              viewConfig  :{getRowClass :function(record, index){var c = record.get('match_branch');if(c==null || c==''){return 'empty';}else{return 'valid';}}},
              columns     :[
              {header:'Column Value',dataIndex:'tmp_branch', flex:1},
              {
                header:'Match to Branch ID',dataIndex:'match_branch',reference:'match_branch',flex:1, tdCls:'x-change-cell',
                editor: new Ext.form.field.ComboBox({triggerAction : 'all',editable      :false,reference     :'prin_update_branch_source_grid_combo',store         :Ext.create("OMM.store.Get_account_branchid"),valueField    :'branch_id',displayField  :'branch',listeners     :{el:{click:'onPrinBranchID'}}})
              }]
            }]
          }]
        }],
          buttons:[{text:'Convert Branch',ui:'blue',handler:'onConvertBranch'}, 
          {
            text:'Cancel', 
            ui:'blue',
            scope:this,
            handler:function(button, form){
              button.up('window').close();
            }}],
          listeners:{
            beforerender:this.onBatch_response_render
          }
      });
      win.show();
    },

    onPrinBranchID:function(){
        var acc_id                                  = this.lookupReference('acc_id');
        var prin_update_branch_source_grid_combo    = this.lookupReference('prin_update_branch_source_grid_combo');
        prin_update_branch_source_grid_combo.getStore().load({params:{acc_id:acc_id.getValue()}});
    },

    onPrinUpdate_branch_source_change:function(value, paging,params){
      var acc_id                         = this.lookupReference('acc_id');
      var filesignature                  = this.lookupReference('filesignature');
      var session_userid                 = this.lookupReference('session_userid');
      var prin_update_branch_source_grid = this.lookupReference('prin_update_branch_source_grid');
      var prin_update_branch_source      = this.lookupReference('prin_update_branch_source');
      prin_update_branch_source_grid.getStore().load({params:{acc_id:acc_id.getValue(), filesignature:filesignature.getValue(), user_id:session_userid.getValue(),field_name:prin_update_branch_source.getValue()}});
    },

    onConvertBranch:function(button,form,grid,record,rowIndex,colIndex){
      var acc_id                                = this.lookupReference('acc_id');
      var filesignature                         = this.lookupReference('filesignature');
      var session_userid                        = this.lookupReference('session_userid');
      var prin_update_branch_source             = this.lookupReference('prin_update_branch_source');
      var prin_update_branch_source_grid        = this.lookupReference('prin_update_branch_source_grid');
      var grid                                  = prin_update_branch_source_grid.getStore();
      var match_branch                          = this.lookupReference('match_branch');
      var prin_update_branch_source_grid_combo  = this.lookupReference('prin_update_branch_source_grid_combo');
      var prin_update_branch_form               = this.lookupReference('prin_update_branch_form');
      var i                                     = 0;
      
      if(prin_update_branch_source.getValue()==null){
        Ext.MessageBox.show({title:'WARNING',msg:'Cant convert Branch ID if required field is null or empty',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}})
      }else{
        Ext.Msg.confirm('Convert Branch','Are you sure you want to convert select Branch ID',function(answer){
          if(answer=='yes'){
            grid.each(function(record){
              var branch_old  = record.get('tmp_branch');
              var branch_new  = record.get('match_branch');
              var waitMsg     = Ext.MessageBox.wait('Processing your request. Please wait...');
              Ext.Ajax.request({
                url             :'../data/batch_convert_branch.php',
                submitEmptyText :false,
                params          :{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),user_id:session_userid.getValue(),branch_old:branch_old,branch_new:branch_new,branchcol:prin_update_branch_source.getValue()},
                success         :function(response){
                  var response=Ext.decode(response.responseText);
                  if(response.success==true){
                    console.log('Update branch id  '+i);
                    i++;
                  }else{
                    console.log('Something went wrong cant update Branch Id '+i);
                    i++;
                  }
                  prin_update_branch_form.getForm().reset();
                  prin_update_branch_source_grid.getStore().removeAll()
                  waitMsg.hide();
                  button.up('window').close();
                },failure:function(){waitMsg.hide();alert('Something went wrong. Please contact your IT for assistant')}
              });
            });
          }
        });
      }
    },
    //END FUNCTION : CONVERSION FOR BRANCH

    //CONVERT : PLAN COVERAGE
    onPrinCvrtPlan:function(btn){
      var isMinimized                 = false;
      var principal_update_field_form = this.lookupReference('principal_update_field_form');
      
      var win = new Ext.Window({
        closeAction :'hide',
        closable    :false,
        width       :'70%',
        modal       :true,
        height      :400,
        layout      :{type:'vbox',align:'stretch'},
        title       :'Matched Plan Level',
        controller  :'batchupload',
        scope       :this,
        initCenter  :true,
        autoShow    :true,
        titleAlign  :'center',
        // tools       :[{type:'restore',hidden:true,handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.expand('',true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type:'minimize',handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.collapse();winWidth=window.getWidth();window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();isMinimized=true}}],
        items       :[
        {xtype:'displayfield',name:'acc_id',reference:'acc_id',hidden:true},{xtype:'displayfield',name:'filesignature',reference:'filesignature',hidden:true},{xtype:'displayfield',name:'session_userid',reference:'session_userid',hidden:true},
        {
          xtype      :'form',
          defaults   :{anchor:'100%',labelWidth:200,editable:false,emptyText:'Select column'},
          reference  :'prin_update_plan_form',
          items      :[
          {xtype:'combo',fieldLabel:'<span style="color: rgb(255, 0,0); font-weight:bold;">Plan Level/ID * </span>',name:'prin_update_plan_source',reference:'prin_update_plan_source',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name',margin:5},
          {xtype:'combo',fieldLabel:'<span style="color: rgb(255, 0, 0); font-weight:bold;">Branch ID',name:'prin_update_plan_branch_source',reference:'prin_update_plan_branch_source',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name',margin:5},
          {xtype:'combo',fieldLabel:'Distinct Branch ID',name:'prin_update_plan_branch_distinct_source',reference:'prin_update_plan_branch_distinct_source',emptyText:'Select Branch ID',store:Ext.create("OMM.store.batchupload.BranchItemStore"),displayField:'tmp_branch',valueField:'tmp_branch',margin:5,listeners:{el:{click:'onPrinUpdate_plan_branch_distinct_click'},change:'onPrinUpdate_plan_branch_distinct_change'}},
          {
            xtype :'form',
            width :'100%',
            items :[
            {
              xtype     :'gridpanel',
              title     :'Source Matching',
              reference :'prin_update_plan_source_grid',
              autoScroll:true,
              height    :300,
              selModel  :'cellmodel',
              plugins   :{ptype:'cellediting',clicksToEdit:1},
              store     :Ext.create("OMM.store.batchupload.PlanSource_store"),
              viewConfig:{getRowClass:function(record,index){var c=record.get('match_plan');if(c==null||c==''){return'empty'}else{return'valid'}}},
              columns   :[
              {header:'Branch ID Value',dataIndex:'tmp_branch',flex:1},
              {header:'Plan ID Value',dataIndex:'tmp_plan',flex:1},
              {header:'Match to Plan ID',dataIndex:'match_plan',reference:'match_plan',tdCls:'x-change-cell',emptyText:'Select Plan ID to match',flex:1,editor:new Ext.form.field.ComboBox({triggerAction:'all',editable:false,reference:'prin_update_plan_source_grid_combo',emptyText:'Select Plan ID to match',store:Ext.create("OMM.store.Get_account_plans"),valueField:'planid',displayField:'planid',listeners:{el:{click:'onPrinPlanID'}}})}
              ]
            }]
          }]
        }],
        buttons   :[
        {text:'Convert Plan',ui:'blue',handler:'onConvertPlan'},
        {
          text    :'Cancel',
          ui      :'blue',
          scope   :this,
          handler :function(button,form){
            button.up('window').close();
          }}],
        listeners :{beforerender:this.onBatch_response_render}
        });
    },

    onConvertPlan:function(button,record, index, val, meta, rowIndex){
      var acc_id                                    = this.lookupReference('acc_id');
      var filesignature                             = this.lookupReference('filesignature');
      var session_userid                            = this.lookupReference('session_userid');
      var prin_update_plan_source                   = this.lookupReference('prin_update_plan_source');
      var prin_update_plan_branch_source            = this.lookupReference('prin_update_plan_branch_source');
      var prin_update_plan_branch_distinct_source   = this.lookupReference('prin_update_plan_branch_distinct_source');
      var prin_update_plan_source_grid              = this.lookupReference('prin_update_plan_source_grid');
      var prin_update_plan_source_grid_combo        = this.lookupReference('prin_update_plan_source_grid_combo');
      var match_plan                                = this.lookupReference('match_plan');
      var prin_update_plan_form                     = this.lookupReference('prin_update_plan_form');
      var grid                                      = prin_update_plan_source_grid.getStore();
      var rec                                       = grid.getAt(rowIndex);
      var i                                         = 0;

      if(prin_update_plan_branch_distinct_source.getValue()==null){
        Ext.MessageBox.show({title:'WARNING',msg:'Cant convert Plan ID if required fields is null or empty',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
      }else{
        Ext.Msg.confirm('Convert Plan Level','Are you sure you want to convert Plan Level'+"\n"+grid.data,function(answer){
          if(answer=='yes'){
            grid.each(function(record){
              var branch     = record.get('tmp_branch');
              var planid_old = record.get('tmp_plan');
              var planid_new = record.get('match_plan');              
              var waitMsg    = Ext.MessageBox.wait('Processing your request. Please wait...');
              console.log(branch);

              Ext.Ajax.request({
                url            :'../data/batch_convert_plan.php',
                timeout        :5000,
                params         :{acc_id:acc_id.getValue(),planidcol:prin_update_plan_source.getValue(),branchcol:prin_update_plan_branch_source.getValue(),planid_old:planid_old,planid_new:planid_new,branch:branch,filesignature:filesignature.getValue(),user_id:session_userid.getValue()},
                success        :function(response){
                  var response  = Ext.decode(response.responseText);
                  if(response.success==true){
                    console.log('Update plan id'+i);
                    i++;
                  }else{
                    console.log('Something went wrong cant update Plan Level/ID '+i);
                    i++;
                  }
                  waitMsg.hide();
                }
              });
            });
            prin_update_plan_source_grid.getStore().removeAll();
            button.up('window').close();
          }})}
    },
    //END FUNCTION : CONVERSION FOR PLAN COVERAGE

    //CONVERT : DEPENDENT ENROLLMENT STATUS
    onDepCvrtStatus:function(btn){
      var isMinimized   = false;
      var win = new Ext.Window({
        closeAction   :'hide',
        closable      :false,
        width         :'70%',
        modal         :true,
        height        :350,
        layout        :{type:'vbox',align:'stretch'},
        title         :'Dependent Setting',
        controller    :'batchupload',
        scope         :this,
        defaults      :{anchor:'100%',labelWidth:200,editable:false,emptyText:'Select column'},
        initCenter    :true,
        autoShow      :true,
        titleAlign    :'center',
        // tools         :[{type:'restore',hidden:true,handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.expand('',true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type:'minimize',handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.collapse();winWidth=window.getWidth();window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();isMinimized=true}}],
        items         :[
        {xtype:'displayfield',fieldLabel:'acc_id',reference:'acc_id',hidden:true},{xtype:'displayfield',fieldLabel:'filesignature',reference:'filesignature',hidden:true},{xtype:'displayfield',fieldLabel:'session_userid',reference:'session_userid',hidden:true},
        {
          xtype       :'form',
          defaults    :{anchor:'100%',labelWidth:200,editable:false,emptyText:'Select Column'},
          reference   :'dep_update_status_form',
          items:[
          {xtype:'combo',fieldLabel:'Dependent Enrollment Status',name:'dep_update_status_source',reference:'dep_update_status_source',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name',margin:5,listeners:{change:'onDepUpdate_status_change'}},
          {
            xtype   :'form',
            width   :'100%',
            items   :[
            {
              xtype     :'gridpanel',
              title     :'Source Matching',
              reference :'dep_update_status_source_grid',
              autoScroll:true,
              height    :300,
              selModel  :'cellmodel',
              plugins   :{ptype:'cellediting',clicksToEdit:1},
              store     :Ext.create("OMM.store.batchupload.DepStatusStore"),
              viewConfig:{getRowClass:function(record,index){var c=record.get('match_status');if(c==null||c==''){return'empty'}else{return'valid'}}},
              columns   :[
              {header:'Column Value',dataIndex:'tmp_status',flex:1},
              {header:'Match to Status',dataIndex:'match_status',reference:'status',flex:1,tdCls:'x-change-cell',editor:new Ext.form.field.ComboBox({triggerAction:'all',editable:false,reference:'prin_update_branch_source_grid_combo',store:Ext.create("OMM.store.Get_status"),valueField:'id',displayField:'status'})}
              ]
            }]
          }]
        }],
        buttons   :[{text:'Convert Status',ui:'blue',handler:'onConvertDepStatus'},{text:'Cancel',ui:'blue',scope:this,handler:function(button,form){button.up('window').close();}}],
        listeners :{
          beforerender  :this.onBatch_response_render}
      });
        win.show();
    },

    onDepUpdate_status_change:function(value, paging,params){
      var acc_id                         = this.lookupReference('acc_id');
      var filesignature                  = this.lookupReference('filesignature');
      var session_userid                 = this.lookupReference('session_userid');
      var dep_update_status_source_grid  = this.lookupReference('dep_update_status_source_grid');
      var dep_update_status_source       = this.lookupReference('dep_update_status_source');
      dep_update_status_source_grid.getStore().load({params:{acc_id:acc_id.getValue(), filesignature:filesignature.getValue(), user_id:session_userid.getValue(),field_name:dep_update_status_source.getValue()}});
    },

    onConvertDepStatus:function(button, form, grid, record, rowIndex, colIndex){
      var acc_id                        = this.lookupReference('acc_id');
      var filesignature                 = this.lookupReference('filesignature');
      var session_userid                = this.lookupReference('session_userid');
      var dep_update_status_source      = this.lookupReference('dep_update_status_source');
      var dep_update_status_source_grid = this.lookupReference('dep_update_status_source_grid');
      var grid                          = dep_update_status_source_grid.getStore();
      var dep_update_status_form        = this.lookupReference('dep_update_status_form');
      var i                             = 0;

      if(dep_update_status_source.getValue()==null){
        Ext.MessageBox.show({title:'WARNING', msg:'Cant Convert Dependent Status if required field is null or empty', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
      }else{
        Ext.Msg.confirm('Convert Dependent Status','Are you sure you want to convert Dependent Status',
          function(answer){
            if(answer=='yes'){
              grid.each(function(record){
                var depstatus_old = record.get('tmp_status');
                var depstatus_new = record.get('match_status');
                var waitMsg       = Ext.MessageBox.wait('Processing your request. Please wait...');
                
                Ext.Ajax.request({
                  url             :'../data/batch_convert_dep_status.php',
                  waitMsg         :'Please wait, Processing you request',
                  submitEmptyText :false,
                  params          :{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),user_id:session_userid.getValue(),depstatus_old:depstatus_old,depstatus_new:depstatus_new,depstatuscol:dep_update_status_source.getValue()},
                  success         :function(response){
                    var response  = Ext.decode(response.responseText);
                    if(response.success==true){
                      console.log('Dependent status update '+i);
                      i++;
                    }else{
                      console.log('Something went wrong cant update dependent status '+i);
                      i++
                    }
                    dep_update_status_source_grid.getStore().removeAll();
                    waitMsg.hide();
                    button.up('window').close();
                  },failure:function(){waitMsg.hide();alert('Something went wrong. Please contact IT for your assistant')}
                });
              });
            }
        });
      }
    },
    //END FUNCTION : CONVERSION FOR DEPENDENT STATUS

    //FUNCTIONS FOR BATCH PROCESS
    onRemovePlanGrid:function(){      
      var prin_update_plan_source_grid        = this.lookupReference('prin_update_plan_source_grid');
      prin_update_plan_source_grid.getStore().removeAll();
    },

    onPrinPlanID:function(){
      var acc_id                                = this.lookupReference('acc_id');
      var branch_id                             = this.lookupReference('prin_update_plan_branch_distinct_source');
      var prin_update_plan_source_grid_combo    = this.lookupReference('prin_update_plan_source_grid_combo');
      prin_update_plan_source_grid_combo.getStore().load({params:{acc_id:acc_id.getValue(), branch_id:branch_id.getValue()}});
    },
    
    onPrinUpdate_plan_source_change:function(value,paging, params){
      var prin_update_plan_source_grid   = this.lookupReference('prin_update_plan_source_grid');
      var prin_update_plan_source        = this.lookupReference('prin_update_plan_source');
      prin_update_plan_source_grid.getStore().load({params:{field_name:prin_update_plan_source.getValue()}});
    },

    onPrinUpdate_plan_branch_distinct_click:function(value, paging,params){
      var acc_id                                  = this.lookupReference('acc_id');
      var filesignature                           = this.lookupReference('filesignature');
      var session_userid                          = this.lookupReference('session_userid');
      var prin_update_plan_source                 = this.lookupReference('prin_update_plan_source');
      var prin_update_plan_branch_source          = this.lookupReference('prin_update_plan_branch_source');
      var prin_update_plan_branch_distinct_source = this.lookupReference('prin_update_plan_branch_distinct_source');

      console.log(prin_update_plan_source.getValue() +"\n "+prin_update_plan_branch_source.getValue())

      if(prin_update_plan_source.getValue()==null || prin_update_plan_branch_source.getValue()==null){
        Ext.MessageBox.show({title:'WARNING', msg:'Please fill Plan and Branch field above', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}})
      }else{
        prin_update_plan_branch_distinct_source.getStore().load({params:{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),user_id:session_userid.getValue(),branch_field:prin_update_plan_branch_source.getValue()}});
        console.log('click combo')
      }
    },

    onPrinUpdate_plan_branch_distinct_change:function(value,paging,params){
      var acc_id                                   = this.lookupReference('acc_id');
      var filesignature                            = this.lookupReference('filesignature');
      var session_userid                           = this.lookupReference('session_userid');
      var prin_update_plan_source                  = this.lookupReference('prin_update_plan_source');
      var prin_update_plan_source_grid             = this.lookupReference('prin_update_plan_source_grid');
      var prin_update_plan_branch_source           = this.lookupReference('prin_update_plan_branch_source');
      var prin_update_plan_branch_distinct_source  = this.lookupReference('prin_update_plan_branch_distinct_source');
      prin_update_plan_source_grid.getStore().load({params:{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),user_id:session_userid.getValue(),planid_field:prin_update_plan_source.getValue(),branch_field:prin_update_plan_branch_source.getValue(),branch:prin_update_plan_branch_distinct_source.getValue()}});
    },
    //END OF FUNCTIONS

    //BEFORERENDER : BATCH UPLOAD
    onBatchUploadBeforerender:function(){

      console.log(window.console);if(window.console||window.console.firebug){console.clear()}

      var temptable       = this.lookupReference('temptable_list');
      var prin            = this.lookupReference('menu_prin');
      var dep             = this.lookupReference('menu_dep');
      var batch_toolbar   = this.lookupReference('batch_toolbar');
      var batch_main_panel= this.lookupReference('batch_main_panel');

      // prin.setDisabled(true);
      // dep.setDisabled(true);
      batch_toolbar.hide();

      batch_main_panel.add({xtype:'uploadfile'});
      batch_main_panel.remove(temptable, true);
    },
    //END FUNCTION : BEFORERENDER BATCH UPLOAD

    //BATCH : UPLOAD BUTTON
    onBatchUploadfile:function(){

      var temptable       = this.lookupReference('temptable_list');
      var prin            = this.lookupReference('menu_prin');
      var dep             = this.lookupReference('menu_dep');
      var upload          = this.lookupReference('menu_upload');
      var batch_toolbar   = this.lookupReference('batch_toolbar');
      var batch_main_panel= this.lookupReference('batch_main_panel');

      Ext.Msg.confirm('File Upload','Are you sure you want to cancel the process and upload another file', function(answer){
        if(answer=='yes'){
          console.log(window.console);if(window.console||window.console.firebug){console.clear()}
          // upload.setDisabled(true);
          prin.setDisabled(false);
          dep.setDisabled(false);
          batch_toolbar.hide();
          batch_main_panel.add({xtype:'uploadfile'});
          batch_main_panel.remove(temptable, true);
        }
      });
    },

    onUF_step1_next:function(form){
      var me    = this.lookupReference('uploadfile');
      var step1 = this.lookupReference('UF_step1');
      var form  = step1.getForm();
      var l     = me.getLayout();

      if(form.isValid()){
        form.submit({
          url             :'../data/batch_up_file.php',
          waitMsg         :'Processing your request. Please wait',
          headers         :{'Content-Type':'multipart/form-data; charset=UTF-8'},
          submitEmptyText :false,
          success         :function(form,action){
            Ext.MessageBox.show({title:'Success',msg:"<b>File Uploaded!</b><br><br>"+"File Name : "+action.result.file_name+"<br>"+"File Size : "+action.result.file_size+"<br>"+"Operator : "+action.result.operator+"<br>"+"Date Modified : "+action.result.timestamp,closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}});
            // console.log(l.getActiveItem());
            me.down('#UF_step2').setDisabled(false);
            l.setActiveItem(1);
            form.reset();
            me.down('#UF_step1').setDisabled(true);
          },failure       :function(form,action){
            Ext.MessageBox.show({title:'Failed',msg:action.result.message,closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
          }
        });
      }else{
        Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check details of form or contact IT for assistant',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
      }
    },

    onUF_worksheet_click:function(){
      var worksheet_name = this.lookupReference('worksheet_name');
      worksheet_name.getStore().load();
    },

    onUF_step2_next:function(form){
      var me          = this.lookupReference('uploadfile');
      var step2       = this.lookupReference('UF_step2');
      var form        = step2.getForm();
      var l           = me.getLayout();
      var FieldConfig = this.FieldConfig=Ext.create('OMM.store.batchupload.FieldConfig');

      if(form.isValid()){
        form.submit({
          method              :'POST',
          timeout             :2000000,
          scope               :this,
          waitMsg             :'Processing your request. Please wait',
          url                 :'../data/post_worksheet.php',
          submitEmptyText     :false,
          disableCaching      :true,
          observable          :false,
          useDefaultXhrHeader :false,
          success             :function(form,action){
            FieldConfig.removeAll();
            console.log('Field clear store');
            FieldConfig.on('load',this.onStoreLoad,this,{single:true});
            FieldConfig.load();
            me.down('#UF_step3').setDisabled(false);
            l.setActiveItem(2);
            form.reset();
            step2.setDisabled(true)
          },failure           :function(form,action){Ext.MessageBox.show({title:'WARNING',msg:action.result.message,closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}})}
        })
      }else{Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check details of form or contact IT for assistant',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}})}
    },

    onStoreLoad: function() {
      var me=this;
      var i=0;
      var UF_step3_form=this.lookupReference('UF_step3_form');

      this.FieldConfig.each(function(record){
        var field_name=record.get('field_name');

        if(UF_step3_form.getForm().findField(field_name)){
          var field_name=field_name+"_"+i;
          me.addCombo(me,field_name);
          i++;
        }else if(field_name=='status_match' || field_name=='Status_match'){
          var field_name = field_name+"_"+i;
          me.addCombo(me,field_name);
          i++;
        }else if(field_name=='status_msg' || field_name=='Status_msg'){
          var field_name = field_name+"_"+i;
          me.addCombo(me, field_name);
          i++;
        }else if(field_name=='status' || field_name=='Status'){
          var field_name = field_name+"_"+i;
          me.addCombo(me, field_name);
          i++;
        }else if(field_name=='fname'){
          var field_name=field_name+"_"+i;
          me.addCombo(me,field_name);
          i++
        }else if(field_name=='lname'){
          var field_name=field_name+"_"+i;
          me.addCombo(me,field_name);
          i++
        }else if(field_name=='mname'){
          var field_name=field_name+"_"+i;
          me.addCombo(me,field_name);
          i++
        }else if(field_name=='birthdate'){
          var field_name=field_name+"_"+i;
          me.addCombo(me,field_name);
          i++
        }else if(field_name==null){
          var field_name="Blank_Field_"+i;
          me.addCombo(me,field_name);
          i++
        }else if(field_name==''){
          var field_name="Blank_Field_"+i;
          me.addCombo(me,field_name);
          i++
        }else if(field_name=='ID'||field_name=='id'){
          var field_name="ID_"+i;
          me.addCombo(me,field_name);
          i++
        }else{
          me.addCombo(me,field_name)
        }
      });      
    },

    addCombo:  function(panel,field_name,total){
      var me    = this.lookupReference('uploadfile');
      var step2 = this.lookupReference('UF_step2');
      var form  = step2.getForm();
      var l     = me.getLayout();
      console.log(field_name);

      if(field_name.indexOf("date")>=0||field_name.indexOf("Date")>=0||field_name.indexOf("DATE")>=0){

        me.down('#UF_step3_form').add({
          bodyPadding   :'5px 10px 5px 10px',
          fieldLabel    :field_name,
          xtype         :'combo',
          store         :field_dataType,
          name          :field_name,
          editable      :false,
          queryMode     :'local',
          valueField    :'value',
          displayField  :'field_dataType',
          value         :'date',
          displayValue  :'Date field',
          listeners     :{
            select  :function(combo,records){
              var value   = this.getValue();
              if(value=='delete_field'){
                this.setFieldStyle('background:#e44959')
              }else{
                this.setFieldStyle('background:#FFFFFF')
              }
            }}
        });

      }else{

        me.down('#UF_step3_form').add({
          bodyPadding   :'5px 10px 5px 10px',
          fieldLabel    :field_name,
          xtype         :'combo',
          store         :field_dataType,
          name          :field_name,
          editable      :false,
          queryMode     :'local',
          valueField    :'value',
          displayField  :'field_dataType',
          value         :'VARCHAR(250)',
          displayValue  :'Text field',
          listeners     :{
            select  :function(combo,records){
              var value   = this.getValue();
              if(value=='delete_field'){
                this.setFieldStyle('background:#e44959')
              }else{
                this.setFieldStyle('background:#FFFFFF')
              }
            }}
          });
      }
    },

    onUF_step3_next:function(){
      var me                = this.lookupReference('uploadfile');
      var step3             = this.lookupReference('UF_step3');
      var form              = step3.getForm();
      var l                 = me.getLayout();
      var batch_main_panel  = this.lookupReference('batch_main_panel');
      var prin              = this.lookupReference('menu_prin');
      var dep               = this.lookupReference('menu_dep');
      var upload            = this.lookupReference('menu_upload');
      var batch_toolbar     = this.lookupReference('batch_toolbar');

      if(form.isValid){
        form.getFields().each(function(field){
          if(field.rawValue=='Delete field'){
            console.log("Deleted daw "+field.rawValue);
            field.destroy();
          }else{
            console.log(field.rawValue)
          }
        });

        form.submit({
          url     :'../data/batch_create_tmptbl.php',
          waitMsg :'Processing your request. Please wait...',
          params  :{fieldname:Ext.encode(form.getFieldValues())},
          success :function(form,action){
            var result=action.result;
            if(result.success==true){
              Ext.MessageBox.show({title:'Success',msg:"<b>Temporary table created!</b><br></br>Total record :"+result.totalparse+"<br>",closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}});
              me.down('#UF_step1').setDisabled(false);
              me.down('#UF_step2').setDisabled(true);
              me.down('#UF_step3').setDisabled(true);
              l.setActiveItem(0);
              // prin.setDisabled(false);
              // dep.setDisabled(false);
              // upload.setDisabled(false);
              batch_toolbar.show();
              batch_main_panel.remove(me,true);
              batch_main_panel.add({xtype:'temptable_list'});
            }else{
              Ext.MessageBox.show({title:'WARNING',msg:response.message,closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}})
            }
          },failure :function(form,action){
            var result=action.result;
            Ext.MessageBox.show({title:'WARNING',msg:result.message,closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
          }
        })
        }else{
          Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check details of form or contact IT for assistant',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}})
        }
    },

    onTempTableBeforerender:function(){
      var me             = this.lookupReference('uploadfile'),
          ColumnField    = this.ColumnField = Ext.create('OMM.store.batchupload.TempTableStore'),
          temptable_list = this.lookupReference('temptable_list');

      ColumnField.removeAll();
      console.log('Temporary Grid clear store');
      ColumnField.on('load',function(store,record,successful,eOpts){
      ColumnField.each(function(record){
        var field_name=record.get('field_name');
          temptable_list.down('#temptable_grid').headerCt.add({header:field_name,dataIndex:field_name,editor: {xtype: 'textfield'}});              
        });
      console.log(temptable_list.down('#temptable_grid').headerCt.getGridColumns());
      var gridColumns = temptable_list.down('#temptable_grid').headerCt.getGridColumns();
      // for(var i = gridColumns.length; i >= 1; i--){
      for (var i = 0; i < gridColumns.length; i++) {
        if (gridColumns[i].text == 'status_match') {
          // alert(i);
          temptable_list.down('#temptable_grid').headerCt.items.getAt(i).setVisible(false);
        }
        if (gridColumns[i].text == 'status') {
          // alert(i);
          temptable_list.down('#temptable_grid').headerCt.items.getAt(i).setVisible(false);
        }
        if (gridColumns[i].text == 'status_msg') {
          // alert(i);
          temptable_list.down('#temptable_grid').headerCt.items.getAt(i).setVisible(false);
        }
      }

      });
      ColumnField.load();    

    },

    onTempTableAfterrender:function(record){
      var temptable_grid = this.lookupReference('temptable_grid').getStore();
      temptable_grid.load();
      console.log('Temporary Grid record : ',temptable_grid.data);   
    },
    //END PARSE DATA

    //PRINCIPAL INCLUSION
    onPrinInclusion_menu:function(){
      console.log(window.console);
      if(window.console||window.console.firebug){console.clear()}
      Ext.create('OMM.view.batchupload.Principal_inclusion').show();
    },

    onPrinIncReset:function(){
      var principal_inclusion_field_form = this.lookupReference('principal_inclusion_field_form');
      var prin_inc_opt_field_form        = this.lookupReference('principal_inclusion_optional_field_form');
      var form                           = principal_inclusion_field_form.getForm();
      form.reset();
      prin_inc_opt_field_form.removeAll();
    },

    onPrinInc_upload:function(button, field){
      var me                                = this;
      var id                                = 1;
      var principal_inclusion_field_form    = this.lookupReference('principal_inclusion_field_form');
      var form                              = principal_inclusion_field_form.getForm();
      var option                            = this.lookupReference('option');
      var TempData                          = this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata');
      var principal_inclusion_opt_field_form=this.lookupReference('principal_inclusion_optional_field_form');
      // console.log(Ext.encode(form.getFieldValues()+form2.findField('lname').getFormSubmit();));

      if(form.isValid()){
        Ext.Msg.confirm('PRINCIPAL INCLUSION','Are you sure you want to save member',function(answer){
          if(answer=='yes'){
            option.destroy();
            principal_inclusion_opt_field_form.getForm().getFields().each(function(field){
              if(field.rawValue=='' || field.rawValue==null || field.rawValue=='Blank_Field'){
                console.log("NULL field :"+field.fieldLabel);
                field.destroy();
              }else{
               // console.log(field.rawValue);
              }
            });
            var win=new Ext.Window({
              closeAction :'hide',
              width       :'70%',
              modal       :true,
              maximized   :true,
              layout      :'vbox',
              autoShow    :true,
              autoScroll  :true
            });

            TempData.load({
            callback: function(records, operation, success) {
              var total_count   = records.length
              console.log(records.length);
              me.prin_inclusion_AjaxRequest_getId(id, total_count)
            }
            });
          }
        });
      }else{
        Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check required fields or contact IT for assistant',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}})
      }
    },

    prin_inclusion_AjaxRequest_getId:function(id, total_count){
      var win                  = Ext.WindowManager.getActive();
      var layout               = this.lookupReference('principal_inclusion_tab');
      var l                    = layout.getLayout();
      var prinInclusion_result = this.lookupReference('principal_inclusion_result');
      var me                   = this;

      console.log('id'+id+' total'+total_count);
      if(total_count>=id){
        win.add({
        xtype:'label',
        html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px">Record:'+id
        });
        me.prin_inclusion_AjaxRequest_ProcessId(id,total_count)
      }else{
        win.show();
        Ext.MessageBox.show({title:'PRINCIPAL INCLUSION', msg:'Process finished. Please check details posted in Window',icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
        console.log(l.getActiveItem());
        layout.down('#principal_inclusion_result').setDisabled(false);
        layout.down('#principal_inclusion_field_form').setDisabled(true);
        layout.setActiveItem(1);
        // layout.down('#PU_step2').setDisabled(true);
        prinInclusion_result.add({xtype:'principal_failed_panel'});
      }
    },

    prin_inclusion_AjaxRequest_ProcessId:function(id,total_count){
      var prinInclusion_field_form= this.lookupReference('principal_inclusion_field_form');
      var form                    = prinInclusion_field_form.getForm();
      var prin_inc_req_field_form = this.lookupReference('principal_inclusion_required_field_form');
      var form2                   = prin_inc_req_field_form.getForm();
      var acc_id                  = this.lookupReference('acc_id');
      var session_userid          = this.lookupReference('session_userid');
      var filesignature           = this.lookupReference('filesignature');
      var me                      = this;

      Ext.Ajax.request({
        url             :'../data/prin_inc_upload.php',
        method          :'POST',
        params          :{id:id,acc_id:acc_id.getValue(),session_userid:session_userid.getValue(),filesignature:filesignature.getValue(),fields:Ext.encode(form.getFieldValues()),keys:Ext.encode(form2.getFieldValues())},
        submitEmptyText :false,
        success         :function(response){
          var response  = Ext.decode(response.responseText),
              result    = response.data[0].result;
          console.log(id);
          console.log(result);
          me.prin_inclusion_AjaxRequest_CallSuccess(id, total_count, result);
        },failure         :function(response){
          var result    = Ext.decode(response.responseText);
          console.log(result);
          me.prin_inclusion_AjaxRequest_CallSuccess(id,total_count,result)
          console.log('failed');
        }
      });
      
      // form.submit({url:'../data/prin_inc_upload.php',method:'POST',params:{id:id,acc_id:acc_id.getValue(),session_userid:session_userid.getValue(),filesignature:filesignature.getValue(),fields:Ext.encode(form.getFieldValues()),keys:Ext.encode(form2.getFieldValues())},submitEmptyText:false,success:function(form,action){var success=action.result.success var result=action.result.data[0].result console.log(id);console.log(result);me.prin_inclusion_AjaxRequest_CallSuccess(id,total_count,result)},failure:function(form,action){var result=action.result.data[0].result console.log(result);me.prin_inclusion_AjaxRequest_CallSuccess(id,total_count,result)console.log('failed')}});
    },

    prin_inclusion_AjaxRequest_CallSuccess:function(id,total_count,result){
      var win = Ext.WindowManager.getActive();
      var me  = this;

      win.add({
        xtype :'label',
        html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top: 5px;margin-left:5px;">'+result
      });
     me.prin_inclusion_AjaxRequest_getId(id + 1, total_count)
    },

    //END FUNCTION PRINCIPAL INCLUSION

    //PRINCIPAL UPDATE
    onPrinUpdate_menu:function(){

      console.log(window.console);if(window.console||window.console.firebug){console.clear()}
      var rowfield        = this.rowfield= Ext.create('OMM.store.batchupload.TempTableStore');

      rowfield.removeAll();
      console.log('Row Field store clear')
      rowfield.load();// upload.setDisabled(true);prin.setDisabled(true);dep.setDisabled(true);prinInc.show();
      Ext.create('OMM.view.batchupload.Principal_update').show();// batch_main_panel.remove(temptable, true);
    
    },

    onUpdateUniquekey:function(value, paging, params){
      if(window.console||window.console.firebug){
        console.clear()
      }
      var update_selection             = this.lookupReference('update_selection');
      var unique_key_opt               = this.lookupReference('unique_key_opt');
      var principal_update_field_form  = this.lookupReference('principal_update_field_form');
      var principal_update_unique_form = this.lookupReference('principal_update_unique_form');
      
      this.selected_update_val         = value.getValue();
      this.selected_update_raw         = value.getRawValue();
      console.log('Unique key : '+this.selected_update_val);
      
      if(this.selected_update_val=='empid'){
        principal_update_unique_form.removeAll();
        principal_update_field_form.removeAll();
        principal_update_unique_form.add({
          bodyPadding   :'5px 10px 5px 10px',
          fieldLabel    :this.selected_update_raw,
          xtype         :'combo',
          store         :Ext.create("OMM.store.batchupload.TempTableStore"),
          name          :this.selected_update_val,
          editable      :false,
          valueField    :'field_name',
          displayField  :'field_name',
          closable      :true
        });
      }else if(this.selected_update_val=='al_id'){
        principal_update_unique_form.removeAll();
        principal_update_field_form.removeAll();
        principal_update_unique_form.add({fieldLabel:'ActiveLink ID',name:'al_id'});
      }else if(this.selected_update_val=='fn_ln_db'){
        principal_update_unique_form.removeAll();
        principal_update_field_form.removeAll();
        principal_update_unique_form.add({fieldLabel:'First Name',name:'fname'},{fieldLabel:'Last Name',name:'lname'},{fieldLabel:'Birthdate',name:'birthdate'})}
    },

    onUniqueKeyNext:function(){ 
      var me    = this.lookupReference('principal_update_tab');
      var step1 = this.lookupReference('PU_step1');
      var form  = step1.getForm();
      var l     = me.getLayout();

      console.log(l.getActiveItem());
      me.down('#PU_step2').setDisabled(false);
      l.setActiveItem(1);// form.reset();// me.down('#PU_step1').setDisabled(true);
    },

    onFieldSelectReset:function(){
      var PU_step2                     = this.lookupReference('PU_step2');
      var principal_update_field_form  = this.lookupReference('principal_update_field_form');
      var form                         = PU_step2.getForm();

      form.reset();
      principal_update_field_form.removeAll();
    },

    onPUBranchID:function(){
        var PU_branch    = this.lookupReference('PU_branch');
        PU_branch.getStore().load({});
    },

    onPUCompanyID:function(){
        var PU_planLevel  = this.lookupReference('PU_planLevel');
        var PU_branch     = this.lookupReference('PU_branch');
        // PU_planLevel.getStore().load({params:{branch_id:PU_branch.getValue()}});   
        PU_planLevel.getStore().load({});   
    },

    onUpdateSelectionClick:function(value, paging, params){
        var unique_key_opt           = this.lookupReference('unique_key_opt');
        if(unique_key_opt.getValue()=='' || unique_key_opt.getValue() == null){Ext.MessageBox.show({title:'WARNING', msg:'Select Unique key first',icon:Ext.Msg.WARNING,closable:false, buttonText:{ok:'OK'}});}
    },

    onOptionFieldSelectionChange:function(value, paging, params){
      if(window.console || window.console.firebug){
        console.clear();
      }

      var principal_inclusion      = this.lookupReference('principal_inclusion');
      var prin_inc_opt_field_form  = this.lookupReference('principal_inclusion_optional_field_form');

      this.selected_optional_val=value.getValue();
      this.selected_optional_raw=value.getRawValue();
      console.log('selectedValue: '+this.selected_update_val);
      if(prin_inc_opt_field_form.getForm().findField('empid')&&this.selected_optional_val=='empid'||prin_inc_opt_field_form.getForm().findField('id_number')&&this.selected_optional_val=='id_number'||prin_inc_opt_field_form.getForm().findField('mname')&&this.selected_optional_val=='mname'||prin_inc_opt_field_form.getForm().findField('ename')&&this.selected_optional_val=='ename'||prin_inc_opt_field_form.getForm().findField('designation')&&this.selected_optional_val=='designation'||prin_inc_opt_field_form.getForm().findField('sss_no')&&this.selected_optional_val=='sss_no'||prin_inc_opt_field_form.getForm().findField('pag_ibig_no')&&this.selected_optional_val=='pag_ibig_no'||prin_inc_opt_field_form.getForm().findField('unified_id_no')&&this.selected_optional_val=='unified_id_no'||prin_inc_opt_field_form.getForm().findField('tin')&&this.selected_optional_val=='tin'||prin_inc_opt_field_form.getForm().findField('philhealth')&&this.selected_optional_val=='philhealth'){
        Ext.MessageBox.show({title:'WARNING', msg:'Cannot Duplicate field', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
      }else{
          prin_inc_opt_field_form.add({
            bodyPadding : '5px 10px 5px 10px',
            fieldLabel  :this.selected_optional_raw,
            xtype       :'combo',
            store       :Ext.create("OMM.store.batchupload.TempTableStore"),
            name        :this.selected_optional_val,
            editable    :false,
            valueField  :'field_name',
            displayField:'field_name',
            closable    :true
          }); 
      }
    },

    onUpdateSelectionChange:function(value, paging, params){
      if(window.console||window.console.firebug){
        console.clear()
      }

      var principal_update                = this.lookupReference('principal_update');
      var principal_update_selection_form = this.lookupReference('principal_update_selection_form');
      var principal_update_field_form     = this.lookupReference('principal_update_field_form');
      var unique_key_opt                  = this.lookupReference('unique_key_opt');
      var update_selection                = this.lookupReference('update_selection');
      
      this.selected_update_val    = value.getValue();
      this.selected_update_raw    = value.getRawValue();
      console.log('selectedValue: '+this.selected_update_val);
      
      if(unique_key_opt.getValue()=='empid'&&this.selected_update_val=='empid'){
        Ext.MessageBox.show({title:'WARNING',msg:'Cannot update employee number if Unique key is employee number',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}})
      }else if(unique_key_opt.getValue()=='id_number'&&this.selected_update_val=='id_number'){
        Ext.MessageBox.show({title:'WARNING',msg:'Cannot update HMO ID Number if Unique key is HMO ID Number',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}})
      }else if(unique_key_opt.getValue()!='empid'&&unique_key_opt.getValue()!='al_id'&&unique_key_opt.getValue()!='id_number'&&this.selected_update_val=='fname'||unique_key_opt.getValue()!='empid'&&unique_key_opt.getValue()!='id_number'&&unique_key_opt.getValue()!='al_id'&&this.selected_update_val=='lname'){
        Ext.MessageBox.show({title:'WARNING',msg:'Cannot update Last, First Name if Unique key contains Last and First Name',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}})
      }else if(unique_key_opt.getValue()=='fn_ln_db'&&this.selected_update_val=='birthdate'){
        Ext.MessageBox.show({title:'WARNING',msg:'Cannot update Last, First Name and Birthdate if Unique key is Last, First Name and Birthdate',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}})
      }else{
        if(principal_update_field_form.getForm().findField('al_id')&&this.selected_update_val=='al_id'||principal_update_field_form.getForm().findField('branch_id')&&this.selected_update_val=='branch_id'||principal_update_field_form.getForm().findField('plan_id')&&this.selected_update_val=='plan_id'||principal_update_field_form.getForm().findField('mbr_status')&&this.selected_update_val=='mbr_status'||principal_update_field_form.getForm().findField('empid')&&this.selected_update_val=='empid'||principal_update_field_form.getForm().findField('id_number')&&this.selected_update_val=='id_number'||principal_update_field_form.getForm().findField('fname')&&this.selected_update_val=='fname'||principal_update_field_form.getForm().findField('mname')&&this.selected_update_val=='mname'||principal_update_field_form.getForm().findField('lname')&&this.selected_update_val=='lname'||principal_update_field_form.getForm().findField('ename')&&this.selected_update_val=='ename'||principal_update_field_form.getForm().findField('birthdate')&&this.selected_update_val=='birthdate'||principal_update_field_form.getForm().findField('gender')&&this.selected_update_val=='gender'||principal_update_field_form.getForm().findField('civil_status')&&this.selected_update_val=='civil_status'||principal_update_field_form.getForm().findField('philhealth')&&this.selected_update_val=='philhealth'||principal_update_field_form.getForm().findField('sss_no')&&this.selected_update_val=='sss_no'||principal_update_field_form.getForm().findField('pag_ibig_no')&&this.selected_update_val=='pag_ibig_no'||principal_update_field_form.getForm().findField('unified_id_no')&&this.selected_update_val=='unified_id_no'||principal_update_field_form.getForm().findField('tin')&&this.selected_update_val=='tin'||principal_update_field_form.getForm().findField('designation')&&this.selected_update_val=='designation'||principal_update_field_form.getForm().findField('emp_hire_date')&&this.selected_update_val=='emp_hire_date'||principal_update_field_form.getForm().findField('hospital_hmo')&&this.selected_update_val=='hmo_prov_details'||principal_update_field_form.getForm().findField('orig_effectivity_date')&&this.selected_update_val=='hmo_dates'){
          Ext.MessageBox.show({title:'WARNING',msg:'Cannot duplicate field',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}})
        }else if(this.selected_update_val=='mbr_status'){
          // Ext.MessageBox.show({title:'INFO', msg:'Suggest to convert first Member status field', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}, fn:'onCvrtMemStat', scope:this});
          principal_update_field_form.add({defaults:{anchor:'95%',labelWidth:200,editable:false,emptyText:'Select column'},defaultType:'combo',xtype:'fieldset',reference:'fs_mbrstat',title:'Member Status',collapsible:true,bodyPadding:5,items:[{fieldLabel:'Member Status',name:'mbr_status',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},{xtype:'button',text:'Convert Column Values to Member status Identifier',ui:'soft-green',listeners:{el:{click:'onCvrtMemStat'}}}]})
        }else if(this.selected_update_val=='gender'){
          // Ext.MessageBox.show({title:'INFO',msg:'Suggest to convert first Gender field',icon:Ext.Msg.INFO,closable:false,buttonText:{ok:'OK'},fn:'onCvrtGender',scope:this});
          principal_update_field_form.add({
            defaults    :{anchor:'95%',labelWidth:200,editable:false,emptyText:'Select column'},
            defaultType :'combo',
            xtype       :'fieldset',
            title       :'Gender',
            reference   :'fs_gender',
            collapsible :true,
            bodyPadding :5,
            items       :[
              {fieldLabel:'Gender',name:'gender',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
              {xtype:'button',text:'Convert Column Values to Gender Identifier',ui:'soft-green',listeners:{el:{click:'onCvrtGender'}}}
            ]
          });
        }else if(this.selected_update_val=='id_number'){
          principal_update_field_form.add({
            defaults      :{anchor:'95%',labelWidth:200,editable:false,allowBlank:false,emptyText:'Select column'},
            defaultType   :'combo',
            xtype         :'fieldset',
            title         :'HMO DATES',
            collapsible   :true,
            bodyPadding   :5,
            items         :[
              {
                xtype:'panel', bodyPadding :'5 0 10 0',
                items:[
                  {xtype:'label',style:'color:#e44959;font-weight:bold;font-face:arial;margin-top: 5px;margin-bottom: 10px;',text:'NOTE : Required fields under HMO ID'}
                  // {
                  //   xtype:'checkbox',
                  //   boxLabel:'Get original hmo_id_release_date',
                  //   name:'release_date_orig',
                  //   anchor:'100%',
                  //   checked:false,
                  //   inputValue:'1',
                  //   uncheckedValue:'0', 
                  //   margin:'10 0 0 0',
                  //   listeners :{
                  //     change :function(){
                  //       alert('check!')
                  //     }
                  //   }

                  // }
                ]
              },
              // {fieldLabel:'Original Effective Date',name:'orig_effectivity_date', allowBlank:true,store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
              {fieldLabel:'HMO ID Number',name:'id_number', allowBlank:false,store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
              {fieldLabel:'HMO ID Release Date',name:'hmo_id_release_date', allowBlank:false,store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'}
            ]
          });
        }else if(this.selected_update_val=='civil_status'){
          // Ext.MessageBox.show({title:'INFO',msg:'Suggest to convert first Civil Status field',icon:Ext.Msg.INFO,closable:false,buttonText:{ok:'OK'},fn:'onCvrtCivil_stat',scope:this});
          principal_update_field_form.add({
            defaults    :{anchor:'95%',labelWidth:200,editable:false,emptyText:'Select column'},
            defaultType :'combo',
            xtype       :'fieldset',
            title       :'Civil Status',
            reference   :'fs_civilstat',
            collapsible :true,
            bodyPadding :5,
            items       :[
              {fieldLabel:'Civil status',name:'civil_status',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
              {xtype:'button',text:'Convert Column Values to Civil Status Identifier',ui:'soft-green',listeners:{el:{click:'onCvrtCivil_stat'}}}
            ]
          });
        }else if(this.selected_update_val=='branch_id'){
          // Ext.MessageBox.show({title:'INFO',msg:'Suggest to convert first Branch field', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}, fn:'onPrinCvrtBranch', scope:this});
          principal_update_field_form.add({
            defaults     :{anchor:'95%',labelWidth:200,editable:false,emptyText:'Select column'},
            defaultType  :'combo',
            xtype        :'fieldset',
            reference    :'fs_branch',
            title        :'BRANCH ID',
            collapsible  :true,
            bodyPadding  :5,
            items        :[
              {fieldLabel:'Branch ID',name:'branch_id',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name',reference:'branch_id'},
              {xtype:'button',text:'Convert Column Values to Branch Identifier',ui:'soft-green',listeners:{el:{click:'onPrinCvrtBranch'}}}
            ]
          });
        }else if(this.selected_update_val=='plan_id'){
          // Ext.MessageBox.show({title:'INFO',msg:'Suggest to convert first Plan ID field', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}, fn:'onPrinCvrtPlan', scope:this});
          principal_update_field_form.add({
            defaults      :{anchor:'95%',labelWidth:200,editable:false,emptyText:'Select column'},
            defaultType   :'combo',
            xtype         :'fieldset',
            reference     :'fs_planid',
            title         :'PLAN ID/ PLAN LEVEL',
            collapsible   :true,
            bodyPadding   :5,
            items         :[
              {fieldLabel:'Plan Level',name:'plan_id',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
              {xtype:'button',text:'Convert Column Values to Plan Identifier',ui:'soft-green',listeners:{el:{click:'onPrinCvrtPlan'}}}
            ]
          });
        }else if(this.selected_update_val=='hmo_prov_details'){
          principal_update_field_form.add({
            defaults      :{anchor:'95%',labelWidth:200,editable:false,emptyText:'Select column'},
            defaultType   :'combo',
            xtype         :'fieldset',
            title         :'HMO PROVIDER DETAILS',
            collapsible   :true,
            bodyPadding   :5,
            items         :[
              {fieldLabel:'HMO Hospital Provider',name:'hospital_hmo',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
              {fieldLabel:'HMO Dental Provider',name:'dental_hmo',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'}
            ]
          });
        }else if(this.selected_update_val=='hmo_dates'){
          principal_update_field_form.add({
            defaults      :{anchor:'95%',labelWidth:200,editable:false,allowBlank:false,emptyText:'Select column'},
            defaultType   :'combo',
            xtype         :'fieldset',
            title         :'HMO DATES',
            collapsible   :true,
            bodyPadding   :5,
            items         :[
              {
                xtype:'panel', bodyPadding :'5 0 10 0',
                items:[
                 {xtype:'label',style:'color:#e44959;font-weight:bold;font-face:arial;margin-top: 5px;margin-bottom: 10px;',text:'NOTE : Required fields under HMO DATES'}
                ]
              },
              // {fieldLabel:'Original Effective Date',name:'orig_effectivity_date', allowBlank:true,store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
              {fieldLabel:'Current Effective Date',name:'curr_effectivity_date', allowBlank:false,store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
              {fieldLabel:'Maturity Date',name:'maturity_date', allowBlank:false,store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'}
            ]
          });
        }else{
          principal_update_field_form.add({
            bodyPadding   :'5px 10px 5px 10px',
            fieldLabel    :this.selected_update_raw,
            xtype         :'combo',
            store         :Ext.create("OMM.store.batchupload.TempTableStore"),
            name          :this.selected_update_val,
            editable      :false,
            valueField    :'field_name',
            displayField  :'field_name',
            closable      :true
          });
        }
      }
    },

    onFieldSelectNext:function(){
      var layout                  = this.lookupReference('principal_update_tab');
      var prinUpdate_form         = this.lookupReference('principal_update_field_form');
      var prinUpdate_unique_form  = this.lookupReference('principal_update_unique_form');
      var form                    = prinUpdate_form.getForm();
      var form_unique             = prinUpdate_unique_form.getForm();
      var l                       = layout.getLayout();
      var TempData                = this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata');
      var me                      = this;
      var id                      = 1;

      if(form_unique.isValid()){
        form.getFields().each(function(field){
          if(field.rawValue==''||field.rawValue==null){
            console.log(field.fieldLabel)
          }else{
            console.log(field.fieldLabel)
          }
        });

        Ext.Msg.confirm('PRINCIPAL UPDATE','Are you sure you want to update member',function(answer){
          if(answer=='yes'){
            if(form.isValid()){
              localStorage.removeItem("hmo_id_release_date");
              form.getFields().each(function(field){
                if(field.rawValue==''||field.rawValue==null || field.rawValue=='Blank_Field'){
                  console.log("NULL field :"+field.fieldLabel);
                  field.destroy();
                }else if(field.name=='hmo_id_release_date'){
                  var id_release_date = field.rawValue;
                  localStorage.setItem("hmo_id_release_date",Ext.encode(id_release_date));
                  field.destroy();
                }
              });

              var win=new Ext.Window({
                closeAction :'hide',
                width       :'70%',
                modal       :true,
                maximized   :true,
                layout      :'vbox',
                autoShow    :true,
                autoScroll  :true
              });
           
                // var rowIndex = TempData.indexOfId(id);
                // TempData.load();
                // TempData.on('load',function(store,record,successful,eOpts){
                //   TempData.each(function(record){
                //     var id  = record.get('id');
                //   });
                // });

                  TempData.load({
                  callback: function(records, operation, success) {
                    var total_count   = records.length
                    console.log(records.length);
                    me.prin_update_AjaxRequest_getId(id, total_count)
                  }
                  });
                }else{
                  Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check required fields',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
                }
              }
            });
            }else{
              Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check Unique key Form or contact IT for assistant',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                  console.log(l.getActiveItem());
                  layout.setActiveItem(0)
                }
    },
    //END FUNCTION DEPENDENT UPDATE

    prin_update_AjaxRequest_getId:function(id, total_count){
      var win = Ext.WindowManager.getActive();
      var layout                  = this.lookupReference('principal_update_tab');
      var prinUpdate_res_f        = this.lookupReference('principal_update_result');
      var l                       = layout.getLayout();
      var me = this;
      console.log('id'+id+' total'+total_count)
      if(total_count>=id){
      win.add({
      xtype:'label',
      html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px">Record:'+id
      });
        me.prin_update_AjaxRequest_ProcessId(id,total_count)
      }else{
        Ext.MessageBox.show({title:'PRINCIPAL UPDATE', msg:'Process finished. Please check details posted in Window',icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
        localStorage.removeItem("hmo_id_release_date");
        console.log(l.getActiveItem());
        layout.down('#principal_update_result').setDisabled(false);
        layout.setActiveItem(2);
        layout.down('#PU_step1').setDisabled(true);
        layout.down('#PU_step2').setDisabled(true);
        prinUpdate_res_f.add({xtype:'principal_failed_panel'});        
      }

    },

    prin_update_AjaxRequest_ProcessId:function(id,total_count){
      var prinUpdate_unique_form  = this.lookupReference('principal_update_unique_form');
      var form_unique             = prinUpdate_unique_form.getForm();
      var form_unique_field       = this.lookupReference('unique_key_opt');
      var prinUpdate_form         = this.lookupReference('principal_update_field_form');
      var form                    =  prinUpdate_form.getForm();
      var acc_id                  = this.lookupReference('acc_id');
      var session_userid          = this.lookupReference('session_userid');
      var filesignature           = this.lookupReference('filesignature');
      var hmo_id_release_date     = localStorage.getItem("hmo_id_release_date");
      var obj                     = JSON.parse(hmo_id_release_date);
      var me                      = this;

      var sParams = '';
      if(obj=='' || obj==null){
        sParams ={acc_id:acc_id.getValue(),session_userid:session_userid.getValue(),filesignature:filesignature.getValue(),id:id,key_opt:form_unique_field.getValue(),keys:Ext.encode(form_unique.getFieldValues()),fields:Ext.encode(form.getFieldValues())};
      }else{
        sParams ={hmo_id_release_date:obj,acc_id:acc_id.getValue(),session_userid:session_userid.getValue(),filesignature:filesignature.getValue(),id:id,key_opt:form_unique_field.getValue(),keys:Ext.encode(form_unique.getFieldValues()),fields:Ext.encode(form.getFieldValues())};
      }

      Ext.Ajax.request({
        url     :'../data/prin_update_upload.php',
        method      :'POST',
        params      :sParams,
        submitEmptyText :false,
        success         :function(response){
          var response  = Ext.decode(response.responseText),
              result    = response.data[0].result;
          console.log(id);console.log(result);
          me.prin_update_AjaxRequest_CallSuccess(id, total_count, result);
        },failure       :function(response){
          var result    = Ext.decode(response.responseText);
          console.log(result);
          me.prin_update_AjaxRequest_CallSuccess(id, total_count, result);
          console.log('failed!');
        }
      });    
    // form.submit({url:'../data/prin_update_upload.php',method:'POST',params:sParams,submitEmptyText:false,success:function(form,action){var success=action.result.success var result=action.result.data[0].result console.log(id);console.log(result);me.prin_update_AjaxRequest_CallSuccess(id,total_count,result)},failure:function(form,action){var result=action.result.data[0].result console.log(result);me.prin_update_AjaxRequest_CallSuccess(id,total_count,result)console.log('failed')}});
    },

    prin_update_AjaxRequest_CallSuccess:function(id,total_count,result){
      var win = Ext.WindowManager.getActive();
      var me  = this;

      win.add({
        xtype :'label',
        html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top: 5px;margin-left:5px;">'+result

      });
     me.prin_update_AjaxRequest_getId(id + 1, total_count)
    },

    // PRINCIPAL DELETION
    onPrinDeletion_menu:function(){
      console.log(window.console);
      if(window.console||window.console.firebug){console.clear()}
      Ext.create('OMM.view.batchupload.Principal_deletion').show();
    },

    onUniqueKey_select:function(value, paging, params){
      if(window.console||window.console.firebug){console.clear()}

      var deletion_field_form      = this.lookupReference('deletion_field_form');
      this.selected_val            = value.getValue();
      this.selected_raw            = value.getRawValue();

      if(this.selected_val=='empid_fn_ln'){
        deletion_field_form.removeAll();
        deletion_field_form.add({fieldLabel:'Employee number', name:'empid'},{fieldLabel:'First Name', name:'fname'},{fieldLabel:'Last Name', name:'lname'})
      }else{
        deletion_field_form.removeAll();
        deletion_field_form.add({fieldLabel:this.selected_raw,name:this.selected_val});
      }
    },

    onDel_reset:function(){
      var deletion_field_form     = this.lookupReference('deletion_field_form');
      var form                    = deletion_field_form.getForm();
      form.reset();
      deletion_field_form.removeAll();
    },
    
    onPrinDel_del:function(button, field){
      var me                                = this;
      var id                                = 1;
      var deletion_field_form               = this.lookupReference('deletion_field_form');
      var form                              = deletion_field_form.getForm();
      var TempData                          = this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata');
      // console.log(Ext.encode(form.getFieldValues()+form2.findField('lname').getFormSubmit();));

      if(form.isValid()){
        Ext.Msg.confirm('PRINCIPAL DELETION','Are you sure you want to delete member',function(answer){
          if(answer=='yes'){
            var win=new Ext.Window({
              closeAction :'hide',
              width       :'70%',
              modal       :true,
              maximized   :true,
              layout      :'vbox',
              autoShow    :true,
              autoScroll  :true
            });

            TempData.load({
            callback: function(records, operation, success) {
              var total_count   = records.length
              console.log(records.length);
              me.prin_deletion_AjaxRequest_getId(id, total_count)
            }
            });
          }
        });
      }else{
        Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check required fields or contact IT for assistant',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}})
      }
    },

    prin_deletion_AjaxRequest_getId:function(id,total_count){
      var win                  = Ext.WindowManager.getActive();
      var layout               = this.lookupReference('principal_deletion_tab');
      var l                    = layout.getLayout();
      var prin_deletion_result = this.lookupReference('principal_deletion_result');
      var me                   = this;

      console.log('id'+id+' total'+total_count);
      if(total_count>=id){
        win.add({
        xtype:'label',
        html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px">Record:'+id
        });
        me.prin_deletion_AjaxRequest_ProcessId(id,total_count)
      }else{
        Ext.MessageBox.show({title:'PRINCIPAL DELETION', msg:'Process finished. Please check details posted in Window',icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
        console.log(l.getActiveItem());
        layout.down('#principal_deletion_result').setDisabled(false);
        layout.setActiveItem(1);
        // layout.down('#principal_inclusion_field_form').setDisabled(true);
        // layout.down('#PU_step2').setDisabled(true);
        prin_deletion_result.add({xtype:'principal_failed_panel'});        
      }    
    },

    prin_deletion_AjaxRequest_ProcessId:function(id, total_count){
      var unique_key_opt          = this.lookupReference('unique_key_opt');
      var deletion_field_form     = this.lookupReference('deletion_field_form');
      var form                    = deletion_field_form.getForm();
      var acc_id                  = this.lookupReference('acc_id');
      var session_userid          = this.lookupReference('session_userid');
      var filesignature           = this.lookupReference('filesignature');
      var me                      = this;

      Ext.Ajax.request({
        url             :'../data/prin_del_upload.php',
        method          :'POST',
        params          :{id:id,acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),session_userid:session_userid.getValue(),key_opt:unique_key_opt.getValue(),keys:Ext.encode(form.getFieldValues())},
        submitEmptyText :false,
        success         :function(response){
          var response  = Ext.decode(response.responseText),
              result    = response.data[0].result;
          console.log(id);
          console.log(result);
          me.prin_deletion_AjaxRequest_CallSuccess(id, total_count, result)
        },failure       :function(response){
          var response = Ext.decode(response.responseText),
              result   = response.data[0].result;
              console.log(result);
              me.prin_deletion_AjaxRequest_CallSuccess(id, total_count, result);
        }
      });
    // form.submit({url:'../data/prin_del_upload.php',method:'POST',params:{id:id,acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),session_userid:session_userid.getValue(),key_opt:unique_key_opt.getValue(),keys:Ext.encode(form.getFieldValues())},submitEmptyText:false,success:function(form,action){var success=action.result.success var result=action.result.data[0].result console.log(id);console.log(result);me.prin_deletion_AjaxRequest_CallSuccess(id,total_count,result)},failure:function(form,action){var result=action.result.data[0].result console.log(result);me.prin_deletion_AjaxRequest_CallSuccess(id,total_count,result)console.log('failed')}});
    },

    prin_deletion_AjaxRequest_CallSuccess:function(id, total_count, result){
      var win = Ext.WindowManager.getActive();
      var me  = this;

      win.add({
        xtype :'label',
        html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top: 5px;margin-left:5px;">'+result
      });
     me.prin_deletion_AjaxRequest_getId(id + 1, total_count)
    },

    // END PRINCIPAL DELETION

    //PRINCIPAL HMO CARD ID RELEASE DATE
    onPrinUpdateHMOID_menu:function(){
      if(window.console||window.console.firebug){
        console.clear();
      }

      Ext.create('OMM.view.batchupload.Principal_hmoid_update').show();
    },

    onHMOIDUniqueKeyNext:function(){
      var layout                      = this.lookupReference('principal_hmoid_update_tab'),
          step1                       = this.lookupReference('PHMO_step1'),
          form                        = step1.getForm(),
          l                           = layout.getLayout(),
          PHMO_step2                  = this.lookupReference('PHMO_step2'),
          prinUpdateHmo_form          = this.lookupReference('principal_hmoid_update_field_form'),
          prinUpdateHmo_unique_form   = this.lookupReference('principal_hmoid_update_unique_form'),
          form                        = prinUpdateHmo_form.getForm(),
          form_unique                 = prinUpdateHmo_unique_form.getForm(),
          TempData                    = this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
          PHMO_step1                  = this.lookupReference('PHMO_step1'),
          unique_key_opt              = this.lookupReference('unique_key_opt'),
          id                          = 1,
          me                          = this;

      // if(form_unique.isValid()){
        console.log(unique_key_opt.getValue())
      if(unique_key_opt.getValue()==null || unique_key_opt.getValue()==''){
        Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please select unique key or check Unique Key Form or contact IT for assistant',closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK;'}})
        console.log(l.getActiveItem());layout.setActiveItem(0);        
      }else{
      if(form_unique.isValid()){
        if(form.isValid()){
          Ext.Msg.confirm('SLA UPDATE - HMO ID NUMBER RELEASE DATE', 'Are you sure you want to update member', function(answer){
            if(answer=='yes'){
              var win = new Ext.Window({
                closeAction   :'hide',
                width         :'70%',
                modal         :true,
                maximized     :true,
                layout        :'vbox',
                autoShow      :true,
                autoScroll    :true
              });

              TempData.load({
              callback :function(records, operation, success){
                var total_count = records.length;
                console.log(records.length);
                me.prin_update_hmoid_AjaxRequest_getId(id, total_count);
              }
              });
            }
          });
        }else{
          Ext.MessageBox.show({title:'WARNING' ,msg:'Something went wrong please check required fields', closable:false, buttonText:{ok:'OK'}});
        }
      }
      }
    },

    prin_update_hmoid_AjaxRequest_getId:function(id, total_count){
      var win         = Ext.WindowManager.getActive(),
          layout      = this.lookupReference('principal_hmoid_update_tab'),
          PHMO_step2  = this.lookupReference('PHMO_step2'),
          l           = layout.getLayout(),
          me          = this;

          console.log(id);
          if(total_count>=id){
            win.add({
              xtype   :'label',
              html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px">Record:'+id
            });
            me.prin_update_hmoid_AjaxRequest_ProcessId(id,total_count);
          }else{
            Ext.MessageBox.show({title:'SLA UPDATE - HMO ID RELEASE DATE', msg:'Process finished. Please check details posted in Window',icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
            console.log(l.getActiveItem());
            layout.down('#PHMO_step2').setDisabled(true);
            layout.setActiveItem(1);
            layout.down('#PHMO_step1').setDisabled(true);
            PHMO_step2.add({xtype:'principal_failed_panel'});
          }
    },
    
    prin_update_hmoid_AjaxRequest_ProcessId:function(id, total_count){
      var prinUpdateHmo_unique_form    = this.lookupReference('principal_hmoid_update_unique_form'),
          form_unique                  = prinUpdateHmo_unique_form.getForm(),
          form_unique_field            = this.lookupReference('unique_key_opt')
          prinUpdateHmo_form           = this.lookupReference('principal_hmoid_update_field_form'),
          form                         = prinUpdateHmo_form.getForm(),
          acc_id                       = this.lookupReference('acc_id'),
          session_userid               = this.lookupReference('session_userid'),
          filesignature                = this.lookupReference('filesignature'),
          update_action                ='data_correction',
          me                           = this;
      
      var sParams = '';
      sParams ={acc_id:acc_id.getValue(),ops:session_userid.getValue(),filesignature:filesignature.getValue(),id:id,key_opt:form_unique_field.getValue(),action:update_action,keys:Ext.encode(form_unique.getFieldValues()),fields:Ext.encode(form.getFieldValues())};

      Ext.Ajax.request({
        url             :'../data/sla_id_releasedate_updates.php',
        method          :'POST',
        params          :sParams,
        submitEmptyText :false,
        success         :function(response){
          var response  = Ext.decode(response.responseText),
              result    = response.data[0].result;
          // var success     = action.result.success,
          //     result      = action.result.data[0]. result;
          console.log(id);
          console.lgo(result);
          me.prin_update_hmoid_AjaxRequest_CallSuccess(id, total_count, result)
        }, failure:function(response){
          // var result  = action.result.data[0].result;
          var result    = response.data[0].result;
          console.log(result);
          me.prin_update_hmoid_AjaxRequest_CallSuccess(id, total_count, result);
          console.log('failed');
        }
      });
    },
    prin_update_hmoid_AjaxRequest_CallSuccess:function(id, total_count, result){
      var win = Ext.WindowManager.getActive(),
          me  = this;

      win.add({
        xtype  :'label',
        html   :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top: 5px;margin-left:5px;">'+result
      });
      me.prin_update_hmoid_AjaxRequest_getId(id + 1, total_count);
    },

    //END PRINCIPAL HMO CARD ID RELEASE DATE
    onUpdateHmoUniqueKey:function(value, paging,params){
      var unique_key_opt                     = this.lookupReference('unique_key_opt'),
          principal_hmoid_update_unique_form = this.lookupReference('principal_hmoid_update_unique_form'),
          principal_hmoid_update_field_form  = this.lookupReference('principal_hmoid_update_field_form');

          this.selected_update_val           = value.getValue();
          this.selected_update_raw           = value.getRawValue();

      if(this.selected_update_val=='empid'){
        principal_hmoid_update_unique_form.removeAll();
        // principal_hmoid_update_field_form.removeAll();
        principal_hmoid_update_unique_form.add({fieldLabel:this.selected_update_raw, name:this.selected_update_val});
      }else if(this.selected_update_val=='fn_ln_db'){
        principal_hmoid_update_unique_form.removeAll();
        // principal_hmoid_update_field_form.removeAll();
        principal_hmoid_update_unique_form.add({fieldLabel:'First Name', name:'fname'},{fieldLabel:'Last Name', name:'lname'},{fieldLabel:'Birthdate', name:'birthdate'});
      }
    },


    //PRINCIPAL ENDORSE DATE UPDATE
    onPrinUpdateEndorsedate_menu:function(){
      if(window.console||window.console.firebug){
        console.clear();
      }

      Ext.create('OMM.view.batchupload.Principal_endorsedate_update').show();
    },

    onEndodateFieldSelectReset:function(){
      var PEDU_step2                               = this.lookupReference('PEDU_step2');
      var principal_endorsedate_update_field_form  = this.lookupReference('principal_endorsedate_update_field_form');
      var form                                     = PEDU_step2.getForm();

      form.reset();
      principal_endorsedate_update_field_form.removeAll();
    },

    onUpdateEndoDateUniqueKey:function(value,paging,params){ 
      if(window.console||window.console.firebug){
        console.clear();
      }

      var unique_key_opt                = this.lookupReference('unique_key_opt'),
          principal_endorsedate_update_unique_form = this.lookupReference('principal_endorsedate_update_unique_form');

      this.selected_update_val                = value.getValue();
      this.selected_update_raw                = value.getRawValue();
      console.log('Unique key :'+ this.selected_update_val);

      if(this.selected_update_val=='empid'){
        principal_endorsedate_update_unique_form.removeAll();
        principal_endorsedate_update_unique_form.add({fieldLabel:this.selected_update_raw,name  :this.selected_update_val});
      }else if(this.selected_update_val=='fn_ln_db'){
        principal_endorsedate_update_unique_form.removeAll();
        principal_endorsedate_update_unique_form.add({fieldLabel:'First Name', name:'fname'},{fieldLabel:'Last Name', name:'lname'},{fieldLabel:'Birthdate', name:'birthdate'});
      }
    },

    onEndorseDateUniqueKeyNext:function(){
      var me    = this.lookupReference('principal_endorsedate_update_tab'),
          step1 = this.lookupReference('PEDU_step1'),
          form  = step1.getForm(),
          l     = me.getLayout();

      console.log(l.getActiveItem());
      me.down('#PEDU_step2').setDisabled(false);
      l.setActiveItem(1);
    },

    onUpdateEndoDateActionClick:function(value, paging,params){

    },

    onUpdateEndoDateActionChange:function(value, paging, params){
      if(window.console||window.console.firebug){
      console.clear()
      }

      var principal_endorsedate_update_field_form     = this.lookupReference('principal_endorsedate_update_field_form');
      var update_selection                            = this.lookupReference('update_selection');

      this.sel_updateAction_val = value.getValue();

      console.log('selectedValue :' +this.sel_updateAction_val);

      if(value.getRawValue()=='new'){
        principal_endorsedate_update_field_form.removeAll();
        principal_endorsedate_update_field_form.add({
          bodyPadding   :'5px 10px 5px 10px',
          fieldLabel    :'HR Endorsed Date',
          name          :'hr_endorsed_date'
        },{
          bodyPadding   :'5px 10px 5px 10px',
          fieldLabel    :'OPS Endorsed Date',
          name          :'ops_endorsed_date'
        });
      }else{
        principal_endorsedate_update_field_form.removeAll();

      }
    },

    onUpdateEndoDateSelectionClick:function(value, paging, params){
        var unique_key_opt           = this.lookupReference('unique_key_opt');
        if(unique_key_opt.getValue()=='' || unique_key_opt.getValue() == null){Ext.MessageBox.show({title:'WARNING', msg:'Select Unique key first',icon:Ext.Msg.WARNING,closable:false, buttonText:{ok:'OK'}});}
    },

    onUpdateEndoDateSelectionChange:function(value, paging, params){
      if(window.console||window.console.firebug){
        console.clear()
      }

      var principal_endorsedate_update_field_form     = this.lookupReference('principal_endorsedate_update_field_form');
      var update_selection                = this.lookupReference('update_selection');
      
      this.selected_update_val    = value.getValue();
      this.selected_update_raw    = value.getRawValue();
      console.log('selectedValue: '+this.selected_update_val);
      
      if(principal_endorsedate_update_field_form.getForm().findField('hr_endorsed_date')&&this.selected_update_val=='hr_endorsed_date'||principal_endorsedate_update_field_form.getForm().findField('ops_endorsed_date')&&this.selected_update_val=='ops_endorsed_date'){
        Ext.MessageBox.show({title:'WARNING',msg:'Cannot duplicate field',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}})
      }else{
        principal_endorsedate_update_field_form.add({
          bodyPadding   :'5px 10px 5px 10px',
          fieldLabel    :this.selected_update_raw,
          xtype         :'combo',
          store         :Ext.create("OMM.store.batchupload.TempTableStore"),
          name          :this.selected_update_val,
          editable      :false,
          valueField    :'field_name',
          displayField  :'field_name',
          closable      :true
        });
      }     
    },


    onEndoDateFieldSelectNext:function(){
      var layout                         = this.lookupReference('principal_endorsedate_update_tab'),
          prinUpdateEndoDate_form        = this.lookupReference('principal_endorsedate_update_field_form'),
          prinUpdateEndoDate_unique_form = this.lookupReference('principal_endorsedate_update_unique_form'),
          form                           = prinUpdateEndoDate_form.getForm(),
          form_unique                    = prinUpdateEndoDate_unique_form.getForm(),
          l                              = layout.getLayout(),
          TempData                       = this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
          me                             = this,
          id                             = 1;
          PEDU_step2                     = this.lookupReference('PEDU_step2');


          if(form_unique.isValid()){
            form.getFields().each(function(field){
              if(field.rawValue==''||field.rawValue==null){
                console.log(field.fieldLabel)
              }
            });

            if(PEDU_step2.isValid()){
              Ext.Msg.confirm('SLA Update - Endorse Date','Are you sure you want to update member',function(answer){
                if(answer=='yes'){
                  if(form.isValid()){
                    form.getFields().each(function(field){
                      if(field.rawValue==''||field.rawValue==null){
                        console.log("NULL field :"+field.fieldLabel);
                        field.destroy();
                      }
                    });
                    var win = new Ext.Window({
                      closeAction  :'hide',
                      width        :'70%',
                      modal        :true,
                      maximized    :true,
                      layout       :'vbox',
                      autoShow     :true,
                      autoScroll   :true
                    });

                    TempData.load({
                      callback:function(records, operations,success){
                        var total_count = records.length
                        console.log(records.length);
                        me.prin_update_endodate_AjaxRequest_getId(id, total_count);
                      }
                    });
                  }else{
                    Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check required fields', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                  }
                }
              });
            }else{
              Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please fill up required fields', icon:Ext.msg.WARNING, closable:false, buttonText:{ok:'Ok'}});
            }
          }else{
            Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please check unique key form or contact IT for assistant', icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
            console.log(l.getActiveItem());
            layout.setActiveItem(0)
          }

    },

    prin_update_endodate_AjaxRequest_getId:function(id, total_count){
      var win        = Ext.WindowManager.getActive(),
          layout     = this.lookupReference('principal_endorsedate_update_tab'),
          PEDU_step3 = this.lookupReference('PEDU_step3'),
          l          = layout.getLayout(),
          me         = this;

      console.log('id'+id+' total '+total_count);
      if(total_count>=id){
        win.add({
          xtype   :'label',
          html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px">Record:'+id
        });
        me.prin_update_endodate_AjaxRequest_ProcessId(id, total_count);
      }else{
          Ext.MessageBox.show({title:'SLA Update - Endorse Date', msg:'Process finished. Please check details posted in Window',icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
          localStorage.removeItem("hmo_id_release_date");
          console.log(l.getActiveItem());
          layout.down('#PEDU_step3').setDisabled(false);
          layout.setActiveItem(2);
          layout.down('#PEDU_step1').setDisabled(true);
          layout.down('#PEDU_step2').setDisabled(true);
          PEDU_step3.add({xtype:'principal_failed_panel'}); 
      }
    },

    prin_update_endodate_AjaxRequest_ProcessId:function(id, total_count){
      var prinUpdate_endodate_unique_form   = this.lookupReference('principal_endorsedate_update_unique_form'),
          form_unique                       = prinUpdate_endodate_unique_form.getForm(),
          form_unique_field                 = this.lookupReference('unique_key_opt'),
          prinUpdate_form                   = this.lookupReference('principal_endorsedate_update_field_form'),
          form                              = prinUpdate_form.getForm(),
          acc_id                            = this.lookupReference('acc_id'),
          session_userid                    = this.lookupReference('session_userid'),
          filesignature                     = this.lookupReference('filesignature'),
          update_action                     = this.lookupReference('update_action'),
          me                                = this;

      var sParams = '';
      sParams ={acc_id:acc_id.getValue(),ops:session_userid.getValue(),filesignature:filesignature.getValue(),id:id,key_opt:form_unique_field.getValue(),action:update_action.getValue(),keys:Ext.encode(form_unique.getFieldValues()),fields:Ext.encode(form.getFieldValues())};

      Ext.Ajax.request({
        url             :'../data/sla_endorsement_updates.php',
        method          :'POST',
        params          :sParams,
        submitEmptyText :false,
        success         :function(response){
          var response  = Ext.decode(response.responseText),
              result    = response.data[0].result;
          // var success     = action.result.success,
          //     result      = action.result.data[0]. result;
          console.log(id);
          console.log(result);
          me.prin_update_endodate_AjaxRequest_CallSuccess(id, total_count, result)
        }, failure:function(response){
          // var result  = action.result.data[0].result;
          var result    = response.data[0].result;
          console.log(result);
          me.prin_update_endodate_AjaxRequest_CallSuccess(id, total_count, result);
          console.log('failed');
        }
      });
      // form.submit({});
    },

    prin_update_endodate_AjaxRequest_CallSuccess:function(id, total_count, result){
      var win = Ext.WindowManager.getActive(),
          me  = this;

      win.add({
        xtype :'label',
        html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top: 5px;margin-left:5px;">'+result
      });
     me.prin_update_endodate_AjaxRequest_getId(id + 1, total_count)
    },

    //END OF PRINICPAL ENDORSE DATE UPDATE

    //PRINCIPAL HMO CARD UPDATE
    onPrinUpdateCard_menu:function(){
      if(window.console||window.console.firebug){
        console.clear();
      }

      Ext.create('OMM.view.batchupload.Principal_card_update').show();

    },


    onUpdateCardUniqueKey:function(value,paging,params){
      if(window.console||window.console.firebug){
        console.clear();
      }

      var unique_key_opt                      = this.lookupReference('unique_key_opt'),
          principal_card_update_unique_form   = this.lookupReference('principal_card_update_unique_form'),
          principal_card_update_field_form    = this.lookupReference('principal_card_update_field_form');

      this.selected_update_val                = value.getValue();
      this.selected_update_raw                = value.getRawValue();
      console.log('Unique key :'+ this.selected_update_val);

      if(this.selected_update_val=='empid'){
        principal_card_update_unique_form.removeAll();
        // principal_card_update_field_form.removeAll();
        principal_card_update_unique_form.add({fieldLabel:this.selected_update_raw,name  :this.selected_update_val});
      }else if(this.selected_update_val=='fn_ln_db'){
        principal_card_update_unique_form.removeAll();
        // principal_card_update_field_form.removeAll();
        principal_card_update_unique_form.add({fieldLabel:'First Name', name:'fname'},{fieldLabel:'Last Name', name:'lname'},{fieldLabel:'Birthdate', name:'birthdate'});
      }

    },

    onUpdateCardActionBeforeselect:function(combo, record, index, e){
      console.log(record.data);
      if(record.data.action=='replacement'){
        return false;
      }
    },

    onUpdateCardSelectionClick:function(value, paging, params){
        var unique_key_opt           = this.lookupReference('unique_key_opt');
        if(unique_key_opt.getValue()=='' || unique_key_opt.getValue() == null){Ext.MessageBox.show({title:'WARNING', msg:'Select Unique key first',icon:Ext.Msg.WARNING,closable:false, buttonText:{ok:'OK'}});}
    },

    onUpdateCardSelectionChange:function(value, paging, params){
      if(window.console||window.console.firebug){
        console.clear()
      }

      var principal_card_update_field_form     = this.lookupReference('principal_card_update_field_form');
      var update_selection                = this.lookupReference('update_selection');
      
      this.selected_update_val    = value.getValue();
      this.selected_update_raw    = value.getRawValue();
      console.log('selectedValue: '+this.selected_update_val);
      
      if(principal_card_update_field_form.getForm().findField('phys_id_recv_date')&&this.selected_update_val=='phys_id_recv_date'||principal_card_update_field_form.getForm().findField('phys_id_transmittal_date')&&this.selected_update_val=='phys_id_transmittal_date'){
        Ext.MessageBox.show({title:'WARNING',msg:'Cannot duplicate field',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}})
      }else{
        principal_card_update_field_form.add({
          bodyPadding   :'5px 10px 5px 10px',
          fieldLabel    :this.selected_update_raw,
          xtype         :'combo',
          store         :Ext.create("OMM.store.batchupload.TempTableStore"),
          name          :this.selected_update_val,
          editable      :false,
          valueField    :'field_name',
          displayField  :'field_name',
          closable      :true
        });
      }
     
    },
    
    onCardFieldSelectReset:function(){
      var PCU_step2                         = this.lookupReference('PCU_step2');
      var principal_card_update_field_form  = this.lookupReference('principal_card_update_field_form');
      var form                              = PCU_step2.getForm();

      form.reset();
      principal_card_update_field_form.removeAll();
    },

    onTest:function(){
      var update_action              = this.lookupReference('update_action');
      
      console.log(update_action);
      console.log(update_action.getValue());

    },


    // onCardUniqueKeyNext:function(){ 
    //   var me    = this.lookupReference('principal_card_update_tab');
    //   var step1 = this.lookupReference('PCU_step1');
    //   var form  = step1.getForm();
    //   var l     = me.getLayout();

    //   console.log(l.getActiveItem());
    //   me.down('#PCU_step2').setDisabled(false);
    //   l.setActiveItem(1);// form.reset();// me.down('#PU_step1').setDisabled(true);
    // },

    onCardFieldSelectNext:function(){
      var layout                     = this.lookupReference('principal_card_update_tab');
      var prinUpdateCard_form        = this.lookupReference('principal_card_update_field_form');
      var prinUpdateCard_unique_form = this.lookupReference('principal_card_update_unique_form');
      var form                       = prinUpdateCard_form.getForm();
      var form_unique                = prinUpdateCard_unique_form.getForm();
      var l                          = layout.getLayout();
      var TempData                   = this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata');
      var me                         = this;
      var id                         = 1;
      var PCU_step1                  = this.lookupReference('PCU_step1');

      if(form_unique.isValid()){
          form.getFields().each(function(field){
            if(field.rawValue==''||field.rawValue==null){
              console.log(field.fieldLabel)
            }else{
              console.log(field.fieldLabel)
            }
          });

          if(PCU_step1.isValid()){
            Ext.Msg.confirm('PRINCIPAL UPDATE','Are you sure you want to update member',function(answer){
              if(answer=='yes'){
                if(form.isValid()){
                  form.getFields().each(function(field){
                    if(field.rawValue==''||field.rawValue==null){
                      console.log("NULL field :"+field.fieldLabel);
                      field.destroy()
                    }
                  });

                  var win=new Ext.Window({
                    closeAction :'hide',
                    width       :'70%',
                    modal       :true,
                    maximized   :true,
                    layout      :'vbox',
                    autoShow    :true,
                    autoScroll  :true
                  });

                    TempData.load({
                    callback: function(records, operation, success) {
                      var total_count   = records.length
                      console.log(records.length);
                      me.prin_update_card_AjaxRequest_getId(id, total_count)
                    }
                    });
                    }else{
                      Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check required fields',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
                    }
                  }
                });
          }else{
            Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please fill up required fields.', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
          }
        }else{
              Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check Unique key Form or contact IT for assistant',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                  console.log(l.getActiveItem());
                  layout.setActiveItem(0)
                }
    },

    prin_update_card_AjaxRequest_getId:function(id, total_count){
        var win = Ext.WindowManager.getActive();
        var layout    = this.lookupReference('principal_card_update_tab');
        var PCU_step2 = this.lookupReference('PCU_step2');
        var l         = layout.getLayout();
        var me        = this;

        console.log('id'+id+' total'+total_count)
        if(total_count>=id){
          win.add({
            xtype:'label',
            html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px">Record:'+id
          });
          me.prin_update_card_AjaxRequest_ProcessId(id,total_count)
        }else{
          Ext.MessageBox.show({title:'PRINCIPAL UPDATE', msg:'Process finished. Please check details posted in Window',icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
          localStorage.removeItem("hmo_id_release_date");
          console.log(l.getActiveItem());
          layout.down('#PCU_step1').setDisabled(true);
          layout.down('#PCU_step2').setDisabled(false);
          layout.setActiveItem(1);
          PCU_step2.add({xtype:'principal_failed_panel'});
          // layout.down('#PCU_step1').setDisabled(true);
          // layout.down('#PCU_step2').setDisabled(true); 
        }
    },

    prin_update_card_AjaxRequest_ProcessId:function(id,total_count){
      var prinUpdate_card_unique_form  = this.lookupReference('principal_card_update_unique_form');
      var form_unique                  = prinUpdate_card_unique_form.getForm();
      var form_unique_field            = this.lookupReference('unique_key_opt');
      var prinUpdate_form              = this.lookupReference('principal_card_update_field_form');
      var form                         = prinUpdate_form.getForm(),
          values                       = form.getValues();
      var acc_id                       = this.lookupReference('acc_id');
      var session_userid               = this.lookupReference('session_userid');
      var filesignature                = this.lookupReference('filesignature');
      var update_action                = this.lookupReference('update_action');
      var me                           = this;

      var sParams = '';
          sParams ={ops:session_userid.getValue(),acc_id:acc_id.getValue(),card_released_date:values['card_released_date'],filesignature:filesignature.getValue(),keys:Ext.encode(form_unique.getFieldValues()),key_opt:form_unique_field.getValue(),id:id};

      Ext.Ajax.request({
        url             :'../data/sla_pid_released_batchupdate.php',
        //sla_phys_id_update
        method          :'POST',
        params          :sParams,
        submitEmptyText :false,
        success         :function(response){
          var response  = Ext.decode(response.responseText),
              result    = response.data[0].result;
          console.log(id);
          console.log(result);
          me.prin_update_card_AjaxRequest_CallSuccess(id,total_count,result)
        },
        failure:function(response){
          var response  = Ext.decode(response.responseText),
              result    = response.data[0].result;
          console.log(result);
          me.prin_update_card_AjaxRequest_CallSuccess(id,total_count,result)
          console.log('failed');
        }
      });      
      // form.submit({url:'../data/sla_phys_id_update.php',method:'POST',params:sParams,submitEmptyText:false,success:function(form,action){var success=action.result.success var result=action.result.data[0].result console.log(id);console.log(result);me.prin_update_card_AjaxRequest_CallSuccess(id,total_count,result)},failure:function(form,action){var result=action.result.data[0].result console.log(result);me.prin_update_card_AjaxRequest_CallSuccess(id,total_count,result)console.log('failed')}});
    },

    prin_update_card_AjaxRequest_CallSuccess:function(id,total_count,result){
      var win = Ext.WindowManager.getActive();
      var me  = this;

      win.add({
        xtype :'label',
        html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top: 5px;margin-left:5px;">'+result

      });
     me.prin_update_card_AjaxRequest_getId(id + 1, total_count)
    },
    //END PRINCPAL HMO CARD UPDATE


    //PRINCIPAL UPDATE ENDORSEMENT DATE

    onPrinUpdateEndorsement_menu:function(){
      if(window.console||window.console.firebug){
        console.clear();
      }
      Ext.create('OMM.view.batchupload.Principal_memendo_update').show();
    },

    onUpdateMemEndoUniqueKey:function(value,paging,params){ 
      if(window.console||window.console.firebug){
        console.clear();
      }

      var unique_key_opt                = this.lookupReference('unique_key_opt'),
          principal_memendo_update_unique_form = this.lookupReference('principal_memendo_update_unique_form');

      this.selected_update_val                = value.getValue();
      this.selected_update_raw                = value.getRawValue();
      console.log('Unique key :'+ this.selected_update_val);

      if(this.selected_update_val=='empid'){
        principal_memendo_update_unique_form.removeAll();
        principal_memendo_update_unique_form.add({fieldLabel:this.selected_update_raw,name  :this.selected_update_val});
      }else if(this.selected_update_val=='fn_ln_db'){
        principal_memendo_update_unique_form.removeAll();
        principal_memendo_update_unique_form.add({fieldLabel:'First Name', name:'fname'},{fieldLabel:'Last Name', name:'lname'},{fieldLabel:'Birthdate', name:'birthdate'});
      }
    },

   onEndorsementUniqueKeyNext:function(){
      var layout                          =this.lookupReference('principal_memendo_update_tab'),
          prinUpdate_memendo_form         =this.lookupReference('principal_memendo_update_field_form'),
          prinUpdate_memendo_unique_form  =this.lookupReference('principal_memendo_update_unique_form'),
          form                            =prinUpdate_memendo_form.getForm(),
          form_unique                     =prinUpdate_memendo_unique_form.getForm(),
          l                               =layout.getLayout(),
          TempData                        =this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
          PENDO_step1                     =this.lookupReference('PENDO_step1'),
          PENDO_step2                     =this.lookupReference('PENDO_step2'),
          me                              =this,
          id                              =1;

      console.log(PENDO_step2)
      if(form_unique.isValid()){
       if(PENDO_step1.isValid()){
        Ext.Msg.confirm('SLA MEMBER ENDORSEMENT','Are you sure you want to update member details',function(answer){
          if(answer=='yes'){
            if(form.isValid()){
              form.getFields().each(function(field){
                if(field.rawValue==''||field.rawValue==null){
                  console.log('NULL field :'|field.fieldLabel);
                  field.destroy();
                }
              });
              var win = new Ext.Window({
                closeAction   :'hide',
                width         :'70%',
                modal         :true,
                maximized     :true,
                layout        :'vbox',
                autoShow      :true
              });


                TempData.load({
                callback: function(records, operation, success) {
                  var total_count   = records.length
                  console.log(records.length);
                  me.prin_update_memendo_AjaxRequest_getId(id, total_count)
                }
                });
            }else{
              Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please fill up required fields',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
            }
          }
        });
       }else{
        Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please fill up required fields',closable:false, icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
       }
      }else{
        Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check unique key form or contact IT for assistant',closable:false, icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
      }

    },

    prin_update_memendo_AjaxRequest_getId:function(id,total_count){
      var win         =Ext.WindowManager.getActive(),
          layout      =this.lookupReference('principal_memendo_update_tab'),
          PENDO_step2 =this.lookupReference('PENDO_step2'),
          l           =layout.getLayout(),
          me          =this;


        console.log('id'+id+' total'+total_count);
        if(total_count>=id){
            win.add({
              xtype   :'label',
              html    :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px;">Record:'+id
            });
            me.prin_update_memendo_AjaxRequest_ProcessId(id,total_count);
          }else{
            Ext.MessageBox.show({title:'SLA MEMBER ENDORSEMENT',msg:'Process finished. Please check details posted in Window.',closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}});
            layout.down('#PENDO_step1').setDisabled(true);
            layout.down('#PENDO_step2').setDisabled(false);
            layout.setActiveItem(1);
            PENDO_step2.add({xtype:'principal_failed_panel'});
          }
    },

    prin_update_memendo_AjaxRequest_ProcessId:function(id,total_count){
      var prinUpdate_memendo_unique_form       =this.lookupReference('principal_memendo_update_unique_form'),
          form_unique                          =prinUpdate_memendo_unique_form.getForm(),
          form_unique_field                    =this.lookupReference('unique_key_opt'),
          prinUpdate_form                      =this.lookupReference('principal_memendo_update_field_form'),
          form                                 =prinUpdate_form.getForm(),
          values                               =form.getValues(),
          acc_id                                =this.lookupReference('acc_id'),
          session_userid                        =this.lookupReference('session_userid'),
          filesignature                         =this.lookupReference('filesignature'),
          me                                    =this;

      var sParams = '';
          sParams ={ops:session_userid.getValue(),acc_id:acc_id.getValue(),hr_endorsed_date:values['hr_endorsed_date'],ops_endorsed_date:values['ops_endorsed_date'],filesignature:filesignature.getValue(),keys:Ext.encode(form_unique.getFieldValues()),key_opt:form_unique_field.getValue(),id:id};

        Ext.Ajax.request({
          url             :'../data/sla_modify_endorsement.php',
          method          :'POST',
          params          :sParams,
          submitEmptyText :false,
          success         :function(response){
            var response  = Ext.decode(response.responseText),
                result    =response.data[0].result;
            console.log(id);
            console.log(response.data)
            console.log(result);
            me.prin_update_memendo_AjaxRequest_CallSuccess(id, total_count,result);
          },failure     :function(response){
            var response  = Ext.decode(response.responseText),
                result    =response.data[0].result;
            console.log(result);
            me.prin_update_memendo_AjaxRequest_CallSuccess(id, total_count, result);
            console.log('failed');
          }
        });
    },

    prin_update_memendo_AjaxRequest_CallSuccess:function(id, total_count, result){
      var win   = Ext.WindowManager.getActive(),
          me    = this;

      win.add({
        xtype   :'label',
        html    :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top:5px;margin-left:5px;">'+result
      });
      me.prin_update_memendo_AjaxRequest_getId(id + 1, total_count);

    },

    //END FUNCTION

    //PRINCIPAL UPDATE PHYSICAL ID TRANSMITTAL DATE    
    onPrinUpdateCardTransmittal_menu:function(){
      if(window.console||window.console.firebug){
        console.clear();
      }

      Ext.create('OMM.view.batchupload.Principal_card_transmittal_update').show();
    },

    onUpdateCardTransmitUniqueKey:function(value, paging, params){
      if(window.console||window.console.firebug){
        console.clear();
      }

      var unique_key_opt                                =this.lookupReference('unique_key_opt'),
          principal_card_transmittal_update_unique_form =this.lookupReference('principal_card_transmittal_update_unique_form');

      this.selected_update_val                   =value.getValue();
      this.selected_update_raw                   =value.getRawValue();
      console.log('Unique key :'+this.selected_update_val);

      if(this.selected_update_val=='empid'){
        principal_card_transmittal_update_unique_form.removeAll();
        principal_card_transmittal_update_unique_form.add({fieldLabel:this.selected_update_raw,name:this.selected_update_val});
      }else if(this.selected_update_val=='fn_ln_db'){
        principal_card_transmittal_update_unique_form.removeAll();
        principal_card_transmittal_update_unique_form.add({fieldLabel:'First Name',name:'fname'},{fieldLabel:'Last Name', name:'lname'},{fieldLabel:'Birthdate',name:'birthdate'});
      }
    },  

    onCardTransmittalFieldSelectNext:function(){
      var layout                              =this.lookupReference('principal_card_transmittal_update_tab'),
          prinUpdateCardTransmit_form         =this.lookupReference('principal_card_transmittal_update_field_form'),
          prinUpdateCardTransmit_unique_form  =this.lookupReference('principal_card_transmittal_update_unique_form'),
          form                                =prinUpdateCardTransmit_form.getForm(),
          form_unique                         =prinUpdateCardTransmit_unique_form.getForm(),
          l                                   =layout.getLayout(),
          TempData                            =this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
          PCTU_step1                          =this.lookupReference('PCTU_step1'),
          me                                  =this,
          id                                  =1;

      if(form_unique.isValid()){
        if(PCTU_step1.isValid()){
          Ext.Msg.confirm('SLA PHYSICAL ID TRANSMITTAL UPDATE','Are you sure you want to update member details',function(answer){
            if(answer=='yes'){
              if(form.isValid()){
                form.getFields().each(function(field){
                  if(field.rawValue==''||field.rawValue==null){
                    console.log('NULL field :'+field.fieldLabel);
                    field.destroy();
                  }
                });

                var win = new Ext.Window({
                  closeAction   :'hide',
                  width         :'70%',
                  modal         :true,
                  maximized     :true,
                  layout        :'vbox',
                  autoShow      :true
                });


                TempData.load({
                callback: function(records, operation, success) {
                  var total_count   = records.length
                  console.log(records.length);
                  me.prin_update_card_transmittal_AjaxRequest_getId(id, total_count)
                }
                });
              }else{
                Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please fill up required fields', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
              }
            }
          });
        }else{
          Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please fill up required fields', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
        }
      }else{
        Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check Unique key form or contact IT for assistant', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
      }
    },

    prin_update_card_transmittal_AjaxRequest_getId:function(id,total_count){
      var win         =Ext.WindowManager.getActive(),
          layout      =this.lookupReference('principal_card_transmittal_update_tab'),
          PCTU_step2  =this.lookupReference('PCTU_step2'),
          l           =layout.getLayout(),
          me          =this;

        console.log('id'+id+' total'+total_count);
        if(total_count>=id){
          win.add({
            xtype :'label',
            html  :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px;">Record:'+id
          });
          me.prin_update_card_transmittal_AjaxRequest_ProcessId(id,total_count);
        }else{
          Ext.MessageBox.show({title:'SLA PHYSICAL ID TRANSMITTAL UPDATE',msg:'Process finished. Please check details posted in Window',icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
          layout.down('#PCTU_step1').setDisabled(true);
          layout.down('#PCTU_step2').setDisabled(false);
          layout.setActiveItem(1);
          PCTU_step2.add({xtype:'principal_failed_panel'});
        }
    },


    prin_update_card_transmittal_AjaxRequest_ProcessId:function(id, total_count){
      var prinUpdate_card_trasmit_unique_form   =this.lookupReference('principal_card_transmittal_update_unique_form'),
          form_unique                           =prinUpdate_card_trasmit_unique_form.getForm(),
          form_unique_field                     =this.lookupReference('unique_key_opt'),
          prinUpdate_form                       =this.lookupReference('principal_card_transmittal_update_field_form'),
          form                                  =prinUpdate_form.getForm(),
          values                                =form.getValues(),
          acc_id                                =this.lookupReference('acc_id'),
          session_userid                        =this.lookupReference('session_userid'),
          filesignature                         =this.lookupReference('filesignature'),
          me                                    =this;

      var sParams = '';
          sParams ={ops:session_userid.getValue(),acc_id:acc_id.getValue(),card_transmittal_date:values['card_transmittal_date'],filesignature:filesignature.getValue(),keys:Ext.encode(form_unique.getFieldValues()),key_opt:form_unique_field.getValue(),id:id};

      Ext.Ajax.request({
        url             :'../data/sla_pid_transmittal_batchupdate.php',
        method          :'POST',
        params          :sParams,
        submitEmptyText :false,
        success         :function(response){
          var response  = Ext.decode(response.responseText),
              result    =response.data[0].result;
          console.log(id);
          console.log(result);
          me.prin_update_card_transmittal_AjaxRequest_CallSuccess(id, total_count,result);
        },failure     :function(response){
          var response  = Ext.decode(response.responseText),
              result    =response.data[0].result;
          console.log(result);
          me.prin_update_card_AjaxRequest_CallSuccess(id, total_count, result);
          console.log('failed');
        }
      });
    },

    prin_update_card_transmittal_AjaxRequest_CallSuccess:function(id, total_count, result){
      var win   = Ext.WindowManager.getActive(),
          me    = this;

      win.add({
        xtype   :'label',
        html    :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top:5px;margin-left:5px;">'+result
      });
      me.prin_update_card_transmittal_AjaxRequest_getId(id + 1, total_count);
    },
    //END PRINCIPAL UPDATE PHYSICAL ID TRANSMITTAL DATE

    //DEPENDENT INCLUSION      
    onDepInclusion_menu:function(){
      console.log(window.console);
      if(window.console||window.console.firebug){
        console.clear()
      }
      Ext.create('OMM.view.batchupload.Dependent_inclusion').show();

    },

    onDepInc_upload:function(){
      var dependen_inclusion             = this.lookupReference('Dependent_inclusion');
      var dependent_inclusion_field_form = this.lookupReference('dependent_inclusion_field_form');
      var form                           = dependent_inclusion_field_form.getForm();
      var dependent_inclusion_tab        = this.lookupReference('dependent_inclusion_tab');
      var l                              = dependent_inclusion_tab.getLayout();
      var dependent_inclusion_result     = this.lookupReference('dependent_inclusion_result');
      var TempData                       = this.TempData = Ext.create('OMM.store.batchupload.TempTableStore_loaddata');
      var me                             = this;
      var id                             = 1;


      if(form.isValid()){

        Ext.Msg.confirm('DEPENDENT INCLUSION','Are you sure you want to save dependent', function(answer){
          if(answer=='yes'){
              form.getFields().each(function(field){
                if(field.rawValue==''||field.rawValue==null){
                  console.log('NULL FIELD : '+field.fieldLabel);
                  field.destroy()
                }
              });

              var win = new Ext.Window({
                autoShow    :true,
                closeAction :'hide',
                width       :'70%',
                modal       :true,
                maximized   :true,
                layout      :'vbox',
                autoScroll  :true
              });

              TempData.load({
                callback:function(records, operation, success){
                  var total_count = records.length
                  console.log(records.length);
                  me.dep_inclusion_AjaxRequest_getId(id, total_count)
                }
              });
          }
        });
      }else{
        Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please check required fields', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
      }

  },
    //END DEPENDENT INCLUSION

    dep_inclusion_AjaxRequest_getId:function(id, total_count){
      var win         = Ext.WindowManager.getActive();
      var layout      = this.lookupReference('dependent_inclusion_tab');
      var dep_inc_res = this.lookupReference('dependent_inclusion_result');
      var l           = layout.getLayout();
      var me          = this; 

      console.log('id'+id+' total'+total_count);
      if(total_count>=id){
        win.add({
          xtype:'label',
          html:'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px">Record:'+id
        });
        me.dep_inclusion_AjaxRequest_ProcessId(id, total_count);
      }else{
        Ext.MessageBox.show({title:'DEPENDENT INCLUSION',msg:'Process finished. Please check details posted in Window', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
        console.log(l.getActiveItem());
        l.setActiveItem(1);
        dep_inc_res.add({xtype:'principal_failed_panel'});
      }
    },

    dep_inclusion_AjaxRequest_ProcessId:function(id, total_count){
      var acc_id                         = this.lookupReference('acc_id');
      var filesignature                  = this.lookupReference('filesignature');
      var session_userid                 = this.lookupReference('session_userid');
      var dependent_inclusion_field_form = this.lookupReference('dependent_inclusion_field_form');
      var form                           = dependent_inclusion_field_form.getForm();
      var me                             = this;

      Ext.Ajax.request({
        url             :'../data/dep_inc_upload.php',
        submitEmptyText :false,
        params          :{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),session_userid:session_userid.getValue(),fields:Ext.encode(form.getFieldValues())},
        success         :function(response){
          var response  = Ext.decode(response.responseText),
              result    = response.data[0].result;
          console.log(id);
          console.log(result);
          me.dep_inclusion_AjaxRequest_CallSuccess(id, total_count, result);
        }, failure      :function(response){
          var response  = Ext.decode(response.responseText),
              result    = response.data[0].result;
          console.log(result);
          me.dep_inclusion_AjaxRequest_CallSuccess(id, total_count, form);
        }
      });

      // result.submit({url:'../data/dep_inc_upload.php',submitEmptyText:false,params:{acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),session_userid:session_userid.getValue(),fields:Ext.encode(form.getFieldValues())},success:function(form,action){var success=action.result.success,result=action.result.data[0].result;console.log(id);console.log(result);me.dep_inclusion_AjaxRequest_CallSuccess(id,total_count,result)},failure:function(form,action){Ext.MessageBox.show({title:'WARNING',msg:action.result.message,closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}})}});
    },

    dep_inclusion_AjaxRequest_CallSuccess:function(id, total_count, result){
      var win = Ext.WindowManager.getActive();
      var me  = this;

      win.add({
        xtype :'label',
        html  :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top:5px;margin-left:5px;">'+result
      });
      me.dep_inclusion_AjaxRequest_getId(id+1, total_count);
    },

    //DEPENDENT UPDATE
    onDepUpdate_menu:function(){
      console.log(window.console);
      if(window.console||window.console.firebug){
        console.clear()
      }
      Ext.create('OMM.view.batchupload.Dependent_update').show();
    },

    onDepUpdateUniquekey:function(value, paging, params){
      if(window.console||window.console.firebug){
        console.clear();
      }

      var dependent_update_field_form   = this.lookupReference('dependent_update_field_form');
      var dependent_update_unique_form  = this.lookupReference('dependent_update_unique_form');
      var DU_step2                      = this.lookupReference('DU_step2');

      this.selected_update_val = value.getValue();
      this.selected_update_raw = value.getRawValue();

      console.log('Unique key : '+ this.selected_update_val);
      if(this.selected_update_val=='hmo_id'){
        dependent_update_unique_form.removeAll();
        dependent_update_field_form.removeAll();
        DU_step2.getForm().reset();
        dependent_update_unique_form.add({fieldLabel:this.selected_update_raw, name :this.selected_update_val});
      }else if(this.selected_update_val=='al_id'){
        dependent_update_unique_form.removeAll();
        dependent_update_field_form.removeAll();
        DU_step2.getForm().reset();
        dependent_update_unique_form.add({fieldLabel:this.selected_update_raw, name :this.selected_update_val});
        // dependent_update_unique_form.add({fieldLabel:'First Name', name:'fname'},{fieldLabel:'Last Name', name:'lname'},{fieldLabel:'Birthdate', name:'birthdate'});
      }else if(this.selected_update_val=='empid_fn_ln'){
        dependent_update_unique_form.removeAll();
        dependent_update_field_form.removeAll();
        DU_step2.getForm().reset();
        dependent_update_unique_form.add({fieldLabel:'Principal Employee ID number', name:'empid'},{fieldLabel:'First Name', name:'fname'},{fieldLabel:'Last Name', name:'lname'});
      }else if(this.selected_update_val="empid_fn_ln_bd"){
        dependent_update_unique_form.removeAll();
        dependent_update_field_form.removeAll();
        DU_step2.getForm().reset();
        dependent_update_unique_form.add({fieldLabel:'Principal Employee ID number', name:'empid'},{fieldLabel:'First Name', name:'fname'},{fieldLabel:'Last Name', name:'lname'},{fieldLabel:'Birthdate', name:'bdate'});
      }
    },

    onDepUpdateUniquekeynext:function(){ 
      var me    = this.lookupReference('dependent_update_tab');
      var step1 = this.lookupReference('DU_step1');
      var form  = step1.getForm();
      var l     = me.getLayout();

      console.log(l.getActiveItem());
      me.down('#DU_step2').setDisabled(false);
      l.setActiveItem(1);// form.reset();// me.down('#PU_step1').setDisabled(true);
    },
    
    onDepUpdateUniquekeyReset:function(){
      var DU_step1                      = this.lookupReference('DU_step1');
      var dependent_update_unique_form  = this.lookupReference('dependent_update_unique_form');

      DU_step1.getForm().reset();
      dependent_update_unique_form.removeAll();
    },

    onDepUpdateFieldSelectReset:function(){
      var DU_step2                      = this.lookupReference('DU_step2');
      var dependent_update_field_form   = this.lookupReference('dependent_update_field_form');

      DU_step2.getForm().reset();
      dependent_update_field_form.removeAll();
    },

    onDepUpdateSelectionClick:function(value, paging, params){
      var unique_key_opt      = this.lookupReference('unique_key_opt');
      if(unique_key_opt.getValue()=='' || unique_key_opt.getValue()==null){Ext.MessageBox.show({title:'WARNING', msg:'Select Unique key first', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}})}
    },

    onDepUpdateSelectionChange:function(value, pagin, params){
      if(window.console || window.console.firebug){
        console.clear();
      }

      var dependent_update                 = this.lookupReference('dependent_update');
      var dependent_update_selection_form  = this.lookupReference('dependent_update_selection_form');
      var dependent_update_field_form      = this.lookupReference('dependent_update_field_form');
      var unique_key_opt                   = this.lookupReference('unique_key_opt');
      var update_selection                 = this.lookupReference('update_selection');

      this.selected_update_val=value.getValue();
      this.selected_update_raw=value.getRawValue();
      console.log('selectedValue: '+this.selected_update_val);

      if(unique_key_opt.getValue()=='hmo_id' && this.selected_update_val=='hmo_id'){
        Ext.MessageBox.show({title:'WARNING', msg:'Cannot update HMO ID Number is Unique key is HMO ID Number', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
      }else if(unique_key_opt.getValue()!='hmo_id'&&unique_key_opt.getValue()!='al_id'&&this.selected_update_val=='fname'||unique_key_opt.getValue()!='hmo_id'&&unique_key_opt.getValue()!='al_id'&&this.selected_update_val=='lname'){
        Ext.MessageBox.show({title:'WARNING', msg:'Cannot Update Last, First Name if Unique key contains Last and First Name', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
      }else if(unique_key_opt.getValue()=='empid_fn_ln'&&this.selected_update_val=='empid'){
        Ext.MessageBox.show({title:'WARNING', msg:'Cannot update Principal Employee ID number if Unique key contains Principal Employee ID Number', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
      }else if(unique_key_opt.getValue()=='empid_fn_ln_bd'&&this.selected_update_val=='empid'){
        Ext.MessageBox.show({title:'WARNING', msg:'Cannot update Principal Employee ID number if Unique key contains Principal Employee ID Number', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
      }else if(unique_key_opt.getValue()=='empid_fn_ln_bd'&&this.selected_update_val=='bdate') {
        Ext.MessageBox.show({title:'WARNING', msg:'Cannot update Birthdate if Unique key contains Birthdate', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});        
      // }else if(unique_key_opt.getValue()=='empid_fn_ln'&&this.selected_update_val=='bdate'){
      //   Ext.MessageBox.show({title:'WARNING', msg:'Cannot update Birthdate if Unique key contains Birthdate', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
      }else{
        // if(dependent_update_field_form.getForm().findField('dep_status')&&this.selected_update_val=='hmo_id'){
        //   Ext.MessageBox.show({title:'WARNING', msg:'Cannot Update HMO ID if Dependent Status is selected', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
        // }else if(dependent_update_field_form.getForm().findField('hmo_id')&&this.selected_update_val=='dep_status'){
        //   Ext.MessageBox.show({title:'WARNING', msg:'Dependent Status automatically change to Active when HMO ID is selected/ updated', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
        // }else

        if(dependent_update_field_form.getForm().findField('empid')&&this.selected_update_val=='empid'||dependent_update_field_form.getForm().findField('hmo_id')&&this.selected_update_val=='hmo_id'||dependent_update_field_form.getForm().findField('lname')&&this.selected_update_val=='lname'||dependent_update_field_form.getForm().findField('fname')&&this.selected_update_val=='fname'||dependent_update_field_form.getForm().findField('mname')&&this.selected_update_val=='mname'||dependent_update_field_form.getForm().findField('ename')&&this.selected_update_val=='ename'||dependent_update_field_form.getForm().findField('bdate')&&this.selected_update_val=='bdate'||dependent_update_field_form.getForm().findField('gender')&&this.selected_update_val=='gender'||dependent_update_field_form.getForm().findField('civil_status')&&this.selected_update_val=='civil_status'||dependent_update_field_form.getForm().findField('relationship')&&this.selected_update_val=='relationship'||dependent_update_field_form.getForm().findField('d_type')&&this.selected_update_val=='d_type'||dependent_update_field_form.getForm().findField('hospital_hmo')&&this.selected_update_val=='hmo_prov_details'||dependent_update_field_form.getForm().findField('orig_effectivity_date')&&this.selected_update_val=='hmo_dates'||dependent_update_field_form.getForm().findField('plan_id')&&this.selected_update_val=='plan_id'||dependent_update_field_form.getForm().findField('dep_status')&&this.selected_update_val=='dep_status'){
          Ext.MessageBox.show({title:'WARNING', msg:'Cannot duplicate field', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
        }else if(this.selected_update_val=='gender'){
            // Ext.MessageBox.show({title:'INFO', msg:'Suggest to convert first Gender field', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}, fn:'onCvrtGender', scope:this});
            dependent_update_field_form.add({
              defaults    :{anchor:'95%', labelWidth:200, editable:false, emptyText:'Select column'},
              defaultType :'combo',
              xtype       :'fieldset',
              reference   :'fs_gender',
              title       :'Gender',
              collapsible :true,
              bodyPadding :5,
              items       :[
                {fieldLabel:'Gender', name:'gender',store:Ext.create("OMM.store.batchupload.TempTableStore"), displayField:'field_name', valueField:'field_name'},
                {xtype:'button', text:'Convert Column Values to Gender Identifier', ui:'soft-green', listeners:{el:{click:'onCvrtGender'}}}                
              ]
            });
          }else if(this.selected_update_val=='civil_status'){
            // Ext.MessageBox.show({title:'INFO', msg:'Suggest to Convert first Civil Status field', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}, fn:'onCvrtCivil_stat', scope:this});
            dependent_update_field_form.add({
              defaults    :{anchor:'95%', labelWidth:200, editable:false, emptyText:'Select column'},
              defaultType :'combo',
              xtype       :'fieldset',
              reference   :'fs_civilstat',
              title       :'Civil Status',
              collapsible :true,
              bodyPadding :5,
              items       :[
                {fieldLabel:'Civil status', name:'civil_status', store:Ext.create("OMM.store.batchupload.TempTableStore"), displayField:'field_name', valueField:'field_name'},
                {xtype:'button', text:'Convert Column Values to Civil Status Identifier', ui:'soft-green', listeners:{el:{click:'onCvrtCivil_stat'}}}
              ]
            });
          }else if(this.selected_update_val=='dep_status'){
            // Ext.MessageBox.show({title:'INFO',msg:'Suggets to Convert first Dependent Status field', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}, fn:'onDepCvrtStatus', scope:this});
            dependent_update_field_form.add({
              defaults    :{anchor:'95%', labelWidth:200, editable:false, emptyText:'Select column'},
              defaultType :'combo',
              xtype       :'fieldset',
              reference   :'fs_dep_stat',
              title       :'DEPENDENT ENROLLMENT STATUS',
              collapsible :true,
              bodyPadding :5, 
              items       :[
                {fieldLabel:'Dependent Enrollment Status',name:'dep_status',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
                {xtype:'button',text:'Convert Column Values to Dependent Status', ui:'soft-green', listeners:{el:{click:'onDepCvrtStatus'}}},
                {
                  xtype:'panel', bodyPadding :5,
                  items:[
                  {xtype:'label', html:'<font-face:arial;font-size:13px;><br>NOTE : As you select Dependent Enrollment Status the record/s will be update regardless if you select HMO ID Number</font><p style="color:red;font-face:arial;font-style:oblique;font-size:13px;">The system will only accept dependents code eg: 1(active), 3(for endorsement). You may use Convert function if the user doesnt have code provided or just dependent details only.'}
                  // {xtype:'label', style:'color:red;font-face:arial;font-style:oblique;font-size:13px;', text:'NOTE : As you select Dependent Enrollment Status the record/s will be update regardless if you select HMO ID Number'}
                  ]
                }
              ]
            });
        }
        else if(this.selected_update_val=='plan_id'){
          // Ext.MessageBox.show({title:'INFO', msg:'Suugest to convert first Plan ID field', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'},fn:'onPrinCvrtPlan', scope:this});
            dependent_update_field_form.add({
              defaults    :{anchor:'95%', labelWidth:200, editable:false, emptyText:'Select column'},
              defaultType :'combo',
              xtype       :'fieldset',
              reference   :'fs_planid',
              title       :'PLAN ID/ PLAN LEVEL',
              collapsible :true,
              bodyPadding :5, 
              items       :[
                {fieldLabel:'Plan Level',name:'plan_id',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
                {xtype:'button',text:'Convert Column Values to Plan Identifier', ui:'soft-green', listeners:{el:{click:'onPrinCvrtPlan'}}}               
              ]
            });
        }else if(this.selected_update_val=='hmo_id'){
          dependent_update_field_form.add({
            bodyPadding   :'5px 10px 5px 10px',
            fieldLabel    :this.selected_update_raw,
            name          :this.selected_update_val
          }
          // {bodyPadding:'5px 10px 5px 10px',fieldLabel:'Dependent Status',name:'dep_status',value:'1',hidden:true}
          );          
        }
        // else if(this.selected_update_val=='hmo_prov_details'){dependent_update_field_form.add({defaults:{anchor:'95%',labelWidth:200,editable:false,emptyText:'Select column',store:Ext.create('OMM.store.batchupload.TempTableStore'),displayField:'field_name',valueField:'field_name'},defaultType:'combo',xtype:'fieldset',title:'HMO PROVIDER DETAILS',collapsible:true,bodyPadding:5,items:[{fieldLabel:'HMO Hospital Provider',name:'hospital_hmo'},{fieldLabel:'HMO Dental Provider',name:'dental_hmo'}]})}else if(this.selected_update_val=='hmo_dates'){dependent_update_field_form.add({defaults:{anchor:'95%',labelWidth:200,editable:false,emptyText:'Select column',store:Ext.create('OMM.store.batchupload.TempTableStore'),displayField:'field_name',valueField:'field_name'},defaultType:'combo',xtype:'fieldset',title:'HMO DATES',collapsible:true,bodyPadding:5,items:[{fieldLabel:'Original Effective Date',name:'orig_effectivity_date'},{fieldLabel:'Current Effective Date',name:'curr_effectivity_date'},{fieldLabel:'Maturity Date',name:'maturity_date'}]})}
        else{
          dependent_update_field_form.add({
            bodyPadding   :'5px 10px 5px 10px',
            fieldLabel    :this.selected_update_raw,
            name          :this.selected_update_val
          });
        }
      }
    },

    onDepUpdateFieldSelectNext:function(){
        var dependent_update      = this.lookupReference('dependent_update');
        var depUpdate_form        = this.lookupReference('dependent_update_field_form');
        var depUpdate_unique_form = this.lookupReference('dependent_update_unique_form');
        var form                  = depUpdate_form.getForm();
        var form_unique           = depUpdate_unique_form.getForm();
        var form_unique_field     = this.lookupReference('unique_key_opt');
        var acc_id                = this.lookupReference('acc_id');
        var session_userid        = this.lookupReference('session_userid');
        var filesignature         = this.lookupReference('filesignature');
        var depUpdate_res_f       = this.lookupReference('dependent_update_result');
        var TempData              = this.TempData = Ext.create('OMM.store.batchupload.TempTableStore_loaddata');
        var me                    = this;
        var id                    = 1;

        if(form_unique.isValid()){
          form.getFields().each(function(field){
            if(field.rawValue=='' || field.rawValue==null){
              console.log("NULL field :"+field.fieldLabel);
              alert('NULL field value :'+field.fieldLabel);
            }else{
            // console.log(field.rawValue);
            }
          });
          Ext.Msg.confirm('DEPENDENT UPDATE','Are you sure you want to update dependent', function(answer){
            if(answer=='yes'){

              form.getFields().each(function(field){
                if(field.rawValue=='' || field.rawValue==null || field.rawValue=='Blank_Field'){
                  console.log("NULL field :"+field.fieldLabel);
                  field.destroy();
                }else{
                // console.log(field.rawValue);
                }
              });

              var win=new Ext.Window({
                closeAction :'hide',
                width       :'70%',
                modal       :true,
                maximized   :true,
                layout      :'vbox',
                autoShow    :true,
                autoScroll  :true
              });
              TempData.load({
              callback: function(records, operation, success) {
                var total_count   = records.length
                console.log(records.length);
                me.dep_update_AjaxRequest_getId(id, total_count)
              }
              });
            }
          });
        }else{
          Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please check Unique key Form or Contact IT for assistant', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
          console.log(l.getActiveItem());
          l.setActiveItem(0);
        }
      },

      dep_update_AjaxRequest_getId:function(id, total_count){

        var win = Ext.WindowManager.getActive();
        var layout                  = this.lookupReference('dependent_update_tab');
        var depUpdate_res_f         = this.lookupReference('dependent_update_result');
        var l                       = layout.getLayout();
        var me = this;
        console.log('id'+id+' total'+total_count)
        if(total_count>=id){
        win.add({
          xtype:'label',
          html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px">Record:'+id
        });
          me.dep_update_AjaxRequest_ProcessId(id,total_count)
        }else{
          Ext.MessageBox.show({title:'DEPENDENT UPDATE', msg:'Process finished. Please check details posted in Window',icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
          console.log(l.getActiveItem());
          layout.down('#dependent_update_result').setDisabled(false);
          l.setActiveItem(2);
          layout.down('#DU_step1').setDisabled(true);
          layout.down('#DU_step2').setDisabled(true);
          depUpdate_res_f.add({xtype:'principal_failed_panel'});
        }     
      },

      dep_update_AjaxRequest_ProcessId:function(id, total_count){
        var depUpdate_form        = this.lookupReference('dependent_update_field_form');
        var depUpdate_unique_form = this.lookupReference('dependent_update_unique_form');
        var form                  = depUpdate_form.getForm();
        var form_unique           = depUpdate_unique_form.getForm();
        var form_unique_field     = this.lookupReference('unique_key_opt');
        var acc_id                = this.lookupReference('acc_id');
        var session_userid        = this.lookupReference('session_userid');
        var filesignature         = this.lookupReference('filesignature');
        var me                    = this;

        Ext.Ajax.request({
          url             :'../data/dep_update_upload.php',
          method          :'POST',
          params          :{acc_id:acc_id.getValue(), session_userid:session_userid.getValue(), filesignature:filesignature.getValue(), id:id, key_opt:form_unique_field.getValue(), keys:Ext.encode(form_unique.getFieldValues()), fields:Ext.encode(form.getFieldValues())},
          submitEmptyText :false,
          success         :function(response){
            var response  = Ext.decode(response.responseText),
                result    = response.data[0].result;

            console.log(id);
            console.log(result);
            me.dep_update_AjaxRequest_CallSuccess(id, total_count, result);
          }
        });
        // form.submit({url:'../data/dep_update_upload.php',method:'POST',params:{acc_id:acc_id.getValue(),session_userid:session_userid.getValue(),filesignature:filesignature.getValue(),id:id,key_opt:form_unique_field.getValue(),keys:Ext.encode(form_unique.getFieldValues()),fields:Ext.encode(form.getFieldValues())},submitEmptyText:false,success:function(form,action){var success=action.result.success var result=action.result.data[0].result console.log(id);console.log(result);me.dep_update_AjaxRequest_CallSuccess(id,total_count,result)}});        
      },

      dep_update_AjaxRequest_CallSuccess:function(id, total_count, result){    
         var win  = Ext.WindowManager.getActive();
         var me   = this;

        win.add({
          xtype:'label',
          html:'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top:5px;margin-left:5px;">'+result
        });
        me.dep_update_AjaxRequest_getId(id + 1, total_count)
      },

      onDownloadError:function(){
        var acc_id  = this.lookupReference('acc_id');
        var filesignature = this.lookupReference('filesignature');
        var session_userid = this.lookupReference('session_userid');
        var waitMsg = Ext.MessageBox.wait('Processing your request. Please wait...');
        
        Ext.Ajax.request({
          url:'../data/download_error.php',                                            
          // wait        :true,
          // waitMsg     :'Processing your request',
          // waitConfig  :{duration:3000, increment:3, text:'Loading...', scope:this, fn:function(){Ext.MessageBox.hide();}},
          params:{acc_id:acc_id.getValue(), filesignature:filesignature.getValue(), session_userid:session_userid.getValue()},
          success:function(response){
            var response = Ext.decode(response.responseText);
            waitMsg.hide();
            Ext.MessageBox.show({title:'SUCCESS', msg:response.message, icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}}); 
          },failure:function(response){
            waitMsg.hide();
            Ext.MessageBox.show({title:'WARNING', msg:response.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
          }
        });
      },
    //END FUNCTION DEPENDENT UPDATE

    //DEPENDENT DELETION
    onDepDeletion_menu:function(){
      console.log(window.console);
      if(window.console||window.console.firebug){console.clear()}
      Ext.create('OMM.view.batchupload.Dependent_deletion').show();
    },

    onDepDel_del:function(){
      var me                    = this;
      var id                    = 1;
      var deletion_field_form   = this.lookupReference('deletion_field_form');
      var form                  = deletion_field_form.getForm();
      var TempData              = this.TempData=Ext.create('OMM.store.batchupload.TempTableStore_loaddata');
      // console.log(Ext.encode(form.getFieldValues()+form2.findField('lname').getFormSubmit();));

      if(form.isValid()){
        Ext.Msg.confirm('DEPENDENT DELETION','Are you sure you want to delete dependent',function(answer){
          if(answer=='yes'){
            var win=new Ext.Window({
              closeAction :'hide',
              width       :'70%',
              modal       :true,
              maximized   :true,
              layout      :'vbox',
              autoShow    :true,
              autoScroll  :true
            });

            TempData.load({
            callback: function(records, operation, success) {
              var total_count   = records.length
              console.log(records.length);
              me.del_deletion_AjaxRequest_getId(id, total_count)
            }
            });
          }
        });
      }else{
        Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please check required fields or contact IT for assistant',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}})
      }
    },

    del_deletion_AjaxRequest_getId:function(id, total_count){
      var win                  = Ext.WindowManager.getActive();
      var layout               = this.lookupReference('dependent_deletion_tab');
      var l                    = layout.getLayout();
      var dep_deletion_result = this.lookupReference('dependent_deletion_result');
      var me                   = this;

      console.log('id'+id+' total'+total_count);
      if(total_count>=id){
        win.add({
        xtype:'label',
        html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-left:5px;height:1px">Record:'+id
        });
        me.del_deletion_AjaxRequest_ProcessId(id,total_count)
      }else{
        Ext.MessageBox.show({title:'DEPENDENT DELETION', msg:'Process finished. Please check details posted in Window',icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
        console.log(l.getActiveItem());
        layout.down('#dependent_deletion_result').setDisabled(false);
        layout.setActiveItem(1);
        dep_deletion_result.add({xtype:'principal_failed_panel'});        
      }    
    },

    del_deletion_AjaxRequest_ProcessId:function(id, total_count){
      var unique_key_opt          = this.lookupReference('unique_key_opt');
      var deletion_field_form     = this.lookupReference('deletion_field_form');
      var form                    = deletion_field_form.getForm();
      var acc_id                  = this.lookupReference('acc_id');
      var session_userid          = this.lookupReference('session_userid');
      var filesignature           = this.lookupReference('filesignature');
      var me                      = this;

      Ext.Ajax.request({
        url             :'../data/dep_del_upload.php',
        method          :'POST',
        params          :{id:id,acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),session_userid:session_userid.getValue(), key_opt:unique_key_opt.getValue(), keys:Ext.encode(form.getFieldValues())},
        submitEmptyText :false,
        success         :function(response){
          var response = Ext.decode(response.responseText),
              result   = response.data[0].result;
          console.log(id);
          console.log(result);
          me.dep_deletion_AjaxRequest_CallSuccess(id, total_count, result);
        },failure       :function(response){
          var response  = Ext.decode(response.responseText),
              result    = response.data[0].result;
          console.log(result);
          me.dep_deletion_AjaxRequest_CallSuccess(id, total_count, result);
          console.log('failed');
        }       
      });

      // form.submit({url:'../data/dep_del_upload.php',method:'POST',params:{id:id,acc_id:acc_id.getValue(),filesignature:filesignature.getValue(),session_userid:session_userid.getValue(),key_opt:unique_key_opt.getValue(),keys:Ext.encode(form.getFieldValues())},submitEmptyText:false,success:function(form,action){var success=action.result.success var result=action.result.data[0].result console.log(id);console.log(result);me.dep_deletion_AjaxRequest_CallSuccess(id,total_count,result)},failure:function(form,action){var result=action.result.data[0].result console.log(result);me.dep_deletion_AjaxRequest_CallSuccess(id,total_count,result)console.log('failed')}});
    },

    dep_deletion_AjaxRequest_CallSuccess:function(id, total_count, result){      
      var win = Ext.WindowManager.getActive();
      var me  = this;

      win.add({
        xtype :'label',
        html :'<p style="color:#3AB09E;font-weight:bold;font-face:arial;margin-top: 5px;margin-left:5px;">'+result
      });
     me.del_deletion_AjaxRequest_getId(id + 1, total_count)
    
    },
    //END FUNCTION DEPENDENT DELETION

    //FAILED GRID BEFORERENDER FOR ALL BATCH PROCESS
    onPrin_failed_grid_beforerender:function(){
      var ColumnField            = this.ColumnField = Ext.create('OMM.store.batchupload.TempTableStore');
      var principal_failed_panel = this.lookupReference('principal_failed_panel');

      ColumnField.removeAll();
      console.log('TempTable Error Grid clear store');
      ColumnField.on('load', function(store, record, successful, eOpts){
        ColumnField.each(function(record){
          var field_name  = record.get('field_name');
          principal_failed_panel.down('#principal_failed_grid').headerCt.add({header:field_name, dataIndex:field_name,filter:'list', editor: {xtype: 'textfield'}});
        });
      });
      ColumnField.load();
    },

    onPrin_failed_grid_afterrender:function(record, index){
      var acc_id                = this.lookupReference('acc_id');
      var session_userid        = this.lookupReference('session_userid');
      var filesignature         = this.lookupReference('filesignature');
      var total                 = this.lookupReference('prin_total');
      var success               = this.lookupReference('prin_success');
      var failed                = this.lookupReference('prin_failed');
      var warning               = this.lookupReference('prin_warning');
      var principal_failed_grid = this.lookupReference('principal_failed_grid').getStore();

      Ext.Ajax.request({
        url   :'../data/batch_processed_summary.php',
        params:{acc_id:acc_id.getValue(),session_userid:session_userid.getValue(), filesignature:filesignature.getValue()},
        success:function(response){
          var response = Ext.decode(response.responseText);
          total.setValue(response.total_rows);
          success.setValue(response.success_rows);
          failed.setValue(response.failed_rows);
          warning.setValue(response.warning_rows);
          principal_failed_grid.load({params:{acc_id:acc_id.getValue(),session_userid:session_userid.getValue(),filesignature:filesignature.getValue()}});
        },failure:function(response){
          Ext.MessageBox.show({title:'WARNING', msg:'Cant show result', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}})
        }
      });

    },
    //END FUNCTION FAILED GRID

    // SAME ERROR FOR UPDATE
    onSamerecordError_SelectionChange:function(model, records){
      var rec = records[0];
      var frm_samerecorderror = this.lookupReference('frm_samerecorderror');
      if(rec){
        frm_samerecorderror.getForm().loadRecord(rec);
      }
    },

    onSamerecordError_save:function(button){
      var form    = this.lookupReference('frm_samerecorderror').getForm();

      Ext.Msg.confirm('Update Member','Are you sure you want to update members information',function(answer){
        if(answer=='yes'){
          if(form.isValid()){
            form.submit({
              url             :'../data/principal_update.php',
              waitMsg         :'Processing your request',
              timeout         :120000,
              submitEmptyText :false,
              success         :function(form, action){
                button.up('window').close();
                Ext.MessageBox.show({title:'SUCCESS', msg:'Principal details updated', icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
              },failure       :function(form, action){
                Ext.MessageBox.show({title:'WARNING', msg:action.result.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
              }
            });
          }else{
            Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please contact you IT for you assistant', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
          }
        }
      });
    },

    onFailedGridAfterEdit:function(editor, e){
  
      var me=this;
      var jsonData=Ext.encode(e.record.data);
      alert('update');
      console.log(jsonData);
      Ext.Ajax.request({
        url:'../data/prin_update_upload.php',
        params:{fields:jsonData},
        waitMsg:'Processing your request',
        success:function(response){
          var response = Ext.decode(response.responseText);
          Ext.MessageBox.show({title:'SUCCESS', msg:response.message, icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
        },failure:function(form, action){
          var response = Ext.decode(response.responseText);
          Ext.MessageBox.show({title:'WARNING', msg:response.message, icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
        }
      });
      return true
    }
    //END FUNCTION SAME ERROR RECORD
          
});