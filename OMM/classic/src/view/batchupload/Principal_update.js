var isMinimized = false;
var update_selection=Ext.create('Ext.data.Store',{fields:['field', "value"],
data:[
{"field":"BRANCH ID", "value":"branch_id"},
{"field":"PLAN ID/ PLAN LEVEL", "value":"plan_id"},
// {"field":"Member Status", "value":"mbr_status"},
{"field":"Employee ID", "value":"empid"},
// {"field":"ActiveLink ID", "value":"al_id"},
{"field":"HMO ID Number", "value":"id_number"},
{"field":"Last Name", "value":"lname"},
{"field":"First Name", "value":"fname"},
{"field":"Middle Name", "value":"mname"},
{"field":"Suffix Name", "value":"ename"},
{"field":"Date of Birth", "value":"birthdate"},
{"field":"Gender", "value":"gender"},
{"field":"Civil Status", "value":"civil_status"},
{"field":"PhilHealth", "value":"philhealth"},
{"field":"SSS Number", "value":"sss_no"},
{"field":"Pag-ibig Number", "value":"pag_ibig_no"},
{"field":"Unified ID Number", "value":"unified_id_no"},
{"field":"TIN Number", "value":"tin"},
{"field":"Designation", "value":"designation"},
{"field":"Employee Hire Date", "value":"emp_hire_date"},
// {"field":"HMO PROVIDER DETAILS", "value":"hmo_prov_details"},
{"field":"HMO DATES", "value":"hmo_dates"},
{"field":"HMO Provider", "value":"hospital_hmo"},
{"field":"Dental Provider","value":"dental_hmo"}

// {"field":"ID Release Date and Remarks", "value":"id_release_date"},
// {"field":"HR Correction Date and Remarks", "value":"hr_correction"},
// {"field":"HMO Correction Date and Remarks", "value":"hmo_correction"},
// {"field":"ID Correction Date and Remarks", "value":"id_correction"},
// {"field":"HR Plan Upgrade Endorsement Date and Remarks", "value":"hr_plan_upgrade"},
// {"field":"HMO Plan Upgrade Endorsement Date and Remarks", "value":"hmo_plan_upgrade"}

]});
var update_unique_key = Ext.create('Ext.data.Store',{
fields:["field", "value"],
data:[
{"field":"Employee ID *", "value":"empid"},
// {"field":"ActiveLink ID", "value":"al_id"},
// {"field":"HMO ID Number", "value":"id_number"},
{"field":"First, Last Name and Birthdate", "value":"fn_ln_db"}
]
});
Ext.define('OMM.view.batchupload.Principal_update',{
	extend 		:'Ext.window.Window',
	xtype 		:'principal_update',
	reference 	:'principal_update',

	requires 	 :[ 'OMM.view.batchupload.BatchUploadController','Ext.form.field.Tag','Ext.grid.plugin.CellEditing'],
  controller :'batchupload',

  cls        :'Principal_update-page-container',	
	initCenter  :true,
  autoShow    :true,
  autoDestroy :true,
  modal       :true,
  maximized   :true,
  height      :500,
  width       :'90%',
  title       :'Principal Update',
  titleAlign  :'center',
  closeAction :'destroy',

  tools       :[{type:'restore',hidden:true,handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.expand('',true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type:'minimize',handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.collapse();winWidth=window.getWidth();window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();isMinimized=true}}],

  layout   :'fit',
	items 		  :[
  {
    xtype       :'tabpanel',
    defaultType : 'form',
    layout      : 'card',
    reference   :'principal_update_tab',
    defaults    :{border:false,bodyPadding:10},
    items       :[
    {
      title     :'1 : Unique Key',reference:'PU_step1',itemId:'PU_step1',defaults    :{anchor:'95%',labelWidth:200, editable:false,queryMode   :'local',valueField  :'value',displayField:'field'},   
      items     :[
      {xtype:'displayfield',fieldLabel:'acc_id',text:'acc_id',reference:'acc_id', hidden:true},{xtype:'displayfield',fieldLabel:'user_id',text:'session_userid',reference:'session_userid',hidden:true},{xtype:'displayfield',fieldLabel:'filesignature',text:'filesignature',reference:'filesignature',hidden:true},
      {
        fieldLabel  :'<span style="color: rgb(255, 0, 0); padding-left: 10px; font-size:13px; font-weight:normal">Select Unique key *</span>',  
        xtype       :'combo',
        store       :update_unique_key,
        name        :'unique_key_opt',
        reference   :'unique_key_opt',
        emptyText   :'Select Unique field/s',
        listeners   :{select:'onUpdateUniquekey'}
      },
      {
        defaults    :{bodyPadding:'5px 10px 5px 10px',anchor:'95%', labelWidth:200, editable:false, allowBlank:false,emptyText:'Select column', store:Ext.create("OMM.store.batchupload.TempTableStore"),valueField:'field_name',displayField:'field_name'},
        defaultType :'combo',
        xtype       :'form',
        reference   :'principal_update_unique_form',
        itemId      :'principal_update_unique_form',
        bodyPadding :5,
        tbar        :{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',text:'Unique Key'}]},
        items       :[]
      },{xtype: 'tbspacer',flex: 1}
      ],
      listeners :{
        beforerender:'onBatch_response_render'
      },
      bbar      :{style:'background-color:#4dacec;',defaults:{margin:5,ui:'blue'},items:[{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;">Modify the appropriate column/field for the required column/fields</span>'},'->',{text:'Reset Fields',handler:function(button){button.up('form').getForm().reset()}},{text:'Next',ui:'blue',handler:'onUniqueKeyNext'}]}
    },
    {
      title     :'2 : Field Selection',disabled    :true,reference:'PU_step2',itemId:'PU_step2',overflowY:'scroll',defaults  :{bodyPadding : '5px 10px 5px 10px',anchor:'95%',labelWidth:200, editable:false,queryMode   :'local',valueField  :'value',displayField:'field'},   
      items     :[
      {      
        fieldLabel  :'<span style="color: rgb(255, 0, 0); padding-left: 10px; font-size:13px; font-weight:normal">Select Field/s to Update *</span>',
        xtype       :'combo',
        store       :update_selection,
        name        :'update_selection',
        reference   :'update_selection',
        emptyText   :'Select Field/s',
        listeners   :{el:{click:'onUpdateSelectionClick'},select:'onUpdateSelectionChange'}
      },
      {xtype:'label',style:'color:#e44959;font-weight:bold;font-face:arial;margin-top: 5px;margin-bottom: 15px;margin-left: 10px; line-height: 20px;',text:'Please check the selected field. Field must modify the appropriate column/field name unless field/s with null values and blank field will not be update'},
      {  
        defaults    :{anchor:'95%', labelWidth:200, editable:false,style:'margin-left:10px',emptyText:'Select column',store:Ext.create("OMM.store.batchupload.TempTableStore"),editable:false,valueField:'field_name',displayField:'field_name'},
        defaultType :'combo',
        xtype       :'form',
        reference   :'principal_update_field_form', 
        cls         :'Principal_update-page-container',
        itemId      :'principal_update_field_form',
        tbar        :{layout:{type:'vbox', align:'stretch', pack:'start'}, items:[{xtype:'label', cls:'Principal_update-page-title-text', text:'Field Selection'} ]},
        items       :[]
      }],
      bbar          :{style:'background-color:#4dacec;',defaults:{margin:5,ui:'blue'},items:[{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;">Modify the appropriate column/field for the required column/fields</span>'},'->',{text:'Reset Fields',handler:'onFieldSelectReset'},{text:'Update Record',handler:'onFieldSelectNext'}]}
    },
    {
      title:'3 : Result',disabled:true,reference:'principal_update_result',itemId:'principal_update_result',defaults:{width:'100%'},layout:'fit',bodyPadding:5,
      tbar      :{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',style:'margin-bottom:5px; margin-top:10px',text:'UPDATE RESULT'}]},
      bbar      :{style:'background-color:#4dacec;',defaults:{margin:5,ui:'soft-red'},items:[{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;"> Suggest to download error for your reference because it only shows once</span>'},'->',{text:'Download Error to XLS', disabled:true,handler:function(){window.open('../data/download_error.php','DOWNLOAD ERROR')}}]}
    }]
  }]
});