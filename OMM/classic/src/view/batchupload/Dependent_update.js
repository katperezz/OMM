var isMinimized 	 = false;
var update_selection = Ext.create('Ext.data.Store',{
	fields :['field','value'],
	data   :[
	// {"field":"PLAN ID/ PLAN LEVEL", "value":"plan_id"},
	// {"field":"Principal Employee ID", "value":"empid"},
	{"field":"HMO ID number", "value":"hmo_id"},
	{"field":"Last Name", "value":"lname"},
	{"field":"First Name", "value":"fname"},
	{"field":"Middle Name", "value":"mname"},
	{"field":"Suffix Name", "value":"ename"},
	{"field":"Date of Birth", "value":"bdate"},
	{"field":"Gender", "value":"gender"},
	{"field":"Civil Status", "value":"civil_status"},
	{"field":"Relationship", "value":"relationship"},
	{"field":"Dependent Status", "value":"dep_status"}
	// {"field":"HMO PROVIDER DETAILS", "value":"hmo_prov_details"},{"field":"HMO DATE", "value":"hmo_dates"}
	]
});

var update_unique_key 	= Ext.create('Ext.data.Store',{
	fields 	:["field", "value"],
	data 	:[
	// {"field":"ActiveLink ID Number", "value":"al_id"},
	// {"field":"HMO ID Number", "value":"hmo_id"},
	{"field":"Principal Employee ID, Dependent First and Last Name", "value":"empid_fn_ln"},
	{"field":"Principal Employee ID, Dependent First and Last Name, Birthdate", "value":"empid_fn_ln_bd"}
	]
});

Ext.define('OMM.view.batchupload.Dependent_update',{
	extend 	 	:'Ext.window.Window',
	xtype   	:'dependent_update',
	requires 	:[
		'OMM.view.batchupload.BatchUploadController'
	],
	controller 	:'batchupload',
	title 		:'Dependent Update',
	titleAlign 	:'center',
	initCenter  :true,
	autoShow 	:true,
	autoDestroy :true,
	modal 		:true,
	maximized   :true,
	height 		:500,
	width 		:'90%',
	closaAction :'destroy',
	layout 		:'fit',
	cls 		:'Principal_update-page-container',
	tools 		:[{type 	:'restore',hidden  :true,handler :function(evt, toolEl, owner, tool){var window = owner.up('window');window.expand('', true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type 	:'minimize',handler :function(evt, toolEl, owner, tool){var window = owner.up('window');window.collapse();winWIdth=window.getWidth();window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();;isMinimized=true;}}],
	items 		:[{
		xtype       :'tabpanel',
		reference 	:'dependent_update_tab',
		layout 	 	:'card',
		defaults 	:{border:false, bodyPadding:10},
		defaultType :'form',
		items 		:[
		{
			title:'1 Unique Key',reference:'DU_step1',itemId:'DU_step1',defaults:{anchor:'95%', labelWidth:200, editable:false, queryMode:'local', displayField:'field', valueField:'value'},
			items:[
				{xtype:'displayfield',fieldLabel:'acc_id',reference:'acc_id',hidden:true},
				{xtype:'displayfield',fieldLabel:'session_userid',reference:'session_userid',hidden:true},
				{xtype:'displayfield',fieldLabel:'filesignature',reference:'filesignature',hidden:true},
				{fieldLabel:'<span style ="color:rgb(255,0,0); padding-left:10px; font-size:13px; font-weight:normal">Select Unique key *</span>',xtype:'combo',store:update_unique_key,name:'unique_key_opt',reference:'unique_key_opt',emptyText:'Select Unique field/s',listeners:{select:'onDepUpdateUniquekey'}},
				{defaults:{bodyPadding:'5px 10px 5px 10px',anchor:'95%',labelWidth:200,editable:false,allowBlank:false,emptyText:'Select column',store:Ext.create("OMM.store.batchupload.TempTableStore"),editable:false,valueField:'field_name',displayField:'field_name'},defaultType:'combo',xtype:'form',reference:'dependent_update_unique_form',itemId:'dependent_update_unique_form',bodyPadding:5,tbar:{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',text:'Unique Key'}]},items:[]},
				{xtype:'tbspacer',flex:1}
			],
			listeners:{beforerender:'onBatch_response_render'},
			bbar:{style:'background-color:#4dacec;', defaults:{margin:5, ui:'blue'}, items:[{xtype:'label', style:'font-weight:bold;font-face:arial', html:'NOTE : <span style="color:#ffffff; font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;">Modify the appropriate column/field for the required column/fields</span>'},'->',{text:'Reset Fields', handler:'onDepUpdateUniquekeyReset'}, {text:'Next', ui:'blue', handler:'onDepUpdateUniquekeynext'}]}
		},
		{
			title 		:'2 : Filed Selection',
			disabled 	:true,
			reference 	:'DU_step2',
			itemId 		:'DU_step2',
			overflowY 	:'scroll',
			defaults  	:{bodyPadding:'5px 10px 5px 10px', anchor:'95%', labelWidth:200, editable:false, queryMode:'local', valueField:'value', displayField:'field'},
			items 		:[
				{fieldLabel:'<span style="color: rgb(255,0,0); padding-left:10px; font-size:13px; font-weight:normal;">Select  Field/s to Update *</span>',xtype:'combo',store:update_selection,name:'update_selection',reference:'update_selection',emptyText:'Select Field/s',listeners:{el:{click:'onDepUpdateSelectionClick'},select:'onDepUpdateSelectionChange'}},
				{xtype:'label',style:'color:#e44959;font-weight:bold;font-face:arial;margin-top: 5px;margin-bottom: 15px;margin-left: 10px; line-height:20px;',text:'Please check the sumbit all selected field must modify the appropriate column/field unless field/s with null values will not be update'},
				{defaults:{anchor:'95%',labelWidth:200,editable:false,style:'margin-left:10px',emptyText:'Select column',store:Ext.create('OMM.store.batchupload.TempTableStore'),displayField:'field_name',valueField:'field_name'},defaultType:'combo',xtype:'form',reference:'dependent_update_field_form',itemId:'dependent_update_field_form',cls:'Principal_update-page-container',tbar:{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',text:'Field Selection'}]},items:[]}
			],
			bbar 		:{style:'background-color:#4dacec;',defaults:{margin:5,ui:'blue'},items:[{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;">Modify the appropriate column/field for the required column/fields</span>'},'->',{text:'Reset Fields',handler:'onDepUpdateFieldSelectReset'},{text:'Update Record',handler:'onDepUpdateFieldSelectNext'}]}
		},
		{
			title 		 :'3 : Result', disabled:true, reference:'dependent_update_result', itemId:'dependent_update_result', defaults:{width:'100%'}, layout:'fit', bodyPadding:5,
			tbar 		 :{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',style:'margin-bottom:5px; margin-top:10px',text:'DEPENDENT UPDATE RESULT'}]},
			bbar 		 :{style:'background-color:#4dacec;',defaults:{margin:5,ui:'soft-red'},items:[{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff;font-weight:normal;font-face:arial;font-style:oblique;font-size:13px;"> Suggest to download error for your reference because it only shows once</span>'},'->',{text:'Download Error to XLS', disabled:true,handler:function(){window.open('../data/download_error.php','DOWNLOAD ERROR')}}]}
		}]
	}]
});