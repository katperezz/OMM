Ext.define('OMM.view.batchupload.TempTable_list',{
	extend 		:'Ext.Panel',
	xtype 		:'temptable_list',

	requires 	:[
	    'Ext.grid.selection.Replicator',
	    'Ext.grid.plugin.Clipboard',
	    'Ext.grid.selection.SpreadsheetModel',
	    'Ext.grid.filters.Filters'
    ],

	reference 	:'temptable_list',
	layout 	  	:'fit',
	items 		:[
	{
		xtype        :'gridpanel',
		autoScroll   :true,
		title        :'Temporary table',
		itemId 		 :'temptable_grid',
		reference  	 :'temptable_grid',
		// ui           :'light',
		plugins: [
			'clipboard',
			'selectionreplicator'
		],
		selModel: {
			type: 'spreadsheet',
			// Disables sorting by header click, though it will be still available via menu
			columnSelect: true,
			pruneRemoved: false,
			extensible: 'y'
		},
		store 	 	 :Ext.create('OMM.store.batchupload.TempTableStore_loaddata'),
		viewConfig 	 :{preserveScrollOnRefresh:true,preserveScrollOnReload:true,deferEmptyText:true,emptyText:'<h1>No data found</h1>',getRowClass:function(record,index){var c=record.get('status');if(c!='SUCCESS'||c==''){return'empty'}else{return'valid'}}},
		// selModel 	: 'rowmodel',
		// plugins: [
		// 	Ext.create('Ext.grid.plugin.RowEditing',{
		// 		clicksToEdit:1,
		// 		editing:true,
		// 		listeners:{
		// 			'validateedit':function(editor,e){},
		// 			'afteredit':'onTempTableAfterEdit'}})
		// ],
		columns 	:[
			// {header:'REMARK', dataIndex:'status', width:100,tdCls:'x-change-cell'},
			// {header:'REMARK MSG', dataIndex:'status_msg', width:200},
			// {header:'MATCH', dataIndex:'status_match', width:40}
		],
	    listeners   :{
	        beforerender:'onTempTableBeforerender',
	        afterrender:'onTempTableAfterrender'
	    }
	}
	]
});