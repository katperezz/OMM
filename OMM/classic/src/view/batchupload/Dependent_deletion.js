var deletion_unique_key = Ext.create('Ext.data.Store',{
	fields:["field","value"],
	data:[
		{"field":"ActiveLink ID", "value":"al_id"},
		{"field":"Employee ID, First and Last Name", "value":"empid_fn_ln"}
	]
});
Ext.define('OMM.view.batchupload.Dependent_deletion',{
	extend 		:'Ext.window.Window',
	controller 	:'batchupload',
	initCenter  :true,
	autoShow    :true,
	autoDestroy :true,
	modal       :true,
	maximized   :true,
	height      :500,
	width       :'90%',
	title       :'Dependent Deletion',
	titleAlign  :'center',
	closeAction :'destroy',	
	layout 		:'fit',
	cls        :'Principal_update-page-container',
	tools 		:[{type:'restore',hidden:true,handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.expand('',true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type:'minimize',handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.collapse();winWidth=window.getWidth();window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();isMinimized=true}}],
	items 		:[
	{xtype:'displayfield', fieldLabel:'acc_id', reference:'acc_id', hidden:true},
	{xtype:'displayfield', fieldLabel:'filesignature', reference:'filesignature', hidden:true},
	{xtype:'displayfield', fieldLabel:'session_userid', reference:'session_userid', hidden:true},
	{
		xtype 	 	:'panel',
		layout 	 	:'card',
		reference 	:'dependent_deletion_tab',
		defaults 	:{border:false, bodyPadding:10},
		items		:[
		{
			title:'Unique Key', reference:'DD_step1', itemId:'DD_step1', defaults:{anchor:'95%', labelWidth:200, editable:false, queryMode:'local', valueField:'value', displayField:'field'},
			tbar:{layout:{type:'vbox', align:'stretch', pack:'start'}, items:[{xtype:'label', cls:'Principal_update-page-title-text', text:'FIELD CONFIGURATION', style:'margin-top:20px'}]},
			items :[
			{
				fieldLabel 	:'<span style="color rgb(255,0,0); padding-left:10px; font-size:13px; font-weight:normal">Select Unique key *</span>',
				labelWidth 	:200,
				width 		:'95%',
				editable 	:false,
				xtype 		:'combo',
				store 		:deletion_unique_key,
				name 		:'unique_key_opt',
				reference  	:'unique_key_opt',
				emptyText 	:'Select Unique field/s',
				listeners 	:{select:'onUniqueKey_select'}
			},
			{
				defaults 	:{bodyPadding:'5px 10px 5px 10px', anchor:'95%', labelWidth:200, editable:false, allowBlank:false, emptyText:'Select column', store:Ext.create("OMM.store.batchupload.TempTableStore"), valueField:'field_name', displayField:'field_name'},
				defaultType :'combo',
				xtype 		:'form',
				reference 	:'deletion_field_form',
				itemId 		:'deletion_field_form',
				bodyPadding :5,
				items 		:[]
			}
			],
			bbar 	:{
				style  	:'background-color:#4dacec;',
				defaults:{margin:5, ui:'blue'},
				items 	:[
				{xtype:'label', style:'font-weight:bold; font-face:arial', html:'NOTE <span style="color:#ffffff; font-weight:normal; font-face:arial; font-style:oblique">Fill the required fields</span>'},'->',
				{text:'Reset Fields', handler:'onDel_reset'},{text:'HMO Delete', handler:'onDepDel_del'}
				]
			}
		},
		{
			reference 	:'dependent_deletion_result',
			itemId 		:'dependent_deletion_result',
			defaults 	:{width:'100%'},
			layout 		:'fit',
			bodyPadding	:5,
			tbar 		:{layout:{type:'vbox', align:'stretch', pack:'start'}, items:[{xtype:'label', cls:'Principal_update-page-title-text',style:'margin-bottom:5px; margin-top:10px', text:'DELETION RESULT'}]},
			bbar 		:{
				style 	:'background-color:#4dacec;',
				defaults:{margin:5, ui:'soft-red'},
				items 	:[
				{xtype:'label', style:'font-weight:bold; font-face:arial', html:'NOTE : <span style="color:#ffffff; font-weight:normal; font-face:arial; font-style:oblique; font-size:13px">Suggest to download error for your reference because it only shows once</span>'},'->',
				{text:'Download Error to XLS', handler:'onDownloadError', disabled:true}
				]
			}

		}
		]
	}
	],
	listeners :{
		beforerender :'onBatch_response_render'
	}

});
	