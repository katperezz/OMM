var isMinimized 		= false;
var optional_selection  = Ext.create('Ext.data.Store',{
	fields:['field',"value"],
	data:[
	{"field":"Employee ID","value":"empid"},
	// {"field":"HMO ID Number","value":"id_number"},
	{"field":"Middle Name","value":"mname"},
	{"field":"Suffix Name","value":"ename"},
	// {"field":"Gender","value":"gender"},
	{"field":"PhilHealth","value":"philhealth"},
	{"field":"SSS Number","value":"sss_no"},
	{"field":"Pag-ibig Number","value":"pag_ibig_no"},
	{"field":"Unified ID Number","value":"unified_id_no"},
	{"field":"TIN Number","value":"tin"},
	{"field":"Designation","value":"designation"},
	{"field":"Hire Date", "value":"emp_hire_date"}
	]
});

Ext.define('OMM.view.batchupload.Principal_inclusion',{
	extend 		:'Ext.window.Window',
	xtype 		:'principal_inclusion',
	reference 	:'principal_inclusion',
	requires 	:['OMM.view.batchupload.BatchUploadController','Ext.form.field.Tag','Ext.grid.plugin.CellEditing','Ext.layout.container.Card'],
	controller 	:'batchupload',
	initCenter 	:true,
	autoShow 	:true,
	autoDestroy :true,
	modal 		:true,
	maximized 	:true,
	height 		:500,
	width 		:'90%',
	title 		:'Principal Inclusion',
	titleAlign 	:'center',
	closeAction :'destroy',
	layout 		:'fit',
	cls 		:'Principal_update-page-container',
	tools 		:[{type:'restore',hidden:true,handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.expand('',true);window.setWidth(winWidth);window.center();isMinimized=false;this.hide();this.nextSibling().show()}},{type:'minimize',handler:function(evt,toolEl,owner,tool){var window=owner.up('window');window.collapse();winWidth=window.getWidth();window.setWidth(250);window.alignTo(Ext.getBody(),'bl-bl');this.hide();this.previousSibling().show();isMinimized=true}}],
	items 		:[
	{xtype:'displayfield',fieldLabel:'acc_id',text:'acc_id',reference:'acc_id',hidden:true},{xtype:'displayfield',fieldLabel:'user_id',text:'session_userid',reference:'session_userid',hidden:true},{xtype:'displayfield',fieldLabel:'filesignature',text:'filesignature',reference:'filesignature',hidden:true},
	{
		xtype 		:'panel',
		layout 		:'card',
		reference 	:'principal_inclusion_tab',
		defaults 	:{border:false,bodyPadding:10},
		items 		:[
		{
			xtype 	 	:'form',
			cls 		:'Principal_update-page-container',
			reference 	:'principal_inclusion_field_form',
			overflowY 	:'scroll',
			itemId 		:'principal_inclusion_field_form',
			defaults 	:{anchor:'95%',labelWidth:200,editable:false,style:'margin-left:10px',emptyText:'Select column'},
			defaultType :'combo',
			tbar 		:{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',text:'FIELD CONFIGURATION',style:'margin-top:20px'}]},
			items 		:[
			{
				xtype 		:'form',
				reference 	:'principal_inclusion_optional_field_form',
				cls 		:'Principal_update-page-container',
				overflowY 	:'scroll',
				itemId 		:'principal_inclusion_optional_field_form',
				defaults 	:{anchor:'95%',labelWidth:200,editable:false,style:'margin-left:10px',emptyText:'Select column', store:Ext.create("OMM.store.batchupload.TempTableStore"), displayField:'field_name', valueField:'field_name'},
				defaultType :'combo',
				tbar 		:{
					layout 	:{type:'vbox',align:'stretch',pack:'start'},
					defaults:{bodyPadding:'5px 10px 20px 10px',anchor:'95%',labelWidth:200,editable:false,queryMode:'local',valueField:'value',displayField:'field'},
					itemId 	:'prin_inc_opt_form',
					items 	:[{xtype:'label',cls:'Principal_update-page-title-text',text:'Optional fields',style:'margin-top:5px;color:#e44959;font-size:15px'},{fieldLabel:'<span style="color: rgb(255, 0, 0); padding-left: 10px; font-size:13px; font-weight:normal">Select Field/s to Update *</span>',xtype:'combo',store:optional_selection,name:'optional_selection',reference:'option',emptyText:'Select Field/s',listeners:{select:'onOptionFieldSelectionChange'}}]
				},
				items 		:[
					{fieldLabel:'Employee Number',name:'empid'},
					{fieldLabel:'Middle Name', name:'mname'}
				]
			},
			{
				xtype 		:'form',
				reference 	:'principal_inclusion_required_field_form',
				cls 		:'Principal_update-page-container',
				defaults 	:{anchor:'95%',labelWidth:200,editable:false,style:'margin-left:10px',emptyText:'Select column',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},
				defaultType :'combo',
				tbar 		:{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',text:'Required fields',style:'margin-top:5px;color:#e44959;font-size:15px'}]},
				items 		:[
					{defaults:{anchor:'95%',labelWidth:200,editable:false,style:'margin-left:10px',emptyText:'Select column',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},defaultType:'combo',title:'Branch ID',xtype:'fieldset',layout:'anchor',collapsible:true,items:[{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">Branch ID*</span>',name:'branch_id',allowBlank:false,reference:'branch_id'},{xtype:'button',text:'Convert Column Values to Branch Identifier',ui:'soft-green',listeners:{el:{click:'onPrinCvrtBranch'}}}]},
					{defaults:{anchor:'95%',labelWidth:200,editable:false,style:'margin-left:10px',emptyText:'Select column',store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},defaultType:'combo',title:'Plan Level',xtype:'fieldset',cls:'Principal_update-page-container',layout:'anchor',collapsible:true,items:[{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">Plan Level*</span>',name:'plan_id',allowBlank:false},{xtype:'button',text:'Convert Column Values to Plan Identifier',ui:'soft-green',listeners:{el:{click:'onPrinCvrtPlan'}}}]},
					{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">First Name*</span>',name:'fname',allowBlank:false},
					{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">Last Name*</span>',name:'lname',allowBlank:false},
					{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">Gender*</span>',name:'gender',allowBlank:false},
					{xtype:'button',margin:'0 0 20 10',text:'Convert Column Values to Gender Identifier',ui:'soft-green',listeners:{el:{click:'onCvrtGender'}}},
					{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">Date of Birth*</span>',name:'birthdate',allowBlank:false},
					{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">Civil Status*</span>',name:'civil_status',allowBlank:false},
					{xtype:'button',margin:'0 0 20 10',text:'Convert Column Values to Civil Status Identifier',ui:'soft-green',listeners:{el:{click:'onCvrtCivil_stat'}}},
					{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">Original Effective Date*</span>',name:'orig_effectivity_date',allowBlank:false},
					{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">Current Effective Date*</span>',name:'curr_effectivity_date',allowBlank:false},
					{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">Maturity Date*</span>',name:'maturity_date',allowBlank:false},
					{defaults:{anchor:'95%',labelWidth:200,editable:false,style:'margin-left:10px',emptyText:"Select column", store:Ext.create("OMM.store.batchupload.TempTableStore"),displayField:'field_name',valueField:'field_name'},defaultType:'combo',title:'Member Endorsement',xtype:'fieldset',cls:'Principal_update-page-container',layout:'anchor',collapsible:true,items:[{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">HR Endorsed Date*</span>',name:'hr_endorsed_date',allowBlank:false},{fieldLabel:'<span style="color: rgb(255, 0, 0);font-weight:bold;">OPS Endorsed Date*</span>',name:'ops_endorsed_date',allowBlank:false}]}
				]
			}
			],
			bbar:{style:'background-color:#4dacec;',overflowHandler:'menu',defaults:{margin:5,ui:'blue'},items:[{xtype:'label',style:'font-weight:bold; font-face:arial',html:'NOTE : <span style="color:#ffffff; font-weight:normal;font-face:arial; font-style:oblique">Fill the required fields.</span>'},'->',{text:'Reset Fields',handler:'onPrinIncReset'},{text:'Upload',handler:'onPrinInc_upload'}]}
		},
		{
			reference 	:'principal_inclusion_result',
			itemId 		:'principal_inclusion_result',
			defaults 	:{width:'100%'},
			layout 		:'fit',
			bodyPadding :5,
			tbar 		:{layout:{type:'vbox',align:'stretch',pack:'start'},items:[{xtype:'label',cls:'Principal_update-page-title-text',style:'margin-bottom:5px; margin-top:10px',text:'INCLUSION RESULT'}]},
			bbar 		:{
				style 	:'background-color:#4dacec;',
				defaults:{margin:5,ui:'soft-red'},
				items 	:[
				{xtype:'label',style:'font-weight:bold;font-face:arial',html:'NOTE : <span style="color:#ffffff; font-weight:normal; font-face:arial;font-style:oblique; font-size:13px;"> Suggest to download error for your reference because it only shows once</span>'},
				'->',
				{
					text 	:'Download Error to XLS',
					handler :'onDownloadError', disabled:true
			}
			]}
		}
		]
	}
	],
	listeners:{beforerender:'onBatch_response_render'}
});
