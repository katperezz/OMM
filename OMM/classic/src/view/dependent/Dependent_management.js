var itemsPerPage=25;Ext.create('Ext.data.Store',{id:'dependentStore',autoLoad:false,fields:[{name:'al_dep_id',mapping:'al_dep_id'},{name:'al_prin_id',mapping:'al_prin_id'},{name:'dep_status',mapping:'dep_status'},{name:'status',mapping:'status'},{name:'hmo_id',mapping:'hmo_id'},{name:'fname',mapping:'fname'},{name:'mname',mapping:'mname'},{name:'lname',mapping:'lname'},{name:'ename',mapping:'ename'},{name:'gender',mapping:'gender'},{name:'bdate',mapping:'bdate'},{name:'civil_status',mapping:'civil_status'},{name:'philhealth',mapping:'philhealth'},{name:'relationship',mapping:'relationship'},{name:'date_of_enrollment',mapping:'date_of_enrollment'},{name:'last_update',mapping:'last_update'},{name:'datecreated',mapping:'datecreated'},{name:'remark',mapping:'remark'},{name:'date_of_deletion',mapping:'date_of_deletion'},{name:'dcompanyid',mapping:'dcompanyid'},{name:'dbranchid',mapping:'dbranchid'},{name:'dmbr_status',mapping:'dmbr_status'},{name:'dplan_level',mapping:'dplan_level'},{name:'plan_desc',mapping:'plan_desc'},{name:'hmo_provider',mapping:'hmo_provider'},{name:'dental_provider',mapping:'dental_provider'},{name:'opt_title',mapping:'opt_title'},{name:'opt_desc',mapping:'opt_desc'}],pageSize:itemsPerPage,proxy:{type:'ajax',timeout:2000000,url:'../data/get_dependent_list.php',actionMethods:'POST',extraParams:{search_value:'',dep_search_opt:'false'},reader:{type:'json',rootProperty:'data',totalProperty:'total',successProperty:'success'},writer:{type:'json',rootProperty:'data',encode:true}}});
Ext.define('OMM.view.dependent.Dependent_management',{
	extend 		: 'Ext.Panel',
	title 		: 'Dependent Management',
	xtype 		: 'dependent_management',
    requires 	:[
    
    'Ext.grid.selection.Replicator',
    'Ext.grid.plugin.Clipboard',
    'Ext.grid.selection.SpreadsheetModel',
    'Ext.grid.filters.Filters',

    'Ext.toolbar.Paging',
    'OMM.view.dependent.DependentController'
    ],
    controller 	:'dependent',

	closable 	:true,
	layout 		:'fit',
	bodyPadding :1,
	tbar 		:[
	{
	xtype 	:'panel',
	defaults:{anchor:'100%',labelWidth:100},
	items 	:[
	{
	        xtype 	:'panel',
	        defaults:{anchor:'100%',labelWidth:100},
	        layout 	:'hbox',
	        items 	:[
	        {
	            xtype        : 'combo',
	            fieldLabel   : 'Account name',
	            displayField : 'companyname',
	            valueField   : 'companyid',
	            emptyText    :'Select account',
	            editable     :false,
	            width        :400,
	            name         :'companyname',
	            id           :'dep_companyname',
	            triggerAction: 'all',
	            store        : 'Get_acc_list',
	            afterLabelTextTpl: [
	                '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
	            ],
	            listeners    :{
	                select   :'onDepSelectAccount'                                
	            }
	        },
	        {
	            xtype   :'button',
	            text    :'Export',
	            ui      :'soft-green',
	            margin  :'0 5 0 5',
	            handler :'onDepExportXLS'
	        },
	        {
	            xtype   :'panel',
	            defaults:{labelStyle:'font-size:25px;font-style :font-weight:bold;font-face:arial',fieldStyle:'font-size:25px;font-weight:bold;font-face:arial'},
	            items   :[
	            {
	                xtype       :'displayfield',
	                fieldLabel  :'TOTAL',
	                margin      :'0 0 0 10',
	                id          :'dependent_total'
	            }]
	        }
	        ]
	},
	{xtype:'label',style:'font-weight:bold;font-size:11px;font-face:arial',html:'NOTE : <span style="color:#2f9fe9;font-weight:bold;font-face:arial;font-size:11px;font-style:oblique;">Double click row to view expand information</span>'}                
	// {xtype:'label',style:'color:#57b3cb;font-size:11px;font-weight:bold;font-face:arial;font-style:oblique;',text:'Double click row to view expand information',iconCls:'x-fa fa-info-circle'}                
	]
	},
	'->',
	{xtype:'checkbox',boxLabel:'Check to search employee number',labelAlign:'right',labelStyle:'color:#2f9fe9;font-weight:bold;font-face:arial;font-size:12px',labelWidth:480,name:'dep_search_opt',reference:'dep_search_opt',id:'dep_search_opt',checked:false,padding:'-10 0 0 0',inputValue:'1',uncheckedValue:'0'},                        
	{
	xtype      	    :'textfield',
	id           	:'dep_search_value',
	emptyText     	:'Search Employee',
	enableKeyEvents :true,
	triggers        :{
	    dep_search_value :{
	        cls          :Ext.baseCSSPrefix+'form-search-trigger',
	        handler  	 :'onDepSearchEmp'
	}},
	inputAttrTpl :" data-qtip='Search Value: <br> • emp_number <br> • lastname <br> • firstname <br> • middlename'",
	listeners    :{
	    keyup    :'onDepSearchEmp_enter'
	}
	}
	],
	items 		:[{
			xtype  	 	 :'grid',
			id 	 	 	 :'dependent_grid',
			border 	  	 :false,
			autoScroll 	 :true,
			columnLines  :false,
			store        :'dependentStore',
			headerBorders:false,
			rowLines     :true,
			plugins 	 :[
				'gridfilters',
				'clipboard',
				'selectionreplicator'
			],
			selModel 	 :{
				type 		 :'spreadsheet',
				columnSelect :true,
				pruneRemoved :false,
				extensible 	 :'y'
			},	
			viewConfig   :{preserveScrollOnRefresh:true,preserveScrollOnReload:true,deferEmptyText:true,emptyText:'<h1>No data found</h1>', trackOver:false},	
			columns 	 : [
		        {xtype:'rownumberer', width:45},
				{header:'Last name',dataIndex:'lname',flex:1,filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},
				{header:'First name',dataIndex:'fname',flex:1,filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},
				{header:'Middle name',dataIndex:'mname',flex:1,filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},
				{header:'Suffix',dataIndex:'ename',filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},
				{header:'Gender',dataIndex:'gender',filter:'list'},
				{header:'Civil status',dataIndex:'civil_status',flex:1,filter:'list'},
				{header:'Branch',dataIndex:'dbranchid',flex:1,filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},
				{header:'Status',dataIndex:'status',flex:1,filter:{type:'string',itemDefaults:{emptyText:'Search for...'}}},
				{menuDisabled:true,sortable:false,xtype:'actioncolumn',items:[{iconCls:'x-fa fa-search',tooltip:'Click to view details',handler:'onDependentDetails'}]}
            ],
			listeners 	:{
				beforerender 	:function(form,btn){
					Ext.getCmp('dependent_total').setValue('');
					Ext.getCmp('dependent_grid').filters.clearFilters();
					Ext.getCmp('dependent_grid').getStore().removeAll();
					console.log(window.console);if(window.console||window.console.firebug){console.clear()}
				},                
				rowdblclick 	:'onDependentDetails'
			},
			dockedItems		: [{
				xtype 		: 'pagingtoolbar',
				dock 		: 'bottom',
				store       :'dependentStore',
	    		displayInfo : true,
				moveNext : function(){
					var search_value 	   = Ext.getCmp('dep_search_value').getValue(),
						dep_search_opt     = Ext.getCmp('dep_search_opt'),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						next  			   = me.store.currentPage + 1;

						console.log(next);
						console.log(dep_search_opt)
					if (next <= total) {
					    if (me.fireEvent('beforechange', me, next) !== false) {	   
					        me.store.nextPage({
					        	params:{
					        		search_value:search_value,
					        		dep_search_opt:dep_search_opt.getValue()
					        	}});
					    }
					}
				},
				movePrevious:function(){
					var search_value 	   = Ext.getCmp('dep_search_value').getValue(),
						dep_search_opt     = Ext.getCmp('dep_search_opt'),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						previous  		   = me.store.currentPage - 1;

						console.log(previous);
					if (previous <= total) {
					    if (me.fireEvent('beforechange', me, previous) !== false) {
					        me.store.previousPage({params:{search_value:search_value,dep_search_opt:dep_search_opt.getValue()}});
					    }
					}
				},
				moveFirst:function(){
					var search_value 	   = Ext.getCmp('dep_search_value').getValue(),
						dep_search_opt     = Ext.getCmp('dep_search_opt'),
						me  			   = this;

					me.store.getProxy().extraParams = {search_value:search_value,dep_search_opt:dep_search_opt.getValue()};
					me.store.loadPage(1);
				},
				moveLast:function(){
					var search_value 	   = Ext.getCmp('dep_search_value').getValue(),
						dep_search_opt     = Ext.getCmp('dep_search_opt'),
						me  			   = this,
						total  			   = me.getPageData().pageCount;

					me.store.getProxy().extraParams = {search_value:search_value,dep_search_opt:dep_search_opt.getValue()};
					me.store.loadPage(total);
				},

				doRefresh : function(){
					var search_value 	   = Ext.getCmp('dep_search_value').getValue(),
						dep_search_opt     = Ext.getCmp('dep_search_opt'),
						me  			   = this,
						total  			   = me.getPageData().pageCount,
						current 		   = me.store.currentPage;

					console.log(current)
					if (me.fireEvent('beforechange', me, current) !== false) {
						me.store.getProxy().extraParams = {search_value:search_value,dep_search_opt:dep_search_opt.getValue()};
						me.store.loadPage(current);
					}
				}
			}]
		}]
});