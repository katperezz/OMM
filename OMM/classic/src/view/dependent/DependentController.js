Ext.define('OMM.view.dependent.DependentController', {
    extend  : 'Ext.app.ViewController',
    alias   : 'controller.dependent',

    requires: [
        'Ext.util.TaskRunner',        
        'Ext.form.action.StandardSubmit'
    ],

    //ComboBox - Onchange Dependent type
    onDepType_change_dep_form:function(btn){
        var dep_type       = this.lookupReference('dep_type'),
            acc_id         = this.lookupReference('dep_acc_id'),
            session_userid = this.lookupReference('session_userid');

        dep_type.getStore().load({params:{account:acc_id.getValue(), operator:session_userid.getValue()}})
    },

    //Afterrender - dependent_details
    onAfterrenderDepTab:function(){
        var id                    = 1,
            dep_note_thread      = this.lookupReference('dep_note_thread'),
            activity_note         = this.activity_note=Ext.create('OMM.store.Get_activity_note'),
            OMM_logs_ops          = localStorage.getItem("OMM_logs_ops"),
            obj                   = JSON.parse(OMM_logs_ops),
            OMM_thread_notes      = localStorage.getItem("OMM_thread_notes"),
            obj2                  = JSON.parse(OMM_thread_notes),
            me                    = this;

        activity_note.removeAll();
        activity_note.getProxy().extraParams = {activelink_id:obj2.activelink_id, acc_id:obj2.acc_id, ops:obj};
        activity_note.load({
        callback: function(records, operation, success) {
          var total_count  = records.length,
              i            = 0;
          me.dep_mem_logs_AjaxRequest_getId(id, total_count)
        }
        });
        console.log('Dependent tab render');
        // activity_note.load();
    
    },


    //Ajax request - load dependent activity logs
    dep_mem_logs_AjaxRequest_getId:function(){
        var me               = this,
            i                = 0,
            dep_note_thread  = this.lookupReference('dep_note_thread');

        this.activity_note.each(function(record){
            var note     = record.get('note'),
                operator = record.get('operator'),
                date     = record.get('date');

            if(note=='DELETION' || note=='INCLUSION' || note=='MEMBER REACTIVATION' || note=='MEMBER RESET' || note=='MEMBER UPDATE' || note=='MEMBER DEP DOCUMENT' || note=='RESET DEP DOCUMENT' || note=='USER RESET'){
                dep_note_thread.add({
                bodyStyle    :{"background-color":"#f4f3d7"},
                xtype        :'panel',
                margin       :10,
                bodyPadding  :'0px 0px 15px 0px',
                title        :operator,
                html         :'&nbsp;&nbsp;&nbsp;&nbsp;'+note,
                tools        :[
                {xtype:'displayfield',value:date,labelWidth:25,fieldLabel:' ',beforeLabelTextTpl:['<i class="fa fa-clock-o" style="font-size:15px;color:#467ead;"></i>']}]
            });
            }else{
                dep_note_thread.add({
                bodyStyle    :{"background-color":"#f4f3d7"},
                xtype        :'panel',
                ui           :'thread-panel-note',
                bodyStyle    :{"background-color":"#f6f7f9","padding":"15px"},
                margin       :5,
                bodyPadding  :'0px 0px 15px 0px',
                title        :operator,
                html         :'&nbsp;&nbsp;&nbsp;&nbsp;'+note,
                tools        :[
                {xtype:'displayfield',value:date,labelWidth:25,fieldLabel:' ',beforeLabelTextTpl:['<i class="fa fa-clock-o" style="font-size:15px;color:#467ead;"></i>']}]
            });
            }
            i++;
        });
    },


    //ComboBox - Select Account
    onDepSelectAccount:function(value, paging, params){

        console.log(window.console);if(window.console||window.console.firebug){console.clear()}

        this.selected_db=value.getValue();
        console.log('Dependent select account : '+this.selected_db);
        var g=Ext.getCmp('dependent_grid');
        var dep_search_value = Ext.getCmp('dep_search_value');

        if(this.selected_db!==''){
            Ext.Ajax.request({
                url     :'../data/session_dep_acc.php',
                waitMsg :'Processing your request',
                params  :{session_dep_acc:this.selected_db},
                timeout : 120000, //2minutes
                method  :'POST',
                success :function(){
                    Ext.getCmp('dependent_grid').filters.clearFilters();
                    Ext.getCmp('dependent_grid').getStore().removeAll();
                    // console.log('Dependent current page :'+g.store.currentPage);
                    Ext.getCmp('dependent_grid').getStore().load({params:{page:g.store.currentPage}});
                    dep_search_value.setValue('');
                    
                    Ext.Ajax.request({
                        url     :'../data/post_username.php',
                        success :function(response){
                            var response         = Ext.decode(response.responseText);
                                details          = {};
                                details.ops      = response.username;
                            localStorage.removeItem('OMM_logs_ops');
                            localStorage.removeItem('OMM_thread_notes');
                            localStorage.setItem("OMM_logs_ops",Ext.encode(details));
                        }
                    });

                    Ext.Ajax.request({url:'../data/get_dependent_total.php',success:function(response){var response=Ext.decode(response.responseText);Ext.getCmp('dependent_total').setValue(response.total)},failure:function(response){}})
                }
            })
        }else{Ext.MessageBox.show({title:'WARNING',msg:'Select Account',closable:false,icon:Ext.Msg.WARNING,buttons:Ext.Msg.WARNING,buttonText:{ok:'OK'}})}
    },


    //Button - export dependent list
    onDepExportXLS:function(){

        if(Ext.getCmp('dep_companyname').getValue()==null){
            Ext.MessageBox.show({title:'WARNING',msg:'No account selected',closable:false,icon:Ext.Msg.WARNING,buttons:Ext.Msg.WARNING,buttonText:{ok:'OK'}});                                    
        }else{
            var inf     = new Ext.Window({
                width       :'80%',
                height      :350,
                modal       :true,
                closeAction :'hide',
                layout      :'hbox',
                bodyPadding :5,
                title       :'Set Filter for downloads',
                layout      :{
                    type         : 'accordion',
                    titleCollapse: true,
                    animate      : true,
                    activeOnTop  : true
                },
                items       :[
                {
                    xtype           :'form',
                    title           :'Based on Enrollment Date',
                    defaultType     :'datefield',
                    bodyPadding     :5,
                    defaults        :{anchor:'100%', labelWidth:300, allowBlank:false},
                    url             :'../data/download_dependent_enroll_date.php',
                    standardSubmit  :true,
                    items           :[
                        {fieldLabel:'Start Date (YYYY-MM-DD)',name:'start_date',format:'Y-m-d', value:'1910-01-01', minValue:'1910-01-01', maxValue:new Date(), afterLabelTextTpl:['<span style="color:red;font-weight:bold" data-qtip="Required">* Default Value</span>']},
                        {fieldLabel:'End Date (YYYY-MM-DD)',name:'end_date',format:'Y-m-d',value:new Date(), minValue:'1910-01-01', maxValue:new Date(), afterLabelTextTpl:['<span style="color:red;font-weight:bold" data-qtip="Required">*</span>']}
                    ],
                    bbar            :{// layout      :{type    :'vbox',align   :'left'},
                        items       :[
                        {
                            text    :'Export to XLS',
                            ui      :'soft-purple',
                            handler :function(){

                                var form    = this.up('form').getForm();
                                var values  = form.getValues();
                                if(form.isValid()){
                                    
                                    if(values['start_date'] >= values['end_date']){
                                        Ext.MessageBox.show({title:'WARNING', msg:'Date and time is not valid', closable:false, icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                    }else{                                   
                                        form.submit({
                                            params          :{acc_id:Ext.getCmp('dep_companyname').getValue()},
                                            waitMsg         :'Processing your request',
                                            submitEmptyText :false,
                                            success         :function(){
                                                Ext.MessageBox.show({title:'Success',msg:'Download Success',icon:Ext.Msg.INFO,closable:false,buttonText:{ok:'OK'}});    
                                                form.reset();
                                            },failure       :function(form, action){
                                                
                                                if (action.failureType === Ext.form.action.Action.CONNECT_FAILURE) {
                                                    Ext.MessageBox.show({title:'Export',msg:'Donwload Failed',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                                }
                                                if (action.failureType === Ext.form.action.Action.SERVER_INVALID){
                                                Ext.MessageBox.show({title:'Export',msg:'Donwload Failed',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                                }
                                            }
                                        });
                                    }
                                }else{Ext.MessageBox.show({title:'WARNING', msg:'Date and time is not valid', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});}
                            }
                        }]
                    }
                },
                {
                    xtype           :'form',
                    title           :'Based on Principal Confirm Re-Enrollment Date',
                    defaultType     :'datefield',
                    bodyPadding     :5,
                    defaults        :{anchor:'100%',labelWidth:390, allowBlank:false},
                    url             :'../data/download_dependent_reenroll_date.php',
                    standardSubmit  :true,
                    items           :[
                        {fieldLabel:'Start Date and Time (YYYY-MM-DD hh:mm:ss) ',name:'start_date',format:'Y-m-d H:i:s',value:'1910-01-01 00:00:00', minValue:'1910-01-01', maxValue:new Date(), afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Required">* Default value</span>']},
                        {fieldLabel:'End Date and Time (YYYY-MM-DD hh:mm:ss)',name:'end_date',format:'Y-m-d H:i:s',value:new Date(), minValue:'1910-01-01',afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Required">*</span>']}
                     // maxValue:new Date(), 
                    ],
                    bbar :{
                        items   :[
                        {
                        text    :'Export to XLS',
                        ui      :'soft-purple',
                        handler :function(){

                            var form    = this.up('form').getForm();
                            var values  = form.getValues();
                    
                            if(form.isValid()){            
                                if(values['start_date'] >= values['end_date']){
                                    Ext.MessageBox.show({title:'WARNING',msg:'Date and time is not valid',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                }else{
                                    form.submit({
                                        params          :{acc_id:Ext.getCmp('dep_companyname').getValue()},// timeout     : 300000, //5minutes
                                        waitMsg         :'Processing you request',
                                        submitEmptyText :false,
                                        success     :function(){
                                            Ext.MessageBox.show({title:'Success',msg:'Download Success',closable:false,icon:Ext.Msg.INFO,buttonText:{ok:'OK'}});
                                            form.reset();
                                        },failure   :function(form, action){
                                        
                                            if (action.failureType === Ext.form.action.Action.CONNECT_FAILURE) {
                                                Ext.MessageBox.show({title:'Export',msg:'Donwload Failed',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                            }
                                            if (action.failureType === Ext.form.action.Action.SERVER_INVALID){
                                            Ext.MessageBox.show({title:'Export',msg:'Donwload Failed',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                            }
                                        }
                                    });
                                }
                            }else{Ext.MessageBox.show({title:'WARNING',msg:'Date and time is not valid',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});}
                            }
                        }]
                    }
                }],
                bbar        :{
                    items   :[{xtype:'label',style:'color:#e44959;font-weight:bold;font-face:arial', margin:5,text:'NOTE : Download is not successful if page redirect or reload the window',iconCls:'x-fa fa-info-circle'}]
                }
            });
            inf.show(this);                                
        }
    },


    //textfield - keyup dependent search
    onDepSearchEmp_enter:function(t,e,o){
        var me  = this;

        if(e.getKey() == e.ENTER){
            me.onDepSearchEmp();
        }
    },

    //button - dependent search
    onDepSearchEmp:function(t,e,o){
        var dep_search_opt = this.lookupReference('dep_search_opt');
        var dep_companyname = Ext.getCmp('dep_companyname');
        var g = Ext.getCmp('dependent_grid');

        console.log(Ext.getCmp('dep_search_value').getValue())
        if(dep_companyname.value==null){
            alert('No account selected');
            dep_companyname.focus();
            dep_companyname.expand();
        }else{
            g.store.clearFilter(dep_companyname);
            g.store.load({params:{search_value:Ext.getCmp('dep_search_value').getValue(),dep_search_opt:dep_search_opt.getValue()}});  
        }
    },

    //
    onDepBranchID:function(){
        var dep_acc_id     = this.lookupReference('dep_acc_id');
        var dep_branch_id  = this.lookupReference('dep_branch_id');
        dep_branch_id.getStore().load({params:{acc_id:dep_acc_id.getValue()}});
    },

    //ComboBox - Dependent Acc_id(company ID)
    onDepCompanyID:function(){

        var dep_acc_id      = this.lookupReference('dep_acc_id');
        var dep_planLevel   = this.lookupReference('dep_planLevel');
        var dep_branch_id   = this.lookupReference('dep_branch_id');

        if(dep_branch_id.getValue()=='' || dep_branch_id.getValue()== null ){
            Ext.MessageBox.show({title:'WARNING', msg:'Please select Branch ID first', icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});            
        }else{
            dep_planLevel.getStore().load({params:{acc_id:dep_acc_id.getValue(),branch_id:dep_branch_id.getValue()}});            
        }                                                                 
    },

    //ComboBox - onChange dependent type
    onDepPlanChange:function(){

        var dep_acc_id      = this.lookupReference('dep_acc_id');
        var dep_planLevel   = this.lookupReference('dep_planLevel');
        var dep_branch_id   = this.lookupReference('dep_branch_id');
        var dep_plan_desc   = this.lookupReference('dep_plan_desc');

        Ext.Ajax.request({
            url     :'../data/get_principal_plan.php',
            params  :{acc_id:dep_acc_id.getValue(),plan_id:dep_planLevel.getValue(),branch_id:dep_branch_id.getValue()},
            success:function(response){
                var response  = Ext.decode(response.responseText);
                dep_plan_desc.setValue(response.description)
            },failure:function(){
                Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please contact IT for your assistance',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
            }
        }); 
    },
    
    //CheckBox - Enrollment status option 
    onDepChangeEnrollStat:function(value){
        var dep_enroll_stat = this.lookupReference('dep_enroll_stat'); // var dep_enroll_stat = Ext.getCmp('dep_enroll_stat'+r.data['al_dep_id']);
        this.selected_db    = value.getValue();

        if(value.checked == true) {
            dep_enroll_stat.setDisabled(false);
        }else{
            // dep_enroll_stat.setValue('');
            dep_enroll_stat.setDisabled(true);
        }
    },

    //rowdblclick - dependent grid
    onDependentDetails:function( row, r, tr, rowIndex, e, eOpts,grid,record,colIndex){
        console.log('Dependent details :'+ r.data);
        var view                    = Ext.getCmp("ops_tabpanel"),
            cur_tab_id              = "dep"+r.data['al_dep_id'],
            details                 = {};
            details.activelink_id   = r.data['al_dep_id'],
            details.acc_id          = r.data['dcompanyid'];

        localStorage.setItem("OMM_thread_notes",Ext.encode(details));

        if(Ext.getCmp(cur_tab_id)){
            view.setActiveTab(cur_tab_id)
        }else{
            var myMask = new Ext.LoadMask({
                msg    : 'Processing your request. Please wait...',
                target : view
            });
            myMask.show();

            view.add({
                id       :cur_tab_id,
                xtype    :'dependent_details',
                title    :'Dep Info - '+r.data['fname']+' '+r.data['lname'],
                items    :[
                {
                    xtype    :'panel',
                    layout   :'hbox',
                    overflowY:'scroll',
                    items    :[
                    {
                        xtype       :'panel',
                        width       :'45%',
                        bodyPadding :5,
                        items       :[
                        {
                            xtype       :'fieldset',
                            title       :'<b>Personal Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%',labelWidth:130, allowBlank:true},
                            layout      :'anchor',// overflowY   :'scroll',
                            items       :[
                                {name:'session_userid', reference:'session_userid', hidden:true},
                                {fieldLabel:'First Name',name:'fname',value:r.data['fname'], allowBlank:false},{fieldLabel:'Middle Name',name:'mname',value:r.data['mname']},{fieldLabel:'Last Name',name:'lname',allowBlank:false,value:r.data['lname']},{fieldLabel:'Suffix Name',name:'ename',value:r.data['ename']},
                                {fieldLabel:'Gender',xtype:'combo',name:'gender',value:r.data['gender'],store:Ext.create("OMM.store.Get_gender"),valueField:'gender',displayField:'gender',editable:false,emptyText:'Select Gender'},
                                {xtype:'datefield',fieldLabel:'Birthdate',name:'bdate',value:r.data['bdate']},
                                {fieldLabel:'Civil Status',xtype:'combo',name:'civil_status',value:r.data['civil_status'],store:Ext.create("OMM.store.Get_civilstatus"),valueField:'civil_status',displayField:'civil_status',emptyText:'Select Civil Status',editable:false},
                                {fieldLabel:'Relationship',xtype:'combo',name:'relationship',value:r.data['relationship'],store:Ext.create("OMM.store.Get_relationship"),displayField:'relationship',valueField:'relationship',editable:false},
                                {fieldLabel:'PhilHealth',name:'philhealth',value:r.data['philhealth']}
                            ]
                        },
                        {
                            xtype       :'fieldset',
                            columnWidth :0.5,
                            title       :'<b>Company Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%',labelWidth:130, allowBlank:true},
                            layout      :'anchor',
                            items       :[
                                {fieldLabel:'Principal AL ID',name:'al_prin_id',readOnly:true,value:r.data['al_prin_id'],hidden:true},
                                {fieldLabel:'Dependent AL ID',name:'al_dep_id',allowBlank:false,readOnly:false,value:r.data['al_dep_id'],hidden:true,id:'dep_al_dep_id'+r.data['al_dep_id']},
                                {fieldLabel:'Company ID',name:'dcompanyid',value:r.data['dcompanyid'],editable:false,reference:'dep_acc_id',id:'dep_acc_id'+r.data['al_dep_id']},
                                {fieldLabel:'Branch',name:'dbranchid',value:r.data['dbranchid'],readOnly:true,reference:'dep_branch_id'}
                            ]
                        },

                        {
                            xtype       :'panel',
                            title       :'<b>Required Documents</b>',
                            defaults    :{anchor:'100%',labelWidth:130},
                            ui          :'light',
                            titleCollapse :true,
                            collapsible :true,
                            animate     :true,
                            layout      :'anchor',
                            bodyPadding :5,
                            tbar        :[
                            {
                                text    :'Modify',
                                ui      :'soft-green',
                                handler :function(btn, grid, record, index, eOpts, rowIndex, colIndex, b){
                                    var g        = Ext.getCmp('dep-required-doc-'+r.data['activelink_id']).getStore();
                                    var array_id = new Array();
                                    var i        = 0;
                                    var file_name=0;

                                    g.each(function (record){
                                        if(record.get('active')===true){
                                            array_id[i]         = record.get('doc_id');
                                            file_name           = record.get('filename');
                                            console.log('Dependent document ID :'+ record.get('doc_id') +' File name : '+record.get('filename'));
                                            var data = record.get('doc_id');
                                            i++;// file_name++;
                                        }
                                    });

                                    if (i<1){
                                        Ext.MessageBox.show({title:'WARNING',msg:'Please select document to modify',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
                                    }else if(i>1){
                                        Ext.MessageBox.show({title:'WARNING',msg:'Please select less than 1 document to modify',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                    }else{// alert(file_name.length); console.log(file_name);
                                        if(file_name.length<=0){
                                           
                                            var f   = new Ext.form.Panel({
                                            layout  :'anchor',
                                            defaults:{anchor:'100%',labelWidth:130, allowBlank:false},
                                            items   :[
                                                {xtype:'filefield',name:'doc_file',fieldLabel:'File',msgTarget:'side'}
                                            ],
                                            bbar    :{
                                                items :[
                                                '->',
                                                {
                                                    text    :'Save Document',
                                                    ui      :'soft-green',
                                                    handler :function(){
                                                        var form = this.up('form').getForm();
                                                        if(form.isValid()){
                                                            form.submit({
                                                                url     :'../data/dependent_save_docs.php',
                                                                params  :{al_prin_id:r.data['al_prin_id'],al_dep_id:r.data['al_dep_id'], acc_id:r.data['dcompanyid'], doc_id:array_id},
                                                                waitMsg :'Please wait...',
                                                                method  :'POST',// timeout : 120000, //2minutes
                                                                headers :{'Content-Type':'multipart/form-data; charset=UTF-8'},
                                                                success:function(form,action){
                                                                    if(action.result.message=="ok"){
                                                                        
                                                                        var al_dep_id   = Ext.getCmp('dep_al_dep_id'+r.data['al_dep_id']);
                                                                        var dep_acc_id  = Ext.getCmp('dep_acc_id'+r.data['al_dep_id']);
                                                                        
                                                                        Ext.MessageBox.show({title:'Success', msg:'Success saving documents', closable:false, icon:Ext.Msg.INFO, buttonText:{ok:'OK'}});
                                                                        Ext.getCmp('dep-required-doc-'+r.data['activelink_id']).getStore().load({params:{dep_al_id:al_dep_id.getValue(),acc_id:dep_acc_id.getValue()}});
                                                                        console.log('Dependent document grid load');
                                                                        ad.close(); 
                                                                    }
                                                                },failure:function(form, action){
                                                                    Ext.MessageBox.show({title:'WARNING',msg:action.result.message,closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                                                }
                                                            });
                                                        }else{
                                                            Ext.MessageBox.show({title:'Failed', msg:'Failed saving document',icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}})
                                                        }
                                                    }
                                                }]
                                            }
                                            });
                                            var ad  = new Ext.Window({title:'Add/Replace Required Document',width:500,modal:true,layout:'fit',bodyPadding:5,items:[f]});
                                            ad.show();
                                        
                                        }else{
                                            Ext.MessageBox.show({title:'WARNING',msg:'Reset document first before you modify object',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                        }
                                    }}
                            },
                            {
                            text    :'Reset check document',
                            ui      :'soft-green',
                            handler :function(btn,grid, record, index, eOpts,rowIndex, colIndex){
                                var g = Ext.getCmp('dep-required-doc-'+r.data['activelink_id']).getStore();
                                // var al_dep_id   = Ext.getCmp('dep_al_dep_id'+r.data['al_dep_id']);var dep_acc_id = Ext.getCmp('dep_acc_id'+r.data['al_dep_id']);
                                var array_id = new Array();
                                var i = 0;   

                                g.each(function (record) {

                                if (record.get('active') === true) {
                                    array_id[i] = record.get('doc_id');
                                    console.log('Dependent check document: '+record.get('doc_id'));
                                    var data = record.get('doc_id');
                                    i++;
                                }
                                });

                                if(i<1){
                                    Ext.MessageBox.show({title:'WARNING',msg:'Please select document to reset',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                }else{
                                    Ext.Msg.confirm('Reset Document','Are you sure you want to reset dependent document/s', function(answer){
                                        if(answer==='yes'){
                                            Ext.Ajax.request({
                                                url     :'../data/get_dependent_document_reset.php',
                                                params  :{doc_id:Ext.encode(array_id), al_dep_id:r.data['al_dep_id'], acc_id:r.data['dcompanyid']},
                                                waitMsg :'Processing your request',
                                                success :function(){
                                                    Ext.MessageBox.show({title:'Success',msg:'Document reset success!',icon:Ext.Msg.INFO,closable:false,buttonText:{ok:'OK'}});
                                                    g.removeAll();
                                                    g.load({params:{dep_al_id:r.data['al_dep_id'],acc_id:r.data['dcompanyid']}});
                                                },
                                                failure :function(){
                                                    Ext.MessageBox.show({title:'Failed',msg:'Document reset failed. Please contact IT for your assistance',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                                }
                                            });
                                        }
                                    });                                
                                }
                            }
                            }],
                            items   :[
                            {
                                xtype        :'gridpanel',
                                plugins      :'gridfilters',
                                ui           :'light',
                                headerBorders:false,
                                rowLines     :true,
                                bodyPadding  :10,
                                id           :'dep-required-doc-'+r.data['activelink_id'],
                                viewConfig   :{preserveScrollOnRefresh:true,preserveScrollOnReload:true,deferEmptyText:true,emptyText:'<h1>No data found</h1>'},
                                store        :Ext.create("OMM.store.Get_dependent_documents"),
                                columns      :[
                                    {xtype: 'checkcolumn',dataIndex: 'active',width:30, locked:true},    // {xtype:'rownumberer'},
                                    {header:'Doc ID',dataIndex:'doc_id'},
                                    {header:'Doc name',dataIndex:'doc_name', flex:1},
                                    {header:'Status',dataIndex:'doc_stat', flex:1},
                                    {
                                    header      : 'File name',
                                    dataIndex   : 'fid',
                                    flex        : 1,
                                    renderer    : function (val, meta, record,view, cell, cellIndex, row, rowIndex, e) {
                                        return '<a href=https://www.benefitsmadebetter.com/member/activelink_admins/doc-files.php?fid='+ val  +'>' + record.get('filename') + '</a>';
                                    }},
                                    {header:'Date modified',dataIndex:'modified_on', flex:1}
                                ],
                                listeners   :{
                                    beforerender    :function(){// var al_dep_id   = Ext.getCmp('dep_al_dep_id'+r.data['al_dep_id']);var dep_acc_id = Ext.getCmp('dep_acc_id'+r.data['al_dep_id']);
                                        Ext.getCmp('dep-required-doc-'+r.data['activelink_id']).getStore().load({params:{dep_al_id:r.data['al_dep_id'],acc_id:r.data['dcompanyid']}});
                                    }// rowclick:function(grid,record,rowIndex,colIndex){alert(record.get('filename'))}
                                }
                            }]
                        }
                        ]
                    },
                    {
                        xtype       :'panel',
                        width       :'55%',
                        bodyPadding :5,
                        items       :[
                        {
                            xtype       :'fieldset',
                            columnWidth :0.5,
                            title       :'<b>Enrollment Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%',labelWidth:130, allowBlank:true},
                            layout      :'anchor',
                            items       :[
                                {fieldLabel:'DEPSETTINGSIDX',name:'dep_settings_idx',value:r.data['idx'],hidden:true},
                                {fieldLabel:'HMO ID',name:'hmo_id',value:r.data['hmo_id'],maxLength:50},
                                {fieldLabel:'Dependent type',xtype:'combo',name:'dep_type',value:r.data['dep_type'],reference:'dep_type', store:Ext.create("OMM.store.Get_deptype"),valueField:'dep_type',displayField:'dep_type',emptyText:'Select type',editable:false,listeners:{el:{click:'onDepType_change_dep_form'}}},
                                {xtype:'label',style:'color:#2f9fe9;font-weight:bold;font-style:oblique;font-face:arial',text:'Enrollment status will automatically set as Active if user fill the valid HMO ID'},                                
                                {fieldLabel:'Enrollment Status',flex:1,xtype:'combo',name:'status',reference:'dep_enroll_stat',store:Ext.create("OMM.store.Get_status"),value:r.data['status'],valueField:'status',displayField:'status',emptyText:'Select dependent status',editable:false},
                                {xtype:'checkbox',boxLabel:'Enrollment status option : Check if you want to update status of dependent',labelAlign:'right',labelStyle:'color:#2f9fe9;font-weight:bold;font-face:arial;font-size:12px',labelWidth:480,name:'dep_stat_opt',reference:'dep_stat_opt',checked:false,padding:'-10 0 0 0',inputValue:'1',uncheckedValue:'0',listeners:{change:'onDepChangeEnrollStat'}},
                                {fieldLabel:'Date of Enrollment', name:'date_of_enrollment', value:r.data['date_of_enrollment'], editable:false},
                                {fieldLabel:'Date Created', name:'last_update', value:r.data['datecreated'], editable:false},
                                {fieldLabel:'Last Updated On', name:'datecreated', value:r.data['last_update'], editable:false},
                                {fieldLabel:'Remarks',name:'remark',value:r.data['remark'],editable:false}
                            ]
                        },
                        {
                            xtype       :'fieldset',
                            title       :'<b>HMO Information</b>',
                            collapsible :true,
                            defaultType :'textfield',
                            defaults    :{anchor:'95%', labelWidth:160, allowBlank:true},
                            layout      :'anchor',
                            items       :[          
                                {
                                    xtype      :'form',
                                    reference  :'opt_plan',
                                    defaultType :'textfield',
                                    defaults    :{anchor:'100%', labelWidth:170, width:'100%'},
                                    items       :[                                    
                                        {fieldLabel:'Principal Member Status',name:'pmbr_stat',value:r.data['pmbr_stat'],editable:false},
                                        {fieldLabel:'Principal Plan (Level)',name:'pplan_id',value:r.data['pplan_id'],editable:false},
                                        {
                                            xtype       :'fieldset',
                                            title       :'<b>Plan Information</b>',
                                            defaultType :'textfield',
                                            hidden      :true,
                                            defaults    :{anchor:'100%', labelWidth:170, width:'100%'},
                                            layout      :'anchor',
                                            items       :[
                                                    {fieldLabel:'HMO Provider',name:'hospital_hmo',readOnly:true},
                                                    {fieldLabel:'Dependent Member Status',name:'dmbr_stat',value:r.data['dmbr_stat'],editable:false},
                                                    {fieldLabel:'level',name:'dplan_id',reference:'dep_planLevel',value:r.data['dplan_id'],editable:false},
                                                    {fieldLabel:'Plan Description',name:'plan_desc',readOnly:true,reference:'dep_plan_desc'},
                                                    {fieldLabel:'Benefit limit',name:'benefit_limit',readOnly:true},
                                                    {xtype:'label',style:'font-weight:bold;font-face:arial;margin-bottom:10px;',html:'NOTE : <span style="color:#e44959;font-weight:bold;font-face:arial;font-size:11px;font-style:oblique;">Advise to double check the benefit limit of the member. Value was based on Principal plan information</span>'},
                                                    {
                                                        xtype       :'panel',
                                                        reference   :'dep_opt_panel',
                                                        defaultType :'textfield',
                                                        defaults    :{anchor:'100%', labelWidth:160, width:'100%'},
                                                        items       :[
                                                            {fieldLabel:'Option',name:'opt',readOnly:true},
                                                            {xtype:'label',name:'opt_plan_desc', reference:'opt_plan_desc', style:'color:#2f9fe9;font-weight:bold;font-face:arial',html:'Option description : '}
                                                        ]
                                                    }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }]
                    }]
                }],
                listeners       :{
                    afterrender:function(){
                        var dep_enroll_stat  = this.lookupReference('dep_enroll_stat'),// var dep_enroll_stat  = Ext.getCmp('dep_enroll_stat'+r.data['al_dep_id']);
                            dep_prin_name    = this.lookupReference('dep_prin_name'),
                            dep_prin_emp_num = this.lookupReference('dep_prin_emp_num'),
                            opt_plan         = this.lookupReference('opt_plan'),
                            session_userid   = this.lookupReference('session_userid'),
                            opt_plan_desc    = this.lookupReference('opt_plan_desc'),
                            dep_opt_panel    = this.lookupReference('dep_opt_panel'),
                            dep_plandetails  = Ext.create('OMM.model.Dependent_plandetails_Model');

                        dep_enroll_stat.setDisabled(true);
                        myMask.hide();
                        Ext.Ajax.request({
                        url     :'../data/post_username.php',
                        success :function(response){
                            var response=Ext.decode(response.responseText);
                            session_userid.setValue(response.username);
                            console.log(session_userid.getValue());                            
                            Ext.Ajax.request({
                                url:'../data/get_dependent_plandetails.php',
                                params:{ops:session_userid.getValue(),dep_alid:r.data['al_dep_id'],acc_id:r.data['dcompanyid']},
                                success:function(response){
                                    var response=Ext.decode(response.responseText);                                    
                                    dep_plandetails.set(response.data[0]);
                                    opt_plan.getForm().loadRecord(dep_plandetails);
                                    opt_plan_desc.setHtml('Option description : '+response.data[0].opt_plan_desc);
                                    console.log('Dependent Plan details :'+opt_plan.getForm().getValues());
                                },failure:function(response){
                                    var response=Ext.decode(response.responseText);                                                                       
                                    opt_plan_desc.setHtml('Something went wrong. cant view plan description. Please contact your IT for assistance');

                                }
                            });
                        
                            if(r.data['dcompanyid']!='QBE'){
                                dep_opt_panel.hide();
                            }else{}
                        }
                        });

                        Ext.Ajax.request({
                            url:'../data/get_dependent_principal.php',
                            params:{al_prin_id:r.data['al_prin_id'],acc_id:r.data['dcompanyid']},
                            success:function(response){
                                var response=Ext.decode(response.responseText);
                                dep_prin_name.setValue(response.prin_name);dep_prin_emp_num.setValue(response.prin_emp_num);
                            }
                        });
                    }
                }
            });
            view.setActiveTab(cur_tab_id)}
    },

    //Button - close dependent note panel
    onCancelNote:function(btn){
        var me              = this,
            form            = btn.up('form').getForm(),
            dep_note_panel  = this.lookupReference('dep_note_panel');

        dep_note_panel.setCollapsed(true);
        form.reset();
    },

    //Button - submit dependent note
    onSubmitNote:function(btn){
        var me                    = this,
            form                  = btn.up('form').getForm(),
            values                = form.getValues(),
            OMM_logs_ops          = localStorage.getItem("OMM_logs_ops"),
            obj                   = JSON.parse(OMM_logs_ops),
            OMM_thread_notes      = localStorage.getItem("OMM_thread_notes"),
            obj2                  = JSON.parse(OMM_thread_notes),
            activity_note         = this.activity_note=Ext.create('OMM.store.Get_activity_note'),
            dep_note_text         = this.lookupReference('dep_note_text');

        console.log(values['al_dep_id']);
        var myMask = new Ext.LoadMask({
                msg    : 'posting...',
                target : btn.up('form')
        });
        myMask.show();

        if(dep_note_text.getValue()!='' || dep_note_text.getValue()!=null){
            Ext.Ajax.request({
                url:'../data/activity_post_note.php',
                params:{activelink_id:values['al_dep_id'],acc_id:values['dcompanyid'], ops:obj, note:dep_note_text.getValue()},
                success:function(){
                    activity_note.removeAll();
                    activity_note.getProxy().extraParams = {activelink_id:values['al_dep_id'],acc_id:values['dcompanyid'], ops:obj};
                    activity_note.load({
                    callback: function(records, operation, success) {
                        var total_count   = records.length,
                            rec           = records;
                        dep_note_text.setValue('');
                        myMask.hide();
                        me.dep_mem_logs_AjaxRequest_getId(id, total_count,rec);
                    }
                    });
                },failure:function(){
                    Ext.MessageBox.show({title:'WARNING', msg:'Undifined', icon:Ext.msg.WARNING, buttonText:{ok:'OK'}})
                }
            });
        }else{
            Ext.MessageBox.show({title:'WARNING', msg:'User is advise to atleast input any text', icon:Ext.Msg.WARNING});
            myMask.hide();
        }
    }

});
