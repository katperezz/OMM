Ext.define('OMM.view.dependent.Dependent_details',{
    extend     : 'Ext.form.Panel',
    title      : 'Dependent Details',
    xtype      : 'dependent_details',
    requires   :['OMM.view.dependent.DependentController'],
    controller :'dependent',

    closable   :true,
    closeAction:'hide',
    layout     :'fit',
    bodyPadding:5,
    defaults   : {anchor: '100%'},
    bbar        :{
        overflowHandler     :'menu',
        items               :[
         {
            xtype           :'panel',
            defaults        :{anchor:'100%',labelWidth:100},
            titleCollapse   :true,
            collapsible     :true,
            collapsed       :true,
            animate         :true,
            border          :true,
            hideCollapseTool: true,
            layout          :'anchor',
            bodyPadding     :5,
            dock            : 'bottom',
            width           :'100%',
            title           :'<b>Notes/Logs</b>',
            reference       :'dep_note_panel',
            items           :[
            {
                xtype       :'form',
                height      :300,
                overflowY   :'scroll',
                reference   :'dep_note_thread',

                defaults    :{border:false,autoHeight:true,collapsed:false,titleCollapse:false,hideCollapseTool:true,ui:'thread-panel-action',bodyStyle:{"background-color":"#f4f3d7","padding":"10px"}}
            },
            {
                xtype      : 'textareafield',
                margin     : '15 10 10 10',
                reference  :'dep_note_text',
                grow       : true,
                name       : 'note',
                fieldLabel : 'Note',
                width      :'100%',
                flex       :1
            }],
            bbar    :[
            {
                text    :'Send',
                ui      :'soft-blue',
                handler :'onSubmitNote'
            },{
                text    :'Cancel',
                handler :'onCancelNote'
            }]
        }
        ]
    },
    dockedItems   :[
    {
        xtype :'toolbar',
        dock  :'bottom',
        items :[        
        {
            xtype     :'form',
            layout    :'hbox',
            defaults  :{labelWidth:120,style:'margin-right:15px;font-weight:bold;',anchor:'100%',fieldStyle:'color:#2990A8;font-weight:bold;font-face:arial;'},
            items     :[{xtype:'displayfield',name:'prin_name',reference:'dep_prin_name',
            fieldLabel:'<span style="font-face:arial;">Principal name</span>'},{xtype:'displayfield',name:'prin_emp_num',reference:'dep_prin_emp_num',fieldLabel:'<span style="font-face:arial;">Employee number</span>'}]
        },
        '->',
        {
            text         :'HMO Deletion',
            reference    :'dep_del',
            ui           :'soft-red',
            handler      :function(){
                var form        = this.up('form').getForm();
                var values      = form.getValues();
                var view        = Ext.getCmp("ops_tabpanel");
                var cur_tab_id  = "dep"+values['al_dep_id'];
                var g           = Ext.getCmp('dependent_grid');
                var prin_d_grid     = "prin"+values['al_prin_id'];

                Ext.Msg.confirm('HMO Deletion','Are you sure you want to proceed on this process? Dependent HMO STATUS will be "INACTIVE"',function(answer){
                    if(answer=='yes'){
                        if(values['al_dep_id']==''|| values['al_dep_id']==null || values['fname']=='' || values['fname']==null || values['lname']=='' || values['lname']==null){
                            Ext.Msg.confirm({title:'WARNING', msg:'Cant delete dependent if Activelink ID, First and Last Name is null or empty', icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}})
                        }else{
                            var waitMsg = Ext.MessageBox.wait('Processing your request. Please wait...');
                            Ext.Ajax.request({
                                url             :'../data/dep_single_del.php',
                                params          :{dep_al_id:values['al_dep_id'], session_userid:values['session_userid'],fname:values['fname'], lname:values['lname']},
                                method          :'POST',
                                submitEmptyText :false,
                                success         :function(response){
                                    var response = Ext.decode(response.responseText);
                                    if(response.success==true){
                                        waitMsg.hide();
                                        view.remove(cur_tab_id);
                                        Ext.MessageBox.show({title:'SUCCESS', msg:response.message, icon:Ext.Msg.INFO, closable:false, buttonText:{ok:'OK'}});
                                        // g.store.load();
                                        if(g){
                                            g.store.load();
                                        }else{
                                            // console.log(values['al_prin_id']);
                                            Ext.getCmp(prin_d_grid).down('#prin_dep_grid_p').getStore().load({params:{prin_al_id:values['al_prin_id']}});

                                        }
                                    }else{
                                        waitMsg.hide();
                                        Ext.MessageBox.show({title:'WARNING', msg:response.message, icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                                    }
                                },failure       :function(response){
                                    waitMsg.hide();
                                    Ext.MessageBox.show({title:'WARNING', msg:'Something went wrong. Please contact your IT for assistance',icon:Ext.Msg.WARNING, closable:false, buttonText:{ok:'OK'}});
                                }
                            });
                        }
                    }
                });
            }
        },
        {
            // text      :'Reset Enrolled Dependent',
            text      :'Enrollment Reset',
            reference :'dep_reset',
            ui        :'soft-red',
            handler   :function(){
                var form        = this.up('form').getForm(),
                    values      = form.getValues(),
                    view        = Ext.getCmp("ops_tabpanel"),
                    cur_tab_id  = "dep"+values['al_dep_id'],
                    g           = Ext.getCmp('dependent_grid'),
                    prin_d_grid = "prin"+values['al_prin_id'];


                console.log('dep_reset : '+prin_d_grid);

                Ext.Msg.confirm('Enrollment Reset','Are you sure you want to reset the enrollment? Dependent records will be DELETED on the system',function(answer){
                    if(answer=='yes'){
                       if(values['al_dep_id']=='' || values['dcompanyid']==''){
                            Ext.MessageBox.show({title:'WARNING',msg:'Empty Employee number/ Company ID',closable:false,icon:Ext.Msg.WARNING,buttons:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                       }else{
                            var waitMsg = Ext.MessageBox.wait('Processing your request. Please wait...');
                            Ext.Ajax.request({
                                url             :'../data/get_dependent_reset.php',
                                params          :{al_dep_id:values['al_dep_id'],dcompanyid:values['dcompanyid']},
                                method          :'POST',
                                submitEmptyText :false,
                                success         :function(response){
                                    var response = Ext.decode(response.responseText);
                                    if(response.success==true){
                                        waitMsg.hide();
                                        console.log('load dependent details grid');
                                        view.remove(cur_tab_id);
                                        Ext.MessageBox.show({title:'Success',msg:response.message,icon:Ext.Msg.INFO,closable:false,buttonText:{ok:'OK'}});
                                        if(g){
                                            g.store.load();
                                        }else{
                                            console.log(values['al_prin_id']);
                                            Ext.getCmp(prin_d_grid).down('#prin_dep_grid_p').getStore().load({params:{prin_al_id:values['al_prin_id']}});

                                        }

                                    }else{                
                                        waitMsg.hide();                        
                                        Ext.MessageBox.show({title:'Failed',msg:response.message,closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                    }
                                },
                                failure     :function(form,action,response){
                                    waitMsg.hide();
                                    Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please contact your IT for assistance',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                }
                            });
                        } 
                    }
                });
            }
        },
        {
            text        :'Save Changes',
            reference   :'dep_update',
            ui          :'soft-red',
            handler     :function(){

                var form            = this.up('form').getForm();
                var values          = form.getValues();
                var value           = form.getFieldValues();
                var fields          = form.getFields();
                var view            = Ext.getCmp("ops_tabpanel");
                var cur_tab_id      = "dep"+values['al_dep_id'];
                var g               = Ext.getCmp('dependent_grid');
                var status          = form.findField('status').getValue();
                var prin_d_grid     = "prin"+values['al_prin_id'];

                Ext.Msg.confirm('Update Member','Are you sure you want to update members information',function(answer){
                    if(answer=='yes'){

                        Ext.each(fields.items, function (f) {
                            var field = f.getName(),
                                value = f.getValue();
                            if(field=='philhealth' && value=='-' || field=='hmo_id' && value=='-'){
                                console.log('field :'+field +' value : '+value);
                            }else{
                                if(value=='-'){
                                    console.log('Field with - set blank on sumbit :'+field);
                                    form.findField(field).setValue('');
                                }
                            }
                        });

                        if(form.isValid()){
                            // +'\nhmo_id :'+values['hmo_id']
                            console.log('status :'+status);
                            if(status == 'Active' && values['hmo_id']  =='' || status == 'active' && values['hmo_id']  ==''){
                                Ext.MessageBox.show({title:'WARNING',msg:'Active status requires HMO id number', closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                            }else if(status == 'Active' && values['hmo_id'] =='-' || status == 'active' && values['hmo_id'] =='-'){
                                Ext.MessageBox.show({title:'WARNING',msg:'Active status requires HMO id number',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                            }else if(status =='Active' || status =='active' && values['hmo_id'].length<=4){
                                Ext.MessageBox.show({title:'WARNING',msg:'The minimum length for the HMO ID field is 5',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                            }else if(status == 'Active' || status =='active' && values['hmo_id'].startsWith("-")){
                                Ext.MessageBox.show({title:'WARNING',msg:'System will not accept this value "-1234". Please check if HMO ID field is valid',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});                        
                            }else if(status == 'For endorsement' && values['hmo_id'] !=''){
                                Ext.MessageBox.show({title:'WARNING',msg:'Cannot set member for endorsement if HMO ID field is not null or empty',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});                            
                            }else if(status =='Endorse to HMO' && value['hmo_id'] !='' || status == 'Endorse to HMO' && values['hmo_id'] !=null){
                                Ext.MessageBox.show({title:'WARNING', msg:'Cannot set member Endorse to HMO if HMO ID field is not null or empty', closable:false, icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
                            }else{
                                form.submit({
                                    url             :'../data/dependent_update.php',
                                    waitMsg         :'Processing your request. Please wait...',
                                    timeout         : 120000, //2minutes
                                    method          :'POST',
                                    submitEmptyText :false,
                                    success         :function(form,action){
                                        view.remove(cur_tab_id);
                                        console.log('Dependent update success');
                                        Ext.MessageBox.show({title:'Success',msg:'Dependent update saved',icon:Ext.Msg.INFO,closable:false,buttonText:{ok:'OK'}});
                                        if(g){
                                            g.store.load();
                                        }else{
                                            // console.log(values['al_prin_id']);
                                            Ext.getCmp(prin_d_grid).down('#prin_dep_grid_p').getStore().load({params:{prin_al_id:values['al_prin_id']}});
                                        }
                                    },failure:function(form,action){
                                        console.log('Dependent update failed on PHP');
                                        Ext.MessageBox.show({title:'Failed',msg:'Dependent update failed',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
                                    }
                                });                            
                            }
                        }else{
                            console.log('Dependent update failed');
                            console.log(values)
                            Ext.MessageBox.show({title:'WARNING',msg:'Complete required fields',closable:false,icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                        }
                    }
                });
            }
        }]
    }],
    listeners :{        
        afterrender :'onAfterrenderDepTab'
        // beforerender:'onBeforerenderDepTab'
    }
});
