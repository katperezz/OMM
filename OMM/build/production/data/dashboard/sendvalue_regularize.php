<?php

session_start();
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 2400);//40 minutes

date_default_timezone_set('Asia/Manila');

$date = date('Y-m-d h:i');



include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/AllClassReq.php');

include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/Notification.php');
$notification = new Notification();


if(isset($_POST['emp_no']) && isset($_SESSION['regularization_session'])){

    $emparray = json_decode($_POST['emp_no']);


        if(in_array(null, $emparray) || in_array("", $emparray) || empty($emparray)){

                echo $jsonmsg->JmsgFail("ERROR : No Account / Employee Selected");
        } else{

                $query="UPDATE mastermembertable set regularization_status='regular', date_regularization='".$date."' where regularization_status ='probationary'";
                $query2 = "INSERT INTO regularization_logs (USER,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus) SELECT '".$_SESSION['username']."',emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus FROM mmdb_".$_SESSION['regularization_session'].".mastermembertable WHERE regularization_status ='probationary'";

        

                foreach($emparray as $row){

                         $query="UPDATE mastermembertable set regularization_status='regular', date_regularization='".$date."' where emp_no = '".$row."'";

                          $q = "select date_est_regularization from mastermembertable where emp_no = '".$row."'";

                            // $fregarr = 0;
                            // $regarr = 0;

                            foreach($dbquery->DbSelect($_SESSION['regularization_session'],$q) as $r){

                                            $datereg = new DateTime($r['date_est_regularization']);
                                            $dnow = new DateTime('now');

                                            if($datereg >  $dnow){
                                                     
                                                    $query2 = "INSERT INTO force_regular_logs (USER,activelink_id,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus) SELECT '".$_SESSION['username']."',activelink_id,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus FROM mmdb_".$_SESSION['regularization_session'].".mastermembertable WHERE emp_no = '".$row."' ";
                                                    $dbquery->Dbsqlquery($_SESSION['regularization_session']."_logs",$query2);
                                                   
                                            }
                              
                            }

                             $query3 = "INSERT INTO regularization_logs (USER,activelink_id,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus) SELECT '".$_SESSION['username']."',activelink_id,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus FROM mmdb_".$_SESSION['regularization_session'].".mastermembertable WHERE emp_no = '".$row."' ";


                            $dbquery->Dbsqlquery($_SESSION['regularization_session'],$query);
                            $dbquery->Dbsqlquery($_SESSION['regularization_session']."_logs",$query3);



                         // $query2 = "INSERT INTO regularization_logs (USER,activelink_id,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus) SELECT '".$_SESSION['username']."',activelink_id,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus FROM ".$_SESSION['regularization_session'].".mastermembertable WHERE emp_no = '".$row."' AND date_est_regularization < NOW()";
                         // $query3 = "INSERT INTO force_regular_logs (USER,activelink_id,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus) SELECT '".$_SESSION['username']."',activelink_id,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus FROM ".$_SESSION['regularization_session'].".mastermembertable WHERE emp_no = '".$row."' AND date_est_regularization > NOW()";
                         // $query4 = "INSERT INTO regularization_logs (USER,activelink_id,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus) SELECT '".$_SESSION['username']."',activelink_id,emp_no,sss_no,phil_no,lastname,firstname,middlename,ext,gender,dob,maritalstatus FROM ".$_SESSION['regularization_session'].".mastermembertable WHERE emp_no = '".$row."' AND date_est_regularization = NOW()";


                         // $dbquery->Dbsqlquery($_SESSION['regularization_session'],$query);
                         // $dbquery->Dbsqlquery($_SESSION['regularization_session']."_logs",$query2); // ABOVE ESTEMATED REGULARIZATION
                         // $dbquery->Dbsqlquery($_SESSION['regularization_session']."_logs",$query3); // BELOW ESTEMATED REGULARIZATION
                         // $dbquery->Dbsqlquery($_SESSION['regularization_session']."_logs",$query4); // equal ESTEMATED REGULARIZATION

                    }

            // $des = ($fregarr == 0) ? "" : $fregarr." employee force regular";        
            $arrc = count($emparray);       
            $detail = implode(",",$emparray);      
            $description = $arrc." employee change to regular by ".$_SESSION['username'];

            $notification->InsertNotification($_SESSION['regularization_session'],$_SESSION['username'],$detail,$description," change to regular",date('Y-m-d h:i:s'));
    
    	          echo $jsonmsg->JmsgSucc("Success");
        }

}else{
        echo $jsonmsg->JmsgFail("ERROR : No Account / Employee Selected");


}





?>