<?php

session_start();
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/AllClassReq.php');


$emp_no = isset($_POST['emp_no']) ? $_POST['emp_no'] : "";




try{

	if(!isset($_SESSION['username']) || empty($_SESSION['username'])){

			throw new Exception("SESSION EXPIRED");
	}

	if(!isset($_SESSION['totalcountlist_db']) || empty($_SESSION['totalcountlist_db'])){

			throw new Exception("SESSION EXPIRED");
	}


	if($deletion->DeleteEmployee($_SESSION['totalcountlist_db'],$emp_no,$_SESSION['username'])){

				$deletion->DeleteEmployeeLogs($_SESSION['totalcountlist_db'],$emp_no,$_SESSION['username']);	

				echo $jsonmsg->JmsgSucc("employee successfully deleted");

	}else{

			throw new Exception("error: employee unsuccessfully delete");

	}



}catch(Exception $e){

	echo $jsonmsg->JmsgFail($e->getMessage());

}

?>