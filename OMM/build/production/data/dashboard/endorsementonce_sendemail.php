<?php

ini_set ( 'max_execution_time', 30000);
session_start();



include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/phpexcelpath.php');

include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/SendMail.php');

include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/AllClassReq.php');

include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/Notification.php');
$notification = new Notification();


$SendMail = new SendMail();
// include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/RandomId.php');
// $randomid = new RandomId();
// include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/JsonMsg.php');
// $jsonmsg = new JsonMsg();
// include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/DbQuery.php');
// $dbquery = new DbQuery();



if(isset($_POST['email']) && isset($_POST['subj']) && isset($_POST['msg'])){

    $emailadd = str_replace('"','',$_POST['email']);
    $subj = str_replace('"','',$_POST['subj']);
    $body = str_replace('"','',$_POST['msg']);

    $cc = isset($_POST['cc']) ? json_decode($_POST['cc']) : array();


    
            $dbname = $_SESSION['endorsement_session'];
            $db ="mmdb_".$dbname;
            include('db_connPDO.php'); 


           
            


    function cellColor($cells,$color){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => $color),
                                'font'  => array(
                                                    'bold'  => true,
                                                    'color' => array('rgb' => 'FF0000'),
                                                    'size'  => 9,
                                                    'name'  => 'Verdana'
                                                )
        
                                )
                            );
    }

 



$s =  "SELECT * from mastermembertable where remark ='NOT ENDORSED' and emp_no='".$_SESSION['emp_no']."' and  regularization_status!='deleted'";


$objPHPExcel = new PHPExcel(); 
$objPHPExcel->setActiveSheetIndex(0); 
$rowCount = 6; 

date_default_timezone_set('Asia/Manila');
$today = date("F j, Y, g:i a"); 
 $date = date("Y-m-d h:i");

$objPHPExcel->getActiveSheet()->SetCellValue('A1', "ActiveLink - Masterlist Report");
$objPHPExcel->getActiveSheet()->SetCellValue('A2', "Account Name");
$objPHPExcel->getActiveSheet()->SetCellValue('B2',$dbname);
$objPHPExcel->getActiveSheet()->SetCellValue('B3',$today);
$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Date Created");
cellColor('A1', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('A5', "Employee Number");
cellColor('A5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('B5', "SSS_no"); 
cellColor('B5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('C5', "Philhealth_no"); 
cellColor('C5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('D5', "HMO Number"); 
cellColor('D5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('E5', "HMO_no"); 
cellColor('E5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('F5', "Sub Office"); 
cellColor('F5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('G5', "Sub Office Code"); 
cellColor('G5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('H5', "Site"); 
cellColor('H5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('I5', "Lastname"); 
cellColor('I5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('J5', "Firstname"); 
cellColor('J5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('K5', "Middlename"); 
cellColor('K5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('L5', "Extension_name"); 
cellColor('L5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('M5', "Date of Birth"); 
cellColor('M5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('N5', "Age"); 
cellColor('N5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('O5', "Gender"); 
cellColor('O5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('P5', "Marital Status"); 
cellColor('P5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('Q5', "Job Level"); 
cellColor('Q5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('R5', "Job Description"); 
cellColor('R5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('S5', "HMO Level"); 
cellColor('S5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('T5', "Room on Board"); 
cellColor('T5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('U5', "Eligibility"); 
cellColor('U5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('V5', "Date to Endorse"); 
cellColor('V5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('W5', "Effective Date"); 
cellColor('W5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('X5', "Date Hire"); 
cellColor('X5', 'CCEEFF');
$objPHPExcel->getActiveSheet()->SetCellValue('Y5', "Remarks"); 
cellColor('Y5', 'CCEEFF');


        $qselect = $conn->query($s);
        
        while($row = $qselect->fetch(PDO::FETCH_ASSOC)){    
    
  
                   $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$row['emp_no']);     
                   $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$row['hmo_no']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,$row['phil_no']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,$row['sss_no']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,$row['hmo_no']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,$row['suboffice']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,$row['subofficecode']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,$row['site']);         
                   $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount,$row['lastname']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount,$row['firstname']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount,$row['middlename']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount,$row['ext']);         
                   $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount,$row['dob']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount,$row['age']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount,$row['gender']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount,$row['maritalstatus']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount,$row['joblevel']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount,$row['job_desc']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount,$row['hmolevel']); 
                   $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount,$row['emp_rom']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount,$row['emp_eligibility']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount,$row['dateemp_eligibility']);  
                   $objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount,$row['effectivedate']);      
                   $objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount,$row['datehire']);
                   $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount,$row['remark']);  
   
    
            $rowCount++; 
    } 

    $fname = "Copy_of_".$dbname."_EndorsementToSend.xlsx";
    $fpath = $_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/TempFiletoSend/'.$dbname.$randomid->RanId().'.xlsx';


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
$objWriter->save($fpath);


// $logsfile->DLfile($dbname."_logs",$_SESSION['username'],$fname,"endorsement excel download");

        if(!file_exists($fpath)){


                echo $jsonmsg->JmsgFail("Sending Fail : File Attached Corrupted");  

        }else{

                if($SendMail->SendMailExcel($emailadd,$subj,$body,$fpath,$cc)){

                    $sql = "UPDATE mastermembertable SET remark = 'ENDORSED' WHERE emp_no = '".$_SESSION['emp_no']."'";

                    $query2 = "INSERT INTO endorsement_logs (user,file_id,activelink_id,emp_no,sss_no,phil_no,hmo_no,lastname,firstname,middlename,gender,dob,maritalstatus,category,hmolevel,site,effectivedate,datehire,joblevel,suboffice,subofficecode,ext,job_desc,emp_eligibility,dep_eligibility,emp_rom,dep_rom,emp_amount,dep_amount,dateemp_eligibility,datedep_eligibility,remark,date_created,date_regularization,regularization_status,endorsement_datetime) 
                   SELECT '".$_SESSION['username']."',file_id,activelink_id,emp_no,sss_no,phil_no,hmo_no,lastname,firstname,middlename,gender,dob,maritalstatus,category,hmolevel,site,effectivedate,datehire,joblevel,suboffice,subofficecode,ext,job_desc,emp_eligibility,dep_eligibility,emp_rom,dep_rom,emp_amount,dep_amount,dateemp_eligibility,datedep_eligibility,remark,date_created,date_regularization,regularization_status,'".$date."'
                            FROM mmdb_".$_SESSION['endorsement_session'].".mastermembertable WHERE emp_no = '".$_SESSION['emp_no']."'";

                  $query3 = "INSERT INTO force_endorsed_logs (user,activelink_id,emp_no, dep_no, hmo_no, lastname, firstname, middlename, ext, gender, dob, age, maritalstatus, category, hmolevel, site, effectivedate, datehire, idreleaseddate, date_of_deactivation, joblevel, suboffice, subofficecode, job_desc, emp_eligibility, dep_eligibility, dateemp_eligibility, datedep_eligibility, emp_rom, dep_rom, emp_amount, dep_amount,sss_no, phil_no, remark,  date_est_regularization,date_created,date_hmoid_upload,date_regularization,regularization_status, rule_name) 
                       SELECT '".$_SESSION['username']."',activelink_id,emp_no, dep_no, hmo_no, lastname, firstname, middlename, ext, gender, dob, age, maritalstatus, category, hmolevel,site, effectivedate, datehire,  idreleaseddate, date_of_deactivation, joblevel, suboffice, subofficecode, job_desc, emp_eligibility, dep_eligibility, dateemp_eligibility, datedep_eligibility, emp_rom, dep_rom, emp_amount, dep_amount, sss_no, phil_no, remark, date_est_regularization,date_created,date_hmoid_upload,date_regularization,regularization_status, rule_name
                      FROM mmdb_".$_SESSION['endorsement_session'].".mastermembertable WHERE emp_no = '".$_SESSION['emp_no']."' AND dateemp_eligibility > NOW()";


                     $dbquery->Dbsqlquery($_SESSION['endorsement_session'],$sql); 
                     $dbquery->Dbsqlquery($_SESSION['endorsement_session']."_logs",$query2);
                     $dbquery->Dbsqlquery($_SESSION['endorsement_session']."_logs",$query3);

                     // $activity_logs->SendFileLogs($_SESSION['endorsement_session']."_logs",$_SESSION['username'],"single endorsement send to ".$emailadd,$fpath); 
                     $detail = $_SESSION['emp_no'];
                     $description = "1 employee send to be endorsed by ".$_SESSION['username'];

                     $activity_logs->SendFileLogs($_SESSION['endorsement_session']."_logs",$_SESSION['username'],$detail,$description,"send endorsement",$emailadd,"sent");

                     $notification->InsertNotification($_SESSION['endorsement_session'],$_SESSION['username'],$detail,$description," send endorsement",date('Y-m-d h:i:s'));

                   echo $jsonmsg->JmsgSucc("Message has been sent");

                }else{

                      echo $jsonmsg->JmsgFail("Sending Fail : Internal Error");
                       
                }

        }


}else{


  echo "EMPTY VALUE";
}



?>