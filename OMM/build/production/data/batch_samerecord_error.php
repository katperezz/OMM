<?php
session_start();
header("Content-type: application/json");
include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/AllClassReq.php');


$d = array(
    array(
        "id"=>1,
        "activelink_id"=>1,
        "empid"=>"Perez",
        "branch_name"=>"Perez",
        "lname"=>"Perez",
        "fname"=>"Perez",
        "mname"=>"Perez",
        "birthdate"=>"08-29-1994",
        "extname"=>"Perez",
        "gender"=>"Perez",
        "civil_status"=>"Perez",
        "job_desc"=>"Perez",
        "plan_id"=>"Perez",
        "company_id"=>"Perez",
        "branch_id"=>"Perez",
        "branch"=>"Perez",
        "mbr_status"=>"Perez"
        ),
    array(
        "id"=>2,
        "activelink_id"=>2,
        "empid"=>"Perez",
        "branch_name"=>"Perez",
        "lname"=>"BONSOL",
        "fname"=>"Perez",
        "mname"=>"Perez",
        "birthdate"=>"08-29-1994",
        "extname"=>"Perez",
        "gender"=>"Perez",
        "civil_status"=>"Perez",
        "job_desc"=>"Perez",
        "plan_id"=>"Perez",
        "company_id"=>"Perez",
        "branch_id"=>"Perez",
        "branch"=>"Perez",
        "mbr_status"=>"Perez"
        ),
    array(
        "id"=>3,
        "activelink_id"=>3,
        "empid"=>"Perez",
        "branch_name"=>"Perez",
        "lname"=>"LISBOA",
        "fname"=>"Perez",
        "mname"=>"Perez",
        "birthdate"=>"08-29-1994",
        "extname"=>"Perez",
        "gender"=>"Perez",
        "civil_status"=>"Perez",
        "job_desc"=>"Perez",
        "plan_id"=>"Perez",
        "company_id"=>"Perez",
        "branch_id"=>"Perez",
        "branch"=>"Perez",
        "mbr_status"=>"Perez"
        ),
    );

    $jsonresponse->success = true;
    $jsonresponse->message ="YEY";
    $jsonresponse->total = "0";
    $jsonresponse->data = $d;

    echo json_encode($jsonresponse);    

?>