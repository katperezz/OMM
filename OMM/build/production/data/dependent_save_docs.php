<?php

include ($_SERVER['DOCUMENT_ROOT'].'/devmod5/ActiveBmb/Class/AllClassReq.php');

ini_set('upload_max_size','20M');
ini_set('post_max_size','25M');

function FileError($errinfo){


         switch ($errinfo) { 
            case UPLOAD_ERR_INI_SIZE: 
             // $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini"; 
                $message = "The uploaded file exceeds the upload max filesize"; 
                break; 
            case UPLOAD_ERR_FORM_SIZE: 
                // $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form"; 
                $message = "The uploaded file exceeds the upload max filesize"; 
                break; 
            case UPLOAD_ERR_PARTIAL: 
                $message = "The uploaded file was only partially uploaded"; 
                break; 
            case UPLOAD_ERR_NO_FILE: 
                $message = "No file was uploaded"; 
                break; 
            case UPLOAD_ERR_NO_TMP_DIR: 
                $message = "Missing a temporary folder"; 
                break; 
            case UPLOAD_ERR_CANT_WRITE: 
                $message = "Failed to write file to disk"; 
                break; 
            case UPLOAD_ERR_EXTENSION: 
                $message = "File upload stopped by extension"; 
                break; 

            default: 
                $message = "Unknown upload error"; 
                break; 
        } 
        return $message; 
}


try{

if(!isset($_FILES['doc_file'])){

        throw new Exception("File is not set");

}
 

     if ($_FILES['doc_file']['error'] === UPLOAD_ERR_OK) { 


     $trim = '\\/:*?"<> |%#$^!():[]=_+{}&@';
        
     $_SESSION['fileid'] = $trim.$randomid->RanId();

     $inputfile = $_FILES['doc_file']['tmp_name'];
     $inputname = $_FILES['doc_file']['name'];
     $chfile = pathinfo($inputname);
     $chext = $chfile['extension'];

     $_SESSION['upfilename'] = $chfile['filename']; 


      $dnow = strtotime(date('Y-m-d h:i:s'));



        if($inputfile == NULL || $inputfile == false){

            throw new Exception("Error File");

        }elseif($chext == 'csv' || $chext == 'xls' || $chext == 'xlsx' ){

                 


            $infoname = pathinfo($inputname);

            $filename = $infoname['filename'];    

            $info = pathinfo($inputfile);
            $ext = $info['extension']; 
            $newname = $_SESSION['fileid'].".".$ext; 

            // $_SESSION['target'] = '../membership/TempExcel/'.$newname;
            // move_uploaded_file( $inputfile, $_SESSION['target']);

            // $sql = "insert into uploadpath (file_id,user,pathvalue) VALUE ('".$_SESSION['fileid']."','".$_SESSION['username']."','".$_SESSION['target']."')";


            // $dbquery->Delpathtempdata($_SESSION['inclusion_db'],$_SESSION['username']);
        
            echo $jsonmsg->JmsgSucc("ok");

            $_SESSION['uploadflag'] = 1;

        }else{
            
            throw new Exception("Invalid File");
        }

    } else { 

        $err = FileError($_FILES['doc_file']['error']);
        throw new Exception($err);
            
    } 

}catch(Exception $e){

            // $err = "Message: ".$e->getMessage()."\n File: ".$e->getFile()." Line: " . $e->getLine()."\n Trace: ".$e->getTraceAsString(); 
            // catcherror_log($err);


            echo $jsonmsg->JmsgFail($e->getMessage());


}

?>
