<?php
session_start();


// $d = array("inclusion"=>"110","update"=>"225","deletion"=>"16","reset"=>"40","reactivation"=>"5");
$d =array(
	// array("field_name"=>"last_name","old"=>"perez","new"=>"bonsol"),
	// array("field_name"=>"last_name","old"=>"perez","new"=>"bonsol")
	array(
		"id"=>34843,
		"activelink_id"=>"A000A24059",
		"modified_by"=>"tracy.remoquillo@activelink-consult.com",
		"modified_date"=>"2016-06-14 15:30:22",

		"empid_o"=>"ALdasdasdas16",
		"empid_n"=>"-",

		"fname_o"=>"TRACY",
		"fname_n"=>"TRACY",

		"mname_o"=>"DIOCARES",
		"mname_n"=>"DIOCARES",

		"lname_o"=>"REMOQUILLO",
		"lname_n"=>"REMOQUILLO",

		"ename_o"=>"",
		"ename_n"=>"",

		"birthdate_o"=>"1990-11-03 00:00:00",
		"birthdate_n"=>"1990-11-03 00:00:00",

		"gender_o"=>"F",
		"gender_n"=>"Fe",

		"civil_status_o"=>"Single",
		"civil_status_n"=>"Single",

		"philhealth_o"=>"123456789",
		"philhealth_n"=>"123456789",

		"sss_o"=>"123456789",
		"sss_n"=>"123456789",

		"pagibig_o"=>"123456789",
		"pagibig_n"=>"123456789",

		"unified_id_o"=>"123456789",
		"unified_id_n"=>"123456789",

		"plan_id_o"=>"R1",
		"plan_id_n"=>"R1",

		"branch_id_o"=>"",
		"branch_id_n"=>"",

		"mbr_status_o"=>"ACTIVE",
		"mbr_status_n"=>"ACTIVE",

		"hmo_id_o"=>"312312312",
		"hmo_id_n"=>"3123123",

		"designation_o"=>"Employee",
		"designation_n"=>"Employee",

		"tin_o"=>"000-000-000",
		"tin_n"=>"000-000-000",


		"current_effective_date_o"=>"1990-11-03 00:00:00",
		"current_effective_date_n"=>"1990-11-03 00:00:00",

		"orig_effective_date_o"=>"2015-11-17",
		"orig_effective_date_n"=>"2015-11-17",

		"maturity_date_o"=>"2014-06-28",
		"maturity_date_n"=>"2014-06-28",

		"emp_hire_date_o"=>"0000-00-00 00=>00=>00",
		"emp_hire_date_n"=>"0000-00-00 00=>00=>00",

		"hospital_hmo_o"=>"HMO_PHILCARE",
		"hospital_hmo_n"=>"HMO_PHILCARE",
		
		"dental_hmo_o"=>"HMO_1WORLD",
		"dental_hmo_n"=>"HMO_1WORLD"
		
		// "dep_status_o"=>"HMO_1WORLD",
		// "dep_status_n"=>"dsadas",
		
		// "relationship_o"=>"HMO_1WORLD",
		// "relationship_n"=>"HMO_1WORLD2",

		// "dep_type_o"=>"HMO_1WORLD",
		// "dep_type_n"=>"dasdas"

		)
	);

$msg = array('success' => true,'message'=>'account status success','data'=>$d);
// echo $_POST['totalcountlist_db'];     
echo json_encode($msg);


?>