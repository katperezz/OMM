var Get_gender = Ext.create('Ext.data.Store', {
    fields  :['gender','description'],
    autoLoad : true,
    proxy    : {
        type    : 'ajax',
        url     :'../data/get_gender.php',
        reader : {
            type         : 'json',
            rootProperty : 'data'
        }
    }
});
var Get_civilstatus = Ext.create('Ext.data.Store', {
    fields  :['civil_status','description'],
    autoLoad : true,
    proxy    : {
        type    : 'ajax',
        url     :'../data/get_civilstatus.php',
        reader : {
            type            :'json',
            rootProperty    :'data',
            successProperty :'success',
            messageProperty :'message'
        }
    }
});
var Get_account_branchid = Ext.create('Ext.data.Store', {
    fields  :['branch_id','branch'],
    autoLoad : true,
    proxy    : {
        type    : 'ajax',
        url     :'../data/get_account_branchid.php',
        extraParams     : {
            acc_id      : '',
            branch_id   : ''
        },
        reader : {
            type            :'json',
            rootProperty    :'data',
            successProperty :'success',
            messageProperty :'message'
        }
    }
});
var Get_account_plans = Ext.create('Ext.data.Store', {
    fields        :['planid'],
    autoLoad : true,
    proxy    : {
        type    : 'ajax',
        url     :'../data/get_account_plans.php',
        extraParams     : {
            acc_id      : '',
            branch_id   : ''
        },
        reader : {
            type            :'json',
            rootProperty    :'data',
            successProperty :'success',
            messageProperty :'message'
        }
    }
});

var provider= Ext.create('Ext.data.Store', {
    fields:['prov_code','prov_name'],
    data:[
    {"prov_code":"HMO_1WORLD","prov_name":"1-World Wide Health"},
    {"prov_code":"HMO_ASIANLIFE","prov_name":"AsianLife"},
    {"prov_code":"HMO_COCOLIFE","prov_name":"Cocolife"},
    {"prov_code":"HMO_HMI","prov_name":"Health Maintenance, Inc"},
    {"prov_code":"HMO_INSULAR","prov_name":"INSULAR HEALTH CARE INC."},
    {"prov_code":"HMO_INTELLICARE","prov_name":"Intellicare"},
    {"prov_code":"HMO_MAXI","prov_name":"Maxicare"},
    {"prov_code":"HMO_MEDICARD","prov_name":"MediCard Philippines"},
    {"prov_code":"HMO_PHILCARE","prov_name":"PhilCare Health Care Service"}
    ]
});

Ext.define('OMM.view.dependent.Dependent_Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.dependent',
 
    init:function(){
        /* pop a view when the back button is pressed
        note: if it's aleady at the root it's a noop */
        var that                  = this,
            dependent_management  = Ext.getCmp('dependent_management'),
            main                  = Ext.getCmp('app-main');

        window.onpopstate=function(){
            dependent_management.pop();
            dependent_management.getNavigationBar().hide();
            main.getNavigationBar().show()
        };
        this.backStackDepth=0;
    },

    onPush:function(){
        var main                  = Ext.getCmp('app-main'),
            dependent_management  = Ext.getCmp('dependent_management');

        dependent_management.getNavigationBar().show();
        main.getNavigationBar().hide();
        history.pushState(null,"");
    },

    onBack: function() {
        var main                             = Ext.getCmp('app-main'),
            dependent_management             = Ext.getCmp('dependent_management');
            dependent_management_top_toolbar = this.lookupReference('dependent_management_top_toolbar'),
            // dependent_management_bottom_toolbar = this.lookupReference('dependent_management_bottom_toolbar');

        main.getNavigationBar().show();
        dependent_management.getNavigationBar().hide();
        dependent_management_top_toolbar.show();
        history.back();
        return false;
    },

    onLoadDepMgt:function(){
        var dependent_list = this.lookupReference('dependent_list');
        dependent_list.getStore().removeAll();
        console.log('dependent list removeAll')
        // dependent_list.getStore().load({params:{activelink_id:'A000A80217'}})
        // console.log(dependent_list.getStore().data);
    },

    onDepSelectAccount:function(value, paging, params){
        this.selected_db = value.getValue();
        var dependent_list = this.lookupReference('dependent_list');

        Ext.Ajax.request({
            url        : '../data/session_dep_acc.php',
            params     :{session_dep_acc:this.selected_db},
            timeout    : 300000, //5minutes
            waitMsg    :    'Processing your request',
            method     : 'POST',
            success    : function () {        
                dependent_list.getStore().removeAll();
                // console.log('Principal current page :'+dependent_list.store.currentPage);
                dependent_list.getStore().load({params:{page:1}});
                console.log(dependent_list.getStore().data);

                Ext.Ajax.request({
                    url     :'../data/post_username.php',
                    success :function(response){
                        var response         = Ext.decode(response.responseText);
                            details          = {};
                            details.ops      = response.username;
                        localStorage.removeItem('OMM_logs_ops');
                        localStorage.setItem("OMM_logs_ops",Ext.encode(details));
                    }
                });
            }
        });

    },

    onDepSearchEmp:function(value, paging,params){
        var acc_id           = this.lookupReference('acc_id'),
            dependent_list   = this.lookupReference('dependent_list'),
            dep_search_value = this.lookupReference('dep_search_value'),
            value            = value.getValue();

        if(acc_id.getValue()=='' || acc_id.getValue()==null){
            Ext.Msg.alert('Warning','No account selected');
        }else{
            dependent_list.getStore().removeAll();
            dependent_list.getStore().load({params:{search_value:dep_search_value.getValue()}});
            console.log(dependent_list.getStore().data);
        }
    },

    onDepMgt_tap:function(list, index, node, r){
        var me                               = this,
            main                             = Ext.getCmp('app-main'),
            dependent_management             = Ext.getCmp('dependent_management'),
            dependent_management_top_toolbar = this.lookupReference('dependent_management_top_toolbar'),
            fullname                         = r.get('fullname');

        var details                 = {};
            details.activelink_id   = r.get('activelink_id'),
            details.acc_id          = r.get('company_id');
            
        localStorage.setItem("OMM_thread_notes",Ext.encode(details));     
        dependent_management.getNavigationBar().show();
        dependent_management_top_toolbar.hide();          
        dependent_management.push({
            xtype       :'dependent_details',
            reference   :'dependent_details',
            id          :'dependent_details',
            scroll      :'vertical',
            scrollable  :true,
            layout      :'vbox',
            items       :[
            {
                xtype       : 'label',
                margin      :'10 20',
                itemId      : 'errorBox',
                cls         :'invalidMsg'
            },
            {
                xtype    : 'toolbar',
                docked   : 'bottom',
                alias    : 'bottomtoolbar',
                border   :false,
                margin   :'2 0 2 0',
                layout   : {
                    type: 'hbox',
                    pack: 'center'
                },                
                items:[
                {
                    xtype     :'button',
                    reference :'dep_del',
                    itemId    :'dep_del',
                    text      :'HMO Deletion',
                    ui        :'decline',
                    handler   :'onDepHmoDeletion'
                },
                {
                    xtype       :'button',
                    reference   :'dep_reset',
                    itemId      :'dep_reset',
                    ui          :'decline',
                    text        :'Enrollment Reset',
                    handler     :'onDepUserReset'
                },
                {
                    xtype       :'button',
                    reference   :'dep_update',
                    itemId      :'dep_update',
                    ui          :'confirm',
                    text        :'Update',
                    handler     :'onDepUpdate'
                }
                ]
            },

            {
                xtype       :'panel',
                docked      : 'bottom',
                items       :[
                {
                    xtype       : 'label',
                    margin      :'10 20',
                    reference   :'dep_prin_name',
                    itemId      :'dep_prin_name',
                    cls         :'statusMsg'                    
                },
                 {
                    xtype       : 'label',
                    margin      :'10 20',
                    reference   :'dep_prin_emp_num',
                    itemId      :'dep_prin_emp_num',
                    cls         :'statusMsg'                    
                }
                ]
            },
            {
                xtype       :'fieldset',
                title       :'<b>Personal Information</b>',
                defaultType :'textfield',
                // fieldStyle:'font-size:13px;line-height:8px;',labelStyle:'font-size:12px;line-height:8px;'
                defaults    :{anchor:'95%',labelWidth:130, allowBlank:true},
                items       :[
                {name:'session_userid', reference:'session_userid',itemId:'session_userid', hidden:true},
                {label:'First Name',name:'fname',value:r.data['fname'], allowBlank:false},{label:'Middle Name',name:'mname',value:r.data['mname']},{label:'Last Name',name:'lname',allowBlank:false,value:r.data['lname']},{label:'Suffix Name',name:'ename',value:r.data['ename']},
                {label:'Gender',xtype:'selectfield',store:Get_gender,name:'gender',value:r.data['gender'],readOnly:true,emptyText:'Select Gender',valueField:'gender',displayField:'gender'},                    
                {label:'Birthdate',name:'bdate',value:r.data['bdate']},
                {label:'Civil Status',xtype:'selectfield',store:Get_civilstatus,name:'civil_status',readOnly:true,emptyText:'Select Civil Status',valueField:'civil_status',displayField:'civil_status'},
                // {label:'Civil Status',xtype:'combo',name:'civil_status',value:r.data['civil_status'],store:Ext.create("OMM.store.Get_civilstatus"),valueField:'civil_status',displayField:'civil_status',emptyText:'Select Civil Status',editable:false},
                {label:'Relationship',name:'relationship',value:r.data['relationship']},
                {label:'PhilHealth',name:'philhealth',value:r.data['philhealth']}
                ]
            },
            {
                xtype       :'fieldset',
                title       :'<b>Company Information</b>',
                // collapsed   :true,
                defaultType :'textfield',
                defaults    :{anchor:'95%',labelWidth:140, allowBlank:true},
                items       :[
                {label:'Principal AL ID',name:'al_prin_id',readOnly:true,value:r.data['al_prin_id'],hidden:true},
                {label:'Dependent AL ID',name:'al_dep_id',allowBlank:false,readOnly:false,value:r.data['al_dep_id'],hidden:true,id:'dep_al_dep_id'+r.data['al_dep_id']},
                {label:'Company ID',name:'dcompanyid',value:r.data['dcompanyid'],readOnly:true,reference:'dep_acc_id',id:'dep_acc_id'+r.data['al_dep_id']},
                {label:'Branch',name:'dbranchid',value:r.data['dbranchid'],readOnly:true,reference:'dep_branch_id'}
                ]
            }, 
            {
                xtype       :'fieldset',
                title       :'<b>Enrollment Information</b>',
                // collapsed   :true,
                defaultType :'textfield',
                defaults    :{anchor:'95%',labelWidth:140, allowBlank:true},
                items       :[
                {label:'DEPSETTINGSIDX',name:'dep_settings_idx',value:r.data['idx'],hidden:true},
                {label:'HMO ID',name:'hmo_id',value:r.data['hmo_id'],maxLength:50},
                 // {fieldLabel:'Dependent type',xtype:'combo',name:'dep_type',value:r.data['dep_type'],reference:'dep_type', store:Ext.create("OMM.store.Get_deptype"),valueField:'dep_type',displayField:'dep_type',emptyText:'Select type',editable:false,listeners:{el:{click:'onDepType_change_dep_form'}}},                               
                {label:'Dependent type',name:'dep_type',value:r.data['dep_type'],readOnly:true},
                {xtype:'label',style:'color:#2f9fe9;font-weight:bold;font-style:oblique;font-face:arial',text:'Enrollment status will automatically set as Active if user fill the valid HMO ID'},                                
                {label:'Enrollment Status',labelAlign:'top', flex:1,name:'status',reference:'dep_enroll_stat',itemId:'dep_enroll_stat',value:r.data['status'],readOnly:true},
                // {xtype:'checkbox',boxLabel:'Enrollment status option : Check if you want to update status of dependent',labelAlign:'right',labelStyle:'color:#2f9fe9;font-weight:bold;font-face:arial;font-size:12px',labelWidth:480,name:'dep_stat_opt',reference:'dep_stat_opt',checked:false,padding:'-10 0 0 0',inputValue:'1',uncheckedValue:'0',listeners:{change:'onDepChangeEnrollStat'}},
                {label:'Date of Enrollment',labelAlign:'top',  name:'date_of_enrollment', value:r.data['date_of_enrollment'], readOnly:true},
                {label:'Date Created', labelAlign:'top', name:'last_update', value:r.data['datecreated'], readOnly:true},
                {label:'Last Updated On', labelAlign:'top', name:'datecreated', value:r.data['last_update'], readOnly:true},
                {label:'Remarks',name:'remark',value:r.data['remark'],readOnly:true}
                ]
            }, 
            {
                xtype       :'fieldset',
                title       :'<b>HMO Information</b>',
                defaultType :'textfield',
                defaults    :{anchor:'95%',labelWidth:140, allowBlank:true},
                items       :[
                {
                        xtype       :'formpanel',
                        title       :'<font size="3">Plan Information',
                        itemId      :'opt_plan',
                        reference   :'opt_plan',
                        ui          :'light',
                        margin      :10,
                        scrollable  :true,
                        defaultType :'textfield',                
                        defaults    :{anchor:'95%', labelAlign  :'top',allowBlank:true},
                        items       :[
                            {label:'Principal Member Status',name:'pmbr_stat',value:r.data['pmbr_stat'],readOnly:true},
                            {label:'Principal Plan (Level)',name:'pplan_id',value:r.data['pplan_id'],readOnly:true},                        
                            {label:'HMO Provider',name:'hospital_hmo',readOnly:true},
                            {label:'Dependent Member Status',name:'dmbr_stat',value:r.data['dmbr_stat'],readOnly:true},
                            {label:'level',name:'dplan_id',reference:'dep_planLevel',value:r.data['dplan_id'],readOnly:true},
                            {label:'Plan Description',name:'plan_desc',readOnly:true,reference:'dep_plan_desc'},
                            {label:'Benefit limit',name:'benefit_limit',readOnly:true},
                            {xtype:'label',style:'font-weight:bold;font-face:arial;margin-bottom:10px;',html:'NOTE : <span style="color:#e44959;font-weight:bold;font-face:arial;font-size:11px;font-style:oblique;">Advise to double check the benefit limit of the member. Value was based on Principal plan information</span>'},
                            {
                                xtype       :'panel',
                                reference   :'dep_opt_panel',
                                itemId      :'dep_opt_panel',
                                defaultType :'textfield',
                                defaults    :{anchor:'100%', labelWidth:160, width:'100%'},
                                items       :[
                                    {label:'Option',name:'opt',readOnly:true},
                                    {xtype:'label',name:'opt_plan_desc', reference:'opt_plan_desc',itemId:'opt_plan_desc', style:'color:#2f9fe9;font-weight:bold;font-face:arial',html:'Option description : '}
                                ]
                            }
                        ]
                    }                
                    ]
            }, 
            {
                xtype       :'list',
                title       : 'Required Documents',
                id          :'dep-required-doc-'+r.data['activelink_id'],
                store        :Ext.create("OMM.store.Get_dependent_documents"),                                
                // store       :principalStore,
                items       :[
                {xtype:'label',docked:'top', style:'font-weight:bold;font-size:15px;font-face:arial',html:'Required Documents'}
                ],
                itemTpl     : [
                    '<div class="top_bar"><div class="top-wrapper"><p>Doc ID: {doc_id}</p></div></div>',
                    '<span class="posted">Doc name: {doc_name}</span>',
                    '<span class="posted">Date modified: {modified_on}</span>',
                    '<h2>Status: {doc_stat}</h2>'
                ].join(''),
                listeners:{
                    painted:function(){
                            Ext.getCmp('dep-required-doc-'+r.data['activelink_id']).getStore().load({params:{dep_al_id:r.data['al_dep_id'],acc_id:r.data['dcompanyid']}}); 
                    }
                }
            }
            ],
            listeners:{
                initialize:function(){

                    var me               =this,
                        session_userid   =this.down('#session_userid'),
                        dep_enroll_stat  =this.down('#dep_enroll_stat'),
                        opt_plan         =this.down('#opt_plan'),
                        opt_plan_desc    =this.down('#opt_plan_desc'),
                        dep_opt_panel    =this.down('#dep_opt_panel'),
                        dep_prin_name    =this.down('#dep_prin_name'),
                        dep_prin_emp_num =this.down('#dep_prin_emp_num'),
                        dep_plandetails  =Ext.create('OMM.model.Dependent_plandetails_Model');


                    Ext.Ajax.request({
                    url     :'../data/post_username.php',
                    success :function(response){
                        var response=Ext.decode(response.responseText);
                        session_userid.setValue(response.username);
                        console.log(session_userid.getValue());


                            Ext.Ajax.request({
                                url:'../data/get_dependent_plandetails.php',
                                params:{ops:session_userid.getValue(),dep_alid:r.get('al_dep_id'),acc_id:r.get('dcompanyid')},
                                success:function(response){
                                    var response        = Ext.decode(response.responseText);

                                console.log(response.data[0])
                                if (response.data[0]) {
                                    dep_plandetails.set(response.data[0]);
                                    opt_plan.setRecord(dep_plandetails);
                                    opt_plan_desc.setHtml('Option description : '+response.data[0].opt_plan_desc);
                                }
                                    
                                },failure:function(response){
                                    var response=Ext.decode(response.responseText);                                                                       
                                    opt_plan_desc.setHtml('Something went wrong. cant view plan description. Please contact your IT for assistance');

                                }
                            });

                            if(r.get('dcompanyid')!='QBE'){
                                // opt_plan.hide();
                                dep_opt_panel.hide();
                            }else{}
                    }
                    });

                    Ext.Ajax.request({
                        url:'../data/get_dependent_principal.php',
                        params:{al_prin_id:r.data['al_prin_id'],acc_id:r.data['dcompanyid']},
                        success:function(response){
                            var response=Ext.decode(response.responseText);
                            dep_prin_name.setHtml('Prin name:<b>'+response.prin_name);
                            dep_prin_emp_num.setHtml('Prin empid:<b>'+response.prin_emp_num);
                        }
                    });
                }
            }
        });   
        dependent_management.getNavigationBar().setTitle('Dep Info - '+r.data['fname']+' '+r.data['lname'])
    },

    onDepHmoDeletion:function(btn){

        var form            =btn.up('formpanel'),
            values          =form.getValues(),
            fields          =form.getFields();
            dependent_list  =this.lookupReference('dependent_list'),
            me              =this,
            fields1         =form.query("field"),
            cur_tab_id      ="dep"+values['al_dep_id'];

        Ext.Msg.confirm('HMO Deletion','Are you sure you want to proceed on this process? Dependent HMO STATUS will be "INACTIVE"',function(answer){
            if(answer=='yes'){
                if(values['al_dep_id']==''|| values['al_dep_id']==null || values['fname']=='' || values['fname']==null || values['lname']=='' || values['lname']==null){
                    Ext.Msg.alert('Warning','Cant delete dependent if Active ID, First and Last Name is null or empty')
                }else{

                    var task = Ext.create('Ext.util.DelayedTask', function() {
                        Ext.Viewport.mask({ xtype: 'loadmask',
                        message: "Processing your request..." });
                    }, this);
                    task.delay(200);
                    Ext.Ajax.request({
                        url             :'../data/dep_single_del.php',
                        params          :{dep_al_id:values['al_dep_id'], session_userid:values['session_userid'],fname:values['fname'], lname:values['lname']},
                        method          :'POST',
                        submitEmptyText :false,
                        success         :function(response){
                            var response = Ext.decode(response.responseText);
                            if(response.success==true){
                                task.cancel();
                                Ext.Msg.alert('Success',response.message);
                                me.onBack();
                                dependent_list.getStore().load()
                                dependent_list.getStore().data
                            }else{
                                task.cancel();
                                Ext.Msg.alert('Warning',response.message);
                            }
                        },failure       :function(response){
                            task.cancel();
                            Ext.Msg.alert('Warning','500 Internal Server Error. Please contact IT for assistance');
                        }
                    });
                }
            }
        });
    },        

    onDepUserReset:function(btn){
        var form            = btn.up('formpanel'),
            values          = form.getValues(),
            fields          = form.getFields();
            dependent_list  = this.lookupReference('dependent_list'),
            me              = this,
            fields1         = form.query("field"),
            cur_tab_id  = "dep"+values['al_dep_id'];

        Ext.Msg.confirm('Enrollment Reset','Are you sure you want to reset the enrollment? Dependent records will be DELETED on the system',function(answer){
            if(answer=='yes'){
               if(values['al_dep_id']=='' || values['dcompanyid']==''){
                Ext.Msg.alert('Warning','Empty Employee number/ Company ID')
               }else{
                    var task = Ext.create('Ext.util.DelayedTask', function() {
                        Ext.Viewport.mask({ xtype: 'loadmask',
                        message: "Processing your request..." });
                    }, this);
                    task.delay(200);
                    Ext.Ajax.request({
                        url             :'../data/get_dependent_reset.php',
                        params          :{al_dep_id:values['al_dep_id'],dcompanyid:values['dcompanyid']},
                        method          :'POST',
                        submitEmptyText :false,
                        success         :function(response){
                            var response = Ext.decode(response.responseText);
                            if(response.success==true){
                                task.cancel();
                                Ext.Msg.alert('Success',response.message);
                                me.onBack();
                                dependent_list.getStore().load()
                                dependent_list.getStore().data
                            }else{
                                task.cancel();
                                Ext.Msg.alert('Failed',response.message)
                            }
                        },
                        failure     :function(form,action,response){
                            task.cancel();
                            Ext.Msg.alert('Warning','500 Internal Server Error. Please contact IT for assistance')
                        }
                    });
                } 
            }
        });
    },

    
    onDepUpdate:function(btn){
        var form            = btn.up('formpanel'),
            values          = form.getValues(),
            fields          = form.getFields();
            dependent_list  = this.lookupReference('dependent_list'),
            me              = this,
            fields1         = form.query("field"),
            errorString     = '',
            model           = Ext.create('OMM.model.modern.PrincipalDependentModel',form.getValues()),
            errors          = model.validate(),
            errUI           = form.down('#errorBox');
            status          = values['status'];

        Ext.Msg.confirm('Update Member','Are you sure you want to update members information',function(answer){
            if(answer=='yes'){
                
                var task = Ext.create('Ext.util.DelayedTask', function() {
                    Ext.Viewport.mask({ xtype: 'loadmask',
                    message: "Processing your request..." });
                }, this);
                task.delay(200);

                errUI.setHtml('');

                for (var i = 0; i < fields.length; i++) {
                  fields[i].removeCls('invalidField');
                }

                if(!errors.isValid()){
                  errors.each(function(errorObj){
                    // console.log(errorObj)
                    errorString+=' '+errorObj.getField()+" "+errorObj.getMessage()+"</br>";
                    var s = Ext.String.format('field[name={0}]',errorObj.getField());
                    form.down(s).addCls('invalidField');
                  });

                errUI.setHtml('<p class="invalidMsg"><font color="red">Please provide required fields');
                errUI.show();
                task.cancel();                
              }else{

                errUI.hide();
                Ext.each(fields1, function (f) {
                    var field = f.getName(),
                        value = f.getValue();
                    if(field=='philhealth' && value=='-' || field=='hmo_id' && value=='-'){
                        console.log('field :'+field +' value : '+value);
                    }else{
                        if(value=='-'){
                            console.log('Field with - set blank on sumbit :'+field);
                            form.findField(field).setValue('');
                        }
                    }
                });
                
                console.log('status :'+status);
                if(status == 'Active' && values['hmo_id']  =='' || status == 'active' && values['hmo_id']  ==''){
                    Ext.Msg.alert('Warning','Active status requires HMO id number');
                }else if(status == 'Active' && values['hmo_id'] =='-' || status == 'active' && values['hmo_id'] =='-'){
                    Ext.Msg.alert('Warning','Active status requires HMO id number');
                }else if(status =='Active' || status =='active' && values['hmo_id'].length<=4){
                    Ext.Msg.alert('Warning','The minimum length for the HMO ID field is 5');
                }else if(status == 'Active' || status =='active' && values['hmo_id'].startsWith("-")){
                    Ext.Msg.alert('Warning','System will not accept value "-1234". Please check if HMO ID field is valid')
                }else if(status == 'For endorsement' && values['hmo_id'] !=''){
                    Ext.Msg.alert('Warning','Cannot set member for endorsement if HMO ID field is not null or empty');
                }else if(status =='Endorse to HMO' && value['hmo_id'] !='' || status == 'Endorse to HMO' && values['hmo_id'] !=null){
                    Ext.Msg.alert('Warning','Cannot set member Endorse to HMO if HMO ID field is not null or empty');
                }                

                form.submit({
                    url             :'../data/dependent_update.php',
                    waitMsg         :'Processing your request. Please wait...',
                    submitEmptyText :false,
                    timeout         : 120000, //2minutes
                    method          :'POST',
                    success         :function(form,action){
                        task.cancel();
                        dependent_list.getStore().load()
                        dependent_list.getStore().data
                        me.onBack();
                        Ext.Msg.alert('Success','Dependent update saved');
                    },failure       :function(form,action){
                        task.cancel();     
                        dependent_list.getStore().load() 
                        dependent_list.getStore().data                        
                        Ext.Msg.alert('Failed','Dependent update failed to save');
                    }
                })
            
              }
            }
        });

    }



});
