var Get_gender = Ext.create('Ext.data.Store', {
    fields  :['gender','description'],
    autoLoad : true,
    proxy    : {
        type    : 'ajax',
        url     :'../data/get_gender.php',
        reader : {
            type         : 'json',
            rootProperty : 'data'
        }
    }
});
var Get_civilstatus = Ext.create('Ext.data.Store', {
    fields  :['civil_status','description'],
    autoLoad : true,
    proxy    : {
        type    : 'ajax',
        url     :'../data/get_civilstatus.php',
        reader : {
            type            :'json',
            rootProperty    :'data',
            successProperty :'success',
            messageProperty :'message'
        }
    }
});
var Get_account_branchid = Ext.create('Ext.data.Store', {
    fields  :['branch_id','branch'],
    autoLoad : true,
    proxy    : {
        type    : 'ajax',
        url     :'../data/get_account_branchid.php',
        extraParams     : {
            acc_id      : '',
            branch_id   : ''
        },
        reader : {
            type            :'json',
            rootProperty    :'data',
            successProperty :'success',
            messageProperty :'message'
        }
    }
});
var Get_account_plans = Ext.create('Ext.data.Store', {
    fields        :['planid'],
    autoLoad : true,
    proxy    : {
        type    : 'ajax',
        url     :'../data/get_account_plans.php',
        extraParams     : {
            acc_id      : '',
            branch_id   : ''
        },
        reader : {
            type            :'json',
            rootProperty    :'data',
            successProperty :'success',
            messageProperty :'message'
        }
    }
});

var provider= Ext.create('Ext.data.Store', {
    fields:['prov_code','prov_name'],
    data:[
    {"prov_code":"HMO_1WORLD","prov_name":"1-World Wide Health"},
    {"prov_code":"HMO_ASIANLIFE","prov_name":"AsianLife"},
    {"prov_code":"HMO_COCOLIFE","prov_name":"Cocolife"},
    {"prov_code":"HMO_HMI","prov_name":"Health Maintenance, Inc"},
    {"prov_code":"HMO_INSULAR","prov_name":"INSULAR HEALTH CARE INC."},
    {"prov_code":"HMO_INTELLICARE","prov_name":"Intellicare"},
    {"prov_code":"HMO_MAXI","prov_name":"Maxicare"},
    {"prov_code":"HMO_MEDICARD","prov_name":"MediCard Philippines"},
    {"prov_code":"HMO_PHILCARE","prov_name":"PhilCare Health Care Service"}
    ]
});

Ext.define('OMM.view.principal.Principal_Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.principal',
 
    init:function(){
        /* pop a view when the back button is pressed
        note: if it's aleady at the root it's a noop */
        var that                  = this,
            principal_management  = Ext.getCmp('principal_management'),
            main                  = Ext.getCmp('app-main');

        window.onpopstate=function(){
            principal_management.pop();
            principal_management.getNavigationBar().hide();
            main.getNavigationBar().show()
        };
        this.backStackDepth=0;
    },

    onPush:function(){
        var main                  = Ext.getCmp('app-main'),
            principal_management  = Ext.getCmp('principal_management');

        principal_management.getNavigationBar().show();
        main.getNavigationBar().hide();
        history.pushState(null,"");
    },

    onBack: function() {
        var main                             = Ext.getCmp('app-main'),
            principal_management             = Ext.getCmp('principal_management');
            principal_management_top_toolbar = this.lookupReference('principal_management_top_toolbar'),
            // principal_management_bottom_toolbar = this.lookupReference('principal_management_bottom_toolbar');

        main.getNavigationBar().show();
        principal_management.getNavigationBar().hide();
        principal_management_top_toolbar.show();
        history.back();
        return false;
    },

    onLoadPrinMgt:function(){
        var principal_list = this.lookupReference('principal_list');
        principal_list.getStore().removeAll();
        console.log('principal list removeAll')
        // principal_list.getStore().load({params:{activelink_id:'A000A80217'}})
        // console.log(principal_list.getStore().data);
    },

    onPrinSelectAccount:function(value, paging, params){
        this.selected_db = value.getValue();
        var principal_list = this.lookupReference('principal_list');

        Ext.Ajax.request({
            url        : '../data/session_prin_acc.php',
            params     :{session_prin_acc:this.selected_db},
            timeout    : 300000, //5minutes
            waitMsg    :    'Processing your request',
            method     : 'POST',
            success    : function () {        
                principal_list.getStore().removeAll();
                // console.log('Principal current page :'+principal_list.store.currentPage);
                principal_list.getStore().load({params:{page:1}});
                principal_list.getStore().data
                // console.log(principal_list.getStore().data);

                Ext.Ajax.request({
                    url     :'../data/post_username.php',
                    success :function(response){
                        var response         = Ext.decode(response.responseText);
                            details          = {};
                            details.ops      = response.username;
                        localStorage.removeItem('OMM_logs_ops');
                        localStorage.setItem("OMM_logs_ops",Ext.encode(details));
                    }
                });
            }
        });

    },

    onPrinSearchEmp:function(){
        var acc_id         = this.lookupReference('acc_id'),
            principal_list = this.lookupReference('principal_list'),
            search_value   = this.lookupReference('prin_search_value');

        if(acc_id.getValue()=='' || acc_id.getValue()==null){
            Ext.Msg.alert('Warning','No account selected');
        }else{
            principal_list.getStore().removeAll();
            principal_list.getStore().load({params:{search_value:search_value.getValue()}});
            principal_list.getStore().data
            // console.log(principal_list.getStore().data);
        }
    },


    onPrinMgt_tap:function(list, index, node, r){
        var me                               = this,
            main                             = Ext.getCmp('app-main'),
            principal_management             = Ext.getCmp('principal_management'),
            principal_management_top_toolbar = this.lookupReference('principal_management_top_toolbar'),
            fullname                         = r.get('fullname');

        var details                 = {};
            details.activelink_id   = r.get('activelink_id'),
            details.acc_id          = r.get('company_id');
            
        localStorage.setItem("OMM_thread_notes",Ext.encode(details));

        // principal_management.getNavigationBar().hide();
        principal_management.getNavigationBar().show();
        principal_management_top_toolbar.hide();

        var task_tap = Ext.create('Ext.util.DelayedTask', function() {
            Ext.Viewport.mask({ xtype: 'loadmask',
            message: "Processing your request..." });
        }, this);
        task_tap.delay(200);

        principal_management.push({
            xtype       :'principal_details',
            reference   :'principal_details',
            id          :'principal_details',
            scroll      :'vertical',
            scrollable  :true,
            layout      :'vbox',
            items       :[            
            {
                xtype       : 'label',
                margin      :'10 20',
                itemId      : 'errorBox',
                cls         :'invalidMsg'
            },
            {
                xtype    : 'toolbar',
                docked   : 'bottom',
                alias    : 'bottomtoolbar',
                border   :false,
                margin   :'2 0 2 0',
                layout   : {
                    type: 'hbox',
                    pack: 'center'
                },                
                items:[
                {
                    xtype     :'button',
                    reference :'prin_del',
                    itemId    :'prin_del',
                    text      :'HMO Deletion',
                    ui        :'decline',
                    handler   :'onPrinHmoDel'
                },
                {
                    xtype     :'button',
                    reference :'prin_rectv',
                    itemId    :'prin_rectv',
                    ui        :'decline',
                    text      :'Reactivate',
                    handler   :'onPrinReactivate'
                },
                {
                    xtype       :'button',
                    reference   :'prin_reset',
                    itemId      :'prin_reset',
                    ui          :'decline',
                    text        :'Reset',
                    handler     :'OnPrinUserReset'
                },
                {
                    xtype       :'button',
                    reference   :'prin_update',
                    itemId      :'prin_update',
                    ui          :'confirm',
                    text        :'Update',
                    handler     :'onPrinUpdate'
                }
                ]
            },
            {
                xtype       :'panel',
                docked      : 'bottom',
                items       :[
                {
                    xtype       : 'label',
                    margin      :'10 20',
                    reference   :'prin_bmb_status',
                    itemId      :'prin_bmb_status',
                    cls         :'statusMsg'                    
                }
                ]
            },
            {
                xtype       :'fieldset',
                title       :'<b>Personal Information</b>',
                defaultType :'textfield',
                // fieldStyle:'font-size:13px;line-height:8px;',labelStyle:'font-size:12px;line-height:8px;'
                defaults    :{anchor:'95%',labelWidth:130, allowBlank:true},
                items       :[
                {name:'session_userid',reference:'session_userid',id:'session_userid', hidden:true},
                {label:'First Name',name:'fname', allowBlank:false},
                {label:'Middle Name',name:'mname'},
                {label:'Last Name',name:'lname',allowBlank:false},
                {label:'Suffix Name',name:'ename'},
                {label:'Gender',xtype:'selectfield',store:Get_gender,name:'gender',editable:false,emptyText:'Select Gender',valueField:'gender',displayField:'gender'},
                // xtype:'datepickerfield',
                {label:'Birthdate',name:'birthdate'},
                {label:'Civil Status',xtype:'selectfield',store:Get_civilstatus,name:'civil_status',editable:false,emptyText:'Select Civil Status',valueField:'civil_status',displayField:'civil_status'}
                ]
            },
            {
                xtype       :'fieldset',
                title       :'<b>Company Information</b>',
                // collapsed   :true,
                defaultType :'textfield',
                defaults    :{anchor:'95%',labelWidth:140, allowBlank:true},
                items       :[
                    {label:'ActiveLink ID',name:'activelink_id',allowBlank:false,readOnly:true,hidden:true},
                    {label:'Employee ID',name:'empid',reference:'prin_empid'},
                    // vtype: 'email',
                    {label:'Email',name:'email',reference:'prin_email',itemId:'prin_email'},                                
                    {label:'Company ID',name:'company_id',reference:'prin_acc_id',readOnly:true},                                
                    {label:'Branch',xtype:'selectfield',name:'branch',emptyText:'Select Plan level',store:Get_account_branchid,reference:'prin_branch',emptyText:'Select Branch',editable:false,valueField:'branch',displayField:'branch',listeners:{initialize:'onPrinBranchID', change:'onPrinBranchChange'}},
                    {label:'Branch ID',name:'branch_id',reference:'prin_branch_id', hidden:false},
                    {label:'Designation',name:'designation'},
                    {label:'Last Member Update',name:'member_date',readOnly:true},
                    {label:'Registration Date',name:'reg_date',readOnly:true},
                    {label:'Existing Flag',name:'existing_flag',readOnly:true},
                    {label:'Inclusion Date',name:'inclusion_date',readOnly:true},
                    {label:'Hire Date',name:'emp_hire_date',format:'Y-m-d H:i:s', editable:true}
                ]
            },            
            {
                xtype       :'fieldset',
                columnWidth :0.5,
                title       :'<b>HMO Information</b>',
                defaultType :'textfield',
                defaults    :{anchor:'95%', labelWidth:150, allowBlank:true},
                items       :[
                    {label:'Member Status', name:'mbr_status',reference:'prin_mbr_status', readOnly:true},
                    // {label:'HMO ID Number',name:'id_number', minLength:5, maxLength:30},
                    {label:'HMO Provider',name:'hospital_hmo',editable:false, xtype:'selectfield', store:provider, queryMode:'local', displayField:'prov_name', valueField:'prov_code'},
                    {label:'Dental Provider',name:'dental_hmo', editable:false, xtype:'selectfield', store:provider, queryMode:'local', displayField:'prov_name', valueField:'prov_code'},
                    {label:'Principal Plan level',name:'plan_id',readOnly:true, reference:'prin_planLevel'},
                    // {label:'Principal Plan level',xtype:'selectfield',name:'plan_id',reference:'prin_planLevel',emptyText:'Select Plan level',store:Get_account_plans,displayField:'planid',valueField:'planid',editable:false,listeners:{initialize:'onPrinCompanyID',change:'onPrinPlanChange'}},
                    {label:'Plan Description',name:'plan_desc',readOnly:true, reference:'prin_plan_desc'},
                    {label:'Original Effective Date',minValue:'1910-01-01',format:'Y-m-d',name:'orig_eff_date'},
                    {label:'Current Effective Date',minValue:'1910-01-01',format:'Y-m-d',name:'cur_eff_date'},
                    {label:'Maturity Date',name:'maturity_date',minValue:'1910-01-01',format:'Y-m-d'}                
                ]
            },
            {
                xtype       :'fieldset',
                title       :'<b>Additional Information</b>',
                animate      :true,
                defaultType :'textfield',
                defaults    :{anchor:'95%', labelWidth:130, allowBlank:true},
                items       :[
                    {label:'PhilHealth',name:'philhealth'},
                    {label:'SSS no',name:'sss_no'},
                    {label:'Pag-ibig no',name:'pag_ibig_no'},  
                    {label:'Unified ID no',name:'unified_id_no'},
                    {label:'TIN',name:'tin'}
                ]
            },
            {
                xtype       :'fieldset',
                title       :'<b>Contact  Information</b>',
                defaultType :'textfield',
                defaults    :{anchor:'95%',labelWidth:140, readOnly:true, allowBlank:true,editable:false},
                items       :[
                    {xtype:'label',style:'font-weight:bold;font-size:11px;font-face:arial',margin:8,html:'NOTE : <p><span style="color:#2f9fe9;font-weight:bold;font-face:arial;font-size:11px;font-style:oblique;">User cannot change the information below. For viewing purposes only. </span>'},
                    {label:'Email',name:'email1',itemId:'email1'},
                    {label:'Address',name:'address1',itemId:'address1'},
                    {label:'Contact 1', name :'contact1',itemId:'contact1'},
                    {label:'Contact 2',name:'contact2',itemId:'contact2'}
                    // {fieldLabel:'Inclusion Date',name:'inclusion_date'},
                    //deactivation can see on company information
                ]
            },
            {
                xtype       :'formpanel',
                title       :'<font size="3"><b>SLA Updates',
                itemId      :'sla_form',
                reference   :'sla_form',
                ui          :'light',
                margin      :10,
                scrollable  :true,
                defaultType :'textfield',                
                defaults    :{anchor:'95%', labelAlign  :'top',allowBlank:true},
                items       :[
                    {
                        label :'Policy Number/HMO ID', name:'hmoid_no'
                    },
                    {
                        label:'HMO ID Release Date',destroyPickerOnHide : true, picker: {yearFrom: 2015},xtype:'datepickerfield',minValue:'1900-01-01',maskRe: /[0-9\/]/,format:'Y-m-d',name:'hmoid_released_date'
                    },
                    {
                        xtype   :'button',
                        ui      :'confirm',
                        text    :'Save',                        
                        itemId  :'sla_hmoid',
                        reference  :'sla_hmoid',
                        handler :'onHmoIdUpdate'
                    },
                    {
                        label:'Card Received',destroyPickerOnHide : true, picker: {yearFrom: 2015},xtype:'datepickerfield',minValue:'1900-01-01',format:'Y-m-d',name:'card_received'
                    },
                    {
                        xtype   :'button',
                        ui      :'confirm',
                        text    :'Save',
                        itemId  :'sla_cardr',
                        reference  :'sla_cardr',
                        handler :'onSaveCardReceived'  
                    },
                    {
                        label:'Card Transmit', destroyPickerOnHide : true, picker: {yearFrom: 2015},xtype:'datepickerfield',minValue:'1900/01/01',value: new Date(),format:'Y-m-d',name:'card_transmit'
                    },
                    {
                        xtype   :'button',
                        ui      :'confirm',
                        text    :'Save',
                        itemId  :'sla_cardt',
                        reference  :'sla_cardt',
                        handler :'onSaveCardTransmit'  
                    },
                    {
                        label       :'Current Contract Year',
                        labelAlign  :'left',
                        labelWidth  :200,
                        readOnly    :true,                        
                        name        :'batch_code'
                    },
                    {
                        label       :'SLA Option',
                        labelAlign  :'left',
                        labelWidth  :165,
                        readOnly    :true,
                        name        :'sla_option'
                    },
                    {
                        label       :'HR endorsed date',
                        labelAlign  :'left',
                        labelWidth  :165,
                        readOnly    :true,
                        name        :'hr_endorsed_date'
                    },
                    {
                        label       :'OPS endorsed date',
                        labelAlign  :'left',
                        labelWidth  :165,
                        readOnly    :true,
                        name        :'ops_endorsed_date'
                    }

                    ]
            },
            {
                xtype       :'list',
                title       : 'Principal',
                reference   :'prin_dep_grid_p',
                id          :'prin_dep_grid_p',
                cls         :'x-list',
                store        :Ext.create("OMM.store.principal.Get_principal_dependent_list"),
                // store       :principalStore,
                items       :[
                {xtype:'label',docked:'top', style:'font-weight:bold;font-size:15px;font-face:arial',html:'Dependent/s'}            
                ],
                // itemTpl     : [
                //     '<div class="top_bar"><div class="top-wrapper"><p>{dmbr_status}</p></div></div>',
                //     '<span class="posted">Plan Level: {dplan_level}</span>',
                //     '<h2>Fullname : {fullname}</h2>'
                //     // '<span class="posted">Status : {mbr_status}</span>',
                // ].join(''),

                itemTpl     :[
                    '<div class="top_bar"><p>{fullname}</p></div>',
                    '<span class="posted">HMO ID number : {hmo_id}</span>',
                    '<span>Birthdate : {bdate}</span>',
                    '<span>Plan ID : {planid}</span>',
                    '<span>Status : <font color=green>{dmbr_status}</span>'
                    // '<h2>Fullname : {fullname}</h2>'/<div class="top-wrapper">
                    // '<span class="posted">Status : {mbr_status}</span>',
                ].join(''),

                listeners:{
                }
            }
            ],
            listeners:{
                initialize :function(component, eOpts){

                    var me              = this,
                        session_userid  = Ext.getCmp('session_userid'),
                        prin_del        = this.down('#prin_del'),
                        prin_reset      = this.down('#prin_reset'),
                        prin_rectv      = this.down('#prin_rectv'),
                        prin_update     = this.down('#prin_update'),
                        email           = this.down('#prin_email'),
                        prin_bmb_status = this.down('#prin_bmb_status'),
                        sla_hmoid       = this.down('#sla_hmoid'),
                        sla_cardr       = this.down('#sla_cardr'),
                        sla_cardt       = this.down('#sla_cardt'),
                        email1          = this.down('#email1'),
                        address1        = this.down('#address1'),
                        contact1        = this.down('#contact1'),
                        contact2        = this.down('#contact2'),
                        form            = this.down('#sla_form');

                        Ext.Ajax.request({
                        url     :'../data/post_username.php',
                        success :function(response){
                            var response=Ext.decode(response.responseText);
                            session_userid.setValue(response.username);
                            console.log(session_userid.getValue())
                        }
                        });

                        Ext.Ajax.request({
                            url     :'../data/get_pmember_details.php',
                            waitMsg :    'Processing your request',
                            headers :{'Content-Type': 'application/json'},
                            params  :{p_al_id:r.get('activelink_id'), acc_id:r.get('company_id')},
                            method  :'GET',
                            success :function(response){

                                var principal       = Ext.create('OMM.model.principal.PrincipalModel'),
                                    response        = Ext.decode(response.responseText),
                                    prin_form       = Ext.getCmp('principal_details');
                                    values          = prin_form.getValues(),
                                    prin_dep_grid_p = Ext.getCmp('prin_dep_grid_p');

                                // console.log(prin_form)
                                console.log(response.data[0])
                                if (response.data[0]) {

                                    principal.set(response.data[0]);
                                    console.log('PLAN ' +principal.data['plan_id'])
                                    prin_form.setRecord(principal);
                                    prin_dep_grid_p.getStore().load({params:{prin_al_id:r.get('activelink_id')}});
                                    // console.log('principal dependent grid load');


                                    if(r.get('email')=='' || r.get('email')=='-' || r.get('email')=='-_DEL'){
                                        prin_bmb_status.setHtml('Unregistered');
                                        prin_reset.setDisabled(true);
                                        // prin_update.setDisabled(false);
                                    }else{
                                        prin_bmb_status.setHtml('BMB Status : <b>Registered');
                                        prin_reset.setDisabled(false);
                                        // prin_update.setDisabled(false);
                                    }

                                    console.log(r.get('mbr_status'));

                                    if(r.get('mbr_status')!='ACTIVE'){
                                        prin_del.hide();
                                        prin_rectv.show();
                                        prin_update.setDisabled(true);
                                    }else{
                                        prin_del.show();
                                        prin_rectv.hide();
                                        prin_update.setDisabled(false);
                                    }

                                    var OMM_thread_notes    = localStorage.getItem('OMM_thread_notes'),
                                        OMM_logs_ops        = localStorage.getItem('OMM_logs_ops'),
                                        obj2                = JSON.parse(OMM_thread_notes),
                                        obj                 = JSON.parse(OMM_logs_ops);
                            
                                    Ext.Ajax.request({
                                        url     :'../data/sla_validator.php',
                                        params  :{activelink_id:obj2['activelink_id'],acc_id:obj2['acc_id'],ops:obj['ops']},
                                        success :function(response){
                                            var response    = Ext.decode(response.responseText);

                                            if(response.success==true){
                                                sla_hmoid.setDisabled(false);
                                                sla_cardr.setDisabled(false);
                                                sla_cardt.setDisabled(false);
                                            }else{
                                                sla_hmoid.setDisabled(true);
                                                sla_cardr.setDisabled(true);
                                                sla_cardt.setDisabled(true);
                                                // Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Server failed to load request. Please contact IT for assistant',closable:false, icon:Ext.Msg.WARNING,buttonText:{ok:'OK'}});
                                            }
                                        },failure:function(response){
                                            Ext.Msg.alert('Warning','500 Internal Server Error');
                                                sla_hmoid.setDisabled(true);
                                                sla_cardr.setDisabled(true);
                                                sla_cardt.setDisabled(true);
                                        }
                                    });

                                    Ext.Ajax.request({
                                        url :'../data/get_prin_hmoid.php',
                                        params  :{ops:obj['ops'],activelink_id:obj2['activelink_id']},
                                        success     :function(response){
                                            var response = Ext.decode(response.responseText),
                                                data     = response.data;
                                            form.setValues({hmoid_no:data});
                                        },failure:function(){
                                            Ext.Msg.alert('Failed','Something went wrong.')
                                        }
                                    });

                                    Ext.Ajax.request({
                                        url     :'../data/get_prin_id_released_date.php',
                                        params  :{ops:obj['ops'], activelink_id:obj2['activelink_id']},
                                        success :function(response){
                                            var response = Ext.decode(response.responseText),
                                                data     = response.data;
                                                date     = Ext.Date.parse(data,"Y-m-d");

                                                form.setValues({hmoid_released_date:date});                                        
                                        },failure:function(){
                                            Ext.Msg.alert('Failed','Something went wrong.')
                                        }
                                    });

                                    Ext.Ajax.request({
                                        url     :'../data/get_prin_pid_released_date.php',
                                        params  :{ops:obj['ops'], activelink_id:obj2['activelink_id']},
                                        success :function(response){
                                            var response = Ext.decode(response.responseText),
                                                data     = response.data;
                                                parsedDate = Ext.Date.parse(data, "Y-m-d");
                                                form.setValues({card_received:parsedDate})
                                        },failure:function(){
                                            Ext.Msg.alert('Failed','Something went wrong.')
                                        }
                                    });

                                    Ext.Ajax.request({
                                        url     :'../data/get_prin_pid_transmittal_date.php',
                                        params  :{ops:obj['ops'],activelink_id:obj2['activelink_id']},
                                        success :function(response){
                                            var response = Ext.decode(response.responseText),
                                                data     = response.data,
                                                date     = Ext.Date.parse(data,"Y-m-d")

                                                form.setValues({card_transmit:date});
                                        },failure:function(){
                                            Ext.Msg.alert('Failed','Something went wrong.')
                                        }
                                    });

                                    Ext.Ajax.request({
                                        url     :'../data/sla_getDetails.php',
                                        params  :{ops:obj['ops'], activelink_id:obj2['activelink_id']},
                                        success :function(response){
                                            var response          = Ext.decode(response.responseText),
                                                sla_option        = response.sla_option,
                                                batch_code        = response.batch_code;
                                                hr_endorsed_date  = response.hr_endorsed_date;
                                                ops_endorsed_date = response.ops_endorsed_date;

                                                // form.setValues({sla_option:sla_option,batch_code:batch_code});
                                                form.setValues({sla_option:sla_option,batch_code:batch_code,hr_endorsed_date:hr_endorsed_date,ops_endorsed_date:ops_endorsed_date});

                                        },failure:function(){
                                            Ext.Msg.alert('Failed','Something went wrong.')
                                        }
                                    });

                                    Ext.Ajax.request({
                                    url:'../data/get_pmember_adddetails.php',
                                    method:'GET',
                                    params :{p_al_id:r.get('activelink_id'), acc_id:r.data['company_id']},
                                    success: function ( result, request ) {
                                        var jsonData = Ext.util.JSON.decode(result.responseText);
                                        if(jsonData.success==true){
                                            email1.setValue(jsonData.data[0].email)
                                            address1.setValue(jsonData.data[0].address1)
                                            contact1.setValue(jsonData.data[0].contact1)
                                            contact2.setValue(jsonData.data[0].contact2)
                                        }else{
                                            Ext.Msg.alert('Warning',jsonData.message);
                                        }
                                    },
                                    failure: function ( result, request) {
                                        Ext.Msg.alert('Warning',result.responseText)
                                        return false;
                                    }
                                    });
                                }
                                task_tap.cancel();
                            },
                            failure:function(response){ var response    = Ext.decode(response.responseText);
                                Ext.Msg.alert('WARNING',response.message)}
                        });
                        task_tap.cancel()

                }
            }
        });
        principal_management.getNavigationBar().setTitle(fullname);
    },

    onPrinUpdate:function(btn){
        var form            = btn.up('formpanel'),
            values          = form.getValues(),
            fields          = form.getFields();
            principal_list  = this.lookupReference('principal_list'),
            me              = this,
            fields1         = form.query("field"),
            errorString     = '',
            model           = Ext.create('OMM.model.modern.PrincipalModel',form.getValues()),
            errors          = model.validate(),
            errUI           = form.down('#errorBox');

        Ext.Msg.confirm('Update Member','Are you sure you want to update members information',function(answer){
            if(answer=='yes'){
                
                var task = Ext.create('Ext.util.DelayedTask', function() {
                    Ext.Viewport.mask({ xtype: 'loadmask',
                    message: "Processing your request..." });
                }, this);
                task.delay(200);

                Ext.each(fields.items, function (f) {
                    var field = f.getName(),
                    value = f.getValue();
                    if(field=='philhealth' && value=='-' || field=='sss_no' && value=='-' || field=='tin' && value=='-' || field=='unified_id_no' && value=='-' || field=='pag_ibig_no' && value=='-'){
                        console.log(field);
                        console.log(value);
                    }else{
                    if(value=='-'){
                        form.findField(field).setValue('');
                    }
                    }
                });
                errUI.setHtml('');

                for (var i = 0; i < fields.length; i++) {
                  fields[i].removeCls('invalidField');
                }

                if(!errors.isValid()){
                  errors.each(function(errorObj){
                    // console.log(errorObj)
                    errorString+=' '+errorObj.getField()+" "+errorObj.getMessage()+"</br>";
                    var s = Ext.String.format('field[name={0}]',errorObj.getField());
                    form.down(s).addCls('invalidField');
                  });

                errUI.setHtml('<p class="invalidMsg"><font color="red">Please provide required fields');
                errUI.show();
                task.cancel();                
              }else{
                errUI.hide();            
                Ext.each(fields1, function (f) {
                    var field = f.getName();
                    if(field=='hmoid_no' || field=='hmoid_released_date' || field=='card_received' || field=='card_transmit' || field=='batch_code' || field=='sla_option' || field=='hr_endorsed_date' || field=='ops_endorsed_date' || field=='email1' || field=='address1' || field=='contact1' || field=='contact2' || field=='inclusion_date' ||field=='deactivation_date'){
                        console.log('Deleted fields : '+field);
                        f.destroy();
                    }                    
                });

                form.submit({
                    url             :'../data/principal_update.php',
                    waitMsg         :'Processing your request. Please wait...',
                    submitEmptyText :false,
                    timeout         : 120000, //2minutes
                    method          :'POST',
                    success         :function(form,action){
                        task.cancel();
                        principal_list.getStore().data
                        me.onBack();
                        Ext.Msg.alert('Success','Principal update saved');
                    },failure       :function(form,action){
                        task.cancel();     
                        principal_list.getStore().data
                        Ext.Msg.alert('Failed','Principal update failed');
                    }
                })
            
              }
            }
        });

    },

    onPrinHmoDel:function(btn){
        var form           =btn.up('formpanel'),
            values         =form.getValues(),
            principal_list =this.lookupReference('principal_list'),
            me             =this;

        Ext.Msg.confirm('HMO Deletion','This function may set member as deleted, HMO status as Inavtive and reset BMB account. Are you sure you want to delete Princpal Member',function(answer){
            if(answer=='yes'){
                if(values['fname']=='' || values['lname']=='' || values['empid']=='' || values['fname']==null || values['lname']==null || values['empid']==null){
                    Ext.Msg.alert('Warning','Cant delete principal member if either First, Last Name and Employee id is null or empty');
                }else{
                    var task = Ext.create('Ext.util.DelayedTask',function(){
                        Ext.Viewport.mask({
                            xtype:'loadmask',
                            message:'Processing you request...'
                        })
                    },this);
                    task.delay(200);
                    Ext.Ajax.request({
                        url     :'../data/prin_single_del.php',
                        submitEmptyText :false,
                        params :{acc_id:values['company_id'],session_userid:values['session_userid'],al_id:values['activelink_id'],empid:values['empid'],fname:values['fname'],lname:values['lname']},
                        method :'POST',
                        success :function(response){
                         var response = Ext.decode(response.responseText);
                         
                         if(response.success==true){
                            task.cancel();
                            principal_list.getStore().data
                            me.onBack();
                            Ext.Msg.alert('Success',response.message)
                         }else{
                            task.cancel();
                            principal_list.getStore().data
                            Ext.Msg.alert('Failed',response.message)
                         } 
                        },failure:function(){
                            task.cancel();
                            Ext.Msg.alert('Warning','Something went wrong. Please contact IT for assistance')   
                        }
                    }); 
                }
            }
        });
            
    },
    
    onPrinReactivate:function(btn){
        var form            = btn.up('formpanel'),
            values          = form.getValues(),
            principal_list  = this.lookupReference('principal_list'),
            me              = this;



        Ext.Msg.confirm('Set Member Active','Are you sure you want to set Member Active',function(answer){
           if(answer=='yes'){
            
            var task = Ext.create('Ext.util.DelayedTask',function(){
                Ext.Viewport.mask({
                    xtype :'loadmask',
                    message :'Processing your request...'
                });
            },this);
            task.delay(200);            
            Ext.Ajax.request({
                url             :'../data/prin_set_active.php',
                submitEmptyText :false,
                params          :{acc_id:values['company_id'], session_userid:values['session_userid'], activelink_id:values['activelink_id']},
                method          :'POST',
                success         :function(response){
                    var response = Ext.decode(response.responseText);
                    if(response.success==true){
                        console.log(task)
                        task.cancel();
                        principal_list.getStore().data
                        me.onBack();
                        Ext.Msg.alert('Success',response.message)
                    }else{
                        principal_list.getStore().data
                        me.onBack();
                        Ext.Msg.alert('Failed',response.message)
                    }
                },failure       :function(){
                    task.cancel();
                    principal_list.getStore().data
                    Ext.Msg.alert('Warning','500 Internal Server Error')
                }
            });
           } 
        });

    },

    OnPrinUserReset:function(btn){
        var form            = btn.up('formpanel'),
            values          = form.getValues(),
            principal_list  = this.lookupReference('principal_list'),
            me              = this;
        Ext.Msg.confirm('User Reset', 'Are you sure you want to reset principal member registration information',function(answer){
            if(answer=='yes'){
                var task = Ext.create('Ext.util.DelayedTask', function() {
                    Ext.Viewport.mask({ xtype: 'loadmask',
                    message: "Processing your request..." });
                }, this);
                task.delay(200);

                Ext.Ajax.request({
                    url     :'../data/get_principal_reset.php',
                    submitEmptyText :false,
                    params          :{empid:values['empid'],email:values['email'], p_al_id:values['activelink_id'], acc_id:values['company_id']},
                    method          :'POST',
                    success         :function(response){
                        var response    = Ext.decode(response.responseText);
                        if(response.success==true){
                            task.cancel();
                            principal_list.getStore().load()
                            principal_list.getStore().data
                            me.onBack();
                            // history.back();
                            Ext.Msg.alert('Success',response.message);
                        }else{
                            task.cancel();
                            principal_list.getStore().load()
                            principal_list.getStore().data
                            Ext.Msg.alert('Failed',response.message);
                        }
                    },failure       :function(){
                        task.cancel();
                        Ext.Msg.alert('Warning', '500 Internal Server Error')
                    }
                });
            }
        });

    },

    onPrinMgt_painted:function(){

        var me                    = this;
        var email           = this.down('#prin_email');
        var prin_del        = this.down('#prin_del');
        var prin_rectv      = this.down('#prin_rectv');                                    
        var prin_update     = this.down('#prin_update');      

        console.log(prin_del);
        if(prin_email.getValue()=='' || prin_email.getValue()=='-' || prin_email.getValue()=='-_DEL'){
            prin_bmb_status.setHtml('Unregistered');
            prin_reset.setDisabled(true);
            prin_update.setDisabled(false);
        }else{
            prin_bmb_status.setHtml('BMB Status : <b>Registered');
            prin_reset.setDisabled(false);
            prin_update.setDisabled(false);
            // console.log('BMB STATUS REGISTERED')
        }
        // activity_note.load();

            prin_update.setDisabled(false);
    },


    onSaveCardTransmit:function(btn){
        var form                = btn.up('formpanel'),
            values              = form.getValues(),
            OMM_thread_notes    = localStorage.getItem('OMM_thread_notes'),
            OMM_logs_ops        = localStorage.getItem('OMM_logs_ops'),
            obj2                = JSON.parse(OMM_thread_notes),
            obj                 = JSON.parse(OMM_logs_ops),
            cur_tab_id          ="prin"+obj['activelink_id'],
            me                  =this;

        var task = Ext.create('Ext.util.DelayedTask', function() {
            Ext.Viewport.mask({ xtype: 'loadmask',
            message: "Processing your request..." });
        }, this);
        task.delay(200);

        if(values['card_transmit']==''){
            task.cancel();
            Ext.Msg.alert('Warning','Please fill up required fields');
        }else{
            Ext.Ajax.request({                
                url         :'../data/sla_pid_transmittal_update.php',
                params  :{ops:obj['ops'],acc_id:obj2['acc_id'],activelink_id:obj2['activelink_id'],phys_id_transmittal_date:values['card_transmit']},
                method  :'POST',
                scope   :me,
                success :function(response){
                    var response    = Ext.decode(response.responseText);
                    if(response.success==true){
                        task.cancel();
                        Ext.Msg.alert('Success',response.message);
                        // this.onSlaUpdate_beforerender()
                    }else{
                        task.cancel();
                        Ext.Msg.alert('Failed',response.message);
                    }
                },
                failure :function(){
                    task.cancel();
                    Ext.Msg.alert('Failed','500 Internal Server Error');
                }
            });

        }
    },

    onSaveCardReceived:function(btn){
        var form                = btn.up('formpanel'),
            values              = form.getValues(),
            OMM_thread_notes    = localStorage.getItem('OMM_thread_notes'),
            OMM_logs_ops        = localStorage.getItem('OMM_logs_ops'),
            obj2                = JSON.parse(OMM_thread_notes),
            obj                 = JSON.parse(OMM_logs_ops),
            cur_tab_id          ="prin"+obj['activelink_id'],
            me                  =this;

        var task = Ext.create('Ext.util.DelayedTask', function() {
            Ext.Viewport.mask({ xtype: 'loadmask',
            message: "Processing your request..." });
        }, this);
        task.delay(200);

        if(values['card_received']=='' || values['card_received']==null){
            task.cancel()
            Ext.Msg.alert('Warning','Please fill up required fields');
        }else{
            Ext.Ajax.request({
                url         :'../data/sla_pid_released_update.php',
                // url     :'https://www.benefitsmadebetter.com/devmod5/Activebmb/data/getDependentList.php',
                params      :{ops:obj['ops'],acc_id:obj2['acc_id'],activelink_id:obj2['activelink_id'],phys_id_released_date:values['card_received']},
                method      :'POST',
                scope       :me,
                success     :function(response){
                    var response = Ext.decode(response.responseText);
                    if(response.success==true){
                        task.cancel();
                        Ext.Msg.alert('Success',response.message);
                        // this.onSlaUpdate_beforerender()
                    }else{
                        task.cancel();
                        Ext.Msg.alert('Failed',response.message);
                    }
                },
                failure     :function(){
                    task.cancel();
                    Ext.Msg.alert('Warning','500 Internal Server Error')
                }
            });
        }
    },

    onHmoIdUpdate:function(btn){

        var form                = btn.up('formpanel'),
            values              = form.getValues(),
            OMM_thread_notes    = localStorage.getItem('OMM_thread_notes'),
            OMM_logs_ops        = localStorage.getItem('OMM_logs_ops'),
            obj                 = JSON.parse(OMM_thread_notes),
            obj2                = JSON.parse(OMM_logs_ops),
            cur_tab_id          ="prin"+obj['activelink_id'],
            me                  =this;

        var task = Ext.create('Ext.util.DelayedTask', function() {
            Ext.Viewport.mask({ xtype: 'loadmask',
            message: "Processing your request..." });
        }, this);
        task.delay(200);

        if(values['hmoid_no']==null || values['hmoid_no']==''||values['hmoid_released_date']=='' ||values['hmoid_released_date']==null){
            Ext.Msg.alert('Warning','Please fill up required fields.')
            task.cancel();
        }else{
            Ext.Ajax.request({
                url     :'../data/prin_hmoid_update.php',
                params  :{ops:obj2['ops'],activelink_id:obj['activelink_id'],hmoid_no:values['hmoid_no'],hmoid_released_date:values['hmoid_released_date']},
                method  :'POST',
                scope   :me,
                success :function(response){
                    var response = Ext.decode(response.responseText);
                    if(response.success==true){
                        task.cancel();
                        Ext.Msg.alert('Success',response.message)
                        // this.onSlaUpdate_beforerender()
                        // view.remove(cur_tab_id);
                    }else{
                        task.cancel();
                        Ext.Msg.alert('Failed',response.message);
                    }
                },
                failure :function(){
                    task.cancel();
                    Ext.Msg.alert('Failed','500 Internal Sever Error');
                }
            });
        }
    
    },

    onPrinBranchID:function(){
        var prin_acc_id    = this.lookupReference('prin_acc_id');
        var prin_branch    = this.lookupReference('prin_branch');
        prin_branch.getStore().load({params:{acc_id:prin_acc_id.getValue()}});
    },

    onPrinBranchChange:function(){
        var prin_acc_id     = this.lookupReference('prin_acc_id');
        var prin_branch     = this.lookupReference('prin_branch');
        var prin_branch_id  = this.lookupReference('prin_branch_id');

        Ext.Ajax.request({
            url     :'../data/get_newsite_id.php',
            params  :{acc_id:prin_acc_id.getValue(), branch_name:prin_branch.getValue()},
            success :function(response){
                var response    = Ext.decode(response.responseText);
                prin_branch_id.setValue(response.branch_id);
                console.log(prin_branch_id.getValue())
            },failure:function(){
                alert('Couldnt load ID of branch name')
                // Ext.MessageBox.show({title:'WARNING',msg:'Couldnt load ID of branch name',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
            }
        });
    },

    onPrinCompanyID:function(){
        var prin_acc_id     = this.lookupReference('prin_acc_id');
        var prin_planLevel  = this.lookupReference('prin_planLevel');
        var prin_branch_id  = this.lookupReference('prin_branch_id');

        if(prin_branch_id.getValue()=='' || prin_branch_id.getValue()== null ){
            alert('Please select Branch ID first')
            // Ext.MessageBox.show({title:'WARNING',msg:'Please select Branch ID first', icon:Ext.Msg.WARNING, buttonText:{ok:'OK'}});
        }else{prin_planLevel.getStore().load({params:{acc_id:prin_acc_id.getValue(),branch_id:prin_branch_id.getValue()}});}       

    },

    onPrinPlanChange:function(){

        var prin_acc_id     = this.lookupReference('prin_acc_id');
        var prin_planLevel  = this.lookupReference('prin_planLevel');
        var prin_branch_id  = this.lookupReference('prin_branch_id');
        var prin_plan_desc  = this.lookupReference('prin_plan_desc');

        console.log('PLAN '+prin_planLevel.getValue());

        Ext.Ajax.request({
            url     :'../data/get_principal_plan.php',
            params  :{acc_id:prin_acc_id.getValue(),plan_id:prin_planLevel.getValue(),branch_id:prin_branch_id.getValue()},
            success :function(response){
                var response = Ext.decode(response.responseText);
                prin_plan_desc.setValue(response.description);
            },failure:function(){
                alert('Something went wrong. Please contact IT for assistance')
                // Ext.MessageBox.show({title:'WARNING',msg:'Something went wrong. Please contact IT for assistance',icon:Ext.Msg.WARNING,closable:false,buttonText:{ok:'OK'}});
            }
        });

    }

});
