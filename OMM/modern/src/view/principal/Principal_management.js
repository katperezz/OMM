var itemsPerPage=25;
Ext.define('acc_list', {
    extend : 'Ext.data.Model',
    config : {
        fields : [
            {name: 'companyid', mapping: 'companyid'},
            {name: 'companyname', mapping: 'companyname'}
        ]
    }
});
var Get_acc_list = Ext.create('Ext.data.Store', {
    model    : 'acc_list',
    autoLoad : true,
    proxy    : {
        type    : 'ajax',
        url     :'../data/get_acc_list.php',
        reader : {
            type         : 'json',
            rootProperty : 'data'
        }
    }
});


Ext.define('principalModel', {
    extend : 'Ext.data.Model',
    config : {
            fields:[{name:'activelink_id',mapping:'activelink_id'},{name:'empid',mapping:'empid'},{name:'fname',mapping:'fname'},{name:'mname',mapping:'mname'},{name:'lname',mapping:'lname'},{name:'ename',mapping:'ename'},{name:'fullname',mapping:'fullname'},{name:'birthdate',mapping:'birthdate'},{name:'gender',mapping:'gender'},{name:'civil_status',mapping:'civil_status'},{name:'philhealth',mapping:'philhealth'},{name:'sss_no',mapping:'sss_no'},{name:'pag_ibig_no',mapping:'pag_ibig_no'},{name:'unified_id_no',mapping:'unified_id_no'},{name:'plan_id',mapping:'plan_id'},{name:'branch_id',mapping:'branch_id'},{name:'email',mapping:'email'},{name:'mbr_status',mapping:'mbr_status'},{name:'id_number',mapping:'id_number'},{name:'company_id',mapping:'company_id'},{name:'member_date',mapping:'member_date'},{name:'designation',mapping:'designation'},{name:'tin',mapping:'tin'},{name:'reg_date',mapping:'reg_date'},{name:'inactive_date',mapping:'inactive_date'},{name:'existing_flag',mapping:'existing_flag'},{name:'inclusion_date',mapping:'inclusion_date'},{name:'emp_hire_date',mapping:'emp_hire_date'},{name:'date_of_joining',mapping:'date_of_joining'},{name:'hr_inclusion_endorsement_date',mapping:'hr_inclusion_endorsement_date'},{name:'hmo_inclusion_date',mapping:'hmo_inclusion_date'},{name:'hr_deletion_endorsement_date',mapping:'hr_deletion_endorsement_date'},{name:'hmo_deletion_date',mapping:'hmo_deletion_date'},{name:'plan_desc',mapping:'plan_desc'}]
    }
});

var principalStore = Ext.create('Ext.data.Store', {
    model    : 'principalModel',
    autoLoad : true,
    pageSize :itemsPerPage,
    proxy    :{
        type            :'ajax',
        timeout         :2000000,
        url             :'../data/get_principal_list.php',
        actionMethods   :'POST',
        extraParams     :{search_value:''},
        reader:{
            type:'json',
            rootProperty:'data',
            totalProperty:'total',
            successProperty:'success'
        },
        writer:{type:'json',rootProperty:'data',encode:true}
    }
});

Ext.define('OMM.view.principal.Principal_management',{
    extend      : 'Ext.NavigationView',
    xtype       :'principal_management',
    reference   :'principal_management',
    id          :'principal_management',
    // title:'Add ActiveLink',
    // iconCls:'x-fa fa-gear',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio',

        'Ext.plugin.PullRefresh',
        'Ext.grid.plugin.ViewOptions',
        'OMM.view.principal.Principal_Controller',
        'OMM.view.principal.Principal_details'
    ],
    controller:'principal',

    fullscreen  :true,
    autoDestroy :true,
    // config:{},

    defaultBackButtonText: '',
    navigationBar: {
        backButton:{ 
            docked: 'left',
            iconCls:'fa fa-chevron-left', 
            iconMask: true, 
            ui      :'plain',
            margin  :'15 0 0 0',
            style   :'color:#FFFFFF;'
            // useTitleForBackButtonText: false,
        },

        ui:'ALToolbar',
        docked: 'top',
        hidden: true,
        items: [
        ]
    },

    autodestroy : true,
    fullscreen  : true,
    items       :[
    {
            xtype    : 'toolbar',
            docked   : 'top',
            alias    : 'toptoolbar',
            reference:'principal_management_top_toolbar',
            border   :false,
            layout   : {
                type: 'vbox',
                pack: 'center'
            },
            defaults :{width        :'100%'},
            items:[
            {
                xtype        : 'selectfield',
                // margin       :5,
                reference    :'acc_id',
                placeHolder  :'Select Account',
                autoSelect   :false,
                valueField   : 'companyid',
                displayField : 'companyname',
                store        : Get_acc_list,
                listeners    :{
                    change:'onPrinSelectAccount'
                }           
            },
            {
                xtype :'searchfield',
                reference :'prin_search_value',
                placeHolder :'Search Employee'
                // listeners :{
                //     change :'onPrinSearchEmp'

                // }
            },
            {
                xtype   :'button',
                text    :'Search',
                ui      :'confirm',
                handler :'onPrinSearchEmp'
            },
            {xtype:'label',docked:'bottom',margin:'5 0 5 0',style:'font-weight:bold;font-size:12px;font-face:arial',html:'NOTE : <span style="color:#57b3cb;font-weight:normal;font-face:arial;font-size:12px;font-style:oblique;">Click row to view Principal information. Limited view.</span>'}
            ]
    },
    // {
    //     xtype       :'grid',
    //     // docked  :'center',
    //     reference   :'principal_list',
    //     store       :principalStore,
    //     id          :'principalGrid',
    //     layout      : 'fit',
    //     fullscreen  : true,
    //     // columns     : [
    //     //     {text:'Emp no',dataIndex:'empid', width:'25%'},
    //     //     {text:'Full Name', dataIndex:'fullname', width:'50%'},
    //     //     {text:'Status',dataIndex:'mbr_status', width:'20%'}
    //     // ],
    //     listeners:{
    //         painted:function(){
    //             console.log(Ext.getStore('principalGrid'))
    //         }
    //     }
    // }
    {
        xtype       :'list',
        scroll      :'vertical',
        scrollable  :true,
        layout      :'fit',
        title       : 'Principal',
        reference   :'principal_list',
        store       :principalStore,
        viewConfig   :{
            preserveScrollOnRefresh :true,
            preserveScrollOnReload  :true,
            deferEmptyText          :true,
            emptyText               :'<h1>No data found</h1>'
        },  
        plugins     : [{
            xclass: 'Ext.plugin.ListPaging',
            autoPaging: true,
            painted: function() {
                console.log('Load more button clicked');
            // fire event here
            }
        }],
        // itemTpl     :['{fullname}','<span>HMO ID : {hmo_id}</span>','<span>Bdate : {bdate}</span>','<span>Plan ID : {planid}</span>','<span>Status : <font color=green>{dmbr_status}</span>'].join(''),

        itemTpl     :[
            '<div class="top_bar"><p>{fullname}</p></div>',
            '<span class="posted">Employee number : {empid}</span>',
            '<span>Status : <font color=green>{mbr_status}</span>'
            // '<h2>Fullname : {fullname}</h2>'/<div class="top-wrapper">
            // '<span class="posted">Status : {mbr_status}</span>',
        ].join(''),
        listeners:{
            itemtap :'onPrinMgt_tap'
        }
    }
    // {
    //     xtype:'list',
    //     ui         : 'x-dental_connect',
    //     cls         : 'x-dental_connect',
    //     store       :'Get_dental_tkt',
    //     reference  :'dental_connect_msg_store',
    //     itemTpl     : [
    //         // '{subj}',
    //         //<div class="top_bar">
    //         // '<div class="top_bar"><div class="top-wrapper"><p>{hf_ticket_id}</p></div></div>',
            
    //         '<div class="top_bar"><div class="top-wrapper"><p>OPEN</p></div></div>',
    //         //<hr size="1" color="#868593" width="100%">
    //         '<span class="posted">{created}</span>',
    //         // '<div class="blue-rectangle"><p>{hf_ticket_id}</div></div>',
    //         '<h2>{ticket_msg}</h2>'
    //     ].join(''),

    //     // onItemDisclosure: 'onCC_connect_msg_tap',
    //     listeners:{
    //         itemtap :'onDental_connect_tkt_tap'
    //     }
    // },
    ],



    listeners : {

        painted:'onLoadPrinMgt',
        back : {
            fn :'onBack'
        },
        push : {
            fn :'onPush'
        }
    }

});

