/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('OMM.view.main.Main', {
    extend: 'Ext.NavigationView',
    alias : "widget.app-main",
    xtype : 'app-main',
    id    : 'app-main',

    requires: [
        'Ext.Menu',
        'Ext.MessageBox',

        'OMM.view.main.MainViewController',
        'OMM.view.main.MainModel',
        'OMM.view.main.List',
        'OMM.view.principal.Principal_management',
        'OMM.view.dependent.Dependent_management'
    ],

    controller: 'mainview',
    viewModel: 'main',

navigationBar: {
     hidden: true
},
    config:{

        autodestroy  : true,
        fullscreen   : true,
        defaultBackButtonText: '',
        navigationBar: {
            
            backButton:{ 
                docked: 'left',
                iconCls:'fa fa-chevron-left', 
                iconMask: true, 
                ui      :'plain',
                margin  :'20 0 0 5',
                style   :'color:#FFFFFF;'
                // useTitleForBackButtonText: false,
            },

            // backButton: {
            //     docked: 'left',
            //     iconCls: 'fa-arrow-user',
            //     ui: 'plain'
            // },
            ui      : 'ALToolbar',
            height  :60,
            items   :[
                // {
                //     xtype   :'button',
                //     // text    :'logout',
                //     margin  :'0 10',
                //     iconCls :'fa fa-power-off',
                //     iconMask:true,
                //     align   :'right',
                //     style   :'color:#FFFFFF;',
                //     ui      :'plain',
                //     listeners :{
                //         tap:function(){
                //             alert('tap')
                //         }
                //     }

                // },
                // {
                //     iconCls: 'x-fa fa-arrow-right',
                //     iconMask:true,
                //     align:'right',
                //     // style: 'color:#FFFFFF;',
                //     iconMask: true,
                //     ui: 'plain',
                //     listeners:{
                //         tap:'onSubmitAppointment'
                //     }
                //     // handler:'onSubmitAppointment'
                // }
                {
                    xtype   :'button',
                    id      :'listButton',
                    iconCls :'fa fa-bars',
                    margin  :10,
                    style   :'color:#0a091b;',
                    // iconMask:true,
                    ui      :'plain',
                    handler :function(){
                        if(Ext.Viewport.getMenus().left.isHidden()){
                            Ext.Viewport.showMenu('left')
                        }else{
                            Ext.Viewport.hideMenu('left')
                        }
                    }
                }               
            ]
        },
        items:[
        {
            flex    :1,
            // title   :'ACTIVELINK',
            ui      : 'ALToolbar',
            items   :[
            {
                xtype   :'panel',
                bodyPadding :10,
                items   :[                
                    {
                        xtype       :'component',
                        html        :'<div class="main-logo"><img src="resources/images/AL_header.png"></div>'
                    },
                    {
                        xtype       : 'panel',
                        layout      : 'fit',
                        // title        :'TEST',
                        // border       :true,
                        // height       :500,
                        // width        :'100%',
                        ui          :'light',
                        margin      :'11 0 0 0',
                        id          :'dash_panel',
                        items       :[
                        {
                            xtype       :'textfield',
                            readOnly    :true,
                            reference   :'omm_user',
                            id          :'omm_user',
                            margin      :'-15 5',
                            height      :90,
                            fieldStyle  :'font-size:18px;font-weight:bold;font-face:arial;color:#404040'
                        },
                        {xtype:'label',style:'color:#2f9fe9;font-weight:bold;font-face:arial;font-size: smaller;', margin:5,
                        html:'Welcome to OMM Mobile view! <br>• Use this module for principal/dependent update, user reset and HMO deletion. <br>• All action was recorded to database and it is available on OMM logs tab.',iconCls:'x-fa fa-info-circle'}
                        ]
                    }
                ]
            },
            // {
            //     xtype: 'button',                       
            //     cls: 'd1-btn dashboard',
            //     left: '10%',
            //     top: 250,
            //     iconAlign: 'top',
            //     textAlign   :'center',
            //     iconCls :'fa fa-user',
            //     text: 'Prinicpal',
            //     handler :'showPrincipal'
            // }, 
            // {
            //     xtype: 'button',
            //     cls: 'd2-btn dashboard',
            //     left: '55%',
            //     top: 250,
            //     iconAlign: 'top',
            //     textAlign   :'center',
            //     iconCls: 'fa fa-users',
            //     text: 'Dependent',
            //     handler :'showDependent'
            // },
            {
                xtype :'panel',
                docked :'bottom',
                // height :50,
                // style       :{"background-color":"#4dacec"},
                items:[
                {
                    xtype :'textfield',
                    // value :'TEST',
                    readOnly:true,
                    // label:'Today',
                    reference   :'omm_time',
                    id          :'omm_time',
                    fieldStyle  :'margin-top:-20px;font-size:18px;font-weight:bold;font-face:arial;color:#404040'
                    // style       :{"background-color":"#4dacec","color":"#404040"},
                }
                ],
                listeners:{
                    painted :function(){
                        setInterval(function(){
                        Ext.getCmp('omm_time').setHtml('<p style="margin:5px 10px;"><b>Today : </b>'+Ext.Date.format(new Date(),'F j, Y'));// console.log( Ext.getCmp('omm_time').getValue())
                        },1000);//1sec
                        console.log(Ext.getCmp('omm_time').getValue())

                        Ext.Ajax.request({
                            url     :'../data/post_username.php',
                            success :function(response){
                                var response = Ext.decode(response.responseText);
                                if(response.username=='' || response.username==null){
                                    Ext.getCmp('omm_user').setHtml('<span style="font-weight:normal;font-size:12px; padding:5px">User is not LogIn  <a href="https://www.benefitsmadebetter.com/OMM"><b>Log In here</a></span>');
                                }else{
                                    Ext.getCmp('omm_user').setHtml('<b>'+response.username+'</b>');
                                }
                            }
                        });
                    }
                }
            }
            ],
            //issuu javascript sdk load
            listeners: {
                // initialize : function (container) {
                //     (function(d, s, id) {
                //         var js, fjs = d.getElementsByTagName(s)[0];
                //         if (d.getElementById(id)) return;
                //         js = d.createElement(s); js.id = id;
                //         js.src = "//e.issuu.com/embed.js";
                //         fjs.parentNode.insertBefore(js, fjs);
                //     }(document, 'script', 'facebook-jssdk'));
                //     var html='<div data-configid="15006580/35411722" style="width:400px; height:300px;" class="issuuembed"></div>';
                //     // var html = '<div class="fb-like" data-href="https://www.facebook.com/senchainc" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>';
                //     container.query('panel container')[0].setHtml(html);
                // }
            }
        }]
    },
    
    listeners : {
        back : {
            fn :'onBack'
        },
        push : {
            fn :'onPush'
        }
    },
    initialize: function(){
        this.callParent();

        var menu = Ext.create('Ext.Menu', {
            width       :245,
            autoScroll  :true,
            controller  :'mainview',
            layout      :'vbox',
            cls         :'mainmenu',
            docked      :'left',
            scrollable  :'vertical',
            defaults    :{ui:'mainmenu',leaf:true},        
            items       : [
            {
                docked      : 'top',
                xtype       :'component',
                id          :'ops_logo',
                cls         :'sencha-logo',
                margin      :'15 10',
                html        :'<div class="main-logo"><img src="resources/images/logo.png"></div>'
                // width       :200
            },
            // {
            //     xtype       : 'titlebar',
            //     docked      : 'top',
            //     ui          : 'ViewportToolbar',
            //     cls         :'al-logo',
            //     titleAlign  :'left',
            //     title       :'<div class="image-wrapper"><i class="fa fa-user fa-2x c_icon"/></i></div><font size="2"><p style="margin-top:20px;color:#FFFFFF">John Doe Smith',
            //     // JSON.parse(localStorage.getItem("userdetails")).data[0].mem_name
            //     id          :'dash_menu',
            //     margin      :'0 0 5 0'
            // },
            // title:'<div class="image-wrapper"><div class="circle-icon"><i class="fa fa-bicycle fa-5x"/></div></div><font size="2">John Doe Smith',            
            {
                text    :'Principal',                
                iconCls :'x-fa fa-user',
                handler :'showPrincipal'
            },
            {
                text    :'Dependent',
                iconCls :'x-fa fa-users',
                handler :'showDependent'
            },
            {
                xtype   :'component',
                cls     :'divider',
                html    :'ActiveLink'
            },
            {
                text    :'About Us',
                iconCls :'x-fa fa-info-circle',
                handler :'showAboutUs'
            },
            {   text    :'Logout',
                iconCls :'x-fa fa-sign-out',
                docked  :'bottom',
                handler :'onSignOffCommand'
            }
            ],
            listeners:{
                painted:function(){
                        // Ext.Ajax.request({
                        // url     :'../data/post_username.php',
                        // success :function(response){
                        //     var response=Ext.decode(response.responseText);
                        //     console.log(response.username)
                        //     Ext.getCmp('dash_menu').setTitle('<div class="image-wrapper"><i class="fa fa-user fa-2x c_icon"/></i></div><font size="2"><p style="margin-top:20px;color:#FFFFFF">'+response.username);
                        // }
                        // });
                }
            }
            });
    
        Ext.Viewport.setMenu(menu,{
          side: 'left'
        });
    },

    userload:function(){
    },
    isActive: function() {
    return Ext.os.is.Phone; // || Ext.os.is.Desktop;
    }

});
