/**
 * This view is an example list of people.
 */
Ext.define('OMM.view.main.List', {
    extend: 'Ext.List',
    xtype: 'mainlist',

    requires: [
        'OMM.store.modern.Get_dental_tkt'
    ],

    title: 'Personnel',

    store       :'modern.Get_dental_tkt',
    
    // columns: [
    //     { text: 'created',  dataIndex: 'created', width: 100 },
    //     { text: 'ticket_msg', dataIndex: 'ticket_msg', width: 230 }
    // ],    
    itemTpl     : [
        // '{subj}',
        //<div class="top_bar">
        // '<div class="top_bar"><div class="top-wrapper"><p>{hf_ticket_id}</p></div></div>',
        
        '<div class="top_bar"><div class="top-wrapper"><p>OPEN</p></div></div>',
        //<hr size="1" color="#868593" width="100%">
        '<span class="posted">{created}</span>',
        // '<div class="blue-rectangle"><p>{hf_ticket_id}</div></div>',
        '<h2>{ticket_msg}</h2>'
    ].join(''),

    listeners: {
        select: 'onItemSelected'
    }
});
