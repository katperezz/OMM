/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('OMM.view.main.MainViewController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.mainview',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },


    getSlideRightTransition: function () {
      return { type: 'slide', direction: 'right' };
    },
    getSlideLeftTransition: function () {
      return { type: 'slide', direction: 'left' };
    },

    init: function() {
        /* pop a view when the back button is pressed
           note: if it's aleady at the root it's a noop */
        var that = this;
        var MainNavigationView = Ext.getCmp('app-main');
        var main = Ext.getCmp('app-main');

        window.onpopstate = function() {
            MainNavigationView.pop();
            Ext.getCmp('listButton').show();

        };
        this.backStackDepth = 0;
    },

    onPush:function(){
        history.pushState(null, "");
    },

    onBack: function() {
        var main = Ext.getCmp('app-main');
    // var MainNavigationView = Ext.getCmp('MainNavigationView');
        history.back();
        Ext.getCmp('listButton').show();
        // main.getNavigationBar().hide();
        // Ext.getCmp('dash_toolbar').show();
        // MainNavigationView.getNavigationBar().hide();
        //prevent back button popping main view directly..
        return false;
    },
    
    onTest:function(){
        var user_details = localStorage.getItem("userdetails"),
            obj2         = JSON.parse(user_details),
            mainlist     = this.lookupReference('mainlist');

        mainlist.getStore().load({params:{activelink_id:'A000A80217'}})
        console.log(mainlist.getStore().data);
        // console.log(mainlist.getStore().data)
        alert('test');
    },

    onMainPainted:function(){
        console.log(Ext.getCmp('dash_menu'))
    },

    showDependent:function(){
        var viewport        = Ext.getCmp('app-main');

        Ext.getCmp('listButton').hide();
        Ext.Viewport.hideMenu('left');
        viewport.getNavigationBar().show();
        viewport.push({xtype:'dependent_management'});
        viewport.getNavigationBar().setTitle('Dependent Mgt')
    },

    showPrincipal:function(){

      var viewport         = Ext.getCmp('app-main');

      Ext.getCmp('listButton').hide();
      Ext.Viewport.hideMenu('left');
      viewport.getNavigationBar().show();
      viewport.push({xtype:'principal_management'});
      viewport.getNavigationBar().setTitle('Principal Mgt');

    // var MainNavigationView  = Ext.getCmp('MainNavigationView');

    // Ext.Viewport.hideMenu('left');
    // // MainNavigationView.removeAll();
    // MainNavigationView.push({xtype:'cc_connect'});
    // Ext.getCmp('dash_toolbar').setTitle(Ext.getCmp('cc_connect').title);
    // console.log(Ext.getCmp('cc_connect').title)
    // MainNavigationView.getNavigationBar().hide();
   },

   onSignOffCommand:function(){

          Ext.Ajax.request({
                url : '../data/logout.php',
                method : 'POST',
                waitMsg : 'Logging Out',
                success : function (response) {
                window.location.href = 'https://www.benefitsmadebetter.com/OMM';
                window.location.reload();
                }

          });

   }
});
